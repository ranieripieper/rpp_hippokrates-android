package com.doisdoissete.hippokrates.view.adapter.search;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.R;

public class HashTagAdapter extends PagingBaseAdapter<String> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public HashTagAdapter(Context context, List<String> lst) {
		this.mContext = context;
		if (lst == null) {
			this.items = new ArrayList<String>();
		}
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public String getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		HashTagViewHolder holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.simple_list_item, parent, false);
			holder = new HashTagViewHolder();			
			holder.txt = (TextView)vi.findViewById(android.R.id.text1);
			
			vi.setTag(holder);
		} else {
			holder = (HashTagViewHolder) vi.getTag();
		}

		final String obj = getItem(position);
		holder.txt.setText(obj);

		return vi;
	}

	
	public List<String> getAllItems() {
		return this.items;
	}
}
