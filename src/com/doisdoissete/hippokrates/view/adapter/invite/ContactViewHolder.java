package com.doisdoissete.hippokrates.view.adapter.invite;

import android.widget.CheckBox;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;

public class ContactViewHolder {

	public TextViewPlus txtNome;
	public TextViewPlus txtEmail;
	public CircleImageView img;
	public CheckBox radio;
}
