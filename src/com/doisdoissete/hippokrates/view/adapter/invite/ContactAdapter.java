package com.doisdoissete.hippokrates.view.adapter.invite;

import java.util.ArrayList;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.ContactUser;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class ContactAdapter extends BaseAdapter {
	
	private List<ContactUser> itens = new ArrayList<ContactUser>();
	private LayoutInflater inflater;
	private HippBaseActivity ctx = null;
	private TextViewPlus txtContador;
	private int nrInvites;
	
	public ContactAdapter(HippBaseActivity context, List<ContactUser> value, TextViewPlus txtContador, int nrInvites) {
		inflater = LayoutInflater.from(context);
		this.itens = value;
		this.ctx = context;
		this.nrInvites = nrInvites;
		this.txtContador = txtContador;
	}
	
	@Override
	public int getCount() {
		return itens.size();
	}
	@Override
	public ContactUser getItem(int position) {
		return itens.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ContactViewHolder holder;
		
		if(convertView == null){
			holder = new ContactViewHolder();
			convertView = inflater.inflate(R.layout.select_contacts_row, null);
			holder.txtNome = (TextViewPlus)convertView.findViewById(R.id.txt_nome);
			holder.txtEmail = (TextViewPlus)convertView.findViewById(R.id.txt_email);
			holder.img = (CircleImageView)convertView.findViewById(R.id.img_profile);
			holder.radio = (CheckBox)convertView.findViewById(R.id.radio);
			convertView.setTag(holder);
		} else{
			holder = (ContactViewHolder) convertView.getTag();
		}
		final ContactUser obj = (ContactUser)getItem(position);
		
		holder.radio.setOnCheckedChangeListener(null);
		holder.txtNome.setText(obj.getName());
		holder.txtEmail.setText(obj.getEmail());
		holder.radio.setChecked(obj.isSelected());

		if (obj.getImage() != null) {
			holder.img.setImageBitmap(obj.getImage());
		} else {
			holder.img.setImageResource(R.drawable.img_placeholder_friend);
		}

		holder.radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (nrInvites <= 0 && isChecked) {
					showMessageNrInvitesEnd();
					buttonView.setChecked(false);
				} else {
					if (isChecked) {
						nrInvites--;
					} else {
						nrInvites++;
					}
					obj.setSelected(isChecked);
				}
				
				txtContador.setText(String.valueOf(nrInvites));
			}
		});
		return convertView;
	}
	
	private void showMessageNrInvitesEnd() {
		ctx.showMessage(R.string.msg_nao_possui_convites);
	}
	
	public void add(ContactUser contactUser) {
		itens.add(0, contactUser);
		if (nrInvites > 0) {
			contactUser.setSelected(true);
			nrInvites--;
		} else {
			contactUser.setSelected(false);
		}
		txtContador.setText(String.valueOf(nrInvites));
	}
	
	public void clear() {
		if (itens != null) {
			itens.removeAll(itens);
		}
	}
	
	public void addAll(List<ContactUser> lst) {
		if (itens == null) {
			itens = new ArrayList<ContactUser>();
		}
		itens.addAll(lst);
	}

}
