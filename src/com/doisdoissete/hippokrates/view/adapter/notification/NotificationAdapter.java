package com.doisdoissete.hippokrates.view.adapter.notification;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Notification;

public class NotificationAdapter extends PagingBaseAdapter<Notification> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public NotificationAdapter(Context context, List<Notification> lst) {
		this.mContext = context;
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (lst == null) {
			this.items = new ArrayList<Notification>();
		} else {
			this.items = lst;
		}
	}

	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Notification getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		
		NotificationViewHolder holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.simple_list_item_with_layout, parent, false);
			holder = new NotificationViewHolder();
			holder.txt = (TextViewPlus)vi.findViewById(android.R.id.text1);
			vi.setTag(holder);
		} else {
			holder = (NotificationViewHolder) vi.getTag();
		}
		holder.txt.setAllCaps(false);
		Notification notification = getItem(position);
		Spanned sp = Html.fromHtml(notification.getFormattedBody() );
		holder.txt.setText(sp);
		
		return vi;
	}

}
