package com.doisdoissete.hippokrates.view.adapter.post;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;

public class CommentViewHolder {

	public TextViewPlus txtComment;
	public TextViewPlus txtUser;
	public CircleImageView imgUser;
	
}
