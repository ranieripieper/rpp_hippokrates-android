package com.doisdoissete.hippokrates.view.adapter.post;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Comment;
import com.doisdoissete.hippokrates.view.activity.search.HashtagFeedActivity;

public class CommentAdapter extends PagingBaseAdapter<Comment> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public CommentAdapter(Context context, List<Comment> lst) {
		this.mContext = context;
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (lst == null) {
			this.items = new ArrayList<Comment>();
		} else {
			this.items = lst;
		}
	}

	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Comment getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView; // trying to reuse a recycled view
		
		CommentViewHolder holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.post_comment_row, parent, false);
			holder = new CommentViewHolder();
			holder.txtComment = (TextViewPlus)vi.findViewById(R.id.txt_comment);
			holder.txtUser = (TextViewPlus)vi.findViewById(R.id.txt_user);
			holder.imgUser = (CircleImageView)vi.findViewById(R.id.img_user);
			vi.setTag(holder);
		} else {
			holder = (CommentViewHolder) vi.getTag();
		}
		vi.setEnabled(false);
		vi.setClickable(false);
		Comment obj = getItem(position);
		
		TextViewPlus.TagClick tagClick = new TextViewPlus.TagClick() { 
			@Override
			public void clickedTag(String tag) {
				HashtagFeedActivity.showActivity(mContext, tag);
			}
		};
		holder.txtComment.setText(obj.getContent(), true, tagClick);
		
		if (obj.getUser() != null) {
			if (!TextUtils.isEmpty(obj.getUser().getFullName())) {
				holder.txtUser.setText(obj.getUser().getFullName());
			} else if (!TextUtils.isEmpty(obj.getUser().getName())) {
				holder.txtUser.setText(obj.getUser().getName());
			} else {
				holder.txtUser.setText("");
			}
			
			HippokratesApplication.imageLoaderUser.displayImage(obj.getUser().getProfileImageUrl(), holder.imgUser);
		}
		
		
		return vi;
	}
	
	public void removeItem(Comment comment) {
		this.items.remove(comment);
		notifyDataSetChanged();
	}

}
