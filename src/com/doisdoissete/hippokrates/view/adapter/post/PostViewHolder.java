package com.doisdoissete.hippokrates.view.adapter.post;

import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.SquareImageView;

public class PostViewHolder {

	public TextViewPlus txtDescricao;
	public TextViewPlus txtLikes;
	public TextViewPlus txtComments;
	public TextViewPlus txtData;
	public SquareImageView imgFoto;
	public ImageView imgCirculo;
	public TextViewPlus txtCirculo;
	public TextViewPlus txtNomeMedico;
	public ImageView btMore;
	public ImageView imgLikes;
	public ImageView imgMark;
	public ImageView imgComments;
}
