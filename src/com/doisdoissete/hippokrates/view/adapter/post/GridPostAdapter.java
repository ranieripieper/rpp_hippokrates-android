package com.doisdoissete.hippokrates.view.adapter.post;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.SquareImageView;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Post;

public class GridPostAdapter extends PagingBaseAdapter<Post> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public GridPostAdapter(Context context, List<Post> posts) {
		this.mContext = context;
		if (posts == null) {
			this.items = new ArrayList<Post>();
		}
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Post getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		PostViewHolder holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.grid_delete_row, parent, false);
			holder = new PostViewHolder();
			holder.imgFoto = (SquareImageView)vi.findViewById(R.id.img_foto);
			holder.imgMark = (ImageView)vi.findViewById(R.id.img_select);
			holder.txtCirculo = (TextViewPlus)vi.findViewById(R.id.txt_circulo);
			vi.setTag(holder);
		} else {
			holder = (PostViewHolder) vi.getTag();
		}

		Post post = getItem(position);
		
		HippokratesApplication.imageLoader.displayImage(post.getImageUrl(), holder.imgFoto);
		
		if (post.getCircle() != null) {
			holder.txtCirculo.setText(post.getCircle().getName());
		} else {
			holder.txtCirculo.setText("-");
		}
		
		updateSelectHolder(post, holder);

		return vi;
	}

	public void addPosts(Post[] posts) {
		if (this.items == null) {
			this.items = new ArrayList<Post>();
		}
		this.items.addAll(Arrays.asList(posts));
	}
	
	public List<Post> getAllItems() {
		return this.items;
	}
	
	public void updateSelectHolder(Post post, PostViewHolder holder) {
		if (post.isMark()) {
			holder.imgMark.setVisibility(View.VISIBLE);
			holder.imgFoto.setAlpha(0.5f);
		} else {
			holder.imgFoto.setAlpha(1f);
			holder.imgMark.setVisibility(View.GONE);
		}
	}
}
