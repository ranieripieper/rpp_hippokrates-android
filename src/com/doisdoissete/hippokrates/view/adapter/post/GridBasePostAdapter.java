package com.doisdoissete.hippokrates.view.adapter.post;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.doisdoissete.android.util.ddsutil.view.custom.imageview.SquareImageView;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.BasePost;

public class GridBasePostAdapter extends PagingBaseAdapter<BasePost> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public GridBasePostAdapter(Context context, List<BasePost> posts) {
		this.mContext = context;
		if (posts == null) {
			this.items = new ArrayList<BasePost>();
		}
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public BasePost getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		PostViewHolder holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.grid_feed_row, parent, false);
			holder = new PostViewHolder();
			holder.imgFoto = (SquareImageView)vi.findViewById(R.id.img_foto);
			vi.setTag(holder);
		} else {
			holder = (PostViewHolder) vi.getTag();
		}

		BasePost post = getItem(position);
		
		HippokratesApplication.imageLoader.displayImage(post.getImagesUrl(), holder.imgFoto);

		return vi;
	}

	public void addPosts(BasePost[] posts) {
		if (this.items == null) {
			this.items = new ArrayList<BasePost>();
		}
		this.items.addAll(Arrays.asList(posts));
	}	
	
	public List<BasePost> getAllItems() {
		return this.items;
	}
}
