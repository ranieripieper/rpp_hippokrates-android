package com.doisdoissete.hippokrates.view.adapter.post;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.SquareImageView;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.Comment;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.ActivityFragmentInterface;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.base.comment.CommentViewUtil;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewInterface;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewUtil;
import com.doisdoissete.hippokrates.view.activity.circle.ViewCircleActivity;
import com.doisdoissete.hippokrates.view.activity.post.DetailPostActivity;
import com.doisdoissete.hippokrates.view.activity.post.EditImageActivity;
import com.doisdoissete.hippokrates.view.activity.search.HashtagFeedActivity;
import com.doisdoissete.hippokrates.view.activity.user.ViewUserProfileActivity;

public class PostAdapter extends PagingBaseAdapter<Post> implements RefreshPostAdapter {

	private LayoutInflater inflater;
	private Context mContext;
	private boolean mUserClick = true;
	private boolean mCirculoClick = true;
	private HashtagFeedActivity mActivity;
	private ActivityFragmentInterface mInterface;
	private LocalUser localUser;
	private PostViewInterface postViewInterface;
	
	public PostAdapter(Context context, List<Post> posts, boolean userClick, boolean circuloClick, ActivityFragmentInterface commentInterface, PostViewInterface postViewInterface) {
		this.mContext = context;
		this.mUserClick = userClick;
		this.mCirculoClick = circuloClick;
		if (posts == null) {
			this.items = new ArrayList<Post>();
		}
		this.mInterface = commentInterface;
		this.postViewInterface = postViewInterface;
		localUser = Preferences.getLocalUser(mContext);
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public PostAdapter(Context context, List<Post> posts, ActivityFragmentInterface commentInterface, PostViewInterface postViewInterface) {
		this(context, posts, true, true, commentInterface, postViewInterface);
	}
	
	public PostAdapter(HashtagFeedActivity activity, List<Post> posts, PostViewInterface postViewInterface) {
		this(activity, posts, true, true, activity, postViewInterface);
		this.mActivity = activity;		
	}
	
	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public Post getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		final PostViewHolder holder;

		if (vi == null) {
			vi = inflater.inflate(R.layout.feed_row, parent, false);
			holder = new PostViewHolder();
			holder.txtDescricao = (TextViewPlus)vi.findViewById(R.id.txt_descricao);
			holder.txtLikes = (TextViewPlus)vi.findViewById(R.id.txt_likes);
			holder.txtComments = (TextViewPlus)vi.findViewById(R.id.txt_comments);
			holder.txtData = (TextViewPlus)vi.findViewById(R.id.txt_data);
			holder.txtCirculo = (TextViewPlus)vi.findViewById(R.id.txt_circulo);
			holder.txtNomeMedico = (TextViewPlus)vi.findViewById(R.id.txt_nome_medico);
			holder.imgFoto = (SquareImageView)vi.findViewById(R.id.img_foto);
			holder.imgCirculo = (ImageView)vi.findViewById(R.id.img_circulo);
			holder.btMore = (ImageView)vi.findViewById(R.id.bt_more);
			holder.imgLikes = (ImageView)vi.findViewById(R.id.img_likes);
			holder.imgComments = (ImageView)vi.findViewById(R.id.img_comments);
			vi.setTag(holder);
		} else {
			holder = (PostViewHolder) vi.getTag();
		}

		final Post post = getItem(position);
		
		LocalPost localPost = LocalPost.getLocalPost(post);

		TextViewPlus.TagClick tagClick = new TextViewPlus.TagClick() { 
			@Override
			public void clickedTag(String tag) {
				if (mActivity != null) {
					mActivity.newSearch(tag);
				} else {
					HashtagFeedActivity.showActivity(mContext, tag);
				}
			}
		};
		
		if (localPost != null) {
			if (localPost.isDeleted()) {
				vi.setVisibility(View.GONE);
				return vi;
			}
			updateImageLike(holder.imgLikes, localPost.isUserLiked());
			holder.txtLikes.setText(localPost.getLikesCountStr());
			holder.txtComments.setText(localPost.getCommentsCountStr());
			HippokratesApplication.imageLoader.displayImage(localPost.getImageUrl(), holder.imgFoto);
			holder.txtDescricao.setText(localPost.getDescription(), true, tagClick);
		} else {
			updateImageLike(holder.imgLikes, post.isUserLiked());
			holder.txtLikes.setText(post.getQtLikesStr());
			holder.txtComments.setText(post.getQtComentariosStr());
			HippokratesApplication.imageLoader.displayImage(post.getImageUrl(), holder.imgFoto);
			holder.txtDescricao.setText(post.getDescricao(), true, tagClick);
		}
		
		holder.imgLikes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				likeDislikePost(holder.imgLikes, holder.txtLikes, post);
			}
		});

		holder.txtData.setText(post.getCreatedAtStr());
		
		holder.imgComments.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mInterface != null) {
					CommentViewUtil.showWriteCommentDialog(mContext, post, mInterface, PostAdapter.this);
				}
				
			}
		});
		holder.txtComments.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mInterface != null) {
					CommentViewUtil.showWriteCommentDialog(mContext, post, mInterface, PostAdapter.this);
				}
			}
		});
		
		if (post.getCircle() != null) {
			//se o circulo for público, mostra a especialidade
			final Circle circle;
			
			if (post.getCircle().isPublic()) {
				//circulo fake de especialidade
				circle = new Circle(post.getMedicalSpecialty());
			} else {
				circle = post.getCircle();
			}
			
			holder.txtCirculo.setText(circle.getName());
			HippokratesApplication.imageLoaderCircle.displayImage(circle.getProfileImageUrl(), holder.imgCirculo);	
						
			holder.txtCirculo.setVisibility(View.VISIBLE);
			holder.imgCirculo.setVisibility(View.VISIBLE);
			
			if (mCirculoClick) {
				holder.txtCirculo.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ViewCircleActivity.showActivity(mContext, circle);
					}
				});
				holder.imgCirculo.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ViewCircleActivity.showActivity(mContext, circle);
					}
				});
			}
				
		} else {
			holder.txtCirculo.setVisibility(View.GONE);
			holder.imgCirculo.setVisibility(View.GONE);
		}
		
		if (post.getUser() != null) {
			holder.txtNomeMedico.setText(post.getUser().getName());
			holder.txtNomeMedico.setVisibility(View.VISIBLE);
		} else {
			holder.txtNomeMedico.setVisibility(View.GONE);
		}

		if (mUserClick) {
			holder.txtNomeMedico.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					ViewUserProfileActivity.showActivity(mContext, post.getUser());
				}
			});
		} else {
			holder.txtNomeMedico.setOnClickListener(null);
		}

		
		holder.btMore.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (post != null && (post.getUserId() != null && post.getUserId().equals(localUser.getId())) || 
						 (post.getUser() != null && post.getUser().getId() != null && post.getUser().getId().equals(localUser.getId()))) {
					showEditDeleteDialog(post);
				} else if (post.getCircle() != null && localUser.getId().equals(post.getCircle().getAdminUserId())) {
					showDenunciarDeleteDialog(post);
				} else {
					showDenunciarDialog(post);
				}
				
			}
		});
		vi.setVisibility(View.VISIBLE);
		return vi;
	}
	
	private void likeDislikePost(ImageView imgLikes, TextViewPlus txtLikes, Post post) {
		if (BaseService.internetConnection(mContext)) {
			PostViewUtil.likeDislikePost(mContext, mInterface, post, this);
			updateImageLike(imgLikes, post.isUserLiked());
			txtLikes.setText(post.getQtLikesStr());
		} else {
			mInterface.showMessage(R.string.msg_verifique_conexao);
		}
	}
	
	private void showDenunciarDialog(Post post) {
		HippBaseActivity.showDialogDenunciarPost(mContext, post.getId(), mInterface);
	}
	
	private void showDenunciarDeleteDialog(final Post post) {
		
		View.OnClickListener onClickDenuncar = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				HippBaseActivity.showDialogOptionsDenunciarPost(mContext, post.getId(), mInterface);
			}
		};
		View.OnClickListener onClickDeletar = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				HippBaseActivity.showDefaultDialog(mContext, mContext.getString(R.string.sim), mContext.getString(R.string.nao), mContext.getString(R.string.msg_pergunta_delete_foto_admin), new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						PostViewUtil.callServiceDeletePostAdmin(mContext, mInterface, postViewInterface, post.getId(), post.getCircle().getId());
					}
				}, null);	
			}
		};
		HippBaseActivity.showOptionsDialog(mContext, new int[] {R.string.denunciar_foto, R.string.deletar_foto}, new View.OnClickListener[] {onClickDenuncar, onClickDeletar});
	}

	private void showEditDeleteDialog(final Post post) {
		
		View.OnClickListener onClickEditar = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditImageActivity.showActivity(mContext, post);
			}
		};
		View.OnClickListener onClickDeletar = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				HippBaseActivity.showDefaultDialog(mContext, mContext.getString(R.string.sim), mContext.getString(R.string.nao), mContext.getString(R.string.msg_pergunta_delete_foto), new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						PostViewUtil.callServiceDeletePosts(mContext, mInterface, postViewInterface, Arrays.asList(post.getId()));
					}
				}, null);	
			}
		};
		HippBaseActivity.showOptionsDialog(mContext, new int[] {R.string.edit_post, R.string.deletar_foto}, new View.OnClickListener[] {onClickEditar, onClickDeletar});
	}
	
	
	private void updateImageLike(ImageView imgLikes, boolean isUserLiked) {
		if (isUserLiked) {
			imgLikes.setImageResource(R.drawable.icn_home_liked);
		} else {
			imgLikes.setImageResource(R.drawable.icn_home_likes);
		}
	}
	
	public void addItem(int position, Post obj) {
		if (this.items == null) {
			this.items = new ArrayList<Post>();
		}
		this.items.add(position, obj);
	}	
	
	public void addPosts(Post[] posts) {
		if (this.items == null) {
			this.items = new ArrayList<Post>();
		}
		this.items.addAll(Arrays.asList(posts));
	}	
	
	public List<Post> getAllItems() {
		return this.items;
	}
	
	public void refreshQtdeComments(Comment comment, boolean edit) {
		if (!edit) {
			this.notifyDataSetChanged();
		}
	}
	
	public void removePost(Long postId) {
		if (this.items != null) {
			Post postRemove = null;
			for (Post post : this.items) {
				if (postId.equals(post.getId())) {
					postRemove = post;
				}
			}
			if (postRemove != null) {
				this.items.remove(postRemove);
			}
		}
		notifyDataSetChanged();
	}
	
}
