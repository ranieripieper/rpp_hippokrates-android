package com.doisdoissete.hippokrates.view.adapter.post;

import com.doisdoissete.hippokrates.model.Comment;

public interface RefreshPostAdapter {
	
	void refreshQtdeComments(Comment comment, boolean edit);
}
