package com.doisdoissete.hippokrates.view.adapter.settings;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.hippokrates.view.custom.HipAppDdsSwitch;

public class AlertaViewHolder {

	public TextViewPlus txtAlerta;
	public HipAppDdsSwitch switchAlerta;
}
