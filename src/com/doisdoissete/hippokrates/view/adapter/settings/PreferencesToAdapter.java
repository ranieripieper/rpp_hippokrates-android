package com.doisdoissete.hippokrates.view.adapter.settings;

import android.os.Parcelable;
import android.os.Parcel;

public class PreferencesToAdapter implements Parcelable {
	
	private int id;
	private String paramServer;
	private String preference;
	private boolean checked;
	public static Parcelable.Creator<PreferencesToAdapter> CREATOR = new Parcelable.Creator<PreferencesToAdapter>(){
		public PreferencesToAdapter createFromParcel(Parcel source) {
			return new PreferencesToAdapter(source);
		}
		public PreferencesToAdapter[] newArray(int size) {
			return new PreferencesToAdapter[size];
		}
	};
	
	public PreferencesToAdapter(int id, String paramServer, String preference, boolean checked) {
		super();
		this.preference = preference;
		this.paramServer = paramServer;
		this.checked = checked;
		this.id = id;
	}
	/**
	 * @return the preference
	 */
	public String getPreference() {
		return preference;
	}
	/**
	 * @param preference the preference to set
	 */
	public void setPreference(String preference) {
		this.preference = preference;
	}
	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}
	/**
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the paramServer
	 */
	public String getParamServer() {
		return paramServer;
	}
	/**
	 * @param paramServer the paramServer to set
	 */
	public void setParamServer(String paramServer) {
		this.paramServer = paramServer;
	}
	private PreferencesToAdapter(Parcel in) {
		this.id = in.readInt();
		this.paramServer = in.readString();
		this.preference = in.readString();
		this.checked = in.readByte() != 0;
	}
	public int describeContents() { 
		return 0; 
	}
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeString(this.paramServer);
		dest.writeString(this.preference);
		dest.writeByte(checked ? (byte) 1 : (byte) 0);
	}
	
	
}
