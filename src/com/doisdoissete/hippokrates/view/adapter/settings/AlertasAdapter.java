package com.doisdoissete.hippokrates.view.adapter.settings;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;

import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.view.custom.HipAppDdsSwitch;

public class AlertasAdapter extends BaseAdapter {
	
	private List<PreferencesToAdapter> itens = new ArrayList<PreferencesToAdapter>();
	private LayoutInflater inflater;
	
	public AlertasAdapter(Context context, List<PreferencesToAdapter> value) {
		inflater = LayoutInflater.from(context);
		this.itens = value;
	}
	
	@Override
	public int getCount() {
		return itens.size();
	}
	
	@Override
	public PreferencesToAdapter getItem(int position) {
		return itens.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AlertaViewHolder holder;
		
		if(convertView == null){
			holder = new AlertaViewHolder();
			convertView = inflater.inflate(R.layout.settings_alerta_row, null);
			holder.txtAlerta = (TextViewPlus)convertView.findViewById(android.R.id.text1);
			holder.switchAlerta = (HipAppDdsSwitch)convertView.findViewById(R.id.switch_alerta);
			
			convertView.setTag(holder);
		} else{
			holder = (AlertaViewHolder) convertView.getTag();
		}
		
		final PreferencesToAdapter obj = (PreferencesToAdapter)getItem(position);
		
		
		holder.txtAlerta.setText(obj.getPreference());
		holder.switchAlerta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				obj.setChecked(isChecked);
			}
		});
		
		holder.switchAlerta.setChecked(obj.isChecked());
		
		return convertView;
	}
	
	public void refreshItens(List<PreferencesToAdapter> preferences ) {
		this.itens.removeAll(this.itens);
		this.itens.addAll(preferences);
	}

}