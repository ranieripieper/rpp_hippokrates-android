package com.doisdoissete.hippokrates.view.adapter.circle;

import android.widget.ImageView;
import android.widget.TextView;

public class PendingViewHolder {

	public TextView txtNome;
	public ImageView imgFoto;
	public ImageView imgAccept;
	public ImageView imgReject;
}
