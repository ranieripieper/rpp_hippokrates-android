package com.doisdoissete.hippokrates.view.adapter.circle;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.User;

public class MemberAdapter extends PagingBaseAdapter<User> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public MemberAdapter(Context context, List<User> users) {
		this.mContext = context;
		if (users == null) {
			this.items = new ArrayList<User>();
		}
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public User getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		MemberViewHolder holder = null;

		if (vi == null) {
			vi = inflater.inflate(R.layout.include_member_row, parent, false);
			holder = new MemberViewHolder();			
			holder.txtNome = (TextView)vi.findViewById(R.id.txt_nome);			
			holder.imgFoto = (ImageView)vi.findViewById(R.id.img_foto);
			
			vi.setTag(holder);
		} else {
			holder = (MemberViewHolder) vi.getTag();
		}

		final User obj = getItem(position);
		holder.txtNome.setText(obj.getFullName());
		
		HippokratesApplication.imageLoaderUser.displayImage(obj.getProfileImageUrl(), holder.imgFoto);

		return vi;
	}

	
	public List<User> getAllItems() {
		return this.items;
	}
	
	public void removeItem(User user) {
		if (this.items != null) {
			this.items.remove(user);
			notifyDataSetChanged();
		}
		
	}
}
