package com.doisdoissete.hippokrates.view.adapter.circle;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingBaseAdapter;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.PendingModeration;
import com.doisdoissete.hippokrates.model.User;

public class PendingModerationAdapter extends PagingBaseAdapter<PendingModeration> {

	private LayoutInflater inflater;
	private Context mContext;
	
	public PendingModerationAdapter(Context context, List<PendingModeration> users) {
		this.mContext = context;
		if (users == null) {
			this.items = new ArrayList<PendingModeration>();
		}
		this.inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}
	
	@Override
	public int getViewTypeCount() {
		return 1;
	}

	@Override
	public int getItemViewType(int position) {		
		return 0;
	}
	
	@Override
	public PendingModeration getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getViewRow(position, convertView, parent);
	}
	
	private View getViewRow(int position, View convertView, ViewGroup parent) {
		View vi = convertView;

		final PendingViewHolder holder;

		if (vi == null) {
			vi = inflater.inflate(R.layout.include_pending_row, parent, false);
			holder = new PendingViewHolder();			
			holder.txtNome = (TextView)vi.findViewById(R.id.txt_nome);			
			holder.imgFoto = (ImageView)vi.findViewById(R.id.img_foto);
			holder.imgAccept = (ImageView)vi.findViewById(R.id.img_accept);
			holder.imgReject = (ImageView)vi.findViewById(R.id.img_reject);
			vi.setTag(holder);
		} else {
			holder = (PendingViewHolder) vi.getTag();
		}

		final PendingModeration objItem = getItem(position);
		User obj = objItem.getUser();
		holder.txtNome.setText(obj.getFullName());
		
		HippokratesApplication.imageLoaderUser.displayImage(obj.getProfileImageUrl(), holder.imgFoto);
		
		holder.imgAccept.setAlpha(1f);
		holder.imgReject.setAlpha(1f);
		if (objItem.isApprove()) {
			holder.imgReject.setAlpha(0.5f);
		} else if (objItem.isDecline()) {
			holder.imgAccept.setAlpha(0.5f);
		}
		holder.imgAccept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				holder.imgAccept.setAlpha(1f);
				holder.imgReject.setAlpha(0.5f);
				objItem.setApprove(true);
				objItem.setDecline(false);
			}
		});
		
		holder.imgReject.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				holder.imgAccept.setAlpha(0.5f);
				holder.imgReject.setAlpha(1f);
				objItem.setApprove(false);
				objItem.setDecline(true);
			}
		});
		return vi;
	}

	
	public List<PendingModeration> getAllItems() {
		return this.items;
	}
	
	public void removeItem(PendingModerationAdapter obj) {
		if (this.items != null) {
			this.items.remove(obj);
			notifyDataSetChanged();
		}
		
	}
}
