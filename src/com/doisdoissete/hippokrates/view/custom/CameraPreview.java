package com.doisdoissete.hippokrates.view.custom;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	private SurfaceHolder mHolder;
	private Camera mCamera;

	public CameraPreview(Context context, Camera camera) {
		super(context);
		mCamera = camera;
		mHolder = getHolder();
		mHolder.addCallback(this);
		// deprecated setting, but required on Android versions prior to 3.0
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		try {
			// create the surface and start camera preview
			if (mCamera != null) {
				mCamera.setPreviewDisplay(holder);
				mCamera.startPreview();
			}
		} catch (IOException e) {
			Log.d(VIEW_LOG_TAG, "Error setting camera preview: " + e.getMessage());
		}
	}

	public void refreshCamera(Camera camera) {
		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}
		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}
		// set preview size and make any resize, rotate or
		// reformatting changes here
		// start preview with new settings
		setCamera(camera);
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();
		} catch (Exception e) {
			Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
		}
	}
/*
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
	}*/
	
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		refreshCamera(mCamera);
	}

	public void setCamera(Camera camera) {
		//method to set a camera instance
		mCamera = camera;
		if (camera != null) {
			Camera.Parameters parameters = camera.getParameters();
			List<Size> size = parameters.getSupportedPreviewSizes();

		    Display display = ScreenUtil.getDefaultDisplay(getContext());
	        Point sizepoint = new Point();
	        display.getSize(sizepoint);
	        int width = sizepoint.x;
	        int height = sizepoint.y;
		    // You need to choose the most appropriate previewSize for your app
		    Camera.Size previewSize = determineBestPreviewSize(parameters);

		    parameters.setPreviewSize(previewSize.width, previewSize.height);
		    mCamera.setParameters(parameters);
		    mCamera.setDisplayOrientation(90);
		}
		
	}
	
	public static Camera.Size determineBestPreviewSize(Camera.Parameters parameters) {
	    List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
	    return determineBestSize(sizes);
	}

	public static Camera.Size determineBestPictureSize(Camera.Parameters parameters) {
	    List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
	    return determineBestSize(sizes);
	}

	protected static Camera.Size determineBestSize(List<Camera.Size> sizes) {
	    Camera.Size bestSize = null;
	    long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	    long availableMemory = Runtime.getRuntime().maxMemory() - used;
	    for (Camera.Size currentSize : sizes) {
	        int newArea = currentSize.width * currentSize.height;
	        long neededMemory = newArea * 4 * 4; // newArea * 4 Bytes/pixel * 4 needed copies of the bitmap (for safety :) )
	        boolean isDesiredRatio = (currentSize.width / 4) == (currentSize.height / 3);
	        boolean isBetterSize = (bestSize == null || currentSize.width > bestSize.width);
	        boolean isSafe = neededMemory < availableMemory;
	        if (isDesiredRatio && isBetterSize && isSafe) {
	            bestSize = currentSize;
	        }
	    }
	    if (bestSize == null) {
	        return sizes.get(0);
	    }
	    return bestSize;
	}
	
	
	 private Size getOptimalSize(List<Size> sizes, int w, int h) {


	        final double ASPECT_TOLERANCE = 0.2;        
	        double targetRatio = (double) w / h;         
	        if (sizes == null)             
	            return null;          
	        Size optimalSize = null;         
	        double minDiff = Double.MAX_VALUE;          
	        int targetHeight = h;          

	        for (Size size : sizes) 
	        {             

	            double ratio = (double) size.width / size.height;            
	            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)                
	                continue;             
	            if (Math.abs(size.height - targetHeight) < minDiff) 
	            {                 
	                optimalSize = size;                 
	                minDiff = Math.abs(size.height - targetHeight);             
	            }         
	        }          
	        // Cannot find the one match the aspect ratio, ignore the requirement     

	        if (optimalSize == null)
	        {
	            minDiff = Double.MAX_VALUE;             
	            for (Size size : sizes) {
	                if (Math.abs(size.height - targetHeight) < minDiff)
	                {
	                    optimalSize = size;
	                    minDiff = Math.abs(size.height - targetHeight); 
	                }
	            }
	        }


	        return optimalSize;     
	    }
	 

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		// mCamera.release();

	}
}