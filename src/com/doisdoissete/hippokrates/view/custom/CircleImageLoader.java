package com.doisdoissete.hippokrates.view.custom;

import com.nostra13.universalimageloader.core.ImageLoader;

public class CircleImageLoader extends ImageLoader {

    private volatile static CircleImageLoader instance;

    /** Returns singletone class instance */
    public static CircleImageLoader getInstance() {
        if (instance == null) {
            synchronized (ImageLoader.class) {
                if (instance == null) {
                    instance = new CircleImageLoader();
                }
            }
        }
        return instance;
    }
}