package com.doisdoissete.hippokrates.view.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;

import com.doisdoissete.android.util.ddsutil.view.custom.switchview.DdsSwitch;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;
import com.doisdoissete.hippokrates.R;

public class HipAppDdsSwitch extends DdsSwitch {

	private CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
	
    public HipAppDdsSwitch(Context context) {
        this(context, null);
    }

    public HipAppDdsSwitch(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.switch_switchStyleAttr);
    }

    public HipAppDdsSwitch(Context context, AttributeSet attrs, int defStyle) {
    	super(context, attrs, defStyle);
    	super.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					setTrack(getResources().getDrawable(R.drawable.switch_track));
					setSwitchTextAppearance(getContext(), R.style.mySwitchTextAppearance_white);
				} else {
					setTrack(getResources().getDrawable(R.drawable.switch_track_off));
					setSwitchTextAppearance(getContext(), R.style.mySwitchTextAppearance_gray);
				}
				if (onCheckedChangeListener != null) {
					onCheckedChangeListener.onCheckedChanged(buttonView, isChecked);
				}				
			}
		});
    	
		setSwitchTypeface(FontUtilCache.get("fonts/HelveticaNeue-Light.ttf", getContext()));
		setChecked(true);
    }
    
    @Override
    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
    	onCheckedChangeListener = listener;
    }
    
}
