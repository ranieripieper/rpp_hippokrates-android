package com.doisdoissete.hippokrates.view.custom;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;

import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.SquareImageView;
import com.doisdoissete.hippokrates.R;

/**
 * This is demo code to accompany the Mobiletuts+ tutorial series:
 * - Android SDK: Create a Drawing App
 * 
 * Sue Smith
 * August 2013
 *
 */
public class DrawingView extends SquareImageView {

	private List<Path> paths = new ArrayList<Path>();
	//drawing path
	private Path drawPath;
	//drawing and canvas paint
	private Paint drawPaint, canvasPaint;
	private Paint drawBlurPaint;
	private Paint actualDrawPaint;
	//initial color
	private int paintColor = 0xFFFFFFFF;
	private int paintBlurColor = 0xCCfdfdfd;
	//canvas
	private Canvas drawCanvas;
	//canvas bitmap
	private Bitmap canvasBitmap;
	//brush sizes
	private float brushSize, lastBrushSize;
	//erase flag
	private boolean erase=false;

	private boolean paintEnabled = true;
	private boolean paintBlur = false;
	
	private Bitmap imageBitmap;
	
	public DrawingView(Context context, AttributeSet attrs){
		super(context, attrs);
		setupDrawing();
	}

	//setup drawing
	private void setupDrawing(){

		//prepare for drawing and setup paint stroke properties
		brushSize = getResources().getInteger(R.integer.medium_size);
		lastBrushSize = brushSize;
		drawPath = new Path();
		drawPaint = new Paint();
		drawPaint.setColor(paintColor);
		drawPaint.setAntiAlias(true);
		drawPaint.setStrokeWidth(brushSize);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);
		
		drawBlurPaint = new Paint();
		
		//drawBlurPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_OUT)); 
		drawBlurPaint.setColor(Color.TRANSPARENT);
        
		
		drawBlurPaint.setColor(paintBlurColor);
		drawBlurPaint.setAntiAlias(true);
		drawBlurPaint.setStrokeWidth(brushSize);
		drawBlurPaint.setStyle(Paint.Style.STROKE);
		drawBlurPaint.setStrokeJoin(Paint.Join.ROUND);
		drawBlurPaint.setStrokeCap(Paint.Cap.ROUND);
		drawBlurPaint.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));
		
		canvasPaint = new Paint(Paint.DITHER_FLAG);
	}

	//size assigned to view
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
	}

	//draw the view - will be called after touch event
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
		if (actualDrawPaint != null) {
			canvas.drawPath(drawPath, actualDrawPaint);
		}		
	}

	//register user touches as drawing action
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!paintEnabled) {
			return true;
		}
		
		float touchX = event.getX();
		float touchY = event.getY();
		//respond to down, move and up events
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (!paintBlur) {
				actualDrawPaint = drawPaint;				
			} else {
				actualDrawPaint = drawBlurPaint;
			}
			drawPath.moveTo(touchX, touchY);
			
			break;
		case MotionEvent.ACTION_MOVE:
			if (!paintBlur) {
				actualDrawPaint = drawPaint;				
			} else {
				actualDrawPaint = drawBlurPaint;
			}
			drawPath.lineTo(touchX, touchY);
			break;
		case MotionEvent.ACTION_UP:
			if (!paintBlur) {
				actualDrawPaint = drawPaint;				
			} else {
				actualDrawPaint = drawBlurPaint;
			}
			drawPath.lineTo(touchX, touchY);
			drawCanvas.drawPath(drawPath, actualDrawPaint);
			paths.add(drawPath);
			drawPath = new Path();

			break;
		default:
			return false;
		}
		//redraw
		invalidate();
		return true;

	}

	//update color
	public void setColor(String newColor){
		invalidate();
		paintColor = Color.parseColor(newColor);
		drawPaint.setColor(paintColor);
	}

	/**
	 * @param paintBlur the paintBlur to set
	 */
	public void setPaintBlur(boolean paintBlur) {
		this.paintBlur = paintBlur;
	}

	//set brush size
	public void setBrushSize(float newSize){
		float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 
				newSize, getResources().getDisplayMetrics());
		brushSize=pixelAmount;
		drawPaint.setStrokeWidth(brushSize);
		drawBlurPaint.setStrokeWidth(brushSize);
	}

	//get and set last brush size
	public void setLastBrushSize(float lastSize){
		lastBrushSize=lastSize;
	}
	public float getLastBrushSize(){
		return lastBrushSize;
	}

	//set erase true or false
	public void setErase(boolean isErase){
		erase=isErase;
		if(erase) drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		else drawPaint.setXfermode(null);
	}

	//start new drawing
	public void startNew(){
		drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
		invalidate();
	}
	
	public File saveFile1(String filePath) {
		Bitmap workingBitmap = Bitmap.createBitmap(imageBitmap);
		Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.RGB_565, true);
		Canvas canvas = new Canvas(mutableBitmap);
		for (Path drawPath : paths) {
			canvas.drawPath(drawPath, actualDrawPaint);
		}
		File f = new File(filePath);
		BitmapUtil.copy(imageBitmap, f);
		
		return f;
	}

	public Bitmap getBitmap() {
		buildDrawingCache();
		final Bitmap bmp = Bitmap.createBitmap(getDrawingCache());
		return bmp;
	}
	
	public File saveFile(String path) {
		buildDrawingCache();
		final Bitmap bmp = Bitmap.createBitmap(getDrawingCache());
		setDrawingCacheEnabled(false);
		Canvas c = new Canvas(bmp);

		File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/" + System.currentTimeMillis()+".jpg");
		try {

		    FileOutputStream out = new FileOutputStream(f);
		    c.drawBitmap(bmp, 0, 0, null);
		    bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);

		    out.flush();
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		    return null;
		}
		return f;
	}

	/**
	 * @param enable the enable to set
	 */
	public void setPaintEnabled(boolean enabled) {
		this.paintEnabled = enabled;
	}
	
	public void setBackground(Bitmap bitmap) {
		this.imageBitmap = bitmap;
		setBackground(new BitmapDrawable(getResources(), bitmap));
	}

}
