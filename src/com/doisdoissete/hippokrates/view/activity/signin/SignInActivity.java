package com.doisdoissete.hippokrates.view.activity.signin;

import org.apache.http.HttpStatus;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.date.YearDatePickerFragment;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.LoginResult;
import com.doisdoissete.hippokrates.service.CadastroService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.ProfileUtil;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.HomeActivity;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseSelectImageActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromArrayActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromServerActivity;
import com.doisdoissete.hippokrates.view.custom.HipAppDdsSwitch;

public class SignInActivity extends HippBaseSelectImageActivity {
	
	private static final int ESPECIALIDADE_REQUEST_CODE = 998;
	private static final int UF_FORMACAO_REQUEST_CODE = 997;
	private static final int INST_ENSINO_REQUEST_CODE = 996;
	private static final int REQUEST_TERMO_USO = 995;
	
	private User user;
	private EditTextPlus edtFirstName;
	private EditTextPlus edtLastName;
	private EditTextPlus edtEmail;
	private EditTextPlus edtEspecialidade;
	private EditTextPlus edtSenha;
	private EditTextPlus edtCopnfSenha;
	private EditTextPlus edtInstEnsino;
	private ImageView btnMale;
	private ImageView btnFemale;
	private ImageView imgProfile;
	private String genero;
	private long idEspecialidade = 0l;
	private boolean isTermoAceito = false;
	
	//médico
	private EditTextPlus edtNrTurma;
	private EditTextPlus edtNomeTurma;
	private EditTextPlus edtOcupacao;
	private EditTextPlus edtInstResidencia;
	private HipAppDdsSwitch switchProfessor;
	//estudante
	private EditTextPlus edtUfFormacao;
	private EditTextPlus edtAnoFormatura2;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin_activity);
		
		user = getParcelable(PARAM_IT_USER);
		
		initComponents();
		
		if (Constants.DEGUB) {
			edtFirstName.setText("xxxx");
			edtLastName.setText("xxxx");
			edtEmail.setText("xxxx@xxx.com");
			edtEspecialidade.setText("xxxx");
			edtSenha.setText("qwerty");
			edtCopnfSenha.setText("qwerty");
			edtInstEnsino.setText("ensino");
			genero = Constants.MALE;
			idEspecialidade = 1l;
			edtNrTurma.setText("123");
			edtNomeTurma.setText("edtNomeTurma");
			edtOcupacao.setText("edtOcupacao");
			edtInstResidencia.setText("edtInstResidencia");
			edtUfFormacao.setText("edtUfFormacao");
			edtAnoFormatura2.setText("2000");
		}

	}
	
	@Override
	protected void setImageSelect(Bitmap takenPictureData) {
		hiddenKeyboard();
		imgProfile.setImageBitmap(takenPictureData);
		findViewById(R.id.img_camera_profile).setVisibility(View.GONE);
	}
	
	private void initComponents() {
		edtFirstName = (EditTextPlus)findViewById(R.id.edt_nome);
		edtLastName = (EditTextPlus)findViewById(R.id.edt_sobrenome);
		edtEmail = (EditTextPlus)findViewById(R.id.edt_email);
		btnMale = (ImageView)findViewById(R.id.btn_male);
		btnFemale = (ImageView)findViewById(R.id.btn_female);
		edtEspecialidade =  (EditTextPlus)findViewById(R.id.edt_especialidade);
		imgProfile = (ImageView)findViewById(R.id.img_profile);
		edtSenha = (EditTextPlus)findViewById(R.id.edt_senha);
		edtCopnfSenha = (EditTextPlus)findViewById(R.id.edt_conf_senha);
		edtInstEnsino = (EditTextPlus)findViewById(R.id.edt_inst_ensino);
		//médico
		edtNrTurma = (EditTextPlus)findViewById(R.id.edt_nr_turma);
		edtNomeTurma = (EditTextPlus)findViewById(R.id.edt_nome_turma);
		edtOcupacao = (EditTextPlus)findViewById(R.id.edt_ocupacao);
		edtInstResidencia = (EditTextPlus)findViewById(R.id.edt_inst_residencia);
		switchProfessor = (HipAppDdsSwitch)findViewById(R.id.switch_professor);
		switchProfessor.setChecked(false);
		//aluno
		edtUfFormacao = (EditTextPlus)findViewById(R.id.edt_uf_formacao);
		edtAnoFormatura2 = (EditTextPlus)findViewById(R.id.edt_ano_formatura_2_grau);
		
		if (user != null) {
			edtEmail.setText(user.getEmail());
			edtFirstName.setText(user.getFirstName() == null ? "" : user.getFirstName());
			edtLastName.setText(user.getLastName() == null ? "" : user.getLastName());
			if (ProfileUtil.PROFILE_TYPE_DOCTOR.equalsIgnoreCase(user.getProfileType())) {
				findViewById(R.id.layout_estudante).setVisibility(View.GONE);
				findViewById(R.id.layout_medico).setVisibility(View.VISIBLE);
			} else if (ProfileUtil.PROFILE_TYPE_STUDENT.equalsIgnoreCase(user.getProfileType())) {
				findViewById(R.id.layout_estudante).setVisibility(View.VISIBLE);
				findViewById(R.id.layout_medico).setVisibility(View.GONE);
			}
		}
		
		edtEspecialidade.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hiddenKeyboard();
				SelectFromServerActivity.showActivity(SignInActivity.this, ESPECIALIDADE_REQUEST_CODE, R.drawable.icn_sign_up_especialidade, R.string.txt_especialidade, Paths.MEDICAL_SPECIALITIES);
			}
		});
		
		edtUfFormacao.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hiddenKeyboard();
				SelectFromArrayActivity.showActivity(SignInActivity.this, UF_FORMACAO_REQUEST_CODE, R.drawable.icn_sign_up_cadastro_crm, R.string.txt_estado_crm,  R.array.estados);
			}
		});
		
		edtInstEnsino.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				hiddenKeyboard();
				SelectFromServerActivity.showActivity(SignInActivity.this, INST_ENSINO_REQUEST_CODE, R.drawable.icn_sign_up_instituicao, R.string.txt_instituicao_ensino, Paths.COLLEGES);
			}
		});
		
		edtAnoFormatura2.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				hiddenKeyboard();
				YearDatePickerFragment newFragment = new YearDatePickerFragment(edtAnoFormatura2);
				newFragment.setStyle(DialogFragment.STYLE_NORMAL,
						android.R.style.Theme_Holo_Light_Dialog);
				newFragment.show(getSupportFragmentManager(), "yearPicker");
			}
		});
	}
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == ESPECIALIDADE_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtEspecialidade.setText(result);
			idEspecialidade = data.getExtras().getLong(SelectFromServerActivity.ID_RESULT);
		} else if (requestCode == UF_FORMACAO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtUfFormacao.setText(result);
		} else if (requestCode == INST_ENSINO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtInstEnsino.setText(result);
		} else if (requestCode == REQUEST_TERMO_USO && resultCode == RESULT_OK) {
			isTermoAceito = true;
			callCadastroService();
		}		
		
	}
	
	private boolean isValid() {
		hiddenKeyboard();
		if (edtFirstName.isNotValid() || edtLastName.isNotValid() || edtEmail.isNotValid() || 
				edtSenha.isNotValid() || edtCopnfSenha.isNotValid() ||
				TextUtils.isEmpty(genero) || edtInstEnsino.isNotValid()) {
			return false;
		}
		
		//valida dados médico
		if (ProfileUtil.PROFILE_TYPE_DOCTOR.equals(user.getProfileType())) {
			if (edtNrTurma.isNotValid() || edtNomeTurma.isNotValid() || idEspecialidade <= 0
					 || edtOcupacao.isNotValid()
					 || edtInstResidencia.isNotValid()) {
				return false;
			}
		}
		
		//valida dados estudante
		if (ProfileUtil.PROFILE_TYPE_STUDENT.equals(user.getProfileType())) {
			if (edtUfFormacao.isNotValid() || edtAnoFormatura2.isNotValid()) {
				return false;
			}
		}
		return true;
	}
	
	private void hiddenKeyboard() {
		edtFirstName.hiddenKeyboard(getContext());
		edtLastName.hiddenKeyboard(getContext());
		edtEmail.hiddenKeyboard(getContext());
		edtEspecialidade.hiddenKeyboard(getContext());
		edtSenha.hiddenKeyboard(getContext());
		edtCopnfSenha.hiddenKeyboard(getContext());
		edtInstEnsino.hiddenKeyboard(getContext());
		edtNrTurma.hiddenKeyboard(getContext());
		edtNomeTurma.hiddenKeyboard(getContext());
		edtOcupacao.hiddenKeyboard(getContext());
		edtInstResidencia.hiddenKeyboard(getContext());
		edtUfFormacao.hiddenKeyboard(getContext());
		edtAnoFormatura2.hiddenKeyboard(getContext());
	}
	
	private void callCadastroService() {
		ObserverAsyncTask<LoginResult> observer = new ObserverAsyncTask<LoginResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(LoginResult result) {
				if (result != null && result.isSuccess() && result.getUser() != null && result.getAuthData() != null) {
					LocalUser user = new LocalUser();
					user.setId(result.getUser().getId());
					user.setEmail(edtEmail.getTextStr());
					user.setUsername(user.getUsername());
					user.setAuthToken(result.getAuthData().getAuthToken());
					user.setProfileImg(result.getUser().getProfileImageUrl());	
					user.setProfileType(result.getUser().getProfileType());
					Preferences.setLocalUser(getContext(), user);
					finishAffinity();
					HomeActivity.showActivity(getContext());
					
				} else {
					dismissWaitDialog();
					showMessage(R.string.msg_nome_senha_invalidos);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onError(Exception e) {
				if (e instanceof ConnectionException) {
					ConnectionException ex = (ConnectionException)e;
					if(ex.getHttpStatus() != null && HttpStatus.SC_NOT_FOUND == ex.getHttpStatus().value()) {
						showMessage(R.string.msg_crm_invalido);
						dismissWaitDialog();
					} else {
						showError(e);
					}
				} else {
					showError(e);
				}
				
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		params.add(UrlParameters.PARAM_USER_NAME, edtFirstName.getTextStr());
		params.add(UrlParameters.PARAM_USER_LAST_NAME, edtLastName.getTextStr());
		params.add(UrlParameters.PARAM_USER_EMAIL, edtEmail.getTextStr());
		params.add(UrlParameters.PARAM_USER_GENDER, genero);
		params.add(UrlParameters.PARAM_USER_INVITE_CODE, user.getInviteCode());
		params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_UNIVERSITY_INST_NAME, edtInstEnsino.getTextStr());
		params.add(UrlParameters.PARAM_USER_PASSWORD, edtSenha.getTextStr());
		params.add(UrlParameters.PARAM_USER_PASSWORD_CONF, edtCopnfSenha.getTextStr());
		
		params.add(UrlParameters.PARAM_USER_DEVICE_PLATFORM, Constants.PROVIDER);
		params.add(UrlParameters.PARAM_USER_DEVICE_TOKEN, Preferences.getParseToken(getContext()));
		
		if (switchProfessor.isChecked()) {
			params.add(UrlParameters.PARAM_USER_PROFILE_TYPE, ProfileUtil.PROFILE_TYPE_TEACHER);
		} else {
			params.add(UrlParameters.PARAM_USER_PROFILE_TYPE, user.getProfileType());
		}
		boolean uploadFile = false;
		if (!TextUtils.isEmpty(filePath)) {
			//params.add(UrlParameters.PARAM_USER_PROFILE_IMAGE, new FileSystemResource(filePath));
			params.add(UrlParameters.PARAM_USER_PROFILE_IMAGE, filePath);
			uploadFile = true;
		}
		if (ProfileUtil.PROFILE_TYPE_DOCTOR.equals(user.getProfileType())) {
			params.add(UrlParameters.PARAM_USER_CRM_DATA_NUMBER, user.getCrmNumber());
			params.add(UrlParameters.PARAM_USER_CRM_DATA_STATE, user.getCrmState());
			params.add(UrlParameters.PARAM_USER_MEDICAL_SPECIALTY, String.valueOf(idEspecialidade));
			params.add(UrlParameters.PARAM_USER_OCUPATION, edtOcupacao.getTextStr());
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NUMBER, edtNrTurma.getTextStr());
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NAME, edtNomeTurma.getTextStr());
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_RESIDENCY_INST_NAME, edtInstResidencia.getTextStr());
		} else {
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_STATE, edtUfFormacao.getTextStr());
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_YEAR, edtAnoFormatura2.getTextStr());
		}
		
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Paths.SIGNUP)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setUploadFile(uploadFile)
			.addHeaders("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE)
			.setHttpMethod(HttpMethod.POST)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), LoginResult.class, new CadastroService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	// clicks
	public void btnGeneroClick(View v) {
		if (MALE_TAG.equals(v.getTag())) {
			genero = Constants.MALE;
		} else if (FEMALE_TAG.equals(v.getTag())) {
			genero = Constants.FEMALE;
		}
		adjustIconesGenero();
	}
	
	public void cadastrarClick(View v) {
		if (isValid()) {
			callTermosActivity();
		} else {
			showMessage(R.string.msg_validar_crm_preenchimento_campos);
		}
	}
		
	private void callTermosActivity() {
		if (!isTermoAceito) {
			TermoUsoActivity.showActivity(this, REQUEST_TERMO_USO);
		} else {
			callCadastroService();
		}
		
	}
		
	private void adjustIconesGenero() {
		if (Constants.FEMALE.equals(genero)) {
			btnFemale.setImageResource(R.drawable.btn_sign_up_female_on);
			btnMale.setImageResource(R.drawable.btn_sign_up_male_off);
		} else if (Constants.MALE.equals(genero)) {
			btnFemale.setImageResource(R.drawable.btn_sign_up_female_off);
			btnMale.setImageResource(R.drawable.btn_sign_up_male_on);
		}
	}
	
	public static final void showActivity(Context ctx, User user) {
		Intent it = new Intent(ctx, SignInActivity.class);
		it.putExtra(PARAM_IT_USER, user);
		ctx.startActivity(it);
	}
	
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	@Override
	protected boolean resizeBitmap() {
		return true;
	}
	
	@Override
	protected int maxHeightResize() {
		return Constants.MAX_HEIGHT_USER;
	}
	
	@Override
	protected int maxWidthResize() {
		return Constants.MAX_WIDTH_USER;
	}
}
 