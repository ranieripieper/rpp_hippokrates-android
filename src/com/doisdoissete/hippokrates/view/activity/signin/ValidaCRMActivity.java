package com.doisdoissete.hippokrates.view.activity.signin;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.CRM;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.result.ValidateCRMResult;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.HippUtil;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromArrayActivity;

public class ValidaCRMActivity extends HippBaseActivity {

	private static final String PARAM_IT_VALIDA_ESTUDANTE = "PARAM_IT_VALIDA_ESTUDANTE";
	public static final String CRM_VALID = "CRM_VALID";
	public static final String CRM_NR = "CRM_NR";
	public static final String CRM_STATE = "CRM_STATE";
	private static final int ESTADO_REQUEST_CODE = 999;
	
	private User user;
	
	private boolean validaEstudante = false;
	private View btValidar;
	private EditTextPlus edtEstadoCrm;
	private EditTextPlus edtCrm;
	private EditTextPlus edtPrimeiroNome;
	private EditTextPlus edtUltimoNome;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.valida_crm_activity);
		user = getIntent().getExtras().getParcelable(PARAM_IT_USER);
		validaEstudante = getIntent().getExtras().getBoolean(PARAM_IT_VALIDA_ESTUDANTE);
		
		initComponents();
	}
	
	private void hiddenKeyboard() {
		edtEstadoCrm.hiddenKeyboard(getContext());
		edtCrm.hiddenKeyboard(getContext());
		edtPrimeiroNome.hiddenKeyboard(getContext());
		edtUltimoNome.hiddenKeyboard(getContext());
	}
	
	private void initComponents() {
		btValidar = findViewById(R.id.bt_validar);
		edtCrm = (EditTextPlus)findViewById(R.id.edt_crm);
		edtPrimeiroNome = (EditTextPlus)findViewById(R.id.edt_nome);
		edtUltimoNome = (EditTextPlus)findViewById(R.id.edt_sobrenome);
		edtEstadoCrm = (EditTextPlus)findViewById(R.id.edt_estado_crm);
		
		edtEstadoCrm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hiddenKeyboard();
				SelectFromArrayActivity.showActivity(ValidaCRMActivity.this, ESTADO_REQUEST_CODE, R.drawable.icn_sign_up_cadastro_crm, R.string.txt_estado_crm,  R.array.estados);
			}
		});
		
		findViewById(R.id.bt_fale_conosco).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				hiddenKeyboard();
				faleConoscoClick(v);
			}
		});
		
		if(validaEstudante) {
			edtPrimeiroNome.setEnabled(false);
			edtPrimeiroNome.setText(user.getName());
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == ESTADO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromArrayActivity.TEXT_RESULT);
			edtEstadoCrm.setText(result);
		}
	}
	
	public void validarCrmClick(View v) {
		hiddenKeyboard();
		if (edtCrm.isNotValid() || edtEstadoCrm.isNotValid() || edtPrimeiroNome.isNotValid() || edtUltimoNome.isNotValid()) {
			showMessage(R.string.msg_validar_crm_preenchimento_campos);
		} else {
			callService();
		}
	}
	
	public void faleConoscoClick(View v) {
		Util.sendMail(this, Constants.EMAIL_FALE_CONOSCO, getString(R.string.email_subject_fale_conosco), getString(R.string.email_body_fale_conosco));
	}
	
	private void callService() {
		ObserverAsyncTask<ValidateCRMResult> observer = new ObserverAsyncTask<ValidateCRMResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
			}

			@Override
			public void onPostExecute(ValidateCRMResult result) {
				
				if (result != null && result.isValid() && result.isActive()) {
					user.setCrmNumber(edtCrm.getTextStr());
					user.setCrmState(HippUtil.getSiglaUF(getContext(), edtEstadoCrm.getTextStr()));
					user.setFirstName(edtPrimeiroNome.getTextStr());
					user.setLastName(edtUltimoNome.getTextStr());
					if (validaEstudante) {
						Intent resultIntent = new Intent();
						resultIntent.putExtra(CRM_VALID, true);
						resultIntent.putExtra(CRM_NR, user.getCrmNumber());
						resultIntent.putExtra(CRM_STATE, user.getCrmState());
						setResult(Activity.RESULT_OK, resultIntent);
						finish();
					} else {
						SignInActivity.showActivity(getContext(), user);
					}
					dismissWaitDialog();
				} else {
					dismissWaitDialog();
					showMessage(R.string.msg_codigo_invalido);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				HttpStatus httpStatus = null;
				if (e instanceof ConnectionException) {
					ConnectionException ex = (ConnectionException)e;
					httpStatus = ex.getHttpStatus();
				} else if (e instanceof DdsUtilIOException) {
					DdsUtilIOException ex = (DdsUtilIOException)e;
					httpStatus = ex.getHttpStatus();
				}
					
				if(httpStatus != null && HttpStatus.NOT_FOUND.value() == httpStatus.value()) {
					showMessage(R.string.msg_crm_invalido);
					dismissWaitDialog();
				} else {
					showError(e);
				}
				
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(CRM.FIELD_GET_CRM_NUMBER, edtCrm.getTextStr());
		params.add(CRM.FIELD_GET_CRM_STATE, HippUtil.getSiglaUF(getContext(), edtEstadoCrm.getTextStr()));
		params.add(CRM.FIELD_GET_FIRST_NAME, edtPrimeiroNome.getTextStr());
		params.add(CRM.FIELD_GET_LAST_NAME, edtUltimoNome.getTextStr());
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Paths.VALIDATE_CRM)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), ValidateCRMResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	
	public static final void showActivity(Context ctx, User user) {
		Intent it = new Intent(ctx, ValidaCRMActivity.class);
		it.putExtra(PARAM_IT_USER, user);
		ctx.startActivity(it);
	}
	
	public static final void showActivity(Activity ctx, User user, boolean validaEstudante, int requestCode) {
		Intent it = new Intent(ctx, ValidaCRMActivity.class);
		it.putExtra(PARAM_IT_USER, user);
		it.putExtra(PARAM_IT_VALIDA_ESTUDANTE, validaEstudante);
		ctx.startActivityForResult(it, requestCode);
	}
}
