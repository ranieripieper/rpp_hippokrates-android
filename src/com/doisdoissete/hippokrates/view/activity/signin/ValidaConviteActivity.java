package com.doisdoissete.hippokrates.view.activity.signin;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.EditTextUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.result.ValidateInviteResult;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.ProfileUtil;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class ValidaConviteActivity extends HippBaseActivity {

	private EditTextPlus edtCodigo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.valida_convite_activity);
		
		initComponents();
	}
	
	private void initComponents() {
		findViewById(R.id.bt_fale_conosco).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				faleConoscoClick(v);
			}
		});
		
		edtCodigo = (EditTextPlus)findViewById(R.id.edt_codigo_convite);
		edtCodigo.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(R.integer.max_caracteres_invite)});
		
		edtCodigo.setOnEditorActionListener(new TextView.OnEditorActionListener(){
		    public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event){
		        if(actionId == EditorInfo.IME_ACTION_DONE 
		            || actionId == EditorInfo.IME_NULL
		            || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
		        	EditTextUtil.hiddenKeyboard(ValidaConviteActivity.this, edtCodigo);
		        	validarConviteClick(null);
		            return true;
		        } else {
		            return false;
		        }
		    }
		});
	}
	
	public void validarConviteClick(View v) {
		if (edtCodigo.isValid()) {
			callService();
		} else {
			showMessage(R.string.msg_codigo_invalido);
		}
	}
	
	public void faleConoscoClick(View v) {
		Util.sendMail(this, Constants.EMAIL_FALE_CONOSCO, getString(R.string.email_subject_fale_conosco), getString(R.string.email_body_fale_conosco));
	}
	
	public static final void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, ValidaConviteActivity.class));
	}
	
	//métodos privados
	
	private void callService() {
		edtCodigo.hiddenKeyboard(getContext());
		ObserverAsyncTask<ValidateInviteResult> observer = new ObserverAsyncTask<ValidateInviteResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
			}

			@Override
			public void onPostExecute(ValidateInviteResult result) {
				
				if (result != null && result.isSuccess() && result.getInvite() != null) {
					if (Constants.INVITE_SENT.equals(result.getInvite().getStatus())) {
						User user = new User();
						user.setInviteCode(edtCodigo.getTextStr());
						user.setEmail(result.getInvite().getEmail());
						user.setProfileType(ProfileUtil.getProfileTypeByReceiver(result.getInvite().getReceiverUserProfileType()));
						if (user.isDoctor()) {
							ValidaCRMActivity.showActivity(ValidaConviteActivity.this, user);
						} else {
							SignInActivity.showActivity(getContext(), user);
						}
					} else {
						showMessage(R.string.msg_codigo_invalido);
					}
					
					dismissWaitDialog();
				} else {
					dismissWaitDialog();
					showMessage(R.string.msg_codigo_invalido);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_CODE, edtCodigo.getTextStr());
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Paths.VALIDATE_INVITE)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), ValidateInviteResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
