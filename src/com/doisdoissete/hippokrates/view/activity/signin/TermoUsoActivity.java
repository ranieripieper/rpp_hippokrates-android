package com.doisdoissete.hippokrates.view.activity.signin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class TermoUsoActivity extends HippBaseActivity {

	private static final String PARAM_SHOW_BUTTONS = "PARAM_SHOW_BUTTONS";
	private boolean showButtons = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		showButtons = getIntent().getBooleanExtra(PARAM_SHOW_BUTTONS, showButtons);
		
		setContentView(R.layout.termo_uso_activity);
		
		showWaitDialog(true);
		
		final TextView txtTermos = (TextView)findViewById(R.id.txt_termos);
		
		txtTermos.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				Spanned sp = Html.fromHtml( getString(R.string.txt_termo_uso) );
				txtTermos.setText(sp);
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						txtTermos.setVisibility(View.VISIBLE);
						dismissWaitDialog();
					}
				}, 500);
				
				
			}
		}, 500);
		
		if (showButtons) {
			findViewById(R.id.bt_aceitar).setVisibility(View.VISIBLE);
			findViewById(R.id.bt_recusar).setVisibility(View.VISIBLE);
			findViewById(R.id.bt_aceitar).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent result = new Intent();
					setResult(Activity.RESULT_OK, result);
					finish();
				}
			});
			
			findViewById(R.id.bt_recusar).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent result = new Intent();
					setResult(Activity.RESULT_CANCELED, result);
					finish();
				}
			});
		} else {
			findViewById(R.id.bt_aceitar).setVisibility(View.GONE);
			findViewById(R.id.bt_recusar).setVisibility(View.GONE);
		}		
		
	}	
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	public static void showActivity(Activity context, int requestCode) {
		context.startActivityForResult(new Intent(context, TermoUsoActivity.class), requestCode);
	}
	
	public static void showActivity(Context context, boolean showButtons) {
		Intent it = new Intent(context, TermoUsoActivity.class);
		it.putExtra(PARAM_SHOW_BUTTONS, showButtons);
		context.startActivity(it);
	}
}
