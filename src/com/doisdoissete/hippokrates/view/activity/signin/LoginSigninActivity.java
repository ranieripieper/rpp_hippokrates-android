package com.doisdoissete.hippokrates.view.activity.signin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.util.ProfileUtil;
import com.doisdoissete.hippokrates.view.activity.askforinvite.AskForInviteActivity;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.login.LoginActivity;

public class LoginSigninActivity extends HippBaseActivity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.login_sigin_activity);
	}
	
	public void loginClick(View v) {
		startActivity(new Intent(this, LoginActivity.class));
	}
	
	public void askForInviteClick(View v) {
		AskForInviteActivity.showActivity(this);
	}
	
	public void signinClick(View v) {
		showOptionsDialog(getContext(), new int[]{R.string.txt_option_sou_estudante, R.string.txt_option_sou_medico, }, new View.OnClickListener[] {
			new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ValidaConviteActivity.showActivity(getContext());
				}
			}, new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					User user = new User();
					user.setInviteCode("");
					user.setProfileType(ProfileUtil.PROFILE_TYPE_DOCTOR);
					ValidaCRMActivity.showActivity(getContext(), user);
				}
			}
		});
		
		
	}
	
	public static Intent getIntentActivity(Context ctx) {
		return new Intent(ctx, LoginSigninActivity.class);
	}
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(getIntentActivity(ctx));
	}
}
