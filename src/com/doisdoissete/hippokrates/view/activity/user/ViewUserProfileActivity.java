package com.doisdoissete.hippokrates.view.activity.user;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Build;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.gridview.PagingGridView;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.BasePost;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.UserFeedResult;
import com.doisdoissete.hippokrates.model.result.UserResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewInterface;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewUtil;
import com.doisdoissete.hippokrates.view.activity.post.DetailPostActivity;
import com.doisdoissete.hippokrates.view.adapter.post.GridBasePostAdapter;
import com.doisdoissete.hippokrates.view.adapter.post.PostAdapter;

@EActivity(R.layout.user_view_user_profile)
public class ViewUserProfileActivity extends UserProfileBaseActivity {

	protected static final String PARAM_USER_ID = "PARAM_USER_ID";
	
	public static final int MENU_LIST = 1;
	public static final int MENU_GRID = 2;
	public static final int MENU_INFO = 3;
	
	public int mMenuSelected = MENU_LIST;
	
	@ViewById(R.id.menu_feed_lista)
	ImageView menuFeedList;
	@ViewById(R.id.menu_feed_grid)
	ImageView menuFeedGrid;
	@ViewById(R.id.menu_info)
	ImageView menuInfo;
	
	@ViewById(R.id.img_profile)
	ImageView imgProfile;
	
	@ViewById(R.id.txt_name)
	TextViewPlus txtName;

	@ViewById(R.id.grid_feed)
	PagingGridView pagingGridView;
	
	@ViewById(R.id.list_feed)
	PagingListView pagingListView;
	
	@ViewById(R.id.txt_nenhum_post)
	TextViewPlus txtNenhumPost;
	
	//Toolbar
	@ViewById(R.id.txt_delete_fotos)
	TextViewPlus txtDeletarFotos;
	@ViewById(R.id.img_delete_fotos)
	ImageView imgDeletarFotos;
	
	private UserResult userResult;
	
	private PostAdapter postAdapter;
	private GridBasePostAdapter gridAdapter;
	protected AsyncTaskService asyncInfo;
	
	private boolean myProfile;
	
	@AfterViews
	void postCreate() {
		user = getIntent().getExtras().getParcelable(PARAM_USER);
		if (user != null) {
			init();
		} else {
			long userId = getIntent().getExtras().getLong(PARAM_USER_ID);
			callServiceInfos(userId, true);
		}
	}
	
	private void init() {

		HippokratesApplication.imageLoaderUser.displayImage(user.getProfileImageUrl(), imgProfile);
		txtName.setText(user.getFullName());
		
		LocalUser localUser = Preferences.getLocalUser(getContext());
		if (localUser.getId().equals(user.getId())) {
			myProfile = true;
			txtNenhumPost.setText(R.string.nenhum_post);
		}
		
		pagingListView.setOnTouchListener(new OnTouchListener() {
			GestureDetector gestureDetector = new GestureDetector(new SimpleOnGestureListener() {
            	@Override
            	public boolean onDoubleTap(MotionEvent e) {
            		int position = pagingListView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = postAdapter.getItem(position);
            		PostViewUtil.likeDislikePost(getContext(), ViewUserProfileActivity.this, post, postAdapter);
            		
            		return super.onDoubleTap(e);
            	}
            	
            	@Override
            	public boolean onSingleTapConfirmed(MotionEvent e) {
            		int position = pagingListView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = postAdapter.getItem(position);
    				DetailPostActivity.showActivity(getContext(), post);
            		return super.onSingleTapConfirmed(e);
            	}
            });
			@Override
	         public boolean onTouch(View v, MotionEvent event) {
	                return gestureDetector.onTouchEvent(event);
	         }
		});
		callServicePosts(true);
	}
	
	@Click(R.id.menu_feed_lista)
	void menuFeedListClick() {
		changeMenu(MENU_LIST);
	}
	
	@Click(R.id.menu_feed_grid)
	void menuFeedGridClick() {
		changeMenu(MENU_GRID);
	}
	
	@Click(R.id.menu_info)
	void menuInfoClick() {
		changeMenu(MENU_INFO);
	}
	
	@Click(R.id.txt_delete_fotos)
	void txtDeleteFotosClick() {
		deleteFotos();
	}
	
	@Click(R.id.img_delete_fotos)
	void imgDeleteFotosClick() {
		deleteFotos();
	}
	
	private void deleteFotos() {		
		showOptionsDialog(new int[] { 	R.string.deletar_todas_as_fotos,
										R.string.selecionar_fotos }, 
						  new View.OnClickListener[] {

							new View.OnClickListener() {
					
								@Override
								public void onClick(View v) {
									deleteAllFotos();
								}
							}, new View.OnClickListener() {
					
								@Override
								public void onClick(View v) {
									showSelectFotosDelete();
								}
							} });
	}
	
	private void deleteAllFotos() {
		showDefaultDialog(getString(R.string.sim), getString(R.string.nao), getString(R.string.msg_pergunta_delete_todas_fotos), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				callServiceDeletePosts(null);
			}
		});
	}
	
	private void showSelectFotosDelete() {
		DeleteFotosActivity.showActivity(getContext(), postAdapter.getAllItems(), page, user, pagingListView.hasMoreItems());
	}

	private void changeMenu(int menu) {
		mMenuSelected = menu;
		menuFeedList.setImageResource(R.drawable.icn_user_detail_disposal2);
		menuFeedGrid.setImageResource(R.drawable.icn_user_detail_disposal1);		
		menuInfo.setImageResource(R.drawable.icn_user_detail_infos);
		
		pagingListView.setVisibility(View.GONE);
		pagingGridView.setVisibility(View.GONE);
		txtNenhumPost.setVisibility(View.GONE);
		findViewById(R.id.layout_user_info).setVisibility(View.GONE);
		
		if (mMenuSelected == MENU_LIST || mMenuSelected == MENU_GRID) {
			showMenuListGrid();
		} else if (mMenuSelected == MENU_INFO) {
			menuInfo.setImageResource(R.drawable.icn_user_detail_infos_on);
			findViewById(R.id.layout_user_info).setVisibility(View.VISIBLE);
			callServiceInfos(user.getId(), false);
		}
	}
	
	private void callServiceInfos(long userId, final boolean init) {
		if (userResult != null) {
			return;
		}
		if (asyncInfo != null && (Status.PENDING.equals(asyncInfo.getStatus()) || Status.RUNNING.equals(asyncInfo.getStatus()))) {
			return;
		}
		ObserverAsyncTask<UserResult> observer = new ObserverAsyncTask<UserResult>() {
			@Override
			public void onPreExecute() {
				findViewById(R.id.loading_info).setVisibility(View.VISIBLE);
				findViewById(R.id.layout_user_info_content).setVisibility(View.GONE);
			}

			@Override
			public void onPostExecute(UserResult result) {
				userResult = result;
				if (init) {
					user = result.getUser();
					init();
				}
				populateInfo();				
			}

			@Override
			public void onCancelled() {
				findViewById(R.id.loading_info).setVisibility(View.GONE);
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				findViewById(R.id.loading_info).setVisibility(View.GONE);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		String url = Paths.USER_INFO.replace(UrlParameters.PARAM_USER_ID_URL, String.valueOf(userId));
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		asyncInfo = sb.mappingInto(getContext(), UserResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			asyncInfo.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			asyncInfo.execute();
		}
	}
	

	private void showMenuListGrid() {
		if (postAdapter != null && postAdapter.getCount() <= 0) {
			txtNenhumPost.setVisibility(View.VISIBLE);
			pagingListView.setVisibility(View.GONE);
			pagingListView.setVisibility(View.GONE);			
		} else {
			if (mMenuSelected == MENU_LIST) {
				pagingListView.setVisibility(View.VISIBLE);
			} else if (mMenuSelected == MENU_GRID) {
				pagingGridView.setVisibility(View.VISIBLE);
			}
		}
		
		if (mMenuSelected == MENU_LIST) {
			menuFeedList.setImageResource(R.drawable.icn_user_detail_disposal2_on);
		} else if (mMenuSelected == MENU_GRID) {
			menuFeedGrid.setImageResource(R.drawable.icn_user_detail_disposal1_on);
		}
	}
	
	private void populateInfo() {
		findViewById(R.id.loading_info).setVisibility(View.GONE);
		if (userResult != null && userResult.getUser() != null) {
			LinearLayout layout = (LinearLayout)findViewById(R.id.layout_user_info_content);
			if (!TextUtils.isEmpty(userResult.getUser().getCrmState())) {
				layout.addView(getView(R.drawable.icn_sign_up_cadastro_crm, R.string.estado, userResult.getUser().getCrmState()));
			}
			if (userResult.getUser().getEudcationData() != null) { 
				if(!TextUtils.isEmpty(userResult.getUser().getEudcationData().getUniversityInstitutionName())) {
					layout.addView(getView(R.drawable.icn_sign_up_instituicao, R.string.instituicao_ensino, userResult.getUser().getEudcationData().getUniversityInstitutionName()));
				}
				if(!TextUtils.isEmpty(userResult.getUser().getEudcationData().getResidencyProgramInstituitionName())) {
					layout.addView(getView(R.drawable.icn_sign_up_inst_residencia, R.string.instituicao_residencia, userResult.getUser().getEudcationData().getResidencyProgramInstituitionName()));
				}
				if(!TextUtils.isEmpty(userResult.getUser().getEudcationData().getClassroomNumber())) {
					layout.addView(getView(R.drawable.icn_sign_up_numero_da_turma, R.string.numero_turma, userResult.getUser().getEudcationData().getClassroomNumber()));
				}
				if(!TextUtils.isEmpty(userResult.getUser().getEudcationData().getClassroomName())) {
					layout.addView(getView(R.drawable.icn_sign_up_nome_da_turma, R.string.nome_turma, userResult.getUser().getEudcationData().getClassroomName()));
				}
			}
			if (userResult.getUser().getMedicalSpecialty() != null && !TextUtils.isEmpty(userResult.getUser().getMedicalSpecialty().getName())) {
				layout.addView(getView(R.drawable.icn_sign_up_especialidade, R.string.especialidade, userResult.getUser().getMedicalSpecialty().getName()));
			}
			if (!TextUtils.isEmpty(userResult.getUser().getOccupation())) {
				layout.addView(getView(R.drawable.icn_sign_up_ocupacao, R.string.ocupacao, userResult.getUser().getOccupation()));
			}
			layout.setVisibility(View.VISIBLE);
		} else {
			showMessage(R.string.msg_generic_error);
		}
	}
	
	private View getView(int icon, int label, String value) {
		View v = getLayoutInflater().inflate(R.layout.include_info_user_profile, null);
		ImageView icIcon = (ImageView)v.findViewById(R.id.ic_info);
		TextViewPlus txtLabel = (TextViewPlus)v.findViewById(R.id.txt_label_info);
		TextViewPlus txtInfo = (TextViewPlus)v.findViewById(R.id.txt_info);
		icIcon.setImageResource(icon);
		txtLabel.setText(label);
		txtInfo.setText(value);
		return v;
	}
	
	@Override
	protected void clearList() {
		if (postAdapter != null) {
			postAdapter.removeAllItems();
		}
		if (gridAdapter != null) {
			gridAdapter.removeAllItems();
		}
	}
	
	protected void populateListView(UserFeedResult result) {
		if (postAdapter == null) {
			pagingListView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					callServicePosts(true);
				}
			});
			postAdapter = new PostAdapter(getContext(), new ArrayList<Post>(), false, true, this, new PostViewInterface() {
				
				@Override
				public void removedPosts(List<Long> ids) {
					if (ids != null && ids.size() > 0) {
						LocalPost.deletePosts(ids, postAdapter);
						postAdapter.notifyDataSetChanged();
					}
				}
			});
			pagingListView.setAdapter(postAdapter);
		}
		
		if (gridAdapter == null) {
			pagingGridView.setPagingableListener(new PagingGridView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					callServicePosts(true);
				}
			});
			gridAdapter = new GridBasePostAdapter(getContext(), new ArrayList<BasePost>());
			pagingGridView.setAdapter(gridAdapter);
		}
		
		boolean loadMore = true;
		if (result != null) {			
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (page < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
			} else {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > result.getPost().size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			}
			
			if (result.getPost() != null && !result.getPost().isEmpty()) {
				pagingListView.onFinishLoading(loadMore, result.getPost());
				if (myProfile) {
					txtDeletarFotos.setVisibility(View.VISIBLE);
					imgDeletarFotos.setVisibility(View.VISIBLE);
				}
			} else {
				pagingListView.onFinishLoading(loadMore, new ArrayList<Post>());
			}
			
			if (result.getBasePost() != null) {
				pagingGridView.onFinishLoading(loadMore, result.getBasePost());
			} else {
				pagingGridView.onFinishLoading(loadMore, new ArrayList<BasePost>());
			}
			
			postAdapter.notifyDataSetChanged();
	    	pagingListView.requestLayout();
	    	
	    	gridAdapter.notifyDataSetChanged();
	    	pagingGridView.requestLayout();
	    	
		} else {
			loadMore = false;
			pagingListView.onFinishLoading(loadMore, new ArrayList<Post>());
			pagingGridView.onFinishLoading(loadMore, new ArrayList<BasePost>());
		}
		
		changeMenu(mMenuSelected);
	}
	
	@Override
	public void removedPosts(List<Long> ids) {
		page = 0;
		gridAdapter.removeAllItems();
		postAdapter.removeAllItems();
		gridAdapter.notifyDataSetChanged();
		postAdapter.notifyDataSetChanged();
		pagingListView.onFinishLoading(false, new ArrayList<Post>());
		pagingGridView.onFinishLoading(false, new ArrayList<BasePost>());
		changeMenu(mMenuSelected);
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	public static Intent getIntentActivity(Context ctx, Long userId) {
		Intent it = new Intent(ctx, ViewUserProfileActivity_.class);
		it.putExtra(PARAM_USER_ID, userId);
		return it;
	}
	
	public static void showActivity(Context ctx, User user) {
		Intent it = new Intent(ctx, ViewUserProfileActivity_.class);
		it.putExtra(PARAM_USER, user);
		ctx.startActivity(it);
	}
	
	public static void showActivity(Context ctx, LocalUser localUser) {
		Intent it = new Intent(ctx, ViewUserProfileActivity_.class);
		User user = localUser.getUser();
		it.putExtra(PARAM_USER, user);
		ctx.startActivity(it);
	}
}
