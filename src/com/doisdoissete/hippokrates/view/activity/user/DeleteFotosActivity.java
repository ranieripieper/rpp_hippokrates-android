package com.doisdoissete.hippokrates.view.activity.user;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.view.custom.gridview.PagingGridView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.result.UserFeedResult;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.view.adapter.post.GridPostAdapter;
import com.doisdoissete.hippokrates.view.adapter.post.PostViewHolder;

@EActivity(R.layout.user_delete_fotos_activity)
public class DeleteFotosActivity extends UserProfileBaseActivity {

	public static final String PARAM_LIST_POSTS = "PARAM_LIST_POSTS";
	public static final String PARAM_PAGE = "PAGE";
	public static final String PARAM_HAS_MORE_ITEMS = "PARAM_HAS_MORE_ITEMS";
	
	private ArrayList<Post> posts;
	private GridPostAdapter gridAdapter;
	
	@ViewById(R.id.grid_feed)
	PagingGridView pagingGridView;
	
	@ViewById(R.id.img_delete_fotos)
	ImageView imgDeletarFotos;
	
	private int qtdeSelected = 0;
	
	@AfterViews
	void postCreate() {
		user = getIntent().getExtras().getParcelable(PARAM_USER);
		posts = getIntent().getExtras().getParcelableArrayList(PARAM_LIST_POSTS);
		page = getIntent().getExtras().getInt(PARAM_PAGE);
		boolean hasMoreItems = getIntent().getExtras().getBoolean(PARAM_HAS_MORE_ITEMS);
		
		pagingGridView.setPagingableListener(new PagingGridView.Pagingable() {
			@Override
			public void onLoadMoreItems() {
				callServicePosts(false);
			}
		});
		gridAdapter = new GridPostAdapter(getContext(), new ArrayList<Post>());
		pagingGridView.setAdapter(gridAdapter);
		pagingGridView.onFinishLoading(hasMoreItems, posts);
		gridAdapter.notifyDataSetChanged();
		pagingGridView.requestLayout();
		
		pagingGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View vi, int position, long id) {
				Post post = gridAdapter.getItem(position);
				post.setMark(!post.isMark());
				if (post.isMark()) {
					qtdeSelected++;
				} else {
					qtdeSelected--;
				}
				refreshMenu();
				gridAdapter.updateSelectHolder(post, (PostViewHolder) vi.getTag());
			}
		});
	}
	
	@Click(R.id.img_delete_fotos)
	void deleteClick() {
		showDefaultDialog(getString(R.string.sim), getString(R.string.nao), getString(R.string.msg_pergunta_delete_fotos_selecionadas), new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				executeDeleteFotos();
			}
		});
	}
	
	private void executeDeleteFotos() {
		List<Long> ids = new ArrayList<Long>();
		
		for (Post post : gridAdapter.getAllItems()) {
			if (post.isMark()) {
				ids.add(post.getId());
			}
		}
		callServiceDeletePosts(ids);
	}
	
	private void refreshMenu() {
		if (qtdeSelected <= 0) {
			imgDeletarFotos.setVisibility(View.GONE);
		} else {
			imgDeletarFotos.setVisibility(View.VISIBLE);
		}
		
	}
	
	@Override
	public void removedPosts(List<Long> ids) {
		page = 0;
		qtdeSelected = 0;
		refreshMenu();
		pagingGridView.setVisibility(View.GONE);
		callServicePosts(true);
	}
	
	@Override
	protected void clearList() {
		if (gridAdapter != null) {
			gridAdapter.removeAllItems();
		}
	}
	
	@Override
	protected void populateListView(UserFeedResult result) {		
		showWaitDialog(true);
		boolean loadMore = true;
		if (result != null) {			
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (page < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
			} else {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > result.getPost().size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			}
			
			pagingGridView.onFinishLoading(loadMore, result.getPost());
	    	
	    	gridAdapter.notifyDataSetChanged();
	    	pagingGridView.requestLayout();
	    	
		} else {
			loadMore = false;
			pagingGridView.onFinishLoading(loadMore, new ArrayList<Post>());
		}
		pagingGridView.setVisibility(View.VISIBLE);
		if (gridAdapter.getCount() <= 0) {
			callFinishActivity();
		}
		dismissWaitDialog();
	}
	
	private void callFinishActivity() {
		
	}

	public static void showActivity(Context ctx, List<Post> posts, int page, User user, boolean hasMoreItems) {
		Intent it = new Intent(ctx, DeleteFotosActivity_.class);
		it.putExtra(PARAM_PAGE, page);
		it.putExtra(PARAM_USER, user);
		it.putExtra(PARAM_HAS_MORE_ITEMS, hasMoreItems);
		
		it.putParcelableArrayListExtra(PARAM_LIST_POSTS, new ArrayList<Post>(posts));
		ctx.startActivity(it);
	}
}
