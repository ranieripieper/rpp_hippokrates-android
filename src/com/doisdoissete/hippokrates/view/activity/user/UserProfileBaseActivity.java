package com.doisdoissete.hippokrates.view.activity.user;

import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Build;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.result.UserFeedResult;
import com.doisdoissete.hippokrates.model.result.UserResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.UserFeedService;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewInterface;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewUtil;
import com.doisdoissete.hippokrates.view.adapter.post.GridBasePostAdapter;

public abstract class UserProfileBaseActivity extends HippBaseActivity implements PostViewInterface {

	protected static final String PARAM_USER = "PARAM_USER";
	
	protected int page = 0;
	protected User user;
	protected GridBasePostAdapter gridAdapter;
	protected UserResult userResult;
	private AsyncTaskService asyncServicePosts;
	
	protected void callServicePosts(boolean force) {
		if (!force && asyncServicePosts != null && (Status.PENDING.equals(asyncServicePosts.getStatus()) || Status.RUNNING.equals(asyncServicePosts.getStatus()))) {
			return;
		}
		page++;
		ObserverAsyncTask<UserFeedResult> observer = new ObserverAsyncTask<UserFeedResult>() {
			@Override
			public void onPreExecute() {
				if (page == 1) {
					showWaitDialog(false);
					clearList();
				}				
			}

			@Override
			public void onPostExecute(UserFeedResult result) {
				dismissWaitDialog();
				populateListView(result);				
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				page--;

			}

			@Override
			public void onError(Exception e) {
				showError(e);
				page--;
			}
		};
		
		if (page <= 0) {
			page = 1;
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		String url = Paths.USER_FEED.replace(UrlParameters.PARAM_USER_ID_URL, String.valueOf(user.getId()));
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		asyncServicePosts = sb.mappingInto(getContext(), UserFeedResult.class, new UserFeedService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			asyncServicePosts.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			asyncServicePosts.execute();
		}
	}
	
	protected void callServiceDeletePosts(final List<Long> ids) {
		PostViewUtil.callServiceDeletePosts(getContext(), this, this, ids);
	}
	
	protected abstract void clearList();
	
	protected abstract void populateListView(UserFeedResult result);	
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
}
