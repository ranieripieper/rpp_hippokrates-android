package com.doisdoissete.hippokrates.view.activity;


import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.HippUtil;
import com.doisdoissete.hippokrates.view.activity.signin.LoginSigninActivity;

public class SplashScreenActivity extends com.doisdoissete.android.util.ddsutil.view.SplashScreenActivity {

	@Override
	protected int getContentView() {
		return R.layout.splash_screen;
	}
	
	@Override
	protected void startMainActivity() {
		if (HippUtil.isLogged(SplashScreenActivity.this)) {
			HomeActivity.showActivity(SplashScreenActivity.this);
		} else {
			LoginSigninActivity.showActivity(this);
		}
		//SettingsAlertaActivity.showActivity(this);
		
		/*PushNotificationMessage message = new PushNotificationMessage();
		message.setMessage("teste mensagem 123 teste");
		message.setData(new PushNotificationMessageData());
		message.getData().setNotificableId(3L);
		message.getData().setNotificableType("friendship_circle");
		message.getData().setNotificationType("friendship_circle");
		message.getData().setMessage("teste mensagem 123");
		message.getData().setReceiverUserId(10L);
		message.getData().setSenderUserId(10L);
		message.getData().setSenderUserImageUrl("http://4.bp.blogspot.com/-7XI_WaQtPNE/VVS_6hLtCVI/AAAAAAAA2BI/AUPOTF3e1OU/s640/neuro.jpg");
		message.getData().setReceiverUserImageUrl("http://4.bp.blogspot.com/-7XI_WaQtPNE/VVS_6hLtCVI/AAAAAAAA2BI/AUPOTF3e1OU/s640/neuro.jpg");
		message.getData().setNotificableImageUrl("http://4.bp.blogspot.com/-7XI_WaQtPNE/VVS_6hLtCVI/AAAAAAAA2BI/AUPOTF3e1OU/s640/neuro.jpg");
		ReceiverPush.createNotification(this, message.getData());
		
		message = new PushNotificationMessage();
		message.setMessage("teste mensagem 456 teste");
		message.setData(new PushNotificationMessageData());
		message.getData().setNotificableId(4L);
		message.getData().setNotificableType("friendship_circle");
		message.getData().setNotificationType("friendship_circle");
		message.getData().setMessage("teste mensagem 123456");
		message.getData().setReceiverUserId(10L);
		message.getData().setSenderUserId(10L);
		message.getData().setSenderUserImageUrl("http://4.bp.blogspot.com/-7XI_WaQtPNE/VVS_6hLtCVI/AAAAAAAA2BI/AUPOTF3e1OU/s640/neuro.jpg");
		message.getData().setReceiverUserImageUrl("http://4.bp.blogspot.com/-7XI_WaQtPNE/VVS_6hLtCVI/AAAAAAAA2BI/AUPOTF3e1OU/s640/neuro.jpg");
		message.getData().setNotificableImageUrl("http://4.bp.blogspot.com/-7XI_WaQtPNE/VVS_6hLtCVI/AAAAAAAA2BI/AUPOTF3e1OU/s640/neuro.jpg");
		ReceiverPush.createNotification(this, message.getData());*/
		
		//ViewCircleActivity.showActivity(this, 292L);
		//AdicionarIndicarUsuarioActivity.showActivity(getContext(), circle.getId(), false);
		//ReceiverPush.createNotification(this, "teste", "teste msg");
		//StudentJoinCircleActivity.showActivity(this);
		//AdicionarIndicarUsuarioActivity.showActivity(this, 226l, true);
		//SettingsEditPerfilActivity.showActivity(this);
		//TakePictureActivity.showActivity(this);
		//EditImageActivity.showActivity(this, Constants.FILE_PATH_TESTE);
		//SettingsEditPerfilActivity.showActivity(this);
		//AdicionarIndicarUsuarioActivity.showActivity(this, 1L, true);
		//startActivity(new Intent(this, SearchActivity_.class));
		//HashtagFeedActivity.showActivity(this, "#aadeiusunde");
		//InviteFriendActivity.showActivity(this, 1);
		//CropImageActivity.showActivity(this, "/storage/sdcard0/DCIM/Camera/20140830_165205.jpg");
		//EditImageActivity.showActivity(this, "/storage/sdcard0/DCIM/Camera/20140830_165205.jpg");
		//PostFotoActivity.showActivity(this, "/data/data/com.doisdoissete.hippokrates/files/hippokrates/IMG_img_to_post20150707_123118.jpg", "", "");
		
/*		User user = new User();
		user.setEmail("xxx@xxx.com.br");
		user.setInviteCode("VMAH3636");
		user.setProfileType(ProfileUtil.PROFILE_TYPE_STUDENT);
		user.setFirstName("primeiro nome");
		user.setLastName("segundo nome");
		SignInActivity.showActivity(this, user);*/
		
		//SelectFromServerActivity.showActivity(this, 99, R.drawable.icn_sign_up_instituicao, R.string.txt_instituicao_ensino, Paths.COLLEGES);
	}
	
	protected int getSplashScreenTimeOut() {
		if (Constants.DEGUB) {
			return 1;
		}
		return 1500;
    	
    }
	

}
