package com.doisdoissete.hippokrates.view.activity.settings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.date.YearDatePickerFragment;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.PrivacyRoleData;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.model.result.MeResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.ProfileUtil;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseSelectImageActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromArrayActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromServerActivity;
import com.doisdoissete.hippokrates.view.activity.signin.ValidaCRMActivity;
import com.doisdoissete.hippokrates.view.custom.HipAppDdsSwitch;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@EActivity(R.layout.settings_edit_perfil)
public class SettingsEditPerfilActivity extends HippBaseSelectImageActivity {

	private static final int ESPECIALIDADE_REQUEST_CODE = 998;
	private static final int UF_FORMACAO_REQUEST_CODE = 997;
	private static final int INST_ENSINO_REQUEST_CODE = 996;
	private static final int VALIDAR_CRM_REQUEST_CODE = 995;
	
	@ViewById(R.id.edt_especialidade)
	EditTextPlus edtEspecialidade;
	
	@ViewById(R.id.edt_senha)
	EditTextPlus edtSenha;
	
	@ViewById(R.id.edt_conf_senha)
	EditTextPlus edtConfSenha;
	
	@ViewById(R.id.edt_inst_ensino)
	EditTextPlus edtInstEnsino;
	
	@ViewById(R.id.img_profile)
	ImageView imgProfile;
	
	long idEspecialidade = 0l;
	private String crm;
	private String crmState;
	private String profileType;
	
	//médico
	@ViewById(R.id.edt_nr_turma)
	EditTextPlus edtNrTurma;
	
	@ViewById(R.id.edt_nome_turma)
	EditTextPlus edtNomeTurma;
	
	@ViewById(R.id.edt_ocupacao)
	EditTextPlus edtOcupacao;
	
	@ViewById(R.id.edt_inst_residencia)
	EditTextPlus edtInstResidencia;
	
	@ViewById(R.id.switch_professor)
	HipAppDdsSwitch switchProfessor;
	
	//estudante
	@ViewById(R.id.edt_uf_formacao)
	EditTextPlus edtUfFormacao;
	
	@ViewById(R.id.edt_ano_formatura_2_grau)
	EditTextPlus edtAnoFormatura2;
	
	@ViewById(R.id.switch_se_formou)
	HipAppDdsSwitch switchSeFormou;
	
	//informaçoes privadas / publicas
	@ViewById(R.id.txt_priv_ano_formatura_2_grau)
	TextViewPlus txtPrivAnoFormatura2Grau;
	
	@ViewById(R.id.txt_priv_especialidade)
	TextViewPlus txtPrivEspecialidade;
	
	@ViewById(R.id.txt_priv_inst_ensino)
	TextViewPlus txtPrivInstEnsino;
	
	@ViewById(R.id.txt_priv_inst_residencia)
	TextViewPlus txtPrivInstResidencia;
	
	@ViewById(R.id.txt_priv_nome_turma)
	TextViewPlus txtPrivNomeTurma;
	
	@ViewById(R.id.txt_priv_nr_turma)
	TextViewPlus txtPrivNrTurma;
	
	@ViewById(R.id.txt_priv_ocupacao)
	TextViewPlus txtPrivOcupacao;
	
	@ViewById(R.id.txt_priv_uf_formacao)
	TextViewPlus txtPrivUfFormacao;
	
	private User user;
	
	//clicks
	@Click(R.id.txt_priv_ano_formatura_2_grau)
	void privAnoFormatura2Grau() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_ANO_FORMACAO);
	}
	
	@Click(R.id.txt_priv_especialidade)
	void privEsecialidade() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_ESPECIALIDADE);
	}
	
	@Click(R.id.txt_priv_inst_ensino)
	void privInstEnsino() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_INSTITUICAO_ENSINO);
	}
	
	@Click(R.id.txt_priv_inst_residencia)
	void privInstResidencia() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_INSTITUICAO_RESIDENCIA);
	}
	
	@Click(R.id.txt_priv_nome_turma)
	void privNomeTurma() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_NOME_TURMA);
	}
	
	@Click(R.id.txt_priv_nr_turma)
	void privNrTurma() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_NR_TURMA);
	}
	
	@Click(R.id.txt_priv_ocupacao)
	void privOcupacao() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_OCUPACAO);
	}
	
	@Click(R.id.txt_priv_uf_formacao)
	void privUfFormacao() {
		showDialogchangePrivacidade(PrivacyRoleData.FIELD_UF_FORMACAO);
	}	
	
	private void showDialogchangePrivacidade(final String field) {
		View.OnClickListener onclickInfoPublica = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changePrivacidade(field, false);
			}
		};
		View.OnClickListener onclickInfoPrivada = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changePrivacidade(field, true);
			}
		};
		showOptionsDialog(new int[] {R.string.informacao_publica, R.string.informacao_privada}, 
						new View.OnClickListener[] {onclickInfoPublica, onclickInfoPrivada});
	}
	
	private void changePrivacidade(final String field, final boolean infoPrivada) {
		
		//verifica se é igual a atual
		if ((infoPrivada && user.getPrivacyRoleData().isPrivate(field)) ||
				(!infoPrivada && !user.getPrivacyRoleData().isPrivate(field))) {
			return;
		}
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(BaseResult result) {
				dismissWaitDialog();
				if (result != null && result.isSuccess()) {				
					user.getPrivacyRoleData().updatePrivacy(field, infoPrivada);
					updateTextPrivacidade();					
				} else {
					showMessageAndFinish(R.string.msg_generic_error);					
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				blockScreen(true);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		params.add(UrlParameters.PARAM_KEY, PrivacyRoleData.getFieldToSendUpdate(field));
		params.add(UrlParameters.PARAM_VALUE, String.valueOf(infoPrivada));
		sb.setUrl(Paths.UPDATE_PRIVACY_INFO)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.PUT)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private int getTextPrivacy(PrivacyRoleData privacyRoleData, String field) {
		if (privacyRoleData.isPrivate(field)) {
			return R.string.informacao_privada;
		} else {
			return R.string.informacao_publica;
		}
	}
	
	private LocalUser localUser;
	
	@AfterViews
	void postCreate() {
		switchSeFormou.setChecked(false);
		switchProfessor.setChecked(false);
		localUser = getLocalUser();
		profileType = localUser.getProfileType();
		if (localUser != null) {
			if (ProfileUtil.PROFILE_TYPE_DOCTOR.equalsIgnoreCase(profileType) ||
					ProfileUtil.PROFILE_TYPE_TEACHER.equalsIgnoreCase(profileType)) {
				findViewById(R.id.layout_estudante).setVisibility(View.GONE);
				findViewById(R.id.layout_medico).setVisibility(View.VISIBLE);				
			} else if (ProfileUtil.PROFILE_TYPE_STUDENT.equalsIgnoreCase(profileType)) {
				findViewById(R.id.layout_estudante).setVisibility(View.VISIBLE);
				findViewById(R.id.layout_medico).setVisibility(View.GONE);
				switchSeFormou.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if (isChecked) {
							ValidaCRMActivity.showActivity(SettingsEditPerfilActivity.this, user, true, VALIDAR_CRM_REQUEST_CODE);
						}
					}
				});
			}			
			callGetInfo();
			showImage(localUser.getProfileImg());
		} else {
			finish();
		}
	}
	
	private void showImage(String url) {
		HippokratesApplication.imageLoaderUser.loadImage(url, new ImageLoadingListener() {
			@Override
			public void onLoadingStarted(String imageUri, View view) {
			}
			
			@Override
			public void onLoadingFailed(String imageUri, View view,
					FailReason failReason) {
			}
			
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				findViewById(R.id.img_camera_profile).setVisibility(View.GONE);
				imgProfile.setImageBitmap(loadedImage);
			}
			
			@Override
			public void onLoadingCancelled(String imageUri, View view) {
			}
		});
	}
	
	private void populateView(User user) {
		showImage(user.getProfileImageUrl());
		
		if (user.getEudcationData() != null && !TextUtils.isEmpty(user.getEudcationData().getUniversityInstitutionName())) {
			edtInstEnsino.setText(user.getEudcationData().getUniversityInstitutionName());
		}
		
		if (ProfileUtil.PROFILE_TYPE_STUDENT.equalsIgnoreCase(profileType)) {
			populateStudent(user);
		} else {
			populateDoctorTeacher(user);
		}
		
		updateTextPrivacidade();
		
	}
	
	private void updateTextPrivacidade() {
		txtPrivAnoFormatura2Grau.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_ANO_FORMACAO));
		txtPrivEspecialidade.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_ESPECIALIDADE));
		txtPrivInstEnsino.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_INSTITUICAO_ENSINO));
		txtPrivInstResidencia.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_INSTITUICAO_RESIDENCIA));
		txtPrivNomeTurma.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_NOME_TURMA));
		txtPrivNrTurma.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_NR_TURMA));
		txtPrivOcupacao.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_OCUPACAO));
		txtPrivUfFormacao.setText(getTextPrivacy(user.getPrivacyRoleData(), PrivacyRoleData.FIELD_UF_FORMACAO));
	}
	
	private void populateDoctorTeacher(User user) {
		if (user.getMedicalSpecialty() != null) {
			edtEspecialidade.setText(user.getMedicalSpecialty().getName());
			idEspecialidade = user.getMedicalSpecialty().getId();
		}
		if (user.getEudcationData() != null) {
			edtNomeTurma.setText(user.getEudcationData().getClassroomName());
			edtNrTurma.setText(user.getEudcationData().getClassroomNumber());
			edtInstResidencia.setText(user.getEudcationData().getResidencyProgramInstituitionName());
		}
		if (ProfileUtil.PROFILE_TYPE_TEACHER.equalsIgnoreCase(profileType)) {
			switchProfessor.setChecked(true);
		} else {
			switchProfessor.setChecked(false);
		}
		edtOcupacao.setText(user.getOccupation());
	}
	
	private void populateStudent(User user) {
		if (user.getEudcationData() != null) {
			edtUfFormacao.setText(user.getEudcationData().getCollegeDegreeState());
			edtAnoFormatura2.setText(user.getEudcationData().getCollegeDegreeYear());
		}
	}
	
	private void callGetInfo() {
		ObserverAsyncTask<MeResult> observer = new ObserverAsyncTask<MeResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(MeResult result) {
				dismissWaitDialog();
				if (result != null && result.getUser() != null) {	
					user = result.getUser();
					localUser.update(result.getUser());
					Preferences.setLocalUser(getContext(), localUser);
					populateView(result.getUser());
				} else {
					showMessageAndFinish(R.string.msg_error_buscar_info);					
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				blockScreen(true);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.ME_INFO.replace(UrlParameters.PARAM_USER_ID_URL, String.valueOf(localUser.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), MeResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void callUpdateService() {
		ObserverAsyncTask<MeResult> observer = new ObserverAsyncTask<MeResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(MeResult result) {
				dismissWaitDialog();
				if (result != null && result.isSuccess()) {	
					Preferences.setRefreshMenu(getContext(), true);
					localUser.update(result.getUser());
					Preferences.setLocalUser(getContext(), localUser);
					showMessage(R.string.msg_informacoes_atualizadas);
				} else {
					showMessageAndFinish(R.string.msg_error_buscar_info);					
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				blockScreen(true);
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_UNIVERSITY_INST_NAME, edtInstEnsino.getTextStr());
		
		boolean uploadFile = false;
		if (!TextUtils.isEmpty(filePath)) {
			params.add(UrlParameters.PARAM_USER_PROFILE_IMAGE, new FileSystemResource(filePath));
			uploadFile = true;
		}
		if (ProfileUtil.PROFILE_TYPE_DOCTOR.equals(profileType) || ProfileUtil.PROFILE_TYPE_TEACHER.equals(profileType)) {
			params.add(UrlParameters.PARAM_USER_MEDICAL_SPECIALTY, String.valueOf(idEspecialidade));
			params.add(UrlParameters.PARAM_USER_OCUPATION, edtOcupacao.getTextStr());
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NUMBER, edtNrTurma.getTextStr());
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NAME, edtNomeTurma.getTextStr());
			if (!TextUtils.isEmpty(crm)) {
				params.add(UrlParameters.PARAM_USER_CRM_NUMBER, crm);
			}
			if (!TextUtils.isEmpty(crmState)) {
				params.add(UrlParameters.PARAM_USER_CRM_STATE, crmState);
			}
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_RESIDENCY_INST_NAME, edtInstResidencia.getTextStr());
			if (switchProfessor.isChecked()) {
				params.add(UrlParameters.PARAM_USER_PROFILE_TYPE, ProfileUtil.PROFILE_TYPE_TEACHER);
			} else {
				params.add(UrlParameters.PARAM_USER_PROFILE_TYPE, ProfileUtil.PROFILE_TYPE_DOCTOR);
			}
		} else {
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_STATE, edtUfFormacao.getTextStr());
			params.add(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_YEAR, edtAnoFormatura2.getTextStr());
		}
		
		if (!TextUtils.isEmpty(edtSenha.getTextStr())) {
			params.add(UrlParameters.PARAM_USER_PASSWORD, edtSenha.getTextStr());
			params.add(UrlParameters.PARAM_USER_PASSWORD_CONF, edtConfSenha.getTextStr());
		}		
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.UPDATE_ME_INFO)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setUploadFile(uploadFile)
			.setHttpMethod(HttpMethod.PUT)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), MeResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private boolean isValid() {
		if (edtInstEnsino.isNotValid()) {
			return false;
		}
		
		//valida senha
		if (!TextUtils.isEmpty(edtSenha.getTextStr())) {
			if (!edtSenha.getTextStr().equals(edtConfSenha.getTextStr())) {
				return false;
			}
		}
		
		//valida dados médico
		if (ProfileUtil.PROFILE_TYPE_DOCTOR.equals(profileType)) {
			if (edtNrTurma.isNotValid() || edtNomeTurma.isNotValid() || idEspecialidade <= 0
					 || edtOcupacao.isNotValid()
					 || edtInstResidencia.isNotValid()) {
				return false;
			}
		}
		
		//valida dados estudante
		if (ProfileUtil.PROFILE_TYPE_STUDENT.equals(profileType)) {
			if (edtUfFormacao.isNotValid() || edtAnoFormatura2.isNotValid()) {
				return false;
			}
		}
		return true;
	}
	
	@Click(R.id.bt_salvar)
	void salvarClick() {
		if (isValid()) {
			callUpdateService();
		} else {
			showMessage(R.string.msg_validar_crm_preenchimento_campos);
		}		
	}
	
	@Click(R.id.edt_especialidade)
	void especialidadeClick() {
		SelectFromServerActivity.showActivity(this, ESPECIALIDADE_REQUEST_CODE, R.drawable.icn_sign_up_especialidade, R.string.txt_especialidade, Paths.MEDICAL_SPECIALITIES);
	}
	
	@Click(R.id.edt_uf_formacao)
	void ufFormacaoClick() {
		SelectFromArrayActivity.showActivity(this, UF_FORMACAO_REQUEST_CODE, R.drawable.icn_sign_up_cadastro_crm, R.string.txt_estado_crm,  R.array.estados);
	}
	
	@Click(R.id.edt_inst_ensino)
	void instituicaoEnsinoClick() {
		SelectFromServerActivity.showActivity(this, INST_ENSINO_REQUEST_CODE, R.drawable.icn_sign_up_instituicao, R.string.txt_instituicao_ensino, Paths.COLLEGES);
	}
	
	@Click(R.id.edt_ano_formatura_2_grau)
	void anoFormaturaClick() {
		YearDatePickerFragment newFragment = new YearDatePickerFragment(edtAnoFormatura2);
		newFragment.show(getSupportFragmentManager(), "yearPicker");
	}
	
	@Override
	protected void setImageSelect(Bitmap takenPictureData) {
		imgProfile.setImageBitmap(takenPictureData);
		findViewById(R.id.img_camera_profile).setVisibility(View.GONE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == ESPECIALIDADE_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtEspecialidade.setText(result);
			idEspecialidade = data.getExtras().getLong(SelectFromServerActivity.ID_RESULT);
		} else if (requestCode == UF_FORMACAO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtUfFormacao.setText(result);
		} else if (requestCode == INST_ENSINO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtInstEnsino.setText(result);
		} else if (requestCode == VALIDAR_CRM_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				boolean result = data.getExtras().getBoolean(ValidaCRMActivity.CRM_VALID);
				crm = data.getExtras().getString(ValidaCRMActivity.CRM_NR);
				crmState = data.getExtras().getString(ValidaCRMActivity.CRM_STATE);
				if (result) {
					updateViewStudentToDoctor();
				} else {
					switchSeFormou.setChecked(false);
				}
			} else {
				switchSeFormou.setChecked(false);
			}
			
		}
	}
	
	private void updateViewStudentToDoctor() {
		profileType = ProfileUtil.PROFILE_TYPE_DOCTOR;
		findViewById(R.id.layout_estudante).setVisibility(View.GONE);
		findViewById(R.id.layout_medico).setVisibility(View.VISIBLE);
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, SettingsEditPerfilActivity_.class));
	}
}
