package com.doisdoissete.hippokrates.view.activity.settings;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.signin.LoginSigninActivity;
import com.doisdoissete.hippokrates.view.activity.signin.TermoUsoActivity;

public class SettingsActivity extends HippBaseActivity {

	private ListView lstView;
	private List<String> menu = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_activity);
		lstView = (ListView)findViewById(R.id.list_view);
		
		menu.add(getString(R.string.settings_editar_perfil));
		menu.add(getString(R.string.settings_alertas));
		menu.add(getString(R.string.settings_termos_de_uso_politica_privacidade));
		menu.add(getString(R.string.settings_deletar_conta));
		
		ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, R.layout.settings_meu_item, menu);
		lstView.setAdapter(itemsAdapter);
		
		lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				if (position == 0) {
					SettingsEditPerfilActivity.showActivity(getContext());
				} else if (position == 1) {
					SettingsAlertaActivity.showActivity(getContext());
				} else if (position == 2) {
					TermoUsoActivity.showActivity(SettingsActivity.this, false);
				} else if (position == 3) {
					showDefaultDialog(R.string.sim, R.string.nao, R.string.deseja_deletar_conta, new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							executeDeleteAccount();
						}
					});
				}
			}
		});
	}
	
	private void executeDeleteAccount() {
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					Preferences.logout(getContext());
					showMessageFinishAllAndStartActivity(R.string.msg_conta_deletada, LoginSigninActivity.getIntentActivity(getContext()));
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();	
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.DELETE_ACCOUNT)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.DELETE)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
