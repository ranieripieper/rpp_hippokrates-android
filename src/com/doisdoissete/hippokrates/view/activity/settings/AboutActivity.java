package com.doisdoissete.hippokrates.view.activity.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class AboutActivity extends HippBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_activity);
		
		TextView txtVersion = (TextView)findViewById(R.id.txt_version);
		txtVersion.setText(getString(R.string.app_version, Constants.VERSION));
	}

	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, AboutActivity.class));
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}

	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
