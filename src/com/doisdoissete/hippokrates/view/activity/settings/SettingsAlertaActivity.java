package com.doisdoissete.hippokrates.view.activity.settings;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Preferences;
import com.doisdoissete.hippokrates.model.result.UserResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.background.UpdatePreferencesService;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.adapter.settings.AlertasAdapter;
import com.doisdoissete.hippokrates.view.adapter.settings.PreferencesToAdapter;

public class SettingsAlertaActivity extends HippBaseActivity {

	private ListView lstView;
	private AlertasAdapter adapter;
	private List<PreferencesToAdapter> preferences;
	private boolean updatePreferences = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settigns_alerta_activity);
		lstView = (ListView)findViewById(R.id.list_view);
		
		createPreferences();
		
		adapter = new AlertasAdapter(getContext(), preferences);
		lstView.setAdapter(adapter);
		
		callServiceGetPreferences();
	}
	
	private void callServiceGetPreferences() {
		ObserverAsyncTask<UserResult> observer = new ObserverAsyncTask<UserResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);			
			}

			@Override
			public void onPostExecute(UserResult result) {
				if (result != null && result.getUser() != null && result.getUser().getPreferences() != null) {
					preferences = result.getUser().getPreferences().getPreferencesToAdapter(getResources());
					adapter.refreshItens(preferences);
					adapter.notifyDataSetChanged();
					updatePreferences = true;
					lstView.setVisibility(View.VISIBLE);
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();		
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.USER_PREFERENCES)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), UserResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	public void finish() {
		if (updatePreferences) {
			UpdatePreferencesService.callService(getContext(), new ArrayList<PreferencesToAdapter>(preferences));
		}
		super.finish();
	}
	
	private void createPreferences() {
		preferences = Preferences.createPreferencesToAdapter(getResources());
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, SettingsAlertaActivity.class));
	}
}
