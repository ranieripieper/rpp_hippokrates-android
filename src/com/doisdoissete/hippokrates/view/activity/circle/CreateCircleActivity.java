package com.doisdoissete.hippokrates.view.activity.circle;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.ButtonPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.FriendshipCircleResult;
import com.doisdoissete.hippokrates.service.CreateCircleService;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseSelectImageActivity;

@EActivity(R.layout.circle_create)
public class CreateCircleActivity extends HippBaseSelectImageActivity {
	
	@ViewById(R.id.edt_nome_circulo)
	EditTextPlus edtNomeCirculo;
	@ViewById(R.id.img_circle)
	ImageView imgCirculo;
	@ViewById(R.id.bt_criar_circulo)
	ButtonPlus btCriar;
	@ViewById(R.id.bt_cancelar)
	ButtonPlus btCancelar;

	@Override
	protected void setImageSelect(Bitmap takenPictureData) {
		findViewById(R.id.img_camera_profile).setVisibility(View.GONE);
		imgCirculo.setImageBitmap(takenPictureData);
	}
	
    @Click(R.id.bt_criar_circulo)
    void criarCirculoClickClick() {
    	edtNomeCirculo.hiddenKeyboard(getContext());
        callService();
    }
    
    @Click(R.id.circle_foto)
    void circleFotoClick() {
    	edtNomeCirculo.hiddenKeyboard(getContext());
    	selectTakePicture(null);
    }
    
    
    @Click(R.id.bt_cancelar)
    void cancelarClick() {
        finish();
    }
    
    @Override
    protected void showKeyboard() {
    	super.showKeyboard();
    	btCriar.setVisibility(View.GONE);
    	btCancelar.setVisibility(View.GONE);    	
    }
    
    @Override
    protected void hideKeyboard() {
    	super.hideKeyboard();
    	btCriar.setVisibility(View.VISIBLE);
    	btCancelar.setVisibility(View.VISIBLE);
    }
    
	private void callService() {
		edtNomeCirculo.hiddenKeyboard(getContext());
		
		if (edtNomeCirculo.isNotValid()) {			
			showMessage(R.string.msg_preencha_campos_corretamente);
			return;			
		}
		
		ObserverAsyncTask<FriendshipCircleResult> observer = new ObserverAsyncTask<FriendshipCircleResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(FriendshipCircleResult result) {
				if (result != null && result.getCircle() != null) {
					showSucesso(result.getCircle());
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
	
		MultiValueMap<String, Object> params = BaseService.createParams(edtNomeCirculo);	
		
		boolean uploadFile = false;
		if (!TextUtils.isEmpty(filePath)) {
			params.add(UrlParameters.PARAM_CIRCLE_PROFILE_IMAGE, filePath);
			uploadFile = true;
		}
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.NEW_CIRCLE_TEACHER)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setUploadFile(uploadFile)
			.addHeaders("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), FriendshipCircleResult.class, new CreateCircleService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void showSucesso(final Circle result) {
		showMessage(R.string.circulo_criado_sucesso);
		blockScreen(true);
		LocalUser localUser = getLocalUser();
		localUser.addCircle(result);
		Preferences.setLocalUser(getContext(), localUser);
		Preferences.setRefreshMenu(getContext(), true);
		
		//chama serviço para atualizar menu
		//callRefreshMyCircles();
		new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					ViewCircleActivity.showActivity(getContext(), result);
					finish();
				}
			}, 1000);
		
	}
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, CreateCircleActivity_.class));
	}
	
	@Override
	protected boolean resizeBitmap() {
		return true;
	}
	
	@Override
	protected int maxHeightResize() {
		return Constants.MAX_HEIGHT_CIRCLE;
	}
	
	@Override
	protected int maxWidthResize() {
		
		return Constants.MAX_WIDTH_CIRCLE;
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
