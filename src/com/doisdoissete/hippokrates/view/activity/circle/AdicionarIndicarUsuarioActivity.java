package com.doisdoissete.hippokrates.view.activity.circle;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.model.result.SearchUsersResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.adapter.circle.MemberAdapter;

@EActivity(R.layout.circle_adicionar_indicar_usuario_activity)
public class AdicionarIndicarUsuarioActivity extends HippBaseActivity {

	private static final String PARAM_CIRCLE_ID = "PARAM_CIRCLE_ID";
	private static final String PARAM_ADICIONAR = "PARAM_ADICIONAR";
	
	private boolean adicionar = false;
	private Long circleId;

	private int page = 0;
	private MemberAdapter usersAdapter;
	
	@ViewById(R.id.edt_search)
	EditTextPlus edtSearch;
	@ViewById(R.id.listview)
	PagingListView listView;
	@ViewById(R.id.txt_nenhum_resultado)
	TextViewPlus txtNenhumResultado;
	
	private String mTxtSearch = null;
	
	private Circle circle;
	private LocalUser localUser;
	private boolean addUser = false;
	
	@AfterViews
	void postCreate() {
		adicionar = getIntent().getExtras().getBoolean(PARAM_ADICIONAR);
		circleId = getIntent().getExtras().getLong(PARAM_CIRCLE_ID);
		localUser = getLocalUser();
		circle = localUser.getCircle(circleId);
		
		edtSearch.openKeyboard(getContext());
		
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
        		edtSearch.hiddenKeyboard(getContext());
        		final User user = usersAdapter.getItem(position);
        		String msg = "";
        		if (adicionar) {
        			msg = getString(R.string.msg_deseja_adicionar_usuario_circulo, user.getFullName());
        		} else {
        			msg = getString(R.string.msg_deseja_indicar_usuario_circulo, user.getFullName());
        		}
        		showDefaultDialog(R.string.sim, R.string.nao, msg, new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						callAdicionarIndicarService(user);
					}
				});
        	}
		});
		
		edtSearch.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	        }

	        @Override
	        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	        }

	        @Override
	        public void onTextChanged(CharSequence s, int start, int before, int count) {
	        	refresh();
	        } 

	    });
	}
	
	@Override
	public void finish() {
		Intent data = new Intent();
		if (addUser) {
			setResult(Activity.RESULT_OK, data);
		} else {
			setResult(Activity.RESULT_CANCELED, data);
		}
		super.finish();
	}
	
    public void refresh() {
    	page = 0;
    	callService(edtSearch.getTextStr());
    }
    
    private void clearSearch() {
    	if (usersAdapter != null) {
			usersAdapter.removeAllItems();
		}
    }
    
    private void callAdicionarIndicarService(final User user) {

		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					if (adicionar) {
						showMessage(getString(R.string.msg_usuario_adicionado_circulo_user, user.getFullName()));
						if (circle != null) {
							circle.addTotalUsers();
							localUser.updateCircle(circle);
							Preferences.setLocalUser(getContext(), localUser);
							addUser = true;
						}
					} else {
						showMessage(getString(R.string.msg_usuario_indicado_circulo, user.getFullName()));
					}
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_USER_ID, String.valueOf(user.getId()));
		
		String url = Paths.CIRCLE_ADD_INVITE_MEMBER.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circleId));
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);

		AsyncTaskService asyncAddInd = sb.mappingInto(getContext(), SearchUsersResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			asyncAddInd.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			asyncAddInd.execute();
		}
	}
    
	private void callService(String txtSearch) {
		if ((!txtSearch.equals(mTxtSearch)) || (async != null && (async.isRunning() || async.isPending()))) {
			page = 0;
			if (async != null) {
				async.cancel(true);
			}
			
		}
		if (TextUtils.isEmpty(txtSearch)) {
			clearSearch();
			return;
		}
		page++;
		
		ObserverAsyncTask<SearchUsersResult> observer = new ObserverAsyncTask<SearchUsersResult>() {
			@Override
			public void onPreExecute() {
				if (page <= 1) {
					listView.setVisibility(View.GONE);
					showWaitDialog(false);
					clearSearch();				
				}
				txtNenhumResultado.setVisibility(View.GONE);
			}

			@Override
			public void onPostExecute(SearchUsersResult result) {
				populateUsers(result);
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		String url = Paths.SEARCH_USER_NO_STUDENT.replace(UrlParameters.PARAM_USER_NAME_URL, txtSearch);
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setCharset(Constants.CHARSET)
			.setParams(params);

		async = sb.mappingInto(getContext(), SearchUsersResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
		mTxtSearch = txtSearch;
	}
	
    private void populateUsers(SearchUsersResult result) {
    	if (usersAdapter == null) {
			listView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					callService(edtSearch.getTextStr());
				}
			});

			usersAdapter = new MemberAdapter(getContext(), new ArrayList<User>());
			listView.setAdapter(usersAdapter);
		}
		
		boolean loadMore = true;
		
		List<User> users = new ArrayList<User>();
		if (result == null) {
			loadMore = false;
		} else {
			users = result.getUsers();
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (page < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
			} else if (users != null) {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > users.size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			} else {
				loadMore = false;
			}				
		}
		
		listView.onFinishLoading(loadMore, users);
		
		usersAdapter.notifyDataSetChanged();
		listView.requestLayout();
		
		if (usersAdapter.getCount() <= 0) {
			txtNenhumResultado.setVisibility(View.VISIBLE);
		} else {
			txtNenhumResultado.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}
		
    }
    
    public static void showActivity(Context ctx, Long circleId, boolean adicionar) {
		Intent it = new Intent(ctx, AdicionarIndicarUsuarioActivity_.class);
		it.putExtra(PARAM_CIRCLE_ID, circleId);
		it.putExtra(PARAM_ADICIONAR, adicionar);
		ctx.startActivity(it);
	}	
    
	public static void showActivity(Activity ctx, Long circleId, boolean adicionar, int requestCode) {
		Intent it = new Intent(ctx, AdicionarIndicarUsuarioActivity_.class);
		it.putExtra(PARAM_CIRCLE_ID, circleId);
		it.putExtra(PARAM_ADICIONAR, adicionar);
		ctx.startActivityForResult(it, requestCode);
	}	
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
