package com.doisdoissete.hippokrates.view.activity.circle;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.model.result.FriendshipCircleResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

@EActivity(R.layout.circle_join_student_activity)
public class StudentJoinCircleActivity extends HippBaseActivity {

	@ViewById(R.id.edt_codigo_convite)
	EditTextPlus edtCodigo;
	
	
	@Click(R.id.bt_entrar)
	void entrarClick() {
		if (edtCodigo.isValid()) {
			callServiceJoin();
		} else {
			showMessage(R.string.msg_preencha_campos_corretamente);
		}
	}
	
	@Click(R.id.bt_cancelar)
	void cancelarClick() {
		finish();
	}
	
	private void callServiceJoin() {
		ObserverAsyncTask<FriendshipCircleResult> observer = new ObserverAsyncTask<FriendshipCircleResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);				
			}

			@Override
			public void onPostExecute(FriendshipCircleResult result) {
				if (result != null) {
					if (result.isSuccess()) {
						if (result.getCircle() != null) {
							LocalUser localUser = getLocalUser();
							localUser.addCircle(result.getCircle());
							Preferences.setLocalUser(getContext(), localUser);
							Preferences.setRefreshMenu(getContext(), true);
						} else {
							callRefreshMyCircles();
						}
						showMessageFinishAllAndStartActivity(R.string.msg_usuario_adicionado_circulo, ViewCircleActivity.getIntentActivity(getContext(), result.getCircle()));
						showMessageAndFinish(R.string.msg_usuario_adicionado_circulo);
					} else if (BaseResult.STATUS_403.equals(result.getStatusCode())) {
						showMessage(R.string.msg_usuario_ja_membro_circulo);
						
					} else {
						showMessage(R.string.msg_generic_error);
					}					
				} else {
					showMessage(R.string.msg_generic_error);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				e.printStackTrace();
				showError(e);
				dismissWaitDialog();
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.JOIN_CIRCLE.replace(UrlParameters.PARAM_CIRCLE_CODE_URL, String.valueOf(edtCodigo.getTextStr()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), FriendshipCircleResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	public static void showActivity(Context ctx) {
		Intent it = new Intent(ctx, StudentJoinCircleActivity_.class);
		ctx.startActivity(it);
	}
}
