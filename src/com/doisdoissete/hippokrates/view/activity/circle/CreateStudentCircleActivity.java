package com.doisdoissete.hippokrates.view.activity.circle;

import java.text.ParseException;
import java.util.Date;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.date.DatePickerFragment;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.FriendshipCircleResult;
import com.doisdoissete.hippokrates.service.CreateCircleService;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseSelectImageActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromServerActivity;

@EActivity(R.layout.circle_create_students)
public class CreateStudentCircleActivity extends HippBaseSelectImageActivity {
    
	private static final int INST_ENSINO_REQUEST_CODE = 996;
	
	@ViewById(R.id.edt_universidade)
	EditTextPlus edtUniversidade;
	@ViewById(R.id.edt_materia)
	EditTextPlus edtMateria;
	@ViewById(R.id.edt_data_de)
	EditTextPlus edtTurmaDe;
	@ViewById(R.id.edt_data_ate)
	EditTextPlus edtAte;
	@ViewById(R.id.edt_nome_circulo)
	EditTextPlus edtNomeCirculo;
	@ViewById(R.id.edt_numero_alunos)
	EditTextPlus edtNrAlunos;
	@ViewById(R.id.img_circle)
	ImageView imgCirculo;
	
	//sucesso
	@ViewById(R.id.img_circle_sucesso)
	ImageView imgCirculoSucesso;	
	@ViewById(R.id.txt_nome_circulo)
	TextViewPlus txtNomeCirculo;
	@ViewById(R.id.txt_universidade)
	TextViewPlus txtUniversidade;
	@ViewById(R.id.txt_codigo_acesso_para_circulo)
	TextViewPlus txtCodigoAcesso;
	@ViewById(R.id.txt_nr_alunos)
	TextViewPlus txtNrAlunos;

	@AfterViews
	void completeDebug() {
		if (Constants.DEGUB) {
			edtUniversidade.setText("Universidade A");
			edtMateria.setText("Matéria X");
			edtTurmaDe.setText("01/01/2015");
			edtAte.setText("10/10/2016");
			edtNomeCirculo.setText("Teste " + Math.random());
			edtNrAlunos.setText("50");
		}
		
	}
	
    @Click(R.id.edt_data_de)
    void edtDataDeClick() {
        DatePickerFragment.showTimePickerDialog(getSupportFragmentManager(), edtTurmaDe);
    }
    
    @Click(R.id.edt_data_ate)
    void edtDataAteClick() {
        DatePickerFragment.showTimePickerDialog(getSupportFragmentManager(), edtAte);
    }
    
    @Click(R.id.bt_convidar)
    void convidarClick() {
        callService();
    }
    
    @Click(R.id.bt_cancelar)
    void cancelarClick() {
        finish();
    }
    
    @Click(R.id.bt_ok)
    void okClick() {
        finish();
    }
    
    @Click(R.id.edt_universidade) 
    void univiersidadeClick() {
    	SelectFromServerActivity.showActivity(this, INST_ENSINO_REQUEST_CODE, R.drawable.icn_sign_up_instituicao, R.string.txt_instituicao_ensino, Paths.COLLEGES);
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == INST_ENSINO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtUniversidade.setText(result);
		}
	}
    
    @Override
    protected void setImageSelect(Bitmap takenPictureData) {
    	imgCirculo.setImageBitmap(takenPictureData);
    	imgCirculoSucesso.setImageBitmap(takenPictureData);
		findViewById(R.id.img_camera_profile).setVisibility(View.GONE);
		findViewById(R.id.img_camera_profile_sucesso).setVisibility(View.GONE);
    }
	
	private void callService() {
		edtUniversidade.hiddenKeyboard(getContext());
		edtAte.hiddenKeyboard(getContext());
		edtMateria.hiddenKeyboard(getContext());
		edtNomeCirculo.hiddenKeyboard(getContext());
		edtTurmaDe.hiddenKeyboard(getContext());
		edtNrAlunos.hiddenKeyboard(getContext());
		
		if (edtUniversidade.isNotValid() || edtAte.isNotValid() || edtMateria.isNotValid() || edtNomeCirculo.isNotValid() ||
				edtTurmaDe.isNotValid() || edtNrAlunos.isNotValid()) {			
			showMessage(R.string.msg_preencha_campos_corretamente);
			return;
			
		}
		ObserverAsyncTask<FriendshipCircleResult> observer = new ObserverAsyncTask<FriendshipCircleResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(FriendshipCircleResult result) {
				if (result != null && result.getCircle() != null) {
					showSucesso(result.getCircle());
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
	
		MultiValueMap<String, Object> params = BaseService.createParams(edtUniversidade, edtMateria, edtNomeCirculo, edtNrAlunos);
		try {
			Date dt = DateUtil.dfDiaMesAno.get().parse(edtTurmaDe.getTextStr());
			params.add(UrlParameters.PARAM_CIRCLE_START_DATE, DateUtil.dfDiaMesAno.get().format(dt));
		} catch (ParseException e1) {
			showError(e1);
			return;
		}
		
		try {
			Date dt = DateUtil.dfDiaMesAno.get().parse(edtAte.getTextStr());
			params.add(UrlParameters.PARAM_CIRCLE_END_DATE, DateUtil.dfDiaMesAno.get().format(dt));
		} catch (ParseException e1) {
			showError(e1);
			return;
		}
		
		
		boolean uploadFile = false;
		if (!TextUtils.isEmpty(filePath)) {
			params.add(UrlParameters.PARAM_CIRCLE_PROFILE_IMAGE, filePath);
			uploadFile = true;
		}
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.NEW_CIRCLE_TEACHER)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.addHeaders("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE)
			.setUploadFile(uploadFile)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), FriendshipCircleResult.class, new CreateCircleService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void showSucesso(Circle result) {
		txtCodigoAcesso.setText(result.getCode() != null ? result.getCode() : "-");
		txtNrAlunos.setText(getString(R.string.alunos_poderao_utilizar_codigo, result.getRemainingMembersCount()));
		txtNomeCirculo.setText(result.getName());
		txtUniversidade.setText(edtUniversidade.getTextStr());
		findViewById(R.id.layout_form).setVisibility(View.GONE);
		findViewById(R.id.layout_sucesso).setVisibility(View.VISIBLE);
		
		LocalUser localUser = getLocalUser();
		localUser.addCircle(result);
		Preferences.setLocalUser(getContext(), localUser);
		Preferences.setRefreshMenu(getContext(), true);
		
		//chama serviço para atualizar menu
		//callRefreshMyCircles();
	}

	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, CreateStudentCircleActivity_.class));
	}
	
	
	@Override
	protected boolean resizeBitmap() {
		return true;
	}
	
	@Override
	protected int maxHeightResize() {
		return Constants.MAX_HEIGHT_CIRCLE;
	}
	
	@Override
	protected int maxWidthResize() {
		
		return Constants.MAX_WIDTH_CIRCLE;
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}