package com.doisdoissete.hippokrates.view.activity.circle;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.PendingModeration;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.model.result.PendingModerationsResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.background.DeclinePendingUsersBackgroundService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.adapter.circle.PendingModerationAdapter;

@EActivity(R.layout.circle_pending_users_activity)
public class CirclePendingUsersActivity extends HippBaseActivity {

	private static final String PARAM_CIRCLE = "PARAM_CIRCLE";
	public static final String PARAM_RESULT_PENDING_USERS = "PARAM_RESULT_PENDING_USERS";
	public static final String PARAM_RESULT_PENDING_USERS_APPROVED = "PARAM_RESULT_PENDING_USERS_APPROVED";
	
	@ViewById(R.id.listview_participantes)
	PagingListView listView;
	@ViewById(R.id.txt_nr_pending)
	TextViewPlus txtNrPending;
	private int page = -1;
	private PendingModerationAdapter pendingAdapter;
	private Circle circle;
	private int totalUsersApproveDecline = 0;
	private int totalUsersApproved = 0;
	
	@AfterViews
	void postCreate() {		
		circle = getIntent().getExtras().getParcelable(PARAM_CIRCLE);
		
		txtNrPending.setText(getString(R.string.sugestoes_de_usuarios, circle.getPendingUsersCount()));
		
		callPendingService();
	}
	
	@Click(R.id.bt_salvar)
	void salvarClick() {
		callApproveDeclieService();
	}
	
	private void callPendingService() {
		ObserverAsyncTask<PendingModerationsResult> observer = new ObserverAsyncTask<PendingModerationsResult>() {
			@Override
			public void onPreExecute() {
				if (page == 1) {
					showWaitDialog(true);
				}				
			}

			@Override
			public void onPostExecute(PendingModerationsResult result) {
				if (result != null) {
					populateParticipantes(result);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				page--;
			}

			@Override
			public void onError(Exception e) {
				dismissWaitDialog();
				showError(e);
				page--;
			}
		};
		
		if (page <= 0) {
			page = 1;
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.CIRCLE_PENDING_REQUESTS.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), PendingModerationsResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void callApproveDeclieService() {
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);				
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					callDecline();
					finishActivity();
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				dismissWaitDialog();
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		boolean existApprove = false;
		for (PendingModeration pendingMod : pendingAdapter.getAllItems()) {
			if (pendingMod.isApprove()) {
				params.add(UrlParameters.PARAM_USERS_IDS, String.valueOf(pendingMod.getUser().getId()));
				existApprove = true;
				totalUsersApproveDecline++;
				totalUsersApproved++;
			}
		}
		
		if (!existApprove) {
			callDecline();
			finishActivity();
			return;
		}
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.APPROVE_PENDING_USERS.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void finishActivity() {
		Intent data = new Intent();
		data.putExtra(PARAM_RESULT_PENDING_USERS, totalUsersApproveDecline);
		data.putExtra(PARAM_RESULT_PENDING_USERS_APPROVED, totalUsersApproved);
		if (totalUsersApproveDecline > 0) {
			setResult(Activity.RESULT_OK, data);
		} else {
			setResult(Activity.RESULT_CANCELED, data);
		}
		if (totalUsersApproveDecline > 0) {
			showMessageAndFinish(R.string.msg_solicitacao_sera_processada);
		} else {
			finish();			
		}
	}
	
	private void callDecline() {
		List<Long> userIdsList = new ArrayList<Long>();
		for (PendingModeration pendingMod : pendingAdapter.getAllItems()) {
			if (pendingMod.isDecline()) {
				userIdsList.add(pendingMod.getUser().getId());
				totalUsersApproveDecline++;
			}
		}
		if (userIdsList.size() > 0) {
			DeclinePendingUsersBackgroundService.callService(getContext(), circle.getId(), userIdsList);
		}
	}
	
	private void populateParticipantes(PendingModerationsResult result) {
		if (pendingAdapter == null) {
			listView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					page++;
					callPendingService();
				}
			});

			pendingAdapter = new PendingModerationAdapter(getContext(), new ArrayList<PendingModeration>());
			listView.setAdapter(pendingAdapter);
		}
		
		boolean loadMore = true;
		
		List<PendingModeration> pendingModeration = new ArrayList<PendingModeration>();
		if (result == null) {
			loadMore = false;
		} else {
			pendingModeration = result.getLstPendingModeration();
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (page < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
			} else if (pendingModeration != null) {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > pendingModeration.size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			} else {
				loadMore = false;
			}				
		}
		
		listView.onFinishLoading(loadMore, pendingModeration);
		
		pendingAdapter.notifyDataSetChanged();
		listView.requestLayout();
		
		if (pendingAdapter.getCount() > 0) {
			listView.setVisibility(View.VISIBLE);
		}
	}
	
	
	public static void showActivity(Activity ctx, Circle circle, int requestCode) {		
		ctx.startActivityForResult(getIntentActivity(ctx, circle), requestCode);
	}	
	
	public static Intent getIntentActivity(Context ctx, Circle circle) {
		Intent it = new Intent(ctx, CirclePendingUsersActivity_.class);
		it.putExtra(PARAM_CIRCLE, circle);
		return it;
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
