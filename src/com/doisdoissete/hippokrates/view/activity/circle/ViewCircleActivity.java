package com.doisdoissete.hippokrates.view.activity.circle;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.model.result.FriendshipCircleResult;
import com.doisdoissete.hippokrates.model.result.MembersResult;
import com.doisdoissete.hippokrates.model.result.PostResult;
import com.doisdoissete.hippokrates.service.FeedService;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.UpdateCirclePhotoService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseSelectImageActivity;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewInterface;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewUtil;
import com.doisdoissete.hippokrates.view.activity.post.DetailPostActivity;
import com.doisdoissete.hippokrates.view.activity.user.ViewUserProfileActivity;
import com.doisdoissete.hippokrates.view.adapter.circle.MemberAdapter;
import com.doisdoissete.hippokrates.view.adapter.post.PostAdapter;

@EActivity(R.layout.circle_view_detail)
public class ViewCircleActivity extends HippBaseSelectImageActivity {

	private static final String PARAM_CIRCLE_ID = "PARAM_CIRCLE_ID";
	private static final String PARAM_CIRCLE = "PARAM_CIRCLE";
	
	@ViewById(R.id.img_profile)
	ImageView imgCircle;
	@ViewById(R.id.txt_circle_name)
	TextViewPlus txtName;

	@ViewById(R.id.loading_feed)
	View loadingFeed;
	@ViewById(R.id.listview_feed)
	PagingListView listViewFeed;
	@ViewById(R.id.layout_feed)
	View layoutFeed;
	@ViewById(R.id.txt_posts)
	TextViewPlus txtNrPosts;
	private PostAdapter feedAdapter;
	private int pageFeed = 1;
	@ViewById(R.id.txt_nenhum_post)
	TextViewPlus txtNenhumPost;
	
	@ViewById(R.id.loading_participantes)
	View loadingParticipantes;
	@ViewById(R.id.listview_participantes)
	PagingListView listViewParticipantes;
	@ViewById(R.id.layout_participantes)
	View layoutParticipantes;
	@ViewById(R.id.txt_participantes)
	TextViewPlus txtNrParticipantes;
	private int pageParticipantes = -1;
	private MemberAdapter memberAdapter;
	private Circle circle;
	private Long circleId;
	
	@ViewById(R.id.txt_codigo_acesso)
	TextViewPlus txtCodigoAcesso;
	@ViewById(R.id.txt_sugestoes_usuarios)
	TextViewPlus txtSugestoesUsuarios;
	
	private boolean admin = false;
	private boolean showMenu = false;
	private LocalUser localUser;
	
	@AfterViews
	void postCreate() {
		
		circle = getIntent().getExtras().getParcelable(PARAM_CIRCLE);
		circleId = getIntent().getExtras().getLong(PARAM_CIRCLE_ID);
		localUser = getLocalUser();
		
		if (circle != null) {
			updateBaseInfo();
			circleId = circle.getId();
		}

		listViewFeed.setOnTouchListener(new OnTouchListener() {
			GestureDetector gestureDetector = new GestureDetector(new SimpleOnGestureListener() {
            	@Override
            	public boolean onDoubleTap(MotionEvent e) {
            		int position = listViewFeed.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = feedAdapter.getItem(position);
            		PostViewUtil.likeDislikePost(getContext(), ViewCircleActivity.this, post, feedAdapter);
            		
            		return super.onDoubleTap(e);
            	}
            	
            	@Override
            	public boolean onSingleTapConfirmed(MotionEvent e) {
            		int position = listViewFeed.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = feedAdapter.getItem(position);
    				DetailPostActivity.showActivity(getContext(), post);
            		return super.onSingleTapConfirmed(e);
            	}
            });
			@Override
	         public boolean onTouch(View v, MotionEvent event) {
	                return gestureDetector.onTouchEvent(event);
	         }
		});
		
		callFeedService();
		
		if (admin) {
			listViewParticipantes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long id) {
					final User user = memberAdapter.getItem(position);
					if (admin) {
						showAdminUserOptions(user, localUser);
						return true;
					}
					
					return false;
				}
			});
		}
		
		listViewParticipantes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				final User user = memberAdapter.getItem(position);
				ViewUserProfileActivity.showActivity(getContext(), user);				
			}
		});
		
		invalidateOptionsMenu();
	}
	
	private void updateBaseInfo() {
		txtName.setText(circle.getName());
		if (circle.getProfileImageUrl() != null) {
			HippokratesApplication.imageLoaderCircle.displayImage(circle.getProfileImageUrl(), imgCircle);	
		}		
		
		if (circle.isEspecialidade()) {
			findViewById(R.id.layout_header_participantes).setVisibility(View.GONE);
		}
		
		if (localUser != null && localUser.isCircleAdmin(circle.getId())) {
			admin = true;
		}
		
		invalidateOptionsMenu();
	}
	
	private int REQUEST_CODE_APPROVE_DECLINE_USERS = 988;
	private int REQUEST_CODE_ADD_USERS = 987;
	
	@Click(R.id.txt_sugestoes_usuarios)
	void txtSugestoesClick() {
		CirclePendingUsersActivity.showActivity(ViewCircleActivity.this, circle, REQUEST_CODE_APPROVE_DECLINE_USERS);
	}
	
	@Click(R.id.img_plus_pendentes)
	void imgSugestoesClick() {
		CirclePendingUsersActivity.showActivity(ViewCircleActivity.this, circle, REQUEST_CODE_APPROVE_DECLINE_USERS);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_APPROVE_DECLINE_USERS) {
			if (resultCode == RESULT_OK) {
				if (memberAdapter == null) {
					memberAdapter = new MemberAdapter(getContext(), new ArrayList<User>());
					listViewParticipantes.setAdapter(memberAdapter);
				}
				listViewParticipantes.setVisibility(View.GONE);
				memberAdapter.removeAllItems();				
				
				pageParticipantes = -1;
				if (layoutParticipantes.getVisibility() == View.VISIBLE) {
					showParticipantes();
				}
				int totalUsersApproveDecline = data.getExtras().getInt(CirclePendingUsersActivity.PARAM_RESULT_PENDING_USERS);
				int totalUsersApproved = data.getExtras().getInt(CirclePendingUsersActivity.PARAM_RESULT_PENDING_USERS_APPROVED);
				int totalAtual = circle.getPendingUsersCount() - totalUsersApproveDecline;
				if (totalAtual < 0) {
					totalAtual = 0;
				}
				if (totalUsersApproved > 0) {
					circle.setTotalUsers(circle.getTotalUsers() + totalUsersApproved);
					updateTxtNrParticipantes();
				}
				circle.setPendingUsersCount(totalAtual);
				
				updateTxtSugestoes();
			}
		} else if(requestCode == REQUEST_CODE_ADD_USERS) {
			if (resultCode == RESULT_OK) {
				Circle circlePref = getLocalUser().getCircle(circle.getId());
				if (circlePref != null) {
					txtNrParticipantes.setText(getString(R.string.nr_participantes, circlePref.getTotalUsers()));
				}
				if (memberAdapter != null) {
					memberAdapter.removeAllItems();	
				}
				
				pageParticipantes = -1;
				if (layoutParticipantes.getVisibility() == View.VISIBLE) {
					showParticipantes();
				}
			}			
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	private void showAdminUserOptions(final User user, final LocalUser localUser) {
		if (!user.getId().equals(getLocalUser().getId())) {
			
			View.OnClickListener excluirClick = new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					showDefaultDialog(R.string.sim, R.string.nao, getString(R.string.msg_excluir_usuario, user.getFullName(), circle.getName()),
							new View.OnClickListener() {
								public void onClick(View v) {
									excluirUser(user);
								};
							});
				}
			};
			
			View.OnClickListener tornarAdminClick = new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					showDefaultDialog(R.string.sim, R.string.nao, getString(R.string.msg_tornar_usuario_admin, user.getFullName(), circle.getName()),
							new View.OnClickListener() {
								public void onClick(View v) {
									tornarAdminUser(user);
								};
							});
				}
			};
			
			showOptionsDialog(new int[] {R.string.excluir_usuario, R.string.tornar_admin_do_circulo}, new View.OnClickListener[] {excluirClick, tornarAdminClick});
		}
	}
	
	
	
	@Override
	protected void setImageSelect(final Bitmap takenPictureData) {
		ObserverAsyncTask<FriendshipCircleResult> observer = new ObserverAsyncTask<FriendshipCircleResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(FriendshipCircleResult result) {
				dismissWaitDialog();
				if (result != null && (result.isSuccess() || result.isUpdated())) {
					imgCircle.setImageBitmap(takenPictureData);
					showMessage(R.string.msg_imagem_alterada);
					LocalUser localUser = getLocalUser();
					localUser.updateCircle(result.getCircle());
					Preferences.setLocalUser(getContext(), localUser);
					Preferences.setRefreshMenu(getContext(),  true);
				} else {
					showMessage(R.string.msg_generic_error);
				}				
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {e.printStackTrace();
				dismissWaitDialog();
				showError(e);
			}
		};
		
		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();	
		
		boolean uploadFile = false;
		if (!TextUtils.isEmpty(filePath)) {
			params.add(UrlParameters.PARAM_CIRCLE_PROFILE_IMAGE, filePath);
			uploadFile = true;
		}
		params.add(UrlParameters.PARAM_CIRCLE_ID, String.valueOf(circle.getId()));
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.UPDATE_CIRCLE.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setUploadFile(uploadFile)
			.setHttpMethod(HttpMethod.PUT)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), FriendshipCircleResult.class, new UpdateCirclePhotoService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void tornarAdminUser(final User user) {
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					admin = false;
					invalidateOptionsMenu();
					showMessage(getString(R.string.msg_usuario_tornou_admin_sucesso, user.getFullName(), circle.getName()));
					circle.setAdminUserId(user.getId());
					LocalUser localUser = getLocalUser();
					localUser.updateCircle(circle);
					Preferences.setLocalUser(getContext(), localUser);
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_ACTION, UrlParameters.ACTION_TRANSFER_USER);
		params.add(UrlParameters.PARAM_USER_ID, String.valueOf(user.getId()));
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.MODERATE_CIRCLE_TRANSFER_ADMIN.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void excluirUser(final User user) {
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					showMessage(R.string.msg_usuario_excluido_sucesso);
					removeUserListView(user);
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		if (pageParticipantes <= 0) {
			pageParticipantes = 1;
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_USER_ID, String.valueOf(user.getId()));
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.MODERATE_CIRCLE_REMOVE_USER.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void removeUserListView(User user) {
		if (memberAdapter != null) {
			memberAdapter.removeItem(user);
		}		
		circle.reduceTotalUsers();
		LocalUser localUser = getLocalUser();
		localUser.updateCircle(circle);
		Preferences.setLocalUser(getContext(), localUser);
		updateTxtNrParticipantes();
	}
	
	@Click(R.id.layout_header_posts)
	void showPosts() {
		layoutParticipantes.setVisibility(View.GONE);
		layoutFeed.setVisibility(View.VISIBLE);
	}
	
	@Click(R.id.layout_header_participantes)
	void showParticipantes() {
		layoutFeed.setVisibility(View.GONE);
		layoutParticipantes.setVisibility(View.VISIBLE);
		
		if (pageParticipantes == -1) {
			pageParticipantes = 1;
			callParticipantesService();
		}
	}
	
	private void callParticipantesService() {
		ObserverAsyncTask<MembersResult> observer = new ObserverAsyncTask<MembersResult>() {
			@Override
			public void onPreExecute() {
				if (pageParticipantes == 1) {
					listViewParticipantes.setVisibility(View.GONE);
					loadingParticipantes.setVisibility(View.VISIBLE);
				}				
			}

			@Override
			public void onPostExecute(MembersResult result) {
				if (result != null) {
					populateParticipantes(result);
				} else {
					findViewById(R.id.txt_nenhum_participante).setVisibility(View.VISIBLE);
				}
				loadingParticipantes.setVisibility(View.GONE);
			}

			@Override
			public void onCancelled() {
				loadingParticipantes.setVisibility(View.GONE);
				pageParticipantes--;
			}

			@Override
			public void onError(Exception e) {
				loadingParticipantes.setVisibility(View.GONE);
				showError(e);
				pageParticipantes--;
			}
		};
		
		if (pageParticipantes <= 0) {
			pageParticipantes = 1;
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(pageParticipantes));
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.MEMBERS_CIRCLE.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), MembersResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void populateParticipantes(MembersResult result) {
		if (memberAdapter == null) {
			listViewParticipantes.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					pageParticipantes++;
					callParticipantesService();
				}
			});

			memberAdapter = new MemberAdapter(getContext(), new ArrayList<User>());
			listViewParticipantes.setAdapter(memberAdapter);
		}
		
		boolean loadMore = true;
		
		
		if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
			if (pageParticipantes < result.getMeta().getPagination().getTotalPages()) {
				loadMore = true;
			} else {
				loadMore = false;
			}
			if (result.getMeta().getPagination().getTotalCount() != null) {
				txtNrParticipantes.setText(getString(R.string.nr_participantes, result.getMeta().getPagination().getTotalCountToView()));
			}
		} else {
			if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > result.getMembers().size()) {
				loadMore = false;
	    	} else {
	    		loadMore = true;
	    	} 
		}
		   	
		listViewParticipantes.onFinishLoading(loadMore, result.getMembers());
		
		listViewParticipantes.setVisibility(View.VISIBLE);
		memberAdapter.notifyDataSetChanged();
    	listViewParticipantes.requestLayout();		
	}
	
	private void callFeedService() {
		ObserverAsyncTask<PostResult> observer = new ObserverAsyncTask<PostResult>() {
			@Override
			public void onPreExecute() {
				if (pageFeed == 1) {
					loadingFeed.setVisibility(View.VISIBLE);
				}				
			}

			@Override
			public void onPostExecute(PostResult result) {
				populateListView(result);
				loadingFeed.setVisibility(View.GONE);
				showMenu = true;
				invalidateOptionsMenu();
			}

			@Override
			public void onCancelled() {
				loadingFeed.setVisibility(View.GONE);
				pageFeed--;
			}

			@Override
			public void onError(Exception e) {
				boolean showMsg = false;
				if (e != null && e instanceof DdsUtilIOException) {
					DdsUtilIOException ex = (DdsUtilIOException)e;
					if (HttpStatus.NOT_FOUND.equals(ex.getHttpStatus())) {
						Preferences.setRefreshMenu(getContext(), true);
						if (TextUtils.isEmpty(e.getMessage())) {
							showMessageAndFinish(R.string.msg_generic_error);
						} else {
							showMessageAndFinish(e.getMessage());
						}	
						showMsg = true;
					}
				}
				if (!showMsg) {
					loadingFeed.setVisibility(View.GONE);
					showError(e);
					pageFeed--;	
				}
				
			}
		};
		
		if (pageFeed <= 0) {
			pageFeed = 1;
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(pageFeed));
		String url = "";
		if (circle != null && circle.isEspecialidade()) {
			url = Paths.FEED_ESPECIALIDADE.replace(UrlParameters.PARAM_ESPECIALIDADE_ID_URL, String.valueOf(circleId));
		} else {
			url = Paths.FEED_CIRCLE.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circleId));
		}
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), PostResult.class, new FeedService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void populateListView(PostResult result) {
		if (feedAdapter == null) {
			listViewFeed.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					pageFeed++;
					callFeedService();
				}
			});

			feedAdapter = new PostAdapter(getContext(), new ArrayList<Post>(), true, false, this, new PostViewInterface() {
				
				@Override
				public void removedPosts(List<Long> ids) {
					if (ids != null && ids.size() > 0) {
						LocalPost.deletePosts(ids, feedAdapter);
						feedAdapter.notifyDataSetChanged();
					}
				}
			});
			listViewFeed.setAdapter(feedAdapter);
		}
		
		boolean loadMore = true;

		int totalPosts = 0;
		List<Post> posts = new ArrayList<Post>();
		if (result == null) {
			loadMore = false;
		} else {
			posts = result.getPosts();
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (pageFeed < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
				totalPosts = result.getMeta().getPagination().getTotalCount();
			} else if (posts != null) {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > posts.size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			} else {
				loadMore = false;
			}				
		}
		
		listViewFeed.onFinishLoading(loadMore, posts);
		
		feedAdapter.notifyDataSetChanged();
		listViewFeed.requestLayout();
		
		if (feedAdapter.getCount() <= 0) {
			txtNenhumPost.setVisibility(View.VISIBLE);
		} else {
			txtNenhumPost.setVisibility(View.GONE);
			listViewFeed.setVisibility(View.VISIBLE);
		}	
		
		//atualiza informações do círculo
		if (result.getCircle() != null) {
			circle = result.getCircle();
			
			if (admin) { 
				LocalUser localUser = getLocalUser();
				localUser.updateCircle(circle);
				Preferences.setLocalUser(getContext(), localUser);
				if(circle != null && !TextUtils.isEmpty(circle.getCode())) {
					//Layout codigo
					txtCodigoAcesso.setText(getString(R.string.codigo_acesso, circle.getCode()));
					findViewById(R.id.layout_code).setVisibility(View.VISIBLE);
				}
				
				updateTxtSugestoes();
			}
		}
		updateBaseInfo();
		updateTxtNrParticipantes();
		if (circle.getTotalPosts() == null) {
			txtNrPosts.setText(getString(R.string.nr_posts, totalPosts));
		} else {
			txtNrPosts.setText(getString(R.string.nr_posts, circle.getTotalPosts()));
		}
	}
	
	private void updateTxtNrParticipantes() {
		txtNrParticipantes.setText(getString(R.string.nr_participantes, circle.getTotalUsers()));	
	}
	
	private void updateTxtSugestoes() {
		if (admin && circle.getPendingUsersCount() != null && circle.getPendingUsersCount().intValue() > 0 && circle.isCommon()) {
			txtSugestoesUsuarios.setText(getString(R.string.sugestoes_de_usuarios, circle.getPendingUsersCount()));
			findViewById(R.id.layout_usuarios_pendentes).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.layout_usuarios_pendentes).setVisibility(View.GONE);
		}
	}
	
	private void showMenuDefaultMenuCircle() {
		View.OnClickListener onClickIndicarAmigo = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AdicionarIndicarUsuarioActivity.showActivity(getContext(), circle.getId(), false);
			}
		};
		View.OnClickListener onClickDeixar = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialogDeixarCircle();
			}
		};
		if (Constants.STUDENT_CIRCLE_TYPE.equalsIgnoreCase(circle.getCircleType())) {
			showOptionsDialog(new int[] { R.string.deixar_circulo }, new View.OnClickListener[] { onClickIndicarAmigo });	
		} else {
			showOptionsDialog(new int[] {	R.string.deixar_circulo,
											R.string.indicar_amigo_circulo
											
										}, 
							  new View.OnClickListener[] {onClickDeixar, onClickIndicarAmigo});
		}
	}
	
	private void showMenuAdminCircle() {
		View.OnClickListener onClickEditar = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectTakePicture(null);
			}
		};
		View.OnClickListener onClickAddAmigo = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AdicionarIndicarUsuarioActivity.showActivity(ViewCircleActivity.this, circle.getId(), true, REQUEST_CODE_ADD_USERS);
			}
		};
		View.OnClickListener onClickDelete = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialogDeleteCircle();
			}
		};
		View.OnClickListener onClickDeixar = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialogDeixarCircle();
			}
		};
		
		if (Constants.STUDENT_CIRCLE_TYPE.equalsIgnoreCase(circle.getCircleType())) {
			showOptionsDialog(new int[] {	R.string.editar_imagem_circulo,
											R.string.deletar_circulo,
											R.string.deixar_circulo
										}, 
							  new View.OnClickListener[] {onClickEditar, onClickDelete, onClickDeixar});
		} else {
			showOptionsDialog(new int[] {	R.string.editar_imagem_circulo,
											R.string.adicionar_amigo_circulo,
											R.string.deletar_circulo,
											R.string.deixar_circulo
										}, 
							 new View.OnClickListener[] {onClickEditar, onClickAddAmigo, onClickDelete, onClickDeixar});
		}
	}
	
	private void showDialogDeleteCircle() {
		showDefaultDialog(R.string.sim, R.string.nao, getString(R.string.msg_delete_circulo, circle.getName()), new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				executeDeleteCirculo();
			}
		});
	}
	
	private void showDialogDeixarCircle() {
		showDefaultDialog(R.string.sim, R.string.nao, getString(R.string.msg_deseja_deixar_circulo, circle.getName()), new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				executeDeixarCirculo();
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		List<LocalPost> deletedLocalPost = LocalPost.getDeletedLocalPost();
		if (deletedLocalPost != null && feedAdapter != null) {
			for (LocalPost localPost : deletedLocalPost) {
				feedAdapter.removePost(localPost.getPostId());
			}
		}
		
		if (feedAdapter != null && feedAdapter.getCount() > 0) {
			feedAdapter.notifyDataSetChanged();
		}
	}
	
	private void executeDeixarCirculo() {
		ObserverAsyncTask<PostResult> observer = new ObserverAsyncTask<PostResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(PostResult result) {
				dismissWaitDialog();
				if (result != null && result.isSuccess()) {
					blockScreen(true);
					showMessage(getString(R.string.msg_voce_nao_faz_parte_do_circulo, circle.getName()));
					
					LocalUser localUser = getLocalUser();
					localUser.removeCircle(circle);
					Preferences.setLocalUser(getContext(), localUser);
					Preferences.setRefreshMenu(getContext(), true);
					
					//chama serviço para atualizar menu
					callRefreshMyCircles();
					
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							finish();
						}
					}, 1000);
				} else {					
					showMessage(R.string.msg_generic_error);
				}				
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.DEIXAR_CIRCLE.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.DELETE)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), PostResult.class, new FeedService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void executeDeleteCirculo() {
		ObserverAsyncTask<PostResult> observer = new ObserverAsyncTask<PostResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);			
			}

			@Override
			public void onPostExecute(PostResult result) {
				dismissWaitDialog();
				if (result != null && result.isSuccess()) {
					blockScreen(true);
					showMessage(R.string.msg_circulo_excluido_sucesso);
					
					LocalUser localUser = getLocalUser();
					localUser.removeCircle(circle);
					Preferences.setLocalUser(getContext(), localUser);
					Preferences.setRefreshMenu(getContext(), true);
					
					//chama serviço para atualizar menu
					callRefreshMyCircles();
					
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							finish();
						}
					}, 1000);
				} else {					
					showMessage(R.string.msg_generic_error);
				}
				
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		String url = Paths.DELETE_CIRCLE.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circle.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.DELETE)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(getContext(), PostResult.class, new FeedService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (showMenu) {
			if (admin) {
				getMenuInflater().inflate(R.menu.admin_circle, menu);
			} else if(circle != null && !circle.isEspecialidade() && !circle.isPublic()){
				getMenuInflater().inflate(R.menu.default_circle, menu);
			}
		}
		return super.onCreateOptionsMenu(menu);
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_admin_circle) {
        	showMenuAdminCircle();
            return true;
        } else if (id == R.id.action_mais_opcoes && circle != null && !circle.isEspecialidade()) {
        	showMenuDefaultMenuCircle();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
	
	public static void showActivity(Context ctx, Circle circle) {		
		ctx.startActivity(getIntentActivity(ctx, circle));
	}
	
	public static void showActivity(Context ctx, Long circleId) {		
		ctx.startActivity(getIntentActivity(ctx, circleId));
	}	
	
	public static Intent getIntentActivity(Context ctx, Circle circle) {
		Intent it = new Intent(ctx, ViewCircleActivity_.class);
		it.putExtra(PARAM_CIRCLE, circle);
		return it;
	}
	
	public static Intent getIntentActivity(Context ctx, Long circleId) {
		Intent it = new Intent(ctx, ViewCircleActivity_.class);
		it.putExtra(PARAM_CIRCLE_ID, circleId);
		return it;
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	protected boolean resizeBitmap() {
		return true;
	}
	
	protected boolean dismissWaitDialogResize() {
		return false;
	}
	
	@Override
	protected int maxHeightResize() {
		return Constants.MAX_HEIGHT_CIRCLE;
	}
	
	@Override
	protected int maxWidthResize() {
		return Constants.MAX_WIDTH_CIRCLE;
	}
}
