package com.doisdoissete.hippokrates.view.activity.askforinvite;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.ProfileUtil;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromArrayActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromServerActivity;

public class AskForInviteActivity extends HippBaseActivity {

	private static final int ESTADO_REQUEST_CODE = 999;
	private static final int INST_ENSINO_REQUEST_CODE = 998;
	private String genero;
	
	private EditTextPlus edtPrimeiroNome;
	private EditTextPlus edtUltimoSobrenome;
	private EditTextPlus edtEmail;
	private EditTextPlus edtCrm;
	private EditTextPlus edtInstituicao;
	private EditTextPlus edtEstadoCrm;
	private ImageView btnMale;
	private ImageView btnFemale;
	private RadioGroup radioMedicoEstudante;
	private View btCadastrar;
	private Long idInstituicao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ask_for_invite_activity);
		
		initComponents();
	}
	
	private void initComponents() {
		btCadastrar = findViewById(R.id.bt_cadastrar);
		edtPrimeiroNome = (EditTextPlus)findViewById(R.id.edt_primeiro_nome);
		edtUltimoSobrenome = (EditTextPlus)findViewById(R.id.edt_ultimo_sobrenome);
		edtEmail = (EditTextPlus)findViewById(R.id.edt_email);
		edtCrm = (EditTextPlus)findViewById(R.id.edt_estado_crm);
		edtInstituicao = (EditTextPlus)findViewById(R.id.edt_universidade);
		edtEstadoCrm = (EditTextPlus)findViewById(R.id.edt_estado_crm);
		btnMale = (ImageView)findViewById(R.id.btn_male);
		btnFemale = (ImageView)findViewById(R.id.btn_female);
		radioMedicoEstudante = (RadioGroup)findViewById(R.id.layout_medico_estudante);
		
		edtEstadoCrm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SelectFromArrayActivity.showActivity(AskForInviteActivity.this, ESTADO_REQUEST_CODE, R.drawable.icn_sign_up_cadastro_crm, R.string.txt_estado_crm,  R.array.estados);
			}
		});
		
		edtInstituicao.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SelectFromServerActivity.showActivity(AskForInviteActivity.this, INST_ENSINO_REQUEST_CODE, R.drawable.icn_sign_up_instituicao, R.string.txt_instituicao_ensino, Paths.COLLEGES);
			}
		});
	}
	
	// clicks
	public void btnGeneroClick(View v) {
		if (MALE_TAG.equals(v.getTag())) {
			genero = Constants.MALE;
		} else if (FEMALE_TAG.equals(v.getTag())) {
			genero = Constants.FEMALE;
		}
		adjustIconesGenero();
	}
	
	public void btnCadastrar(View v) {
		if (formIsValid()) {
			callService();
		} else {
			showMessage(R.string.msg_ask_for_invite_erro_preenchimento_campos);
		}
	}
	
	public void radioMedicoEstudanteClick(View v) {
		int id = radioMedicoEstudante.getCheckedRadioButtonId();
		if (id != -1) {
		    if (id == R.id.radio_estudante) {
		        findViewById(R.id.layout_medico).setVisibility(View.GONE);
		        findViewById(R.id.layout_estudante).setVisibility(View.VISIBLE);
		    } else if (id == R.id.radio_medico) {
		    	findViewById(R.id.layout_medico).setVisibility(View.VISIBLE);
		        findViewById(R.id.layout_estudante).setVisibility(View.GONE);
		    }
		}
	}
	
	//m�todos privados
	
	private void callService() {
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
			}

			@Override
			public void onPostExecute(BaseResult result) {
				dismissWaitDialog();
				if (result != null && result.isSuccess()) {
					dismissWaitDialog();
					findViewById(R.id.cadsatro_efetuado).setVisibility(View.VISIBLE);
					findViewById(R.id.layout_content).setVisibility(View.GONE);
					findViewById(R.id.bt_cadastrar).setVisibility(View.GONE);
				} else {
					showError(null);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_USER_FIRST_NAME, edtPrimeiroNome.getTextStr());
		params.add(UrlParameters.PARAM_USER_LAST_NAME, edtUltimoSobrenome.getTextStr());
		params.add(UrlParameters.PARAM_USER_GENDER, genero);
		params.add(UrlParameters.PARAM_USER_EMAIL, edtEmail.getTextStr());
		if (Constants.DEGUB) {
			params.add(UrlParameters.PARAM_TO_EMAIL, Constants.EMAIL_DEBUG);
		}
		int id = radioMedicoEstudante.getCheckedRadioButtonId();
		if (id != -1) {
		    if (id == R.id.radio_estudante) {
				params.add(UrlParameters.PARAM_USER_PROFILE_TYPE, ProfileUtil.PROFILE_TYPE_STUDENT);
				params.add(UrlParameters.PARAM_USER_UNIVERSITY_INSTITUTION_NAME, edtInstituicao.getTextStr());
		    } else if (id == R.id.radio_medico) {
		    	params.add(UrlParameters.PARAM_USER_PROFILE_TYPE, ProfileUtil.PROFILE_TYPE_DOCTOR);
		    	params.add(UrlParameters.PARAM_USER_CRM_NUMBER, edtCrm.getTextStr());
		    	params.add(UrlParameters.PARAM_USER_CRM_STATE, edtEstadoCrm.getTextStr());
		    }
		} else {
			showMessage(R.string.msg_ask_for_invite_erro_preenchimento_campos);
		}
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Paths.ASK_FOR_INVITE)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private boolean formIsValid() {
		if (edtPrimeiroNome.isNotValid() || edtUltimoSobrenome.isNotValid() || edtEmail.isNotValid() || genero == null) {
			return false;
		}
		int id = radioMedicoEstudante.getCheckedRadioButtonId();
	    if (id == R.id.radio_estudante){
	        //valida dados dos estudantes
	    	if (edtInstituicao.isNotValid()) {
	    		return false;
	    	}
	    } else if (id == R.id.radio_medico){
	    	//valida dados dos m�dicos
	    	if (edtCrm.isNotValid() || edtEstadoCrm.isNotValid()) {
	    		return false;
	    	}
	    } else {
	    	return false;
		}
		
		return true;
	}
	
	private void adjustIconesGenero() {
		if (Constants.FEMALE.equals(genero)) {
			btnFemale.setImageResource(R.drawable.btn_sign_up_female_on);
			btnMale.setImageResource(R.drawable.btn_sign_up_male_off);
		} else if (Constants.MALE.equals(genero)) {
			btnFemale.setImageResource(R.drawable.btn_sign_up_female_off);
			btnMale.setImageResource(R.drawable.btn_sign_up_male_on);
		}
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == ESTADO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromArrayActivity.TEXT_RESULT);
			edtEstadoCrm.setText(result);
		} else if (requestCode == INST_ENSINO_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtInstituicao.setText(result);
			idInstituicao = data.getExtras().getLong(SelectFromServerActivity.ID_RESULT);
		}
	}
	
	@Override
	protected void showKeyboard() {
		super.showKeyboard();
		if (btCadastrar != null) {
			btCadastrar.setVisibility(View.VISIBLE);
		}				
	}
	
	@Override
	protected void hideKeyboard() {
		super.hideKeyboard();
		if (btCadastrar != null) {
			btCadastrar.setVisibility(View.GONE);
		}
	}

	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	public static final void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, AskForInviteActivity.class));
	}


}
