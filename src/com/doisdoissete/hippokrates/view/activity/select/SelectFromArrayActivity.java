package com.doisdoissete.hippokrates.view.activity.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class SelectFromArrayActivity extends HippBaseActivity {

	public static final String PARAM_ICON = "PARAM_ICON";
	public static final String PARAM_HINT = "PARAM_HINT";
	public static final String PARAM_RES_ARRAY = "PARAM_RES_ARRAY";
	public static final String TEXT_RESULT = "TEXT_RESULT";
	
	private EditText edtInputUser;
	private int icon;
	private int hint;
	private int array;
	private ListView lstView;
	private ArrayAdapter arrayAdapter;
	private ArrayList<String> allItems = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_array_activity);
		
		icon = getIntent().getExtras().getInt(PARAM_ICON);
		hint = getIntent().getExtras().getInt(PARAM_HINT);
		array = getIntent().getExtras().getInt(PARAM_RES_ARRAY);
		
		edtInputUser = (EditText)findViewById(R.id.edt_input_user);
		edtInputUser.setHint(hint);
		edtInputUser.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
	
		lstView = (ListView)findViewById(R.id.list_view);
		
		ArrayList<String> initArray = new ArrayList<String>();
		allItems.addAll(Arrays.asList(getResources().getStringArray(array)));    
		initArray.addAll(Arrays.asList(getResources().getStringArray(array)));
		
		arrayAdapter = new ArrayAdapter<String>(
	            this,
	            R.layout.simple_list_item, initArray);
		lstView.setAdapter(arrayAdapter); 
		
		edtInputUser.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	        }

	        @Override
	        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	        }

	        @Override
	        public void onTextChanged(CharSequence s, int start, int before, int count) {
	        	refreshListView();
	        } 

	    });
		
		lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent result = new Intent();
				result.putExtra(TEXT_RESULT, (String)arrayAdapter.getItem(position));
				setResult(Activity.RESULT_OK, result);
				finish();
			}
		});
	}
	
	private void refreshListView() {
		synchronized (arrayAdapter) {
			String txt = edtInputUser.getText().toString();
			if (arrayAdapter != null) {
				
				List<String> newItems = new ArrayList<String>();
				if (TextUtils.isEmpty(txt)) {
					newItems.addAll(Arrays.asList(getResources().getStringArray(array)));
				} else {
					for (String tmp : allItems) {
						if (tmp.toUpperCase().indexOf(txt.toUpperCase()) == 0) {
							newItems.add(tmp);
						}
					}
				}
				arrayAdapter.clear();
				arrayAdapter.addAll(newItems);
				arrayAdapter.notifyDataSetChanged();
			}
		}
		
		
	}
	
	public static final void showActivity(Activity ctx, int requestCode, int icEdtText, int hintEdtText, int array) {
		Intent it = new Intent(ctx, SelectFromArrayActivity.class);
		it.putExtra(PARAM_ICON, icEdtText);
		it.putExtra(PARAM_HINT, hintEdtText);
		it.putExtra(PARAM_RES_ARRAY, array);
		ctx.startActivityForResult(it, requestCode);
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
