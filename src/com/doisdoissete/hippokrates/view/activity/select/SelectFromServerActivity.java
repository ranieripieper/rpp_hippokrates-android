package com.doisdoissete.hippokrates.view.activity.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.doisdoissete.hippokrates.model.result.SelectServerListResult;
import com.doisdoissete.hippokrates.service.CollegesService;
import com.doisdoissete.hippokrates.service.MedicalSpecialitiesService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class SelectFromServerActivity extends HippBaseActivity {

	public static final String PARAM_ICON = "PARAM_ICON";
	public static final String PARAM_HINT = "PARAM_HINT";
	public static final String PARAM_URL_SEARCH = "PARAM_URL_SEARCH";
	public static final String TEXT_RESULT = "TEXT_RESULT";
	public static final String ID_RESULT = "ID_RESULT";
	
	private EditText edtInputUser;
	private int icon;
	private int hint;
	private String urlSearch;
	private ListView lstView;
	private ArrayAdapter<IdNameDescModel> arrayAdapter;

	private boolean findOnlyStartWith = false;
	
	private List<IdNameDescModel> resultServer = new ArrayList<IdNameDescModel>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_server_activity);
		
		icon = getIntent().getExtras().getInt(PARAM_ICON);
		hint = getIntent().getExtras().getInt(PARAM_HINT);
		urlSearch = getIntent().getExtras().getString(PARAM_URL_SEARCH);
		edtInputUser = (EditText)findViewById(R.id.edt_input_user);
		edtInputUser.setHint(hint);
		edtInputUser.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
	
		lstView = (ListView)findViewById(R.id.list_view);
		arrayAdapter = new ArrayAdapter<IdNameDescModel>( this, R.layout.simple_list_item, new ArrayList<IdNameDescModel>());
		lstView.setAdapter(arrayAdapter); 
		
		edtInputUser.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	        }

	        @Override
	        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	        }

	        @Override
	        public void onTextChanged(CharSequence s, int start, int before, int count) {
	        	refreshListView();
	        } 

	    });
		
		lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent result = new Intent();
				IdNameDescModel selectItem = (IdNameDescModel)arrayAdapter.getItem(position);
				result.putExtra(TEXT_RESULT, selectItem.getName());
				result.putExtra(ID_RESULT, selectItem.getId());
				setResult(Activity.RESULT_OK, result);
				finish();
			}
		});
		
		callService();
	}
	
	private void refreshListView() {
		synchronized (arrayAdapter) {
			String txt = edtInputUser.getText().toString();
			if (arrayAdapter != null) {
				
				List<IdNameDescModel> newItems = new ArrayList<IdNameDescModel>();
				if (TextUtils.isEmpty(txt)) {
					newItems.addAll(resultServer);
				} else {
					for (IdNameDescModel tmp : resultServer) {
						if (findOnlyStartWith && tmp.getName().toUpperCase().indexOf(txt.toUpperCase()) == 0) {
							newItems.add(tmp);
						} else if (!findOnlyStartWith && tmp.getName().toUpperCase().indexOf(txt.toUpperCase()) >= 0) {
							newItems.add(tmp);
							
						}
					}
				}
				refreshListView(newItems);
			}
		}		
	}

	private void callService() {
		ObserverAsyncTask<SelectServerListResult> observer = new ObserverAsyncTask<SelectServerListResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
			}

			@Override
			public void onPostExecute(SelectServerListResult listResult) {
				
				IdNameDescModel[] result = null;
				if (listResult != null) { 
					if (urlSearch.equals(Paths.MEDICAL_SPECIALITIES) && listResult.getMedicalSpecialities() != null && listResult.getMedicalSpecialities().length > 0) {
						result = listResult.getMedicalSpecialities();
					} else if (urlSearch.equals(Paths.COLLEGES) && listResult.getColleges() != null && listResult.getColleges().length > 0) {
						result = listResult.getColleges();
					}
				}
				
				if (result != null && result.length > 0) {
					resultServer.addAll(Arrays.asList(result));
					synchronized (arrayAdapter) {
						refreshListView(new ArrayList<IdNameDescModel>(resultServer));
					}
					findViewById(R.id.edt_input_user).setVisibility(View.VISIBLE);
					findViewById(R.id.list_view).setVisibility(View.VISIBLE);
					dismissWaitDialog();
				} else {
					dismissWaitDialog();
					showMessage(R.string.msg_entities_not_found);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(urlSearch)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		if (urlSearch.equals(Paths.MEDICAL_SPECIALITIES)) {
			async = sb.mappingInto(getContext(), SelectServerListResult.class, new MedicalSpecialitiesService(sb));
		} else if (urlSearch.equals(Paths.COLLEGES)) {
			async = sb.mappingInto(getContext(), SelectServerListResult.class, new CollegesService(sb));
		} else {
			async = sb.mappingInto(getContext(), SelectServerListResult.class);
		}
		
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void refreshListView(List<IdNameDescModel> lst) {
		if (arrayAdapter == null) {
			arrayAdapter = new ArrayAdapter<IdNameDescModel>( this, R.layout.simple_list_item, lst);
			lstView.setAdapter(arrayAdapter); 
		} else {
			arrayAdapter.clear();
			arrayAdapter.addAll(lst);
			arrayAdapter.notifyDataSetChanged();
		}
	}
	
	public static final void showActivity(Activity ctx, int requestCode, int icEdtText, int hintEdtText, String urlSearch) {
		Intent it = new Intent(ctx, SelectFromServerActivity.class);
		it.putExtra(PARAM_ICON, icEdtText);
		it.putExtra(PARAM_HINT, hintEdtText);
		it.putExtra(PARAM_URL_SEARCH, urlSearch);
		ctx.startActivityForResult(it, requestCode);
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
