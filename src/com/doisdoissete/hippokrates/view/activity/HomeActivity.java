package com.doisdoissete.hippokrates.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.feed.FeedFragment;
import com.doisdoissete.hippokrates.view.activity.feed.PlaceholderFragment;
import com.doisdoissete.hippokrates.view.activity.post.TakePictureActivity;
import com.doisdoissete.hippokrates.view.activity.search.SearchActivity;


public class HomeActivity extends HippBaseActivity
        implements HomeNavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private HomeNavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        mNavigationDrawerFragment = (HomeNavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        
        mTitle = getTitle();
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));     
        
    	callRefreshMyCircles(true);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mNavigationDrawerFragment.getmDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mNavigationDrawerFragment.getmDrawerToggle().onConfigurationChanged(newConfig);
    }
    
    @Override
    protected boolean showHomeAsUpEnabled() {
    	return true;
    }
    
    @Override
    protected boolean showHomeEnabled() {
		return true;
	}
    
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (position == HomeNavigationDrawerFragment.FEED_POSITION) {
        	fragmentManager.beginTransaction()
            	.replace(R.id.container, FeedFragment.getInstance())
            	.commit();
        } else {
        	fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
        }
    }

    public void onSectionAttached(int number) {
/*        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }*/
    }

    public void restoreActionBar() {
       ActionBar actionBar = getSupportActionBar();
       actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
       actionBar.setDisplayShowTitleEnabled(true);
       actionBar.setTitle(mTitle);   
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.home, menu);
        	
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_post) {
        	TakePictureActivity.showActivity(getContext());
            return true;
        } else if (id == R.id.action_search) {
        	SearchActivity.showActivity(getContext());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean homeAsUp() {
    	return false;
    }
    
    public static void showActivity(Context ctx) {
    	ctx.startActivity(getIntentActivity(ctx));
    }
    
    public static Intent getIntentActivity(Context ctx) {
    	return new Intent(ctx, HomeActivity.class);
    	
    }
}
