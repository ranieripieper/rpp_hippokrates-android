package com.doisdoissete.hippokrates.view.activity.invite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.ContactUser;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.SendInviteResult;
import com.doisdoissete.hippokrates.service.ContactService;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.adapter.invite.ContactAdapter;
import com.nispok.snackbar.Snackbar.SnackbarDuration;

public class InviteFriendActivity extends HippBaseActivity {
	
	private static final String PARAM_NR_INVITES = "PARAM_NR_INVITES";
	 
	private TextViewPlus txtNrInvites;
	private EditText edtSearch;
	
	private ListView mContactsList;
	private View btEnviar;
	private View btCancelar;
    // An adapter that binds the result Cursor to the ListView
    private ContactAdapter mCursorAdapter;
    private ArrayList<ContactUser> lstContactUser = new ArrayList<ContactUser>();
    
    private int nrInvites = 0;
    private boolean menuVisible = false;
    private boolean findOnlyStartWith = false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.invite_friends_activity);
    	
    	nrInvites = getIntent().getIntExtra(PARAM_NR_INVITES, 0);
    	
        mContactsList = (ListView)findViewById(R.id.list_view);
        txtNrInvites = (TextViewPlus)findViewById(R.id.txt_nr_invites);
        edtSearch = (EditText)findViewById(R.id.edt_search);
        btEnviar = findViewById(R.id.bt_enviar);
        btCancelar = findViewById(R.id.bt_cancelar);
		
        edtSearch.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	        }

	        @Override
	        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	        }

	        @Override
	        public void onTextChanged(CharSequence s, int start, int before, int count) {
	        	refreshListView();
	        } 

	    });
        
        txtNrInvites.setText(String.valueOf(nrInvites));
        callServiceFindContacts();
        
    }
    
    private void refreshListView() {
		synchronized (mCursorAdapter) {
			String txt = edtSearch.getText().toString();
			if (mCursorAdapter != null) {
				
				List<ContactUser> newItems = new ArrayList<ContactUser>();
				if (TextUtils.isEmpty(txt)) {
					newItems.addAll(lstContactUser);
				} else {
					for (ContactUser tmp : lstContactUser) {
						if (findOnlyStartWith && tmp.getName().toUpperCase().indexOf(txt.toUpperCase()) == 0) {
							newItems.add(tmp);
						} else if (!findOnlyStartWith && tmp.getName().toUpperCase().indexOf(txt.toUpperCase()) >= 0) {
							newItems.add(tmp);							
						}
					}
				}
				refreshListView(newItems);
			}
		}		
	}
    
    @Override
    protected void showKeyboard() {
    	super.showKeyboard();
    	btEnviar.setVisibility(View.GONE);
    	btCancelar.setVisibility(View.GONE);    	
    }
    
    @Override
    protected void hideKeyboard() {
    	super.hideKeyboard();
    	btEnviar.setVisibility(View.VISIBLE);
    	btCancelar.setVisibility(View.VISIBLE);
    }
    
	private void refreshListView(List<ContactUser> lst) {
		if (mCursorAdapter == null) {
			mCursorAdapter = new ContactAdapter(InviteFriendActivity.this, lst, txtNrInvites, nrInvites);
			mContactsList.setAdapter(mCursorAdapter); 
		} else {
			mCursorAdapter.clear();
			mCursorAdapter.addAll(lst);
			mCursorAdapter.notifyDataSetChanged();
		}
	}
    
    public void btEnviarClick(View v) {
    	callServiceSendInvites();
    }
    
    public void btCancelarClick(View v) {
    	finish();		
    }
    
    private void callServiceSendInvites() {
    	String namEmailParam = "";
    	for (ContactUser contact : lstContactUser) {
    		if (contact.isSelected()) {
    			namEmailParam += String.format("%s;", contact.getNameEmailString());
    		}
    	}
    	if (TextUtils.isEmpty(namEmailParam)) {
    		showMessage(R.string.msg_invite_selecione_contatos);
    		return;
    	}
    	//retira último ;
    	namEmailParam = namEmailParam.substring(0, namEmailParam.length()-1);
    	
    	ObserverAsyncTask<SendInviteResult> observer = new ObserverAsyncTask<SendInviteResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
				setMenuVisible(false);
			}

			@Override
			public void onPostExecute(SendInviteResult result) {
				dismissWaitDialog();
				if (result != null && result.isSuccess()) {
			    	showMessage(R.string.msg_sucesso_send_invite);
			    	blockScreen(true);
			    	LocalUser localUser = Preferences.getLocalUser(getContext());
			    	localUser.setRemainingInvites(result.getRemainingInvites());
			    	Preferences.setLocalUser(getContext(), localUser);
			    	Preferences.setRefreshMenu(getContext(), true);
			    	new Handler().postDelayed(new Runnable() {
						
						@Override
						public void run() {
							finish();
						}
					}, SnackbarDuration.LENGTH_SHORT.getDuration());
				} else {
					showMessage(R.string.msg_erro_send_invite);
					setMenuVisible(true);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				setMenuVisible(true);
			}

			@Override
			public void onError(Exception e) {
				dismissWaitDialog();
				setMenuVisible(true);
				if (e instanceof DdsUtilIOException) {
					DdsUtilIOException ex = (DdsUtilIOException)e;
					if (ex.getHttpStatus() != null && ex.getHttpStatus().equals(HttpStatus.BAD_REQUEST)) {
						showMessage(R.string.msg_user_invited);						
						return;
					}
				}
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_EMAIL_ADDRESSES, namEmailParam);
		ServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.SEND_INVITE)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), SendInviteResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
    }
    
    
    private void callServiceFindContacts() {
    	ObserverAsyncTask<ContactUser[]> observer = new ObserverAsyncTask<ContactUser[]>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
				setMenuVisible(false);
			}

			@Override
			public void onPostExecute(ContactUser[] result) {
				edtSearch.setVisibility(View.VISIBLE);
				lstContactUser.addAll(Arrays.asList(result));
				List<ContactUser> lstTmp = new ArrayList<ContactUser>();
				lstTmp.addAll(Arrays.asList(result));
		        mCursorAdapter = new ContactAdapter(InviteFriendActivity.this, lstTmp, txtNrInvites, nrInvites);
		        mContactsList.setAdapter(mCursorAdapter);
		        dismissWaitDialog();
		        setMenuVisible(true);
		        findViewById(R.id.bt_cancelar).setVisibility(View.VISIBLE);
		        findViewById(R.id.bt_enviar).setVisibility(View.VISIBLE);
		        
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				setMenuVisible(true);
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				setMenuVisible(true);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setObserverAsyncTask(observer)
			.setNeedConnection(false)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), ContactUser[].class, new ContactService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
    }
    
    private void setMenuVisible(boolean value) {
    	menuVisible = value;
    	invalidateOptionsMenu();
    }
    
    private void showDialogWriteEmail() {
    	AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    	View v = getLayoutInflater().inflate(R.layout.write_name_email_dialog, null);
        builder.setView(v);
        final EditTextPlus edtNome = (EditTextPlus)v.findViewById(R.id.edt_nome);
        final EditTextPlus edtEmail = (EditTextPlus)v.findViewById(R.id.edt_email);
        final AlertDialog dialog = builder.create();
        v.findViewById(R.id.bt_add_friend).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				valideAddFriend(edtNome, edtEmail, dialog);
			}
		});
        
        edtEmail.setonOkDoneClick(new EditTextPlus.OnOkDoneClick() {			
			@Override
			public void onOkDoneClick(EditTextPlus editText) {
				valideAddFriend(edtNome, edtEmail, dialog);
			}
		});
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
    
    private void valideAddFriend(EditTextPlus edtNome, EditTextPlus edtEmail, Dialog dialog) {
    	edtNome.hiddenKeyboard(getContext());
		edtEmail.hiddenKeyboard(getContext());
		if (edtNome.isValid() && edtEmail.isValid()) {
			edtNome.hiddenKeyboard(getContext());
			edtEmail.hiddenKeyboard(getContext());
			addFriend(edtNome.getTextStr(), edtEmail.getTextStr());
			dialog.dismiss();
		} else {
			showMessage(R.string.msg_preencha_campos_corretamente);
		}
    }
    
    private void addFriend(String nome, String email) {
    	lstContactUser.add(0, new ContactUser(nome, email, true));
    	mCursorAdapter.add(new ContactUser(nome, email, false));
    	mCursorAdapter.notifyDataSetChanged();
    	mContactsList.smoothScrollToPosition(0);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	if (menuVisible && nrInvites > 0) {
    		getMenuInflater().inflate(R.menu.invite_friend, menu);
    	}
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_friend) {
        	showDialogWriteEmail();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    
    public static void showActivity1(Context ctx, int nrInvites) {
    	Intent it = new Intent(ctx, InviteFriendActivity.class);
    	it.putExtra(PARAM_NR_INVITES, nrInvites);
    	ctx.startActivity(it);
    }
    
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
   
}
