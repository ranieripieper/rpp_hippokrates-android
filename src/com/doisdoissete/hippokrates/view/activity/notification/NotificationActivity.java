package com.doisdoissete.hippokrates.view.activity.notification;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Notification;
import com.doisdoissete.hippokrates.model.result.NotificationResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.adapter.notification.NotificationAdapter;

public class NotificationActivity extends HippBaseActivity {

	private PagingListView mListView;
	private NotificationAdapter adapter;
	private int page = 0;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.default_listview_activity);
    	mListView = (PagingListView)findViewById(R.id.layout_content);
		mListView.setHasMoreItems(true);
		callService();
    }
	
	private void callService() {
		ObserverAsyncTask<NotificationResult> observer = new ObserverAsyncTask<NotificationResult>() {
			@Override
			public void onPreExecute() {
				if (page == 1) {
					showWaitDialog(false);
				}				
			}

			@Override
			public void onPostExecute(NotificationResult result) {
				if (result != null) {
					populateListView(result);
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				page--;
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				page--;
			}
		};
		
		if (page <= 0) {
			page = 1;
		}


		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.NOTIFICATION)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), NotificationResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void populateListView(NotificationResult result) {
		if (adapter == null) {
			mListView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					page++;
					callService();
				}
			});
			adapter = new NotificationAdapter(getContext(), new ArrayList<Notification>());
			mListView.setAdapter(adapter);
		}
		
		boolean loadMore = true;
		
		if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
			if (page < result.getMeta().getPagination().getTotalPages()) {
				loadMore = true;
			} else {
				loadMore = false;
			}
		} else {
			if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > result.getNotifications().size()) {
				loadMore = false;
	    	} else {
	    		loadMore = true;
	    	} 
		}
		   	
		mListView.onFinishLoading(loadMore, result.getNotifications());
		
		mListView.setVisibility(View.VISIBLE);
    	adapter.notifyDataSetChanged();
    	mListView.requestLayout();
		
	}
	
	@Override
	protected void onDestroy() {
		Preferences.setLastSeenNotification(getContext(), new Date());
		super.onDestroy();
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}