package com.doisdoissete.hippokrates.view.activity.base.comment;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.result.WriteCommentResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.background.DenunciarComentarioBackgroundService;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.ActivityFragmentInterface;
import com.doisdoissete.hippokrates.view.adapter.post.RefreshPostAdapter;

public class CommentViewUtil {

	public static void showWriteCommentDialog(final Context mContext, final Post post, final ActivityFragmentInterface commentInterface, final String comment, final Long commentId, final RefreshPostAdapter refreshPostAdapter) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
    	View v = LayoutInflater.from(mContext).inflate(R.layout.write_comment_dialog, null);
    	
        builder.setView(v);

        final AlertDialog dialog = builder.create();
        final EditTextPlus edtComment = (EditTextPlus)v.findViewById(R.id.edt_comment);
        final View btComment = v.findViewById(R.id.bt_comment);
        
        if (!TextUtils.isEmpty(comment)) {
        	edtComment.setText(comment);
        }
        
        btComment.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				edtComment.hiddenKeyboard(mContext);
				
				if (TextUtils.isEmpty(edtComment.getTextStr())) {
					commentInterface.showMessage(R.string.msg_preencha_campos_corretamente);
					return;
				}
				
				dialog.dismiss();
				callWriteComment(mContext, post, commentId, edtComment.getTextStr(), commentInterface, refreshPostAdapter);	
			}
		});
        
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
	}
	
	public static void showWriteCommentDialog(final Context mContext, final Post post, final ActivityFragmentInterface commentInterface, final RefreshPostAdapter refreshPostAdapter) {
		showWriteCommentDialog(mContext, post, commentInterface, null, null, refreshPostAdapter);
	}
	
	public static void callWriteComment(final Context mContext, final Post post, final Long commentId, final String comment, final ActivityFragmentInterface commentInterface, final RefreshPostAdapter refreshPostAdapter) {

		ObserverAsyncTask<WriteCommentResult> observer = new ObserverAsyncTask<WriteCommentResult>() {
			@Override
			public void onPreExecute() {
				commentInterface.showWaitDialog(true);		
			}

			@Override
			public void onPostExecute(WriteCommentResult result) {
				if (result != null && result.isSuccess()) {
					commentInterface.showMessage(R.string.comentario_enviado);
					if (commentId == null) {
						post.addQtComentarios();
						LocalPost.updateCommentsCount(post.getId(), post.getCircleId(), post.getQtComentarios());
					}					
					refreshPostAdapter.refreshQtdeComments(result.getComment(), commentId != null);
				} else {
					commentInterface.showMessage(R.string.msg_generic_error);
					showWriteCommentDialog(mContext, post, commentInterface, comment, commentId, refreshPostAdapter);
				}
				commentInterface.dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				commentInterface.dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				commentInterface.showError(e);
				showWriteCommentDialog(mContext, post, commentInterface, comment, commentId, refreshPostAdapter);
			}
		};
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_COMMENT_POST, comment);
		HippAppServiceBuilder sb = new HippAppServiceBuilder(mContext);
		
		if (commentId != null) {
			//update
			String url = Paths.UPDATE_COMMENT_POST.replace(UrlParameters.PARAM_POST_ID_URL, String.valueOf(post.getId())).replace(UrlParameters.PARAM_COMMENT_ID_URL, String.valueOf(commentId));
			sb.setUrl(url)
				.setObserverAsyncTask(observer)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.PUT)
				.setParams(params);
		} else {
			String url = Paths.COMMENT_POST.replace(UrlParameters.PARAM_POST_ID_URL, String.valueOf(post.getId()));
			sb.setUrl(url)
				.setObserverAsyncTask(observer)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.POST)
				.setParams(params);
		}
	
		AsyncTaskService async = sb.mappingInto(mContext, WriteCommentResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	
	public static void denunciarComentario(Context context, ActivityFragmentInterface activityFragmentInterface, Long postId, Long commentId) {
		if (BaseService.internetConnection(context)) {
			DenunciarComentarioBackgroundService.callService(context, postId, commentId);
			activityFragmentInterface.showMessage(R.string.obrigado_autor_comentario_sera_notificado);
		} else {
			activityFragmentInterface.showMessage(R.string.msg_verifique_conexao);
		}
	}
	
}
