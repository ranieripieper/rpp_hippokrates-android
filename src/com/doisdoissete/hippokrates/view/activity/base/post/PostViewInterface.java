package com.doisdoissete.hippokrates.view.activity.base.post;

import java.util.List;

public interface PostViewInterface {

	void removedPosts(List<Long> ids);
}
