package com.doisdoissete.hippokrates.view.activity.base.post;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.BaseAdapter;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.background.LikeBackgroundService;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.ActivityFragmentInterface;

public class PostViewUtil {

	public static void likeDislikePost(Context context, ActivityFragmentInterface activityFragmentInterface, Post post, BaseAdapter adapter) {
		if (BaseService.internetConnection(context)) {
			if (post.isUserLiked()) {
				post.setQtLikes(post.getQtLikes() - 1);
			} else {
				post.setQtLikes(post.getQtLikes() + 1);
			}
			post.setUserLiked(!post.isUserLiked());
			LikeBackgroundService.callService(context, post.getId(), post.isUserLiked());
			if (adapter != null) {
				adapter.notifyDataSetChanged();
			}
			LocalPost.updateLikesCount(post.getId(), post.getCircleId(), post.getQtLikes(), post.isUserLiked());
		} else {
			activityFragmentInterface.showMessage(R.string.msg_verifique_conexao);
		}
	}

	public static void callServiceDeletePostAdmin(final Context context, final ActivityFragmentInterface activityFragmentInterface, final PostViewInterface postViewInterface, final Long postId, final Long circleId) {

		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				activityFragmentInterface.showWaitDialog(false);			
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					postViewInterface.removedPosts(Arrays.asList(postId));
					activityFragmentInterface.showMessage(R.string.msg_foto_deletada);
					LocalPost.deletePost(postId);
				} else {
					activityFragmentInterface.showMessage(R.string.msg_generic_error);
				}
				activityFragmentInterface.dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				activityFragmentInterface.dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				activityFragmentInterface.showError(e);
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add(UrlParameters.PARAM_POST_ID, String.valueOf(postId));

		HippAppServiceBuilder sb = new HippAppServiceBuilder(context);
		sb.setUrl(Paths.MODERATE_CIRCLE_REMOVE_POST.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circleId)))
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(context, BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	
	public static void callServiceDeletePosts(final Context context, final ActivityFragmentInterface activityFragmentInterface, final PostViewInterface postViewInterface, final List<Long> ids) {

		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				activityFragmentInterface.showWaitDialog(false);			
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					postViewInterface.removedPosts(ids);
					if (ids != null && ids.size() > 1) {
						activityFragmentInterface.showMessage(R.string.msg_fotos_deletadas);
					} else {
						activityFragmentInterface.showMessage(R.string.msg_foto_deletada);
					}
					
					LocalPost.deletePosts(ids, null);
				} else {
					activityFragmentInterface.showMessage(R.string.msg_generic_error);
				}
				activityFragmentInterface.dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				activityFragmentInterface.dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				activityFragmentInterface.showError(e);
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		if (ids != null) {
			for (Long id : ids) {
				params.add(UrlParameters.PARAM_POSTS_IDS, String.valueOf(id));
			}			
		}		

		HippAppServiceBuilder sb = new HippAppServiceBuilder(context);
		sb.setUrl(Paths.USER_DELETE_POSTS)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.DELETE)
			.setParams(params);
	
		AsyncTaskService async = sb.mappingInto(context, BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
}
