package com.doisdoissete.hippokrates.view.activity.base;

import org.springframework.http.HttpStatus;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.view.SplashScreenActivity;
import com.doisdoissete.android.util.ddsutil.view.custom.ButtonPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;
import com.doisdoissete.android.util.ddsutil.view.keyboard.SoftKeyboard;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.background.DenunciarFotoBackgroundService;
import com.doisdoissete.hippokrates.service.background.GetMyCirclesBackgroundService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.login.LoginActivity;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.Snackbar.SnackbarDuration;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

public class HippBaseActivity extends ActionBarActivity implements ActivityFragmentInterface {

	public static final String PARAM_IT_USER = "PARAM_USER";
	protected String MALE_TAG = "btn_male";
	protected String FEMALE_TAG = "btn_female";
	
	protected String PARAM_MESSAGE = "MESSAGE";
	
	private SoftKeyboard softKeyboard;
	static public boolean isAPI11 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	private ProgressDialog progress;
	protected LoadingView loadingView;
	protected TouchBlackHoleView blackHole;
	protected AsyncTaskService async;
	protected Toolbar toolbar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	protected <T extends Parcelable> T getParcelable(final String key) {
		if (getIntent() != null && getIntent().getExtras() != null) {
			return getIntent().getExtras().getParcelable(key);
		}
		return null;
	}
	
	protected LocalUser getLocalUser() {
		return Preferences.getLocalUser(getContext());
	}
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		afterViews();
		
		if (getIntent() != null && getIntent().getExtras() != null) {
			String msg = getIntent().getExtras().getString(PARAM_MESSAGE);
			if (!TextUtils.isEmpty(msg)) {
				showMessage(msg);
			}
		}
	}

	protected void callRefreshMyCircles(boolean updateInfo) {
		callRefreshMyCircles(getContext(), updateInfo);
	}
	
	public static void callRefreshMyCircles(Context context) {
		callRefreshMyCircles(context, false);
	}
	
	protected void callRefreshMyCircles() {
		callRefreshMyCircles(getContext(), false);
	}
	 
    public static void callRefreshMyCircles(Context context, boolean updateInfo) {
    	GetMyCirclesBackgroundService.callService(context, updateInfo);
    }
    
	public void afterViews() {
		configActionBar();
		loadingView = (LoadingView)findViewById(R.id.progress_bar);
		blackHole = (TouchBlackHoleView)findViewById(R.id.black_hole);
		
		configSoftKeyboard();		
	}

	
	protected void blockScreen(boolean blockScreen) {
		if (blackHole != null) {
			if (blockScreen) {
				blackHole.disable_touch(true);
				blackHole.setVisibility(View.VISIBLE);
			} else {
				blackHole.disable_touch(false);
				blackHole.setVisibility(View.GONE);
			}			
		}
	}
	
	protected void showDefaultDialog(final int strBtLeft, final int strBtRight, String msg, final OnClickListener onClickBtLeft) {
		showDefaultDialog(getString(strBtLeft), getString(strBtRight), msg, onClickBtLeft, null);
	}
	
	protected void showDefaultDialog(final int strBtLeft, final int strBtRight, int msg, final OnClickListener onClickBtLeft) {
		showDefaultDialog(getString(strBtLeft), getString(strBtRight), getString(msg), onClickBtLeft, null);
	}
	
	protected void showDefaultDialog(final String strBtLeft, final String strBtRight, String msg, final OnClickListener onClickBtLeft) {
		showDefaultDialog(strBtLeft, strBtRight, msg, onClickBtLeft, null);
	}
	
	protected void showOptionsDialog(final int[] textos, final OnClickListener[] onclick) {
		showOptionsDialog(getContext(), textos, onclick);
	}
	
	public static void showOptionsDialog(Context context, final int[] textos, final OnClickListener[] onclick) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	View v = LayoutInflater.from(context).inflate(R.layout.options_dialog, null);
        builder.setView(v);

        final AlertDialog dialog = builder.create();
        TextViewPlus[] textViewPlus = new TextViewPlus[6];
        View[] viewSep = new View[6];
        textViewPlus[0] = (TextViewPlus)v.findViewById(R.id.option_1);
        textViewPlus[1] = (TextViewPlus)v.findViewById(R.id.option_2);
        textViewPlus[2] = (TextViewPlus)v.findViewById(R.id.option_3);
        textViewPlus[3] = (TextViewPlus)v.findViewById(R.id.option_4);
        textViewPlus[4] = (TextViewPlus)v.findViewById(R.id.option_5);
        textViewPlus[5] = (TextViewPlus)v.findViewById(R.id.option_6);
        
        viewSep[0] = v.findViewById(R.id.view_sep_1);
        viewSep[1] = v.findViewById(R.id.view_sep_2);
        viewSep[2] = v.findViewById(R.id.view_sep_3);
        viewSep[3] = v.findViewById(R.id.view_sep_4);
        viewSep[4] = v.findViewById(R.id.view_sep_5);
        
        for (int i=0; i < textos.length; i++) {
        	textViewPlus[i].setText(textos[i]);
        	final View.OnClickListener onclickTmp = onclick[i];
        	textViewPlus[i].setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					onclickTmp.onClick(v);
				}
			});
        	if (i > 0 && i+1 <= textos.length) {
        		viewSep[i-1].setVisibility(View.VISIBLE);
        	}
        	textViewPlus[i].setVisibility(View.VISIBLE);
        }
       
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
	}
	
	public static void showDefaultErrorDialog(Context context, int msg) {
		showDefaultErrorDialog(context, context.getString(msg));
	}
	
	public static void showDefaultErrorDialog(Context context, String msg) {
		showDefaultDialog(context, context.getString(R.string.erro), null, msg, null, null);
	}
	
	public static void showDefaultDialog(Context context, final String strBtLeft, final String strBtRight, String msg, final OnClickListener onClickBtLeft, final OnClickListener onClickBtRight) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	View v = LayoutInflater.from(context).inflate(R.layout.default_2_buttons_dialog, null);
        builder.setView(v);

        final AlertDialog dialog = builder.create();
        TextViewPlus txtMsg = (TextViewPlus)v.findViewById(R.id.txt_msg);
        ButtonPlus btRight = (ButtonPlus)v.findViewById(R.id.bt_right);
        ButtonPlus btLeft = (ButtonPlus)v.findViewById(R.id.bt_left);
        
        txtMsg.setText(msg);
       
        if (TextUtils.isEmpty(strBtLeft)) {
        	btLeft.setVisibility(View.GONE);
        	((LinearLayout.LayoutParams)btRight.getLayoutParams()).weight = 2;
        } else {
        	btLeft.setText(strBtLeft);
        }
        if (TextUtils.isEmpty(strBtRight)) {
        	btRight.setVisibility(View.GONE);
        	((LinearLayout.LayoutParams)btLeft.getLayoutParams()).weight = 2;
        } else {
        	 btRight.setText(strBtRight);
        }
        
        btRight.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (onClickBtRight != null) {
					onClickBtRight.onClick(v);	
				}
							
			}
		});
        btLeft.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (onClickBtLeft != null) {
					onClickBtLeft.onClick(v);	
				}			
			}
		});
        
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
	}
	
	protected void showDefaultDialog(final String strBtLeft, final String strBtRight, String msg, final OnClickListener onClickBtLeft, final OnClickListener onClickBtRight) {
		showDefaultDialog(getContext(), strBtLeft, strBtRight, msg, onClickBtLeft, onClickBtRight);
	}
	
	public void showWaitDialog(boolean blockScreen) {
		blockScreen(blockScreen);
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
			if (findViewById(R.id.layout_include_loading) != null) {
				findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
			}
		} else {
			progress = ProgressDialog.show(this, getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
	}
	
	public void showWaitDialog() {
		showWaitDialog(true);
	}
	
	public void dismissWaitDialog() {
		if (blackHole != null) {
			blackHole.disable_touch(false);
			blackHole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);			
		}
		if (findViewById(R.id.layout_include_loading) != null) {
			findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
	}
	
	private void configSoftKeyboard() {
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.content);
		if (mainLayout != null) {
			InputMethodManager im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
			if (softKeyboard != null) {
				softKeyboard.unRegisterSoftKeyboardCallback();
			}
			softKeyboard = new SoftKeyboard(mainLayout, im);
			softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {

						@Override
						public void onSoftKeyboardHide() {
							new Handler(Looper.getMainLooper()).post(new Runnable() {
										@Override
										public void run() {
											hideKeyboard();
										}
									});
						}

						@Override
						public void onSoftKeyboardShow() {
							new Handler(Looper.getMainLooper()).post(new Runnable() {
										@Override
										public void run() {
											showKeyboard();
										}
									});
						}
					});
		}
	}
	
    public static void showDialogDenunciarPost(final Context mContext, final Long postId, final ActivityFragmentInterface activityFragmentInterface) {
    	showOptionsDialog(mContext, new int[] {R.string.denunciar_foto}, new View.OnClickListener [] {
    			new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						showDialogOptionsDenunciarPost(mContext, postId, activityFragmentInterface);
					}
				}
    	} );
    }
    
    public static void showDialogOptionsDenunciarPost(final Context mContext, final Long postId, final ActivityFragmentInterface activityFragmentInterface) {
    	showOptionsDialog(mContext, new int[] {R.string.foto_impropria, R.string.identifiquei_paciente}, new View.OnClickListener [] {
    			new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						denunciarPost(mContext, Constants.DENUNCIA_INAPROPRIADO, postId, activityFragmentInterface);
					}
				}, 
				new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						denunciarPost(mContext, Constants.DENUNCIA_IDENTIFIQUEI_PACIENTE, postId, activityFragmentInterface);
					}
				}
    	} );
    }
    public static void denunciarPost(final Context mContext, int reportType, Long postId, ActivityFragmentInterface activityFragmentInterface) {
    	if (BaseService.internetConnection(mContext)) {
    		DenunciarFotoBackgroundService.callService(mContext, postId, reportType);
    		activityFragmentInterface.showMessage(R.string.obrigado_autor_sera_notificado);
    	} else {
    		activityFragmentInterface.showMessage(R.string.msg_verifique_conexao);
    	}
    }
	
	protected void showKeyboard() {
		
	}
	
	protected void hideKeyboard() {
		
	}
	
	protected boolean isConfigSoftKeyboard() {
		return false;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (softKeyboard != null) {
			softKeyboard.unRegisterSoftKeyboardCallback();
		}
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		configSoftKeyboard();
	}
	
	protected boolean showHomeAsUpEnabled() {
		return false;
	}
	
	protected boolean showHomeEnabled() {
		return false;
	}
	
    private void configActionBar() {
    	toolbar = (Toolbar)findViewById(R.id.hipapp_toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(showHomeAsUpEnabled());
	    	getSupportActionBar().setDisplayShowHomeEnabled(showHomeEnabled());
	    	getSupportActionBar().setDisplayHomeAsUpEnabled(showHomeAsUpEnabled());
	    	if (homeAsUp()) {
	    		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
	    		    @Override
	    		    public void onClick(View v) {
	    		        onBackPressed();
	    		    }
	    		});
	    	} else {

		    	/*
		    	getSupportActionBar().setDisplayShowHomeEnabled(showHomeEnabled());
		        getSupportActionBar().setDisplayUseLogoEnabled(false);
		        getSupportActionBar().setLogo(null);
		        getSupportActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));   
		        getSupportActionBar().setDisplayHomeAsUpEnabled(showHomeAsUpEnabled());
		        */
	    	}
		}
    }
    
    
    public void showMessageAndFinish(String msg) {
    	showMessage(msg);
    	blockScreen(true);
    	new Handler().postDelayed(new Runnable() {

    		@Override
    		public void run() {
    			finish();
    		}
    	}, 1000);
    }
    
    public void showMessageAndFinish(int resMsgId) {
    	showMessageAndFinish(getString(resMsgId));
    }
    
    public void showMessageFinishAllAndStartActivity(int resMsgId, final Intent it) {
    	showMessageFinishAllAndStartActivity(getString(resMsgId), it);
    }
    
    public void showMessageFinishAllAndStartActivity(String msg, final Intent it) {
    	it.putExtra(PARAM_MESSAGE, msg);
    	finishAffinity();
		startActivity(it);
    }
    
    public void showMessage(int resMsgId) {
    	showMessage(getString(resMsgId));
    }
    
    private String getErros(String msgErro) {
    	try {
    		BaseResult result = Util.getGson().fromJson(msgErro, BaseResult.class);
    		String[] errors = result.getErrors();
        	
        	String msg = "";
        	if (errors != null && errors.length > 0) {
        		for (String m : errors) {
        			msg += m + "\r\n";
        		}
        		msg = msg.substring(0, msg.length()-2);
        	}
        	return msg;
    	} catch(Exception e1) {   
    		e1.printStackTrace();
    	}
    	return null;
    }
    
    
    protected void processDdsUtilIOException(DdsUtilIOException e) {
    	HttpStatus httpStatus = ((DdsUtilIOException)e).getHttpStatus();
    	
    	String msg = getErros(e.getMsgErro());
    	
		if (HttpStatus.UNAUTHORIZED.equals(httpStatus) || Preferences.getLocalUser(getContext()) == null) {
			showMessageFinishAllAndStartActivity(msg == null ? getString(R.string.msg_token_expirou) : msg, LoginActivity.getIntentActivity(getContext()));		
		} else if (HttpStatus.NOT_FOUND.equals(httpStatus)) {
			showMessageAndFinish(msg == null ? getString(R.string.msg_generic_error) : msg);
		} else {
			showMessage(msg == null ? getString(R.string.msg_generic_error) : msg);
		}
    }
    
	public static void showError(Activity context, Exception e) {
		String msgError = "";
		if (e != null) {
			if (e instanceof ConnectionNotFoundException) {
				msgError = context.getString(R.string.msg_sem_conexao);
			} else if (e instanceof DdsUtilIOException) {
				HttpStatus status = ((DdsUtilIOException) e).getHttpStatus();
				
				if (status != null && (HttpStatus.UNAUTHORIZED.equals(status))) {
					Preferences.logout(context);
					context.startActivity(new Intent(context, SplashScreenActivity.class));
					context.finishAffinity();
				}
			}
		}
		
		if (TextUtils.isEmpty(msgError)) {
			msgError = context.getString(R.string.msg_ocorreu_erro);
		}
		
		showDefaultErrorDialog(context, msgError);
	}
	
    public void showError(Exception e) {
		dismissWaitDialog();
		if (e != null) {
			String msgError = "";

			int resMsg = R.string.msg_ocorreu_erro;
			if (e instanceof ConnectionNotFoundException) {
				resMsg = R.string.msg_sem_conexao;
			} else if(e instanceof DdsUtilIOException) {
				processDdsUtilIOException((DdsUtilIOException)e);
				return;
			} else if(e instanceof ConnectionException) {
				String error = ((ConnectionException)e).getMsgErro();
				HttpStatus httpStatus = ((ConnectionException)e).getHttpStatus();
				if (HttpStatus.UNAUTHORIZED.equals(httpStatus)) {
					showMessageFinishAllAndStartActivity(R.string.msg_token_expirou, LoginActivity.getIntentActivity(getContext()));
				} else {
			    	String msg = getErros(error);
			    	
					if (HttpStatus.UNAUTHORIZED.equals(httpStatus) || Preferences.getLocalUser(getContext()) == null) {
						showMessageFinishAllAndStartActivity(msg == null ? getString(R.string.msg_token_expirou) : msg, LoginActivity.getIntentActivity(getContext()));		
						return;
					} else {
						showMessage(msg == null ? getString(R.string.msg_generic_error) : msg);
						return;
					}
				}
			}
			
			if (!TextUtils.isEmpty(msgError)) {
				showMessage(msgError, SnackbarDuration.LENGTH_LONG);
			} else {
				showMessage(resMsg);
			}
	        
		} else {
			showMessage(R.string.msg_ocorreu_erro);
		}
    }
    
    protected void showMessage(int msg, Snackbar.SnackbarDuration duration) {
    	showMessage(getString(msg), duration);
    }
    
    protected void showMessage(String msg, Snackbar.SnackbarDuration duration) {
    	toolbar.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
    	Snackbar mSnackBar = Snackbar.with(this)
                .text(msg)
                .type(SnackbarType.MULTI_LINE)
                .textSize(14)
                .topMargin(toolbar.getMeasuredHeight())
                .color(getResources().getColor(R.color.bg_dark_blue))
                .gravity(Gravity.TOP)
                .textTypeface(FontUtilCache.get("fonts/HelveticaNeue.ttf", HippBaseActivity.this))
                .duration(duration);
    	 SnackbarManager.show(mSnackBar);
    }
    
    protected void showMessage(String msg) {
    	showMessage(msg, SnackbarDuration.LENGTH_SHORT);
    }
    
    protected void dismissMessage() {
    	SnackbarManager.dismiss();    	
    }
    
    protected Context getContext() {
    	return this;
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home && homeAsUp()) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
    
    protected boolean homeAsUp() {
    	return true;
    }

	/**
	 * @return the toolbar
	 */
	public Toolbar getToolbar() {
		return toolbar;
	}
    
    
    
/*    protected void showPopupAlert(int title, int msg) {
    	showPopupAlert(title, msg, null);
    }
    
    protected void showPopupAlert(int title, int msg, OnClickListener onclick) {
    	AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    	View v = getLayoutInflater().inflate(R.layout.default_dialog, null);
        builder.setView(v);
        final TextViewPlus txtTitle = (TextViewPlus)v.findViewById(R.id.txt_title);
        final TextViewPlus txtMsg = (TextViewPlus)v.findViewById(R.id.txt_msg);
        txtTitle.setText(title);
        txtMsg.setText(msg);
        
        final AlertDialog dialog = builder.create();
        if (onclick != null) {
        	v.findViewById(R.id.bt_ok).setOnClickListener(onclick);
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();;
    }*/
}
