package com.doisdoissete.hippokrates.view.activity.base;

import java.io.File;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.media.MediaScannerNotifier;
import com.doisdoissete.android.util.ddsutil.service.SaveFileService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.ResizeBitmapService;
import com.doisdoissete.hippokrates.util.Constants;

public abstract class HippBaseSelectImageActivity extends HippBaseActivity {

	private final int PICTURE_TAKEN_FROM_CAMERA = 9991;
	private final int PICTURE_TAKEN_FROM_GALLERY = 9992;
	private File outFile = null;
	protected String filePath = null;
	private boolean storeImage = true;
	
	protected abstract void setImageSelect(Bitmap takenPictureData);
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    Bitmap takenPictureData = null;

	    switch(requestCode){

	        case PICTURE_TAKEN_FROM_CAMERA:             
	            if(resultCode==Activity.RESULT_OK) {
	                takenPictureData = handleResultFromCamera(data);
	            }               
	            break;
	        case PICTURE_TAKEN_FROM_GALLERY:                
	            if(resultCode==Activity.RESULT_OK) {
	                takenPictureData = handleResultFromChooser(data);                   
	            }
	            break;          
	    }

	    if(takenPictureData != null) {	    	
	    	if (resizeBitmap()) {
	    		callResizeBitmap(takenPictureData);
	    	} else {
	    		setImageSelect(takenPictureData);
	    	} 
	    }       
	}
	
	private void callResizeBitmap(final Bitmap bitmap) {
		ObserverAsyncTask<File> observer = new ObserverAsyncTask<File>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(File file) {
				if (file != null) {
					
					Bitmap resizeBitmap;
					try {
						resizeBitmap = BitmapUtil.media_getBitmapFromFile(file, maxWidthResize() > 0 ? maxWidthResize() : ScreenUtil.getDisplayWidth(getContext())/2);
						if (resizeBitmap != null) {
							filePath = file.getAbsolutePath();
							setImageSelect(resizeBitmap);
						}
						bitmap.recycle();
					} catch (Exception e) {
						setImageSelect(bitmap);
					}	
					
					if(dismissWaitDialogResize()) {
						dismissWaitDialog();
					}
					
				} else {
					setImageSelect(bitmap);
				}
			}

			@Override
			public void onCancelled() {
				if(dismissWaitDialogResize()) {
					dismissWaitDialog();
				}
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onError(Exception e) {
				if(dismissWaitDialogResize()) {
					dismissWaitDialog();
				}
				showError(e);
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		params.add(ResizeBitmapService.PARAM_BITMAP, bitmap);
		
		params.add(ResizeBitmapService.PARAM_FILE_PATH, SaveFileService.getOutputMediaFile(getContext(), getString(R.string.base_dir_save_images), "resize_circle").getAbsolutePath());
		if (maxHeightResize() > 0) {
			params.add(ResizeBitmapService.PARAM_HEIGHT, maxHeightResize());
		} 
		if (maxWidthResize() > 0) {
			params.add(ResizeBitmapService.PARAM_WIDTH, maxWidthResize());
		}
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setObserverAsyncTask(observer)
			.setNeedConnection(false)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), File.class, new ResizeBitmapService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	public void selectTakePicture(View v) {
		View view = this.getCurrentFocus();
		if (view != null) {  
			InputMethodManager imm = (InputMethodManager)getSystemService(
					Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
		
		View.OnClickListener onClickCamera = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 getPictureFromCamera();
			}
		};
		
		View.OnClickListener onClickGaleria = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getPictureFromGallery();
			}
		};
		
		showDefaultDialog(getString(R.string.dialog_txt_camera), getString(R.string.dialog_txt_galeria), getString(R.string.dialog_escolher_foto), onClickCamera, onClickGaleria);
		/*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_escolher_foto)
               .setPositiveButton(R.string.dialog_txt_camera, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	  
                   }
               })
               .setNegativeButton(R.string.dialog_txt_galeria, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   getPictureFromGallery();
                   }
               });
        builder.create().show();*/
	}
	
	//AUXILIAR
	private Bitmap handleResultFromChooser(Intent data){
	    Bitmap takenPictureData = null;

	    Uri photoUri = data.getData();
	    if (photoUri != null){
	        try {
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};
	            Cursor cursor = getContentResolver().query(photoUri, filePathColumn, null, null, null); 
	            cursor.moveToFirst();
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            filePath = cursor.getString(columnIndex);
	            cursor.close();

	            takenPictureData = BitmapUtil.media_getBitmapFromFile(new File(filePath), 160);

	        } catch(Exception e) {
	            Toast.makeText(this, "Error getting selected image.", Toast.LENGTH_SHORT).show();
	        }
	    }

	    return takenPictureData;
	}

	private Bitmap handleResultFromCamera(Intent data) {
	    Bitmap takenPictureData = null;

	    if(data != null) {
	        Bundle extras = data.getExtras();
	        if (extras != null && extras.get("data") != null) {
	            takenPictureData = (Bitmap) extras.get("data");
	        }
	    } else {
	        try{
	            takenPictureData = BitmapUtil.media_getBitmapFromFile(outFile, 160);
	            takenPictureData = BitmapUtil.media_correctImageOrientation(outFile.getAbsolutePath());
	        } catch(Exception e) {
	            Toast.makeText(this, "Error getting saved taken picture.", Toast.LENGTH_SHORT).show();;
	        }
	        
	    }

	    if(storeImage) {
	        //We add the taken picture file to the gallery so user can see the image directly                   
	        new MediaScannerNotifier(this, outFile.getAbsolutePath(), "image/*", false);
	        filePath = outFile.getAbsolutePath();
	    }

	    return takenPictureData;
	}

	protected void getPictureFromCamera() {
	    boolean cameraAvailable = BitmapUtil.device_isHardwareFeatureAvailable(this, PackageManager.FEATURE_CAMERA);
	    if(cameraAvailable && 
	    	Util.system_isIntentAvailable(this, MediaStore.ACTION_IMAGE_CAPTURE)) {

	        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

	        //We prepare the intent to store the taken picture
	        try {
	            File outputDir = BitmapUtil.storage_getExternalPublicFolder(Environment.DIRECTORY_PICTURES, getString(R.string.app_name), true);
	            outFile = BitmapUtil.storage_createUniqueFileName("cameraPic", ".jpg", outputDir);

	            takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outFile));             
	            storeImage = true;
	        } catch(Exception e) {
	            Toast.makeText(this, "Error setting output destination.", Toast.LENGTH_SHORT).show();;
	        }

	        startActivityForResult(takePicIntent, PICTURE_TAKEN_FROM_CAMERA);
	    } else {
	        if (cameraAvailable) {
	            Toast.makeText(this, "No application that can receive the intent camera.", Toast.LENGTH_SHORT).show();;
	        } else {
	            Toast.makeText(this, "No camera present!!", Toast.LENGTH_SHORT).show();;
	        }
	    }
	}

	protected void getPictureFromGallery() {
	    //This takes images directly from gallery
	    Intent gallerypickerIntent = new Intent(Intent.ACTION_PICK);
	    gallerypickerIntent.setType("image/*");
	    startActivityForResult(gallerypickerIntent, PICTURE_TAKEN_FROM_GALLERY); 
	}
	
	protected boolean resizeBitmap() {
		return false;
	}
	
	protected int maxHeightResize() {
		return 0;
	}
	
	protected int maxWidthResize() {
		return 0;
	}
	
	protected boolean dismissWaitDialogResize() {
		return true;
	}
	
}
