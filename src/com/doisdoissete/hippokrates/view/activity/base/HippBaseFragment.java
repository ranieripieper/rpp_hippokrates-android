package com.doisdoissete.hippokrates.view.activity.base;

import org.springframework.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.doisdoissete.android.util.ddsutil.view.custom.util.FontUtilCache;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.login.LoginActivity;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.Snackbar.SnackbarDuration;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

public class HippBaseFragment extends Fragment implements ActivityFragmentInterface {

	protected AsyncTaskService async;
	static public boolean isAPI11 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	private ProgressDialog progress;
	protected LoadingView loadingView;
	protected TouchBlackHoleView black_hole;
	protected View includeLoading;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		loadingView = (LoadingView) view.findViewById(R.id.progress_bar);
		black_hole = (TouchBlackHoleView) view.findViewById(R.id.black_hole);
		includeLoading = view.findViewById(R.id.layout_include_loading);
	}
	

	public void showWaitDialog(boolean blockScreen) {
		if (black_hole != null) {
			if (blockScreen) {
				black_hole.disable_touch(true);
				black_hole.setVisibility(View.VISIBLE);
			} else {
				black_hole.disable_touch(false);
				black_hole.setVisibility(View.GONE);
			}

		}

		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
			if (includeLoading != null) {
				includeLoading.setVisibility(View.VISIBLE);
			}
		} else {
			progress = ProgressDialog.show(getContext(),
					getString(R.string.title_aguarde),
					getString(R.string.msg_aguarde), true);
			progress.show();
		}
	}

	public void showWaitDialog() {
		showWaitDialog(true);
	}

	public void dismissWaitDialog() {
		if (black_hole != null) {
			black_hole.disable_touch(false);
			black_hole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (includeLoading != null) {
			includeLoading.setVisibility(View.GONE);
		}
		if (progress != null) {
			progress.dismiss();
		}
	}

	@Override
	public void setMenuVisibility(final boolean visible) {
		super.setMenuVisibility(visible);
		if (visible) {
			showFragment();
		} else {
			hideFragment();
		}
	}

	protected void showFragment() {
	}

	protected void hideFragment() {
	}

	public void showMessage(int resMsgId) {
		showMessage(getString(resMsgId));
	}

	protected void showMessage(String msg, Snackbar.SnackbarDuration duration) {
		int topMargin = ((HippBaseActivity)getActivity()).getToolbar().getHeight();
		Snackbar mSnackBar = Snackbar
				.with(getActivity())
				.text(msg)
				.topMargin(topMargin)
				.type(SnackbarType.MULTI_LINE)
				.textSize(18)
				.color(getResources().getColor(R.color.bg_dark_blue))
				.gravity(Gravity.TOP)
				.textTypeface(
						FontUtilCache.get("fonts/HelveticaNeue.ttf",
								getActivity())).duration(duration);
		SnackbarManager.show(mSnackBar);
	}
	

    protected void processDdsUtilIOException(DdsUtilIOException e) {
    	HttpStatus httpStatus = ((DdsUtilIOException)e).getHttpStatus();
    	
    	String msg = getErros(e.getMsgErro());
    	
		if (HttpStatus.UNAUTHORIZED.equals(httpStatus) || Preferences.getLocalUser(getContext()) == null) {
			((HippBaseActivity)getActivity()).showMessageFinishAllAndStartActivity(msg == null ? getString(R.string.msg_token_expirou) : msg, LoginActivity.getIntentActivity(getContext()));		
		} else {
			showMessage(msg == null ? getString(R.string.msg_generic_error) : msg);
		}
    }
    
    private String getErros(String msgErro) {
    	try {
    		BaseResult result = Util.getGson().fromJson(msgErro, BaseResult.class);
    		String[] errors = result.getErrors();
        	
        	String msg = "";
        	if (errors != null && errors.length > 0) {
        		for (String m : errors) {
        			msg += m + "\r\n";
        		}
        		msg = msg.substring(0, msg.length()-2);
        	}
        	return msg;
    	} catch(Exception e1) {    		
    	}
    	return null;
    }
    
    
    public void showError(Exception e) {
		dismissWaitDialog();
		if (e != null) {
			String msgError = "";

			int resMsg = R.string.msg_ocorreu_erro;
			if (e instanceof ConnectionNotFoundException) {
				resMsg = R.string.msg_sem_conexao;
			} else if(e instanceof DdsUtilIOException) {
				processDdsUtilIOException((DdsUtilIOException)e);
				return;
			} else if(e instanceof ConnectionException) {
				String error = ((ConnectionException)e).getMsgErro();
				HttpStatus httpStatus = ((ConnectionException)e).getHttpStatus();
				if (HttpStatus.UNAUTHORIZED.equals(httpStatus)) {
					((HippBaseActivity)getActivity()).showMessageFinishAllAndStartActivity(R.string.msg_token_expirou, LoginActivity.getIntentActivity(getContext()));
				} else {
			    	String msg = getErros(error);
			    	
					if (HttpStatus.UNAUTHORIZED.equals(httpStatus) || Preferences.getLocalUser(getContext()) == null) {
						((HippBaseActivity)getActivity()).showMessageFinishAllAndStartActivity(msg == null ? getString(R.string.msg_token_expirou) : msg, LoginActivity.getIntentActivity(getContext()));		
						return;
					} else {
						showMessage(msg == null ? getString(R.string.msg_generic_error) : msg);
						return;
					}
				}
			}
			
			if (!TextUtils.isEmpty(msgError)) {
				showMessage(msgError, SnackbarDuration.LENGTH_LONG);
			} else {
				showMessage(resMsg);
			}
	        
		} else {
			showMessage(R.string.msg_ocorreu_erro);
		}
    }

	protected void showMessage(String msg) {
		showMessage(msg, SnackbarDuration.LENGTH_SHORT);
	}

	protected void dismissMessage() {
		SnackbarManager.dismiss();
	}

	protected Context getContext() {
		return getActivity();
	}
}
