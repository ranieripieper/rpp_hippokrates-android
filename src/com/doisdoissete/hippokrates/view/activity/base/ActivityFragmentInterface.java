package com.doisdoissete.hippokrates.view.activity.base;


public interface ActivityFragmentInterface {

	public void showMessage(int resMsgId);
	public void showError(Exception e);
	public void showWaitDialog(boolean blockScreen);
	public void dismissWaitDialog();
}
