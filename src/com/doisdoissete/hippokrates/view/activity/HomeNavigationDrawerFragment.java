package com.doisdoissete.hippokrates.view.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.doisdoissete.android.util.ddsutil.view.custom.listview.AnimatedExpandableListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.service.background.LogoutBackgroundService;
import com.doisdoissete.hippokrates.util.HippUtil;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.circle.CreateCircleActivity;
import com.doisdoissete.hippokrates.view.activity.circle.CreateStudentCircleActivity;
import com.doisdoissete.hippokrates.view.activity.circle.StudentJoinCircleActivity;
import com.doisdoissete.hippokrates.view.activity.circle.ViewCircleActivity;
import com.doisdoissete.hippokrates.view.activity.invite.InviteFriendActivity;
import com.doisdoissete.hippokrates.view.activity.notification.NotificationActivity;
import com.doisdoissete.hippokrates.view.activity.settings.AboutActivity;
import com.doisdoissete.hippokrates.view.activity.settings.SettingsActivity;
import com.doisdoissete.hippokrates.view.activity.signin.LoginSigninActivity;
import com.doisdoissete.hippokrates.view.activity.user.ViewUserProfileActivity;
import com.doisdoissete.hippokrates.view.menu.MenuHomeAdapter;
import com.doisdoissete.hippokrates.view.menu.MenuHomeChild;
import com.doisdoissete.hippokrates.view.menu.MenuHomeGroup;
import com.doisdoissete.hippokrates.view.menu.MenuItemClickListener;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
@SuppressLint("NewApi")
public class HomeNavigationDrawerFragment extends Fragment {

	public static final int FEED_POSITION = 0;
	
	private LocalUser localUser;
	 private Date dtRefreshMenu = new Date();
	 
    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private AnimatedExpandableListView mDrawerListView;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    
    private MenuHomeAdapter menuHomeAdapter;

    public HomeNavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }

        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition);
        
        localUser = Preferences.getLocalUser(getActivity());
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
        mDrawerListView = (AnimatedExpandableListView) inflater.inflate( R.layout.menu_home, container, false);
        mDrawerListView.setGroupIndicator(null);
        
        mDrawerListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            	MenuHomeGroup menu = menuHomeAdapter.getGroup(groupPosition);
            	if (menu.executeOnClick(getActivity())) {
            		 mDrawerLayout.closeDrawer(mFragmentContainerView);
            	}
            	
				return false;
			}
		});
        
        mDrawerListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				MenuHomeChild menu = menuHomeAdapter.getChild(groupPosition, childPosition);
				if (menu.executeOnClick(getActivity())) {
					mDrawerLayout.closeDrawer(mFragmentContainerView);
				}
				return false;
			}
		});
        
        menuHomeAdapter = new MenuHomeAdapter(getActivity(), createMenu(localUser));
        mDrawerListView.setAdapter(menuHomeAdapter);
        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
        return mDrawerListView;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }
    
    public void notifyDataSetChanged() {
    	localUser = Preferences.getLocalUser(getActivity());
    	menuHomeAdapter = new MenuHomeAdapter(getActivity(), createMenu(localUser));
        mDrawerListView.setAdapter(menuHomeAdapter);
    	menuHomeAdapter.notifyDataSetChanged();
    	mDrawerListView.invalidate();
    	mDrawerListView.requestLayout();
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        android.support.v7.app.ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                
                R.string.navigation_drawer_open,  /* "open drawer" description */
                R.string.navigation_drawer_close  /* "close drawer" description */
                ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        
     // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        
        /*mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                     host Activity 
                mDrawerLayout,                     DrawerLayout object 
                R.drawable.icn_home_menu,              nav drawer image to replace 'Up' caret 
                R.string.navigation_drawer_open,   "open drawer" description for accessibility 
                R.string.navigation_drawer_close   "close drawer" description for accessibility 
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }
                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        */
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            //inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

    	Date dtLastRefresh = Preferences.getLastCallMeInfo(getActivity());
    	if (Preferences.isRefreshMenu(getActivity()) || dtRefreshMenu != null && (dtLastRefresh == null || dtRefreshMenu.before(dtLastRefresh))) {
			notifyDataSetChanged();
			dtRefreshMenu = new Date();  		
    	}
        if (HippUtil.callMeInfo(getActivity()) ) {
     	   HippBaseActivity.callRefreshMyCircles(getActivity());
        }
        
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

      /*  if (item.getItemId() == R.id.action_example) {
            Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
            return true;
        }*/

    	
        return super.onOptionsItemSelected(item);
    }
    
    /**
	 * @return the mDrawerToggle
	 */
	public ActionBarDrawerToggle getmDrawerToggle() {
		return mDrawerToggle;
	}

	/**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity)getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }
    
    public List<MenuHomeGroup> createMenu(final LocalUser localUser) {
    	Preferences.setRefreshMenu(getActivity(), false);
    	
    	List<MenuHomeGroup> items = new ArrayList<MenuHomeGroup>();
        List<MenuHomeChild> childsCirculos = new ArrayList<MenuHomeChild>();
        
        //verifica se o usuário é professor, médico ou aluno para criar os submenus
        boolean showCriarCirculo = false;
        boolean showCriarCirculoAluno = false;
   	 	boolean showParticiparCirculo = false;
   	 	boolean showConvidarAmigos = true;
		if (localUser == null) {
			executeLogout(localUser);
			return null;
		} else {
			if (localUser.isAdmin()) {
				showCriarCirculo = true;
				showCriarCirculoAluno = true;
				showParticiparCirculo = true;
			} else if (localUser.isDoctor()) {
				showCriarCirculo = true;
			} else if (localUser.isStudent()) {
				showConvidarAmigos = false;
				showParticiparCirculo = true;
			} else if (localUser.isTeacher()) {
				showCriarCirculo = true;
				showCriarCirculoAluno = true;
			}
		}        
        
        if (showCriarCirculo) {
        	MenuItemClickListener clickCriarCirculo = new MenuItemClickListener() {
    			@Override
    			public void onClick(Context ctx) {
    				CreateCircleActivity.showActivity(getActivity());
    			}
    		};
        	
       	 	childsCirculos.add(new MenuHomeChild(getString(R.string.menu_criar_circulo), R.drawable.icn_menu_novo_circulo, clickCriarCirculo));
        }
        if (showCriarCirculoAluno) {
        	MenuItemClickListener clickCriarCirculoAluno = new MenuItemClickListener() {
    			@Override
    			public void onClick(Context ctx) {
    				CreateStudentCircleActivity.showActivity(getActivity());
    			}
    		};
       	 	childsCirculos.add(new MenuHomeChild(getString(R.string.menu_criar_circulo_aluno), R.drawable.icn_menu_novo_circulo, clickCriarCirculoAluno));
        }
        if (showParticiparCirculo) {
        	MenuItemClickListener clickParticiparCirculo = new MenuItemClickListener() {
    			@Override
    			public void onClick(Context ctx) {
    				StudentJoinCircleActivity.showActivity(getActivity());
    			}
    		};
       	 	childsCirculos.add(new MenuHomeChild(getString(R.string.menu_participar_circulo), R.drawable.icn_menu_novo_circulo, clickParticiparCirculo));
        }        
        
        //circulos
        if (localUser.getCircles() != null) {
        	for (final Circle circle : localUser.getCircles()) {
        		if (circle.isPublic()) {
        			continue;
        		}
        		MenuItemClickListener clickCirculo = new MenuItemClickListener() {
        			@Override
        			public void onClick(Context ctx) {
        				ViewCircleActivity.showActivity(ctx, circle);
        			}
        		};
        		
        		childsCirculos.add(new MenuHomeChild(circle.getName(), circle.getProfileImageUrl(), clickCirculo));
        		
        	}
        }
        
        if (childsCirculos.size() > 0) {
        	items.add(new MenuHomeGroup(getString(R.string.menu_meus_circulos), R.drawable.icn_menu_meus_circulos, null, childsCirculos));	 
        }
        
        
		MenuItemClickListener clickSettings = new MenuItemClickListener() {
			@Override
			public void onClick(Context ctx) {
				ctx.startActivity(new Intent(ctx, SettingsActivity.class));
			}
		};
		MenuItemClickListener clickNotification = new MenuItemClickListener() {
			@Override
			public void onClick(Context ctx) {
				ctx.startActivity(new Intent(ctx, NotificationActivity.class));
			}
		};
		
		
		MenuItemClickListener clickInviteFriend = new MenuItemClickListener() {
			@Override
			public void onClick(Context ctx) {
				//InviteFriendActivity.showActivity(getActivity(), Preferences.getLocalUser(getActivity()).getRemainingInvites());
				shareHippApp();
			}
		};
		
		MenuItemClickListener clickMeuHistorico = new MenuItemClickListener() {
			@Override
			public void onClick(Context ctx) {
				ViewUserProfileActivity.showActivity(getActivity(), Preferences.getLocalUser(getActivity()).getUser());
			}
		};
		
		MenuItemClickListener clickAbout = new MenuItemClickListener() {
			@Override
			public void onClick(Context ctx) {
				AboutActivity.showActivity(ctx);
			}
		};
        
		items.add(new MenuHomeGroup(getString(R.string.menu_notificacoes), R.drawable.icn_menu_notificacoes, clickNotification));
		items.add(new MenuHomeGroup(getString(R.string.menu_meu_historico), R.drawable.icn_menu_historico, clickMeuHistorico));
		if (showConvidarAmigos) {
			items.add(new MenuHomeGroup(getString(R.string.menu_convidar_amigos), R.drawable.icn_menu_convidar, clickInviteFriend));
		}
		//items.add(new MenuHomeGroup(getString(R.string.menu_minha_assinatura), R.drawable.icn_menu_assinatura));
		items.add(new MenuHomeGroup(getString(R.string.menu_settings), R.drawable.icn_menu_settings, clickSettings));
		items.add(new MenuHomeGroup(getString(R.string.menu_sobre), R.drawable.icn_menu_sobre, clickAbout));
       
		items.add(new MenuHomeGroup("", null));
       
       	items.add(new MenuHomeGroup(getString(R.string.menu_logout), R.drawable.icn_menu_logout, new MenuItemClickListener() {
 			@Override
 			public void onClick(Context ctx) {
 				executeLogout(localUser);
 			}
 		}, null));
       	
       	return items;
    }
    
    private void shareHippApp() {
    	Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
     
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        share.putExtra(Intent.EXTRA_TEXT, "http://www.hippokrates.us/");
     
        startActivity(Intent.createChooser(share, getString(R.string.menu_convidar_amigos)));
    }
    
    private void executeLogout(LocalUser localUser) {
    	if (localUser != null) {
    		LogoutBackgroundService.callService(getActivity(), localUser.getAuthToken());
    	}

    	Preferences.logout(getActivity());
    	getActivity().startActivity(LoginSigninActivity.getIntentActivity(getActivity()));
    	getActivity().finishAffinity();
    }
}
