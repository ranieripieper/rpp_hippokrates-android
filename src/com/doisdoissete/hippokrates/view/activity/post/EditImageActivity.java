package com.doisdoissete.hippokrates.view.activity.post;

import java.io.File;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.service.SaveFileService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.view.custom.DrawingView;
import com.nispok.snackbar.Snackbar.SnackbarDuration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

@EActivity(R.layout.post_edit_image_activity)
public class EditImageActivity extends PostEditImageBaseActivity {
	
	private float smallBrush, largeBrush;
	
	@ViewById(R.id.drawing_view)
	DrawingView drawingView;
	
	@ViewById(R.id.layout_tools_pencil)
	View layoutToolsPencil;
	@ViewById(R.id.tools_options)
	View toolsOptions;
	@ViewById(R.id.paleta_cores)
	View paletaCores;
	
	@ViewById(R.id.icon_selected)
	ImageView imgIconSelected;

	@ViewById(R.id.bt_ok)
	View btOk;
	
	@ViewById(R.id.color_white)
	ImageView colorWhite;
	@ViewById(R.id.color_black)
	ImageView colorBlack;
	@ViewById(R.id.color_blue)
	ImageView colorBlue;
	@ViewById(R.id.color_green)
	ImageView colorGreen;
	@ViewById(R.id.color_red)
	ImageView colorRed;
	@ViewById(R.id.color_yellow)
	ImageView colorYellow;
	
	@ViewById(R.id.large_size)
	ImageView largeSize;
	@ViewById(R.id.small_size)
	ImageView smallSize;

	int i = 1;	   
	
	@AfterViews
	void onPostCreate() {
		pathImg = getIntent().getExtras().getString(PARAM_FILE_PATH);
		post = getIntent().getExtras().getParcelable(PARAM_POST);
		
		drawingView.setPaintEnabled(false);
		drawingView.setPaintBlur(false);
		
		if (!TextUtils.isEmpty(pathImg)) {
			try {
				Bitmap bmp = BitmapUtil.media_getBitmapFromFile(new File(pathImg),
						ScreenUtil.getDisplayWidth(getContext()));

				drawingView.setBackground(bmp);
				
			} catch (Exception e) {
			}
		} else {
			HippokratesApplication.imageLoader.loadImage(post.getImageUrl(), new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					showWaitDialog(true);
				}
				
				@Override
				public void onLoadingFailed(String imageUri, View view,
						FailReason failReason) {
					showMessageAndFinish(R.string.msg_generic_error);
				}
				
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					drawingView.setBackground(loadedImage);
					dismissWaitDialog();
				}
				
				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					dismissWaitDialog();
				}
			});
		}
		
		//sizes from dimensions
		smallBrush = getResources().getInteger(R.integer.small_size);
		largeBrush = getResources().getInteger(R.integer.large_size);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		showMessage(R.string.msg_perservar_paciente, SnackbarDuration.LENGTH_LONG);
	}
	
	private void saveFile() {
		ObserverAsyncTask<File> observer = new ObserverAsyncTask<File>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(File file) {
				if (file != null) {
					if (post != null) {
						PostFotoActivity.showActivity(EditImageActivity.this, file.getAbsolutePath(), file.getAbsolutePath(), post);
					} else {
						PostFotoActivity.showActivity(getContext(), file.getAbsolutePath(), file.getAbsolutePath(), getFilePathCrop());
					}
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		params.add(SaveFileService.PARAM_BITMAP, drawingView.getBitmap());
		params.add(SaveFileService.PARAM_PREFIX, "img_to_post");
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setObserverAsyncTask(observer)
			.setNeedConnection(false)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), File.class, new SaveFileService(sb, getString(R.string.base_dir_save_images)));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void resetColorsSelected() {
		colorWhite.setImageResource(R.drawable.square_shape);
		colorBlack.setImageResource(R.drawable.square_shape);
		colorBlue.setImageResource(R.drawable.square_shape);
		colorGreen.setImageResource(R.drawable.square_shape);
		colorRed.setImageResource(R.drawable.square_shape);
		colorYellow.setImageResource(R.drawable.square_shape);
	}
	
	@Click(R.id.color_white)
	void colorWhiteClick() {
		resetColorsSelected();
		colorWhite.setImageResource(R.drawable.square_shape_selected);
		changeColor(R.color.edit_image_white);
	}
	
	@Click(R.id.color_black)
	void colorBlackClick() {
		resetColorsSelected();
		colorBlack.setImageResource(R.drawable.square_shape_selected);
		changeColor(R.color.edit_image_black);
	}
	
	@Click(R.id.color_yellow)
	void colorYellowClick() {
		resetColorsSelected();
		colorYellow.setImageResource(R.drawable.square_shape_selected);
		changeColor(R.color.edit_image_yellow);
	}
	
	@Click(R.id.color_red)
	void colorRedClick() {
		resetColorsSelected();
		colorRed.setImageResource(R.drawable.square_shape_selected);
		changeColor(R.color.edit_image_red);
	}
	
	@Click(R.id.color_blue)
	void colorBlueClick() {
		resetColorsSelected();
		colorBlue.setImageResource(R.drawable.square_shape_selected);
		changeColor(R.color.edit_image_blue);
	}
	
	@Click(R.id.color_green)
	void colorGreenClick() {
		resetColorsSelected();
		colorGreen.setImageResource(R.drawable.square_shape_selected);
		changeColor(R.color.edit_image_green);
	}
	
	@Click(R.id.large_size) 
	void largeSizeClick() {
		largeSize.setImageResource(R.drawable.circle_shape_selected);
		smallSize.setImageResource(R.drawable.circle_shape);
		changeSize(largeBrush);
	}
	
	@Click(R.id.small_size) 
	void smallSizeClick() {
		largeSize.setImageResource(R.drawable.circle_shape);
		smallSize.setImageResource(R.drawable.circle_shape_selected);
		changeSize(smallBrush);
	}
	
	@Click(R.id.tools_close)
	void closeToolsClick() {
		layoutToolsPencil.setVisibility(View.GONE);
		toolsOptions.setVisibility(View.VISIBLE);
		drawingView.setPaintEnabled(false);
		btOk.setVisibility(View.VISIBLE);
	}
	
	@Click(R.id.tools_pencil)
	void pencilClick() {
		drawingView.setPaintEnabled(true);
		drawingView.setPaintBlur(false);
		imgIconSelected.setImageResource(R.drawable.icn_camera_pencil);
		layoutToolsPencil.setVisibility(View.VISIBLE);
		toolsOptions.setVisibility(View.GONE);
		paletaCores.setVisibility(View.VISIBLE);
		btOk.setVisibility(View.GONE);
	}
	
	@Click(R.id.tools_blur)
	void blurClick() {
		drawingView.setPaintEnabled(true);
		drawingView.setPaintBlur(true);
		imgIconSelected.setImageResource(R.drawable.icn_camera_blur);
		layoutToolsPencil.setVisibility(View.VISIBLE);
		toolsOptions.setVisibility(View.GONE);
		paletaCores.setVisibility(View.GONE);	
		btOk.setVisibility(View.GONE);
	}
	
	@Click(R.id.bt_ok)
	void btOkClick() {
		saveFile();
	}
	
	@Click(R.id.tools_reset)
	void resetClick() {
		drawingView.startNew();
	}
	
	private void changeSize(float size) {
		drawingView.setBrushSize(size);
	}
	
	private void changeColor(int color) {
		int colorInt = getResources().getColor(color);
		String strColor = "#"+Integer.toHexString(colorInt);
		
		drawingView.setColor(strColor);
	}

	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}

	@Override
	protected boolean showHomeEnabled() {
		return true;
	}

	@Override
	protected void onDestroy() {
		deleteFileCrop();
		super.onDestroy();
	}
	
	public static void showActivity(Context ctx, String pathImg, String pathCrop) {
		Intent it = new Intent(ctx, EditImageActivity_.class);
		it.putExtra(PARAM_FILE_PATH, pathImg);
		it.putExtra(PARAM_FILE_PATH_CROP, pathCrop);
		ctx.startActivity(it);
	}
	
	public static void showActivity(Context ctx, Post post) {
		Intent it = new Intent(ctx, EditImageActivity_.class);
		it.putExtra(PARAM_POST, post);
		ctx.startActivity(it);
	}
}
