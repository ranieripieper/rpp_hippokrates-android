package com.doisdoissete.hippokrates.view.activity.post;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.Comment;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.model.result.CommentResult;
import com.doisdoissete.hippokrates.service.CommentService;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.base.comment.CommentViewUtil;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewInterface;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewUtil;
import com.doisdoissete.hippokrates.view.activity.circle.ViewCircleActivity;
import com.doisdoissete.hippokrates.view.activity.search.HashtagFeedActivity;
import com.doisdoissete.hippokrates.view.adapter.post.CommentAdapter;
import com.doisdoissete.hippokrates.view.adapter.post.RefreshPostAdapter;

public class DetailPostActivity extends HippBaseActivity implements RefreshPostAdapter, PostViewInterface {

	private static final String PARAM_POST = "PARAM_POST";
	private static final String PARAM_POST_ID = "PARAM_POST_ID";
	
	private PagingListView mListView;
	private CommentAdapter commentAdapter;
	private int page = 0;
    private Post post;
    private TextViewPlus txtLikes;
    private ImageView imgLikes;
    private TextView txtComments;
    private ImageView imgComments;
    private TextView txtData;
	private ImageView imgUser;
	private TextViewPlus txtDescricao;
	private ImageView imgFoto;
	private LocalUser localUser;
	private Long postId;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.detail_post_activity);
    	localUser = getLocalUser();
    	
    	mListView = (PagingListView)findViewById(R.id.layout_content);
    	imgUser = (CircleImageView)findViewById(R.id.img_header_user);
    	
		mListView.setHasMoreItems(true);
		mListView.hideTxtCarregando();
		post = getIntent().getExtras().getParcelable(PARAM_POST);
		postId = getIntent().getExtras().getLong(PARAM_POST_ID);

		initComponents();
		
		if (post != null) {
			postId = post.getId();
		} else {
			callFetchPost();
		}
		
		mListView.setOnTouchListener(new OnTouchListener() {
			GestureDetector gestureDetector = new GestureDetector(new SimpleOnGestureListener() {
            	@Override
            	public boolean onDoubleTap(MotionEvent e) {
            		PostViewUtil.likeDislikePost(getContext(), DetailPostActivity.this, post, null);
            		updateImageLike();
            		return super.onDoubleTap(e);
            	}
            	
            	@Override
            	public boolean onSingleTapConfirmed(MotionEvent e) {
            		return super.onSingleTapConfirmed(e);
            	}
            	
            	@Override
            	public void onLongPress(MotionEvent e) {
            		int position = mListView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		if (position == 0) {
    					//header
    					onLongPostClick();
    				} else {
    					Comment comment = commentAdapter.getItem(position-1);
    					if ((comment.getUserId() != null && comment.getUserId().equals(localUser.getId())) ||
    							(comment.getUser() != null && comment.getUser().getId() != null && comment.getUser().getId().equals(localUser.getId()))) {
    						showDialogCommentUser(comment);
    					} else {
    						showDialogCommentNotUser(comment);
    					}
    				}
            		super.onLongPress(e);
            	}
            });
			@Override
	         public boolean onTouch(View v, MotionEvent event) {
	                return gestureDetector.onTouchEvent(event);
	         }
		});
		
		if (commentAdapter == null) {
			mListView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					page++;
					callService();
				}
			});
			commentAdapter = new CommentAdapter(getContext(), new ArrayList<Comment>());
			mListView.setAdapter(commentAdapter);
		}
		prepareScreen();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	if (post != null) {
	    	LocalPost localPost = LocalPost.getLocalPost(post);
	    	if (localPost != null) {
	    		post.setDescricao(localPost.getDescription());
	    		post.setImageUrl(localPost.getImageUrl());
	    		if (txtDescricao != null) {
	    			txtDescricao.setText(post.getDescricao());
		    		HippokratesApplication.imageLoader.displayImage(post.getImageUrl(), imgFoto);
	    		}	    		
	    	}
    	}
    }
    
    private void callFetchPost() {
    	ObserverAsyncTask<Post> observer = new ObserverAsyncTask<Post>() {
			@Override
			public void onPreExecute() {	
				findViewById(R.id.layout_include_nr_likes_comments).setVisibility(View.GONE);
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(Post result) {
				if (result != null) {
					findViewById(R.id.layout_include_nr_likes_comments).setVisibility(View.VISIBLE);
					post = result;
					if (post != null && post.getBasePost() != null) {
						post.setDescricao(post.getBasePost().getContent());
						post.setImageUrl(post.getBasePost().getImagesUrl());
					}
					prepareScreen();
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				dismissWaitDialog();
			}
		};


		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.POST_FETCH.replace(UrlParameters.PARAM_POST_ID_URL, String.valueOf(postId)))
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), Post.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
    }
    
    private void prepareScreen() {
    	if (post != null) {
    		findViewById(R.id.layout_content).setVisibility(View.VISIBLE);
    		setTitle(post.getUser().getFullName());
    		createHeaderLikesComments();
    		createHeaderListView();
    		HippokratesApplication.imageLoaderUser.displayImage(post.getUser().getProfileImageUrl(), imgUser);
    	}		
    }
    
    private void initComponents() {
    	txtLikes = (TextViewPlus)findViewById(R.id.txt_likes);
    	imgLikes = (ImageView)findViewById(R.id.img_likes);
		txtComments = (TextView)findViewById(R.id.txt_comments);
		imgComments = (ImageView)findViewById(R.id.img_comments);
		txtData = (TextView)findViewById(R.id.txt_data);
    }
    
    @Override
    public void removedPosts(List<Long> ids) {
    	showMessageAndFinish(R.string.msg_foto_deletada);
    }
    
    private void showDialogEditDeletePost() {
    	showOptionsDialog(new int[] { R.string.edit_post, 
    			R.string.delete_post}, new View.OnClickListener [] {
    			new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						EditImageActivity.showActivity(DetailPostActivity.this, post);
					}
				},
				new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						showDefaultDialog(getString(R.string.sim), getString(R.string.nao), getString(R.string.msg_pergunta_delete_foto), new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								PostViewUtil.callServiceDeletePosts(getContext(), DetailPostActivity.this, DetailPostActivity.this, Arrays.asList(post.getId()));
							}
						});						
					}
				}
    	} );
    }

    private void showDialogDenunciarDeletePostAdmin() {
    	showOptionsDialog(new int[] { R.string.denunciar_foto, 
    			R.string.delete_post}, new View.OnClickListener [] {
    			new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						showDialogOptionsDenunciarPost(getContext(), postId, DetailPostActivity.this);
					}
				},
				new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						showDefaultDialog(getString(R.string.sim), getString(R.string.nao), getString(R.string.msg_pergunta_delete_foto_admin), new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								PostViewUtil.callServiceDeletePostAdmin(getContext(), DetailPostActivity.this, DetailPostActivity.this, post.getId(), post.getCircle().getId());
							}
						});						
					}
				}
    	} );
    }
    
    private void showDialogCommentNotUser(final Comment comment) {
    	showOptionsDialog(new int[] {R.string.denunciar_comentario}, new View.OnClickListener [] {
    			new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						denunciarComentario(comment);
					}
				}
    	} );
    }
    
    private void showDialogCommentUser(final Comment comment) {
    	showOptionsDialog(new int[] {R.string.editar_comentario,  R.string.deletar_comentario}, new View.OnClickListener [] {
    			new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						editarComentario(comment);
					}
				}, 
				new View.OnClickListener() {					
					@Override
					public void onClick(View v) {
						deletarComentario(comment);
					}
				}
    	} );
    }
    
    private void denunciarComentario(Comment comment) {
    	CommentViewUtil.denunciarComentario(getContext(), this, post.getId(), comment.getId());
    }
    
    private void editarComentario(Comment comment) {
    	CommentViewUtil.showWriteCommentDialog(getContext(), post, this, comment.getContent(), comment.getId(), this);
    }
    
    private void deletarComentario(final Comment comment) {
    	showDefaultDialog(getString(R.string.sim), getString(R.string.nao), getString(R.string.deseja_deletar_comentario), new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				callDeleteComment(comment);
			}
		});
    }
    
    private void callDeleteComment(final Comment comment) {
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);		
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					commentAdapter.removeItem(comment);
					post.reduceQtComentarios();
					txtComments.setText(post.getQtComentariosStr());
					LocalPost.updateCommentsCount(post.getId(), post.getCircleId(), post.getQtComentarios());
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		
		//delete
		String url = Paths.DELETE_COMMENT_POST.replace(UrlParameters.PARAM_POST_ID_URL, String.valueOf(post.getId())).replace(UrlParameters.PARAM_COMMENT_ID_URL, String.valueOf(comment.getId()));
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.DELETE)
			.setParams(params);
		
		async = sb.mappingInto(getContext(), BaseResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}

    private void createHeaderLikesComments() {
    	
		updateImageLike();
		txtComments.setText(post.getQtComentariosStr());
		txtLikes.setText(post.getQtLikesStr());
		txtData.setText(post.getCreatedAtStr());
		
		imgLikes.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {				
				PostViewUtil.likeDislikePost(getContext(), DetailPostActivity.this, post, null);
				
				updateImageLike();
			}
		});
    }
    
    private void createHeaderListView() {
    	if (post != null) {
    		View vi = getLayoutInflater().inflate(R.layout.include_post_detail, null);
    		txtDescricao = (TextViewPlus)vi.findViewById(R.id.txt_descricao);
    		TextView txtCirculo = (TextView)vi.findViewById(R.id.txt_circulo);
			TextView txtNomeMedico = (TextView)vi.findViewById(R.id.txt_nome_medico);
			imgFoto = (ImageView)vi.findViewById(R.id.img_foto);
			ImageView imgCirculo = (ImageView)vi.findViewById(R.id.img_circulo);
			ImageView btMore = (ImageView)vi.findViewById(R.id.bt_more);
			
			btMore.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					onLongPostClick();
				}
			});
			
			TextViewPlus.TagClick tagClick = new TextViewPlus.TagClick() { 
				@Override
				public void clickedTag(String tag) {
					HashtagFeedActivity.showActivity(getContext(), tag);
				}
			};
			txtDescricao.setText(post.getDescricao(), true, tagClick);
			
			//comentários
			imgComments.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					CommentViewUtil.showWriteCommentDialog(getContext(), post, DetailPostActivity.this, DetailPostActivity.this);
					
				}
			});
			txtComments.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					CommentViewUtil.showWriteCommentDialog(getContext(), post, DetailPostActivity.this, DetailPostActivity.this);
				}
			});
			
			if (post.getCircle() != null) {
				//se o circulo for público, mostra a especialidade
				final Circle circle;
				
				if (post.getCircle().isPublic()) {
					//circulo fake de especialidade
					circle = new Circle(post.getMedicalSpecialty());
				} else {
					circle = post.getCircle();
				}
				
				txtCirculo.setText(circle.getName());
				HippokratesApplication.imageLoaderCircle.displayImage(circle.getProfileImageUrl(), imgCirculo);
				txtCirculo.setVisibility(View.VISIBLE);
				imgCirculo.setVisibility(View.VISIBLE);
				txtCirculo.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ViewCircleActivity.showActivity(getContext(), circle);
					}
				});
				imgCirculo.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ViewCircleActivity.showActivity(getContext(), circle);
					}
				});
			} else {
				txtCirculo.setVisibility(View.GONE);
				imgCirculo.setVisibility(View.GONE);
			}
			txtNomeMedico.setVisibility(View.GONE);
			
			HippokratesApplication.imageLoader.displayImage(post.getImageUrl(), imgFoto);
			
			mListView.addHeaderView(vi);
			
    	}
    }
    
    private void onLongPostClick() {
    	if (post != null && post.getUserId() != null && post.getUserId().equals(localUser.getId())) {
			showDialogEditDeletePost();
		} else if (post != null) {
			if (post.getCircle() != null && localUser.getId().equals(post.getCircle().getAdminUserId())) {
				showDialogDenunciarDeletePostAdmin();
			} else {
				showDialogDenunciarPost(getContext(), post.getId(), this);
			}
		}
    }
    
	private void callService() {
		ObserverAsyncTask<CommentResult> observer = new ObserverAsyncTask<CommentResult>() {
			@Override
			public void onPreExecute() {				
			}

			@Override
			public void onPostExecute(CommentResult result) {
				if (result != null) {
					populateListView(result);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				page--;
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				page--;
			}
		};
		
		if (page <= 0) {
			page = 1;
		}


		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.POST_COMMENTS.replace(UrlParameters.PARAM_POST_ID_URL, String.valueOf(post.getId())))
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), CommentResult.class, new CommentService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void populateListView(CommentResult result) {
		if (result == null) {
			return;
		}
		if (commentAdapter == null) {
			mListView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					page++;
					callService();
				}
			});
			commentAdapter = new CommentAdapter(getContext(), new ArrayList<Comment>());
			mListView.setAdapter(commentAdapter);
		}
		
		boolean loadMore = true;
		if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
			if (page < result.getMeta().getPagination().getTotalPages()) {
				loadMore = true;
			} else {
				loadMore = false;
			}
			post.setQtComentarios(new Long(result.getMeta().getPagination().getTotalCount()));
			txtComments.setText(post.getQtComentariosStr());			
		} else {
			if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > result.getComments().size()) {
				loadMore = false;
	    	} else {
	    		loadMore = true;
	    	} 
		}

		mListView.onFinishLoading(loadMore, result.getComments());
		mListView.setVisibility(View.VISIBLE);
    	commentAdapter.notifyDataSetChanged();
    	mListView.requestLayout();
		
	}
	
	private void updateImageLike() {
		if (post != null && post.isUserLiked()) {
			imgLikes.setImageResource(R.drawable.icn_home_liked);
		} else {
			imgLikes.setImageResource(R.drawable.icn_home_likes);
		}
		if (post != null) {
			txtLikes.setText(post.getQtLikesStr());
		}
	}
	
	@Override
	public void refreshQtdeComments(Comment comment, boolean edit) {
		if (!edit) {
			if (comment.getUser() == null) {
				comment.setUser(Preferences.getLocalUser(getContext()).getUser());
			}
			if (commentAdapter != null && page > 0) {
				commentAdapter.addFirstPosition(comment);
			}
						
		} else {
			for (int i = 0; i < commentAdapter.getCount(); i++) {
				Comment commentTmp = commentAdapter.getItem(i);
				if (comment.getId().equals(commentTmp.getId())) {
					commentTmp.setContent(comment.getContent());
					break;
				}
			}
		}
		commentAdapter.notifyDataSetChanged();
		
		txtComments.setText(post.getQtComentariosStr());
	}
	
	public static Intent getIntentActivity(Context ctx, Long postId) {
		Intent it = new Intent(ctx, DetailPostActivity.class);
		it.putExtra(PARAM_POST_ID, postId);
		return it;
	}
	
	public static void showActivity(Context ctx, Post post) {
		Intent it = new Intent(ctx, DetailPostActivity.class);
		it.putExtra(PARAM_POST, post);
		ctx.startActivity(it);
	}
	
	@Override
	protected void onDestroy() {
		Preferences.setLastSeenNotification(getContext(), new Date());
		super.onDestroy();
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	@Override
	protected boolean homeAsUp() {
		return true;
	}
}