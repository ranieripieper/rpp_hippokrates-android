package com.doisdoissete.hippokrates.view.activity.post;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.ScreenUtil;
import com.doisdoissete.android.util.ddsutil.view.ViewUtil;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.PostFotoResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.service.PostPhotoService;
import com.doisdoissete.hippokrates.service.UpdatePostPhotoService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.HomeActivity;
import com.doisdoissete.hippokrates.view.activity.select.SelectFromServerActivity;
import com.doisdoissete.hippokrates.view.custom.HipAppDdsSwitch;

@EActivity(R.layout.post_foto_activity)
public class PostFotoActivity extends PostEditImageBaseActivity {
	
	private static final int ESPECIALIDADE_REQUEST_CODE = 998;
	private String filePath;
	
	@ViewById(R.id.img_foto)
	ImageView imgFoto;
	
	@ViewById(R.id.edt_categoria)
	EditTextPlus edtCategoria;
	
	@ViewById(R.id.edt_descricao)
	EditTextPlus edtDescricao;
	
	@ViewById(R.id.switch_privacidade)
	HipAppDdsSwitch switchPrivacidade;
	
	@ViewById(R.id.layout_circulos)
	LinearLayout layoutCirculos;
	
	@ViewById(R.id.txt_compartilha_foto)
	TextViewPlus txtCompartilhaFoto;
	
	private long idCategoria;
	
	private Set<Long> circulos = new LinkedHashSet<Long>();
	
	private LocalUser localUser;
	
	@AfterViews
	void prepareScreen() {
		localUser = getLocalUser();
		
		if (localUser.isStudent()) {
			findViewById(R.id.layout_privacidade).setVisibility(View.GONE);
		}
		filePath = getIntent().getExtras().getString(PARAM_FILE_PATH);
		post = getIntent().getExtras().getParcelable(PARAM_POST);
		
		if (post != null) {
			edtDescricao.setText(post.getDescricao());
			if (post.getMedicalSpecialty() != null) {
				idCategoria = post.getMedicalSpecialty().getId();
				edtCategoria.setText(post.getMedicalSpecialty().getName());
			}
			edtCategoria.setVisibility(View.GONE);
			layoutCirculos.setVisibility(View.GONE);
			findViewById(R.id.layout_privacidade).setVisibility(View.GONE);
		}
		Bitmap bitmap;

		try {
			bitmap = BitmapUtil.media_getBitmapFromFile(new File(filePath),
					ScreenUtil.getDisplayWidth(getContext()));

			if (bitmap != null) {
				imgFoto.setImageBitmap(bitmap);
				imgFoto.requestLayout();
				imgFoto.invalidate();
			}				
		} catch (Exception e) {
		}
		
		switchPrivacidade.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					modifyAlpha(1f);
					ViewUtil.enableDisableView(layoutCirculos, true);
				} else {
					modifyAlpha(0.5f);
					ViewUtil.enableDisableView(layoutCirculos, false);
				}
			}
		});
		
		includeCircles();
	}
	
	private void modifyAlpha(float alpha) {
		layoutCirculos.setAlpha(alpha);
	}
	
	private void includeCircles() {
		LocalUser user = getLocalUser();
		if (user != null && user.getCircles() != null) {
			for (Circle circle : user.getCircles()) {
				if (!Constants.PUBLIC_CIRCLE_TYPE.equals(circle.getCircleType())) {
					includeCircle(circle);
				}				
			}
		}
	}
	
	private void includeCircle(final Circle circle) {
		View v = getLayoutInflater().inflate(R.layout.include_circulo_post, null);
		CheckBox checkBox = (CheckBox)v.findViewById(R.id.ck_circulo);
		ImageView img = (ImageView)v.findViewById(R.id.img_foto);
		TextViewPlus txtCirculo = (TextViewPlus)v.findViewById(R.id.txt_circulo);
		HippokratesApplication.imageLoaderCircle.displayImage(circle.getProfileImageUrl(), img);
		txtCirculo.setText(circle.getName());
		checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					circulos.add(circle.getId());
				} else {
					circulos.remove(circle.getId());
				}
			}
		});
		layoutCirculos.addView(v);
	}
	
	@Click(R.id.edt_categoria)
	void categoriaClick() {
		edtDescricao.hiddenKeyboard(getContext());
		SelectFromServerActivity.showActivity(PostFotoActivity.this, ESPECIALIDADE_REQUEST_CODE, R.drawable.icn_camera_category, R.string.categoria, Paths.MEDICAL_SPECIALITIES);
	}
	
	@Click(R.id.bt_enviar_foto)
	void enviarFotoClick() {
		edtDescricao.hiddenKeyboard(getContext());
		edtCategoria.hiddenKeyboard(getContext());
		if (post != null) {
			if (edtDescricao.isNotValid()) {
				showMessage(R.string.msg_preencha_campos_corretamente);
				return;
			}
		} else {
			if (edtCategoria.isNotValid() || edtDescricao.isNotValid()) {
				showMessage(R.string.msg_preencha_campos_corretamente);
				return;
			} else if (switchPrivacidade.isChecked() && circulos.isEmpty()) {
				showMessage(R.string.msg_escolha_pelo_menos_um_circulo);
				return;
			}
		}
		callService();
	}
	
	private void callService() {
		ObserverAsyncTask<PostFotoResult> observer = new ObserverAsyncTask<PostFotoResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(PostFotoResult result) {
				if (result != null && result.isSuccess() && post == null) {
					showMessageFinishAllAndStartActivity(R.string.foto_enviada_sucesso, HomeActivity.getIntentActivity(PostFotoActivity.this));
				} else if (result != null && result.isSuccess() && post != null) {
			    	showMessage(R.string.foto_enviada_sucesso);
			    	blockScreen(true);
			    	new Handler().postDelayed(new Runnable() {
			    		@Override
			    		public void run() {
			    			//finalizar activity crop
			    			Intent result = new Intent();
							result.putExtra(FINISH_ACTIVITY, true);
							setResult(Activity.RESULT_OK, result);
							finish();
			    		}
			    	}, 1000);
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, Object> params = BaseService.createParams(edtDescricao);
		
		String privacy = Constants.PRIVACY_PUBLIC;
		if (switchPrivacidade.isChecked()) {
			privacy = Constants.PRIVACY_PRIVATE;			
			for (Long id : circulos) {				
				params.add(UrlParameters.PARAM_POST_CIRCLES_ID, String.valueOf(id));
			}
		}
		params.add(UrlParameters.PARAM_POST_PRIVACY_STATUS, privacy);
		params.add(UrlParameters.PARAM_POST_MEDICAL_SPECIALTY, String.valueOf(idCategoria));
		params.add(UrlParameters.PARAM_POST_FILE_PATH, filePath);
		
		boolean uploadFile = false;
		if (!TextUtils.isEmpty(filePath)) {
			params.add(UrlParameters.PARAM_POST_IMAGE, new FileSystemResource(filePath));
			uploadFile = true;
		}
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		
		String url = null;
		BaseService service = null;
		if (post != null) {
			url = Paths.UPDATE_POST_FOTO;
			service = new UpdatePostPhotoService(sb);
			params.add(UrlParameters.PARAM_POST_ID, String.valueOf(post.getBasePostId()));
		} else {
			url = Paths.POST_FOTO;
			service = new PostPhotoService(sb);
		}
		
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setUploadFile(uploadFile)
			.setHttpMethod(HttpMethod.POST)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		if (post != null) {
			async = sb.mappingInto(getContext(), PostFotoResult.class, service);
		} else {
			async = sb.mappingInto(getContext(), PostFotoResult.class, service);
		}
		
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == ESPECIALIDADE_REQUEST_CODE && resultCode == RESULT_OK) {
			String result = data.getExtras().getString(SelectFromServerActivity.TEXT_RESULT);
			edtCategoria.setText(result);
			idCategoria = data.getExtras().getLong(SelectFromServerActivity.ID_RESULT);
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	@Override
	protected void onDestroy() {
		deleteFileEdit();
		deleteFileCrop();
		deleteFilePath();
		super.onDestroy();
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	public static void showActivity(Context ctx, String filePath, String filePathEdit, String filePathCrop) {
		Intent it = new Intent(ctx, PostFotoActivity_.class);
		it.putExtra(PARAM_FILE_PATH, filePath);
		it.putExtra(PARAM_FILE_PATH_CROP, filePathCrop);
		it.putExtra(PARAM_FILE_PATH_EDIT, filePathEdit);
		ctx.startActivity(it);
	}
	
	public static void showActivity(Activity ctx, String filePath, String filePathEdit, Post post) {
		Intent it = new Intent(ctx, PostFotoActivity_.class);
		it.putExtra(PARAM_FILE_PATH, filePath);
		it.putExtra(PARAM_FILE_PATH_EDIT, filePathEdit);
		it.putExtra(PARAM_POST, post);
		ctx.startActivityForResult(it, REQUEST_CODE_FINISH);
	}
}
