package com.doisdoissete.hippokrates.view.activity.post;

import java.io.File;
import java.util.List;

import android.content.Intent;

import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class PostEditImageBaseActivity extends HippBaseActivity {

	protected final static String PARAM_FILE_PATH = "PARAM_FILE_PATH";
	protected final static String PARAM_FILE_PATH_CROP = "PARAM_FILE_PATH_CROP";
	protected final static String PARAM_FILE_PATH_EDIT = "PARAM_FILE_PATH_EDIT";
	protected final static String PARAM_POST = "PARAM_POST";	
	protected final static String FINISH_ACTIVITY = "FINISH_ACTIVITY";
	protected final static int REQUEST_CODE_FINISH = 981;
	
	protected String pathImg;
	protected Post post;
	
	protected List<String> filesDelete;
	
	protected String getFilePathCrop() {
		return getIntent().getExtras().getString(PARAM_FILE_PATH_CROP);
	}
	
	protected String getFilePathEdit() {
		return getIntent().getExtras().getString(PARAM_FILE_PATH_EDIT);
	}
	
	protected String getFilePath() {
		return getIntent().getExtras().getString(PARAM_FILE_PATH);
	}
	
	protected void deleteFileCrop() {
		deleteFileCropEdit(getFilePathCrop());
	}
	
	protected void deleteFileEdit() {
		deleteFileCropEdit(getFilePathEdit());
	}
	
	protected void deleteFilePath() {
		deleteFileCropEdit(getFilePath());
	}
	
	private void deleteFileCropEdit(String file) {
		if (file != null) {
			new File(file).delete();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (REQUEST_CODE_FINISH == requestCode) {
			if (data != null) {
				boolean finish = data.getBooleanExtra(FINISH_ACTIVITY, false);
				if (finish) {
					finish();
				}
			}
			
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
}
