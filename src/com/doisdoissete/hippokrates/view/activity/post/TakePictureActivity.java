package com.doisdoissete.hippokrates.view.activity.post;

import java.io.File;
import java.io.IOException;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.service.SaveFileService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.cameragallery.CameraActivity;
import com.doisdoissete.android.util.ddsutil.view.custom.TouchBlackHoleView;
import com.doisdoissete.android.util.ddsutil.view.custom.loading.LoadingView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class TakePictureActivity extends CameraActivity {
	
	protected AsyncTaskService async;
	protected LoadingView loadingView;
	protected TouchBlackHoleView blackHole;
	protected Toolbar toolbar;
	private Location mLocation;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_take_picture_activity);

		configActionBar();
		loadingView = (LoadingView)findViewById(R.id.progress_bar);
		blackHole = (TouchBlackHoleView)findViewById(R.id.black_hole);
	}
	
	public void showWaitDialog(boolean blockScreen) {
		blockScreen(blockScreen);
		
		if (loadingView != null) {
			loadingView.setVisibility(View.VISIBLE);
			if (findViewById(R.id.layout_include_loading) != null) {
				findViewById(R.id.layout_include_loading).setVisibility(View.VISIBLE);
			}
		}
	}
	
	public void dismissWaitDialog() {
		if (blackHole != null) {
			blackHole.disable_touch(false);
			blackHole.setVisibility(View.GONE);
		}
		if (loadingView != null) {
			loadingView.setVisibility(View.GONE);
		}
		if (findViewById(R.id.layout_include_loading) != null) {
			findViewById(R.id.layout_include_loading).setVisibility(View.GONE);
		}
	}
	
	protected void blockScreen(boolean blockScreen) {
		if (blackHole != null) {
			if (blockScreen) {
				blackHole.disable_touch(true);
				blackHole.setVisibility(View.VISIBLE);
			} else {
				blackHole.disable_touch(false);
				blackHole.setVisibility(View.GONE);
			}
		}
	}

	private void configActionBar() {
    	toolbar = (Toolbar)findViewById(R.id.hipapp_toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(showHomeAsUpEnabled());
	    	getSupportActionBar().setDisplayShowHomeEnabled(showHomeEnabled());
	    	getSupportActionBar().setDisplayHomeAsUpEnabled(showHomeAsUpEnabled());
	    	toolbar.setNavigationOnClickListener(new View.OnClickListener() {
    		    @Override
    		    public void onClick(View v) {
    		        onBackPressed();
    		    }
    		});
		}
    }
    
	@Override
	protected Integer getButtonGalleryId() {
		return R.id.open_gallery;
	}
	
	@Override
	protected Integer getButtonCaptureId() {
		return R.id.button_capture;
	}
	@Override
	protected Integer getButtonSwitchCameraId() {
		return R.id.switch_camera;
	}
	
	@Override
	protected Integer getBottomCoverViewId() {
		return R.id.cover_top_view;
	}

	@Override
	protected Integer getButtonFlashId() {
		return null;
	}

	@Override
	protected Integer getSquareCameraPreviewId() {
		return R.id.camera_preview_view;
	}

	@Override
	protected Integer getTextAutoFlashId() {
		return null;
	}

	@Override
	protected Integer getTopCoverViewId() {
		return R.id.cover_bottom_view;
	}

	
	private void callServiceSaveFile(final byte[] data, final int rotation) {
		ObserverAsyncTask<File> observer = new ObserverAsyncTask<File>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(File file) {
				dismissWaitDialog();
				if (file != null) {
					startPostCropActivity(file.getAbsolutePath(), ExifInterface.ORIENTATION_NORMAL, false);
				} else {
					HippBaseActivity.showDefaultErrorDialog(getContext(), R.string.msg_generic_error);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				startCameraPreview();
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onError(Exception e) {
				HippBaseActivity.showError(TakePictureActivity.this, e);
				startCameraPreview();
			}
		};

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		
		params.add(SaveFileService.PARAM_DATA, data);
		params.add(SaveFileService.PARAM_DATA_ROTATION, rotation);
		params.add(SaveFileService.PARAM_PREFIX, "taked");
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setObserverAsyncTask(observer)
			.setNeedConnection(false)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), File.class, new SaveFileService(sb, getString(R.string.base_dir_save_images)));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void startPostCropActivity(final String filePath, final int orientation, final boolean cropImage) {
		if (cropImage) {
			CropImageActivity.showActivity(getContext(), filePath, orientation);
		} else {
			EditImageActivity.showActivity(getContext(), filePath, null);
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.doisdoissete.android.util.ddsutil.view.cameragallery.CameraActivity#onPictureSelected(java.lang.String, android.graphics.Bitmap)
	 */
	@Override
	protected void onPictureSelected(String filePath, Bitmap bitmap) {
		ExifInterface exif;
		int orientation = ExifInterface.ORIENTATION_NORMAL;

		try {
			exif = new ExifInterface(filePath);
			orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
		} catch (IOException e) {
		}
		
		startPostCropActivity(filePath, orientation, true);
	}
	
	 /**
     * A picture has been taken
     * @param data
     * @param camera
     */
    @Override
    public void onPictureTaken(byte[] data, int rotation) {
    	callServiceSaveFile(data, rotation);
    }
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		dismissWaitDialog();
		super.onActivityResult(requestCode, resultCode, data);
    }
	
	protected boolean showHomeAsUpEnabled() {
		return true;
	}

	protected boolean showHomeEnabled() {
		return true;
	}
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, TakePictureActivity.class));
	}

}