package com.doisdoissete.hippokrates.view.activity.post;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.support.v8.renderscript.*;

import com.doisdoissete.hippokrates.R;

public class BlurTouchImageView extends View {
    private static final int BLUR_RADIUS = 20;
    // TODO: resources should be used
    private static final int BLUR_SIDE = 300;

    private RenderScript mBlurScript = null;
    private ScriptIntrinsicBlur mIntrinsicScript = null;
    private Bitmap mBitmap = null;
    private Bitmap mBlurredBitmap = null;
    private float mX = -1;
    private float mY = -1;

    public BlurTouchImageView(final Context context) {
        super(context);
        init();
    }

    public BlurTouchImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BlurTouchImageView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * Inits our view internal members
     */
    private void init() {
        mBlurScript = RenderScript.create(getContext());
        mIntrinsicScript = ScriptIntrinsicBlur.create(mBlurScript, Element.U8_4(mBlurScript));
        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.teste_imagem);
        mBlurredBitmap = Bitmap.createBitmap(BLUR_SIDE, BLUR_SIDE, mBitmap.getConfig());
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        boolean retval = false;

        mX = event.getX();
        mY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                retval = true;
                invalidate();
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                mX = -1;
                mY = - 1;
                retval = true;
                invalidate();
                break;

            default:
                // nothing to do here
                break;
        }

        return retval;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        // Blur bitmap if it's touched

        canvas.drawBitmap(mBitmap, 0, 0, null);

        if (mX > 0 && mY > 0) {
            // Yeah, it will slooow down drawing, but how else we can prepare needed part of bitmap?
            final Bitmap blurSource = Bitmap.createBitmap(mBitmap, (int) mX - BLUR_SIDE / 2, (int) mY - BLUR_SIDE / 2, BLUR_SIDE, BLUR_SIDE);

            final Allocation inAlloc = Allocation.createFromBitmap(mBlurScript, blurSource, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_GRAPHICS_TEXTURE);
            final Allocation outAlloc = Allocation.createFromBitmap(mBlurScript, mBlurredBitmap);

            mIntrinsicScript.setRadius(BLUR_RADIUS);
            mIntrinsicScript.setInput(inAlloc);
            mIntrinsicScript.forEach(outAlloc);
            outAlloc.copyTo(mBlurredBitmap);

            canvas.drawBitmap(mBlurredBitmap, (int)mX - BLUR_SIDE / 2, (int)mY - BLUR_SIDE / 2, null);
        }
    }
}