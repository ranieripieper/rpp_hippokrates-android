package com.doisdoissete.hippokrates.view.activity.search;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

@EActivity(R.layout.search_activity)
public class SearchActivity extends HippBaseActivity {

	private final int MENU_CATEGORIA = 0;
	private final int MENU_USUARIO = 1;
	private final int MENU_HASHTAG = 2;
	
	@ViewById(R.id.edt_search)
	EditTextPlus edtSearch;
			
	@ViewById(R.id.menu_categoria)
	TextViewPlus menuCategoria;
	@ViewById(R.id.menu_usuario)
	TextViewPlus menuUsuario;
	@ViewById(R.id.menu_hashtag)
	TextViewPlus menuHashTag;
	
	@ViewById(R.id.pager)
	ViewPager mPager;
	
	private int mMenuSelected = -1;
	private boolean refresh = false;
	private BaseSearchFragment actualFragment = null;
	
	/**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private ScreenSlidePagerAdapter mPagerAdapter;
    
    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	if (position == MENU_CATEGORIA) {
        		return SearchEspecialidadesFragment.getInstance();
        	} else if (position == MENU_USUARIO) {
        		return SearchUsuarioFragment.getInstance();
        	} else {
        		return SearchHashtagFragment.getInstance();
        	}
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
    
	@AfterViews
	void postCreate() {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(3);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {			
        	@Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            	showMenu(position);
            	actualFragment = (BaseSearchFragment)mPagerAdapter.getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
		});
		edtSearch.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	        }

	        @Override
	        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	        }

	        @Override
	        public void onTextChanged(CharSequence s, int start, int before, int count) {
	        	if (actualFragment != null) {
	        		actualFragment.refresh();
	        	}
	        	
	        	refresh = true;
	        } 

	    });
		mPager.setCurrentItem(MENU_CATEGORIA);
		actualFragment = (BaseSearchFragment)mPagerAdapter.getItem(MENU_CATEGORIA);
		
		mPager.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				edtSearch.hiddenKeyboard(getContext());
			}
		});
	}
	
	private void showMenu(int menu) {
		edtSearch.hiddenKeyboard(getContext());
		if (menu == mMenuSelected) {
			return;
		}
		mMenuSelected = menu;
		menuCategoria.setTextColor(getResources().getColor(R.color.txt_red));
		menuUsuario.setTextColor(getResources().getColor(R.color.txt_red));
		menuHashTag.setTextColor(getResources().getColor(R.color.txt_red));
		menuCategoria.setBackgroundColor(getResources().getColor(R.color.bg_default));
		menuUsuario.setBackgroundColor(getResources().getColor(R.color.bg_default));
		menuHashTag.setBackgroundColor(getResources().getColor(R.color.bg_default));
		
		Bundle arguments = new Bundle();
		arguments.putBoolean(BaseSearchFragment.PARAM_REFRESH, refresh);
		if (mMenuSelected == MENU_CATEGORIA) {
			menuCategoria.setTextColor(Color.WHITE);
			menuCategoria.setBackgroundColor(getResources().getColor(R.color.bg_red));
			
		} else if (mMenuSelected == MENU_USUARIO) {
			menuUsuario.setTextColor(Color.WHITE);
			menuUsuario.setBackgroundColor(getResources().getColor(R.color.bg_red));
		} else if (mMenuSelected == MENU_HASHTAG) {
			menuHashTag.setTextColor(Color.WHITE);
			menuHashTag.setBackgroundColor(getResources().getColor(R.color.bg_red));
		}
		 
		
		mPager.setCurrentItem(mMenuSelected);
		
	}
	
	@Click(R.id.menu_categoria)
	void menuCategoriaClick() {
		showMenu(MENU_CATEGORIA);
	}
	
	@Click(R.id.menu_usuario)
	void menuUsuarioClick() {
		showMenu(MENU_USUARIO);
	}
	
	@Click(R.id.menu_hashtag)
	void menuHashtagClick() {
		showMenu(MENU_HASHTAG);
	}
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(new Intent(ctx, SearchActivity_.class));
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
