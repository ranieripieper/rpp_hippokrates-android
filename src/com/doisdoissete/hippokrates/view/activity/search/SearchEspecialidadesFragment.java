package com.doisdoissete.hippokrates.view.activity.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.doisdoissete.hippokrates.model.result.SelectServerListResult;
import com.doisdoissete.hippokrates.service.MedicalSpecialitiesService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.view.activity.circle.ViewCircleActivity;

public class SearchEspecialidadesFragment extends BaseSearchFragment {

	private static SearchEspecialidadesFragment instance;
	
	private ArrayList<IdNameDescModel> resultServer = new ArrayList<IdNameDescModel>();
	private ArrayAdapter<IdNameDescModel> arrayAdapter;
	private ListView lstView;
	private boolean findOnlyStartWith = false;
	
    public static SearchEspecialidadesFragment getInstance() {
    	if (instance == null) {
    		instance = new SearchEspecialidadesFragment();
    	}
        return instance;
    }

    public SearchEspecialidadesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_especialidades_fragment, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
    	super.onViewCreated(view, savedInstanceState);

     	lstView = (ListView)getView().findViewById(R.id.listview_categoria);
     	arrayAdapter = new ArrayAdapter<IdNameDescModel>( getActivity(), R.layout.simple_list_item, new ArrayList<IdNameDescModel>());
 		lstView.setAdapter(arrayAdapter); 
 		callService();
 		
 		lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
 			@Override
 			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
 				IdNameDescModel especialidade = arrayAdapter.getItem(position);
 				
 				//cria o circulo "fake"
 				Circle circle = new Circle(especialidade);
 				ViewCircleActivity.showActivity(getContext(), circle);
 			}
		});
    	
    }
    
    @Override
    protected void showFragment() {
    	super.showFragment();
    	if (arrayAdapter != null && !getSearchText().equals(mTxtSearch)) {
    		refreshListView();
    	}
    }
    
    @Override
    public void refresh() {
    	if (resultServer.isEmpty()) {
    		callService();
    	} else {
    		refreshListView();
    	}
    }
    
	private void refreshListView() {
		synchronized (arrayAdapter) {
			mTxtSearch = getSearchText();
			if (arrayAdapter != null) {
				List<IdNameDescModel> newItems = new ArrayList<IdNameDescModel>();
				if (TextUtils.isEmpty(mTxtSearch)) {
					newItems.addAll(resultServer);
				} else {
					for (IdNameDescModel tmp : resultServer) {
						if (findOnlyStartWith && tmp.getName().toUpperCase().indexOf(mTxtSearch.toUpperCase()) == 0) {
							newItems.add(tmp);
						} else if (!findOnlyStartWith && tmp.getName().toUpperCase().indexOf(mTxtSearch.toUpperCase()) >= 0) {
							newItems.add(tmp);
						}
					}
				}
				refreshListView(newItems);
			}
			
		}		
	}

	private void callService() {
		ObserverAsyncTask<SelectServerListResult> observer = new ObserverAsyncTask<SelectServerListResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
			}

			@Override
			public void onPostExecute(SelectServerListResult listResult) {
				
				IdNameDescModel[] result = null;
				if (listResult != null && listResult.getMedicalSpecialities() != null && listResult.getMedicalSpecialities().length > 0) {
					result = listResult.getMedicalSpecialities();
				}
				
				if (result != null && result.length > 0) {
					resultServer.addAll(Arrays.asList(result));
					synchronized (arrayAdapter) {
						refreshListView(new ArrayList<IdNameDescModel>(resultServer));
					}
					
					dismissWaitDialog();
					refreshListView();
				} else {
					dismissWaitDialog();
					lstView.setVisibility(View.GONE);
					getView().findViewById(R.id.txt_nenhum_resultado).setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Paths.MEDICAL_SPECIALITIES)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), SelectServerListResult.class, new MedicalSpecialitiesService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void refreshListView(List<IdNameDescModel> lst) {
		if (getView() != null) {
			if (arrayAdapter == null) {
				arrayAdapter = new ArrayAdapter<IdNameDescModel>( getActivity(), R.layout.simple_list_item, lst);
				lstView.setAdapter(arrayAdapter); 
			} else {
				arrayAdapter.clear();
				arrayAdapter.addAll(lst);
				arrayAdapter.notifyDataSetChanged();
			}
			
			if (lst != null && !lst.isEmpty()) {
				lstView.setVisibility(View.VISIBLE);
				getView().findViewById(R.id.txt_nenhum_resultado).setVisibility(View.GONE);
			} else {
				lstView.setVisibility(View.GONE);
				getView().findViewById(R.id.txt_nenhum_resultado).setVisibility(View.VISIBLE);
			}
		}
	}
	

}
