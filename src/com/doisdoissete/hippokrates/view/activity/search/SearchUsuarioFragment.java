package com.doisdoissete.hippokrates.view.activity.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.result.SearchUsersResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.user.ViewUserProfileActivity;
import com.doisdoissete.hippokrates.view.adapter.circle.MemberAdapter;

public class SearchUsuarioFragment extends BaseSearchFragment {

	private static SearchUsuarioFragment instance = new SearchUsuarioFragment();
	
	private int page = 0;
	private MemberAdapter usersAdapter;
	private PagingListView listView;
	private TextViewPlus txtNenhumResultado;
	
    public static SearchUsuarioFragment getInstance() {
        return instance;
    }

    public SearchUsuarioFragment() {
    }
    
    @Override
    protected void showFragment() {
    	super.showFragment();
    	if (!getSearchText().equals(mTxtSearch)) {
    		page = 0;
    		callService(getSearchText());
    	}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_paging_fragment, container, false);
        listView = (PagingListView)rootView.findViewById(R.id.listview);
        txtNenhumResultado = (TextViewPlus)rootView.findViewById(R.id.txt_nenhum_resultado);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
        		User user = usersAdapter.getItem(position);
        		ViewUserProfileActivity.showActivity(getContext(), user);
        	}
		});
        return rootView;
    }
    
    @Override
    public void refresh() {
    	page = 0;
    	callService(getSearchText());
    }
    
    private void clearSearch() {
    	if (usersAdapter != null) {
			usersAdapter.removeAllItems();
		}
    }
    
    private void callService(String txtSearch) {
		if ((!txtSearch.equals(mTxtSearch)) || (async != null && (async.isRunning() || async.isPending()))) {
			page = 0;
			if (async != null) {
				async.cancel(true);
			}
			
		}
		if (TextUtils.isEmpty(txtSearch)) {
			clearSearch();
			return;
		}
		page++;
		
		ObserverAsyncTask<SearchUsersResult> observer = new ObserverAsyncTask<SearchUsersResult>() {
			@Override
			public void onPreExecute() {
				if (page <= 1) {
					listView.setVisibility(View.GONE);
					showWaitDialog(false);
					clearSearch();				
				}
				txtNenhumResultado.setVisibility(View.GONE);
			}

			@Override
			public void onPostExecute(SearchUsersResult result) {
				populateUsers(result);
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		String url = Paths.SEARCH_USER.replace(UrlParameters.PARAM_USER_NAME_URL, txtSearch);
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setCharset(Constants.CHARSET)
			.setParams(params);

		async = sb.mappingInto(getContext(), SearchUsersResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
		mTxtSearch = txtSearch;
	}
    
    private void populateUsers(SearchUsersResult result) {
    	if (usersAdapter == null) {
			listView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					callService(getSearchText());
				}
			});

			usersAdapter = new MemberAdapter(getContext(), new ArrayList<User>());
			listView.setAdapter(usersAdapter);
		}
		
		boolean loadMore = true;
		
		List<User> users = new ArrayList<User>();
		if (result == null) {
			loadMore = false;
		} else {
			users = result.getUsers();
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (page < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
			} else if (users != null) {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > users.size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			} else {
				loadMore = false;
			}				
		}
		
		listView.onFinishLoading(loadMore, users);
		
		usersAdapter.notifyDataSetChanged();
		listView.requestLayout();
		
		if (usersAdapter.getCount() <= 0) {
			txtNenhumResultado.setVisibility(View.VISIBLE);
		} else {
			txtNenhumResultado.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}
		
    }

}
