package com.doisdoissete.hippokrates.view.activity.search;

import com.doisdoissete.hippokrates.view.activity.base.HippBaseFragment;

public abstract class BaseSearchFragment extends HippBaseFragment {

	public static final String PARAM_REFRESH = "PARAM_REFRESH";
	public abstract void refresh();
	
	protected String mTxtSearch = null;
	
	protected String getSearchText() {
		if (getActivity() != null) {
			return ((SearchActivity_)getActivity()).edtSearch.getTextStr();
		}
		return "";
	}

}
