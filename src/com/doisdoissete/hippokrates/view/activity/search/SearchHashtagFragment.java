package com.doisdoissete.hippokrates.view.activity.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.result.SearchHashtagResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.adapter.search.HashTagAdapter;

public class SearchHashtagFragment extends BaseSearchFragment {

	private static SearchHashtagFragment instance = new SearchHashtagFragment();
	
	private int page = 0;
	private HashTagAdapter arrayAdapter;
	private PagingListView listView;
	private TextViewPlus txtNenhumResultado;
	
    public static SearchHashtagFragment getInstance() {
        return instance;
    }

    public SearchHashtagFragment() {
    }
    
    @Override
    protected void showFragment() {
    	super.showFragment();
    	if (!getSearchText().equals(mTxtSearch)) {
    		page = 0;
    		callService(getSearchText());
    	}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_paging_fragment, container, false);
        listView = (PagingListView)rootView.findViewById(R.id.listview);
        txtNenhumResultado = (TextViewPlus)rootView.findViewById(R.id.txt_nenhum_resultado);
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
        		HashtagFeedActivity.showActivity(getContext(), arrayAdapter.getItem(position));
        	}
		});
        return rootView;
    }
    
    @Override
    public void refresh() {
    	page = 0;
    	callService(getSearchText());
    }
    
    private void clearSearch() {
    	if (arrayAdapter != null) {
    		arrayAdapter.removeAllItems();
		}
    }
    
    private void callService(String txtSearch) {
		if ((!txtSearch.equals(mTxtSearch)) || (async != null && (async.isRunning() || async.isPending()))) {
			page = 0;
			if (async != null) {
				async.cancel(true);
			}
			
		}
		if (TextUtils.isEmpty(txtSearch)) {
			clearSearch();
			return;
		}
		page++;
		
		ObserverAsyncTask<SearchHashtagResult> observer = new ObserverAsyncTask<SearchHashtagResult>() {
			@Override
			public void onPreExecute() {
				if (page <= 1) {
					listView.setVisibility(View.GONE);
					showWaitDialog(false);
					clearSearch();				
				}
				txtNenhumResultado.setVisibility(View.GONE);
			}

			@Override
			public void onPostExecute(SearchHashtagResult result) {
				populateView(result);
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		String url = Paths.SEARCH_HASHTAG.replace(UrlParameters.PARAM_HASHTAG_URL, txtSearch);
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setCharset(Constants.CHARSET)
			.setParams(params);

		async = sb.mappingInto(getContext(), SearchHashtagResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
		mTxtSearch = txtSearch;
	}
    
    private void populateView(SearchHashtagResult result) {
    	if (arrayAdapter == null) {
			listView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					callService(getSearchText());
				}
			});

			arrayAdapter = new HashTagAdapter(getContext(), new ArrayList<String>());
			listView.setAdapter(arrayAdapter);
		}
		
		boolean loadMore = true;
		
		List<String> hashTags = new ArrayList<String>();
		if (result == null) {
			loadMore = false;
		} else {
			hashTags = result.getHashtags();
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (page < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
			} else if (hashTags != null) {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > hashTags.size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			} else {
				loadMore = false;
			}				
		}
		
		listView.onFinishLoading(loadMore, hashTags);
		
		arrayAdapter.notifyDataSetChanged();
		listView.requestLayout();
		
		if (arrayAdapter.getCount() <= 0) {
			txtNenhumResultado.setVisibility(View.VISIBLE);
		} else {
			txtNenhumResultado.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}
		
    }

}
