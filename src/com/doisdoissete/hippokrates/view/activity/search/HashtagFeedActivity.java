package com.doisdoissete.hippokrates.view.activity.search;

import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.TextViewPlus;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.result.PostResult;
import com.doisdoissete.hippokrates.service.FeedService;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewInterface;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewUtil;
import com.doisdoissete.hippokrates.view.activity.post.DetailPostActivity;
import com.doisdoissete.hippokrates.view.adapter.post.PostAdapter;

@EActivity(R.layout.search_hashtag_feed_activity)
public class HashtagFeedActivity extends HippBaseActivity {

	private final static String PARAM_HASHTAG = "PARAM_HASHTAG";
	
	@ViewById(R.id.layout_content)
    PagingListView mListView;
	@ViewById(R.id.txt_header_busca)
    TextViewPlus txtHeaderBusca;
	@ViewById(R.id.txt_nenhum_resultado)
    TextViewPlus txtNenhumResultado;
	private PostAdapter adapter;
	private int page = 1;
	private String hashtag;
	
	
	@AfterViews
	void postCreate() {
		hashtag = getIntent().getExtras().getString(PARAM_HASHTAG);
		txtHeaderBusca.setText(getString(R.string.resultados_para, hashtag));
		callService();
		
		mListView.setOnTouchListener(new OnTouchListener() {
			GestureDetector gestureDetector = new GestureDetector(new SimpleOnGestureListener() {
            	@Override
            	public boolean onDoubleTap(MotionEvent e) {
            		int position = mListView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = adapter.getItem(position);
            		PostViewUtil.likeDislikePost(getContext(), HashtagFeedActivity.this, post, adapter);
            		
            		return super.onDoubleTap(e);
            	}
            	
            	@Override
            	public boolean onSingleTapConfirmed(MotionEvent e) {
            		int position = mListView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = adapter.getItem(position);
    				DetailPostActivity.showActivity(getContext(), post);
            		return super.onSingleTapConfirmed(e);
            	}
            });
			@Override
	         public boolean onTouch(View v, MotionEvent event) {
	                return gestureDetector.onTouchEvent(event);
	         }
		});
	}
	
	private void callService() {
		ObserverAsyncTask<PostResult> observer = new ObserverAsyncTask<PostResult>() {
			@Override
			public void onPreExecute() {
				if (page == 1) {
					showWaitDialog(false);
				}				
			}

			@Override
			public void onPostExecute(PostResult result) {
				populateListView(result);
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				page--;
			}

			@Override
			public void onError(Exception e) {
				showError(e);
				page--;
			}
		};
		
		if (page <= 0) {
			page = 1;
		}

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		String url = Paths.FEED_HASHTAG.replace(UrlParameters.PARAM_HASHTAG_URL, hashtag.replace("#", ""));
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(url)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), PostResult.class, new FeedService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	public void newSearch(String tag) {
		page = 0;
		hashtag = tag;
		txtHeaderBusca.setText(getString(R.string.resultados_para, hashtag));
		if (adapter != null) {
			adapter.removeAllItems();
		}
		callService();
	}
	
	private void populateListView(PostResult result) {
		if (adapter == null) {
			mListView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					page++;
					callService();
				}
			});

			adapter = new PostAdapter(HashtagFeedActivity.this, new ArrayList<Post>(), new PostViewInterface() {
				
				@Override
				public void removedPosts(List<Long> ids) {
					if (ids != null && ids.size() > 0) {
						LocalPost.deletePosts(ids, adapter);
						adapter.notifyDataSetChanged();
					}
				}
			});
			mListView.setAdapter(adapter);
		}
		
		boolean loadMore = true;
		
		List<Post> lstResult = new ArrayList<Post>();
		if (result == null) {
			loadMore = false;
		} else {
			lstResult = result.getPosts();
			if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
				if (page < result.getMeta().getPagination().getTotalPages()) {
					loadMore = true;
				} else {
					loadMore = false;
				}
			} else if (lstResult != null) {
				if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > lstResult.size()) {
					loadMore = false;
		    	} else {
		    		loadMore = true;
		    	} 
			} else {
				loadMore = false;
			}				
		}
		
		mListView.onFinishLoading(loadMore, lstResult);
		
		adapter.notifyDataSetChanged();
		mListView.requestLayout();
		
		if (adapter.getCount() <= 0) {
			txtNenhumResultado.setVisibility(View.VISIBLE);
		} else {
			txtNenhumResultado.setVisibility(View.GONE);
			mListView.setVisibility(View.VISIBLE);
		}
	}
	
	
	public static void showActivity(Context ctx, String hashtag) {
		Intent it = new Intent(ctx, HashtagFeedActivity_.class);
		it.putExtra(PARAM_HASHTAG, hashtag);
		ctx.startActivity(it);
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
}
