package com.doisdoissete.hippokrates.view.activity.feed;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;

import com.doisdoissete.android.util.ddsutil.service.asynctask.AsyncTaskService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.PagingListView;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.result.PostResult;
import com.doisdoissete.hippokrates.service.FeedService;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseFragment;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewInterface;
import com.doisdoissete.hippokrates.view.activity.base.post.PostViewUtil;
import com.doisdoissete.hippokrates.view.activity.post.DetailPostActivity;
import com.doisdoissete.hippokrates.view.adapter.post.PostAdapter;

public class FeedFragment extends HippBaseFragment {

    private static FeedFragment instance;
    private PagingListView mListView;
	private PostAdapter adapter;
	private int mPage = 1;
	private SwipeRefreshLayout mSwipeRefreshLayout;
	
    public static FeedFragment getInstance() {
    	if (instance == null) {
    		instance = new FeedFragment();
    	}
    	return instance;
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.feed_fragment, container, false);
		mListView = (PagingListView)rootView.findViewById(R.id.layout_content);

		mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);
		mListView.setOnTouchListener(new OnTouchListener() {
			GestureDetector gestureDetector = new GestureDetector(new SimpleOnGestureListener() {
            	@Override
            	public boolean onDoubleTap(MotionEvent e) {
            		int position = mListView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = adapter.getItem(position);
            		PostViewUtil.likeDislikePost(getContext(), FeedFragment.this, post, adapter);
            		return super.onDoubleTap(e);
            	}
            	
            	@Override
            	public boolean onSingleTapConfirmed(MotionEvent e) {
            		int position = mListView.pointToPosition(Float.valueOf(e.getX()).intValue(), Float.valueOf(e.getY()).intValue());
            		Post post = adapter.getItem(position);
    				DetailPostActivity.showActivity(getContext(), post);
            		return super.onSingleTapConfirmed(e);
            	}
            });
			@Override
	         public boolean onTouch(View v, MotionEvent event) {
	                return gestureDetector.onTouchEvent(event);
	         }
		});
		mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.loading_color));
		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				callServiceLoadNew();
			}
		});
		
		return rootView;
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		callService();
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		List<LocalPost> deletedLocalPost = LocalPost.getDeletedLocalPost();
		if (deletedLocalPost != null && adapter != null) {
			for (LocalPost localPost : deletedLocalPost) {
				adapter.removePost(localPost.getPostId());
			}
		}
		if (adapter != null && adapter.getCount() > 0) {
			adapter.notifyDataSetChanged();
			adapter.notifyDataSetChanged();
		}
	}
	
	private void callServiceLoadNew() {
		ObserverAsyncTask<PostResult> observer = new ObserverAsyncTask<PostResult>() {
			@Override
			public void onPreExecute() {
			}

			@Override
			public void onPostExecute(PostResult result) {
				populateNewItems(result);
				mSwipeRefreshLayout.setRefreshing(false);
			}

			@Override
			public void onCancelled() {
				mSwipeRefreshLayout.setRefreshing(false);
			}

			@Override
			public void onError(Exception e) {
				mSwipeRefreshLayout.setRefreshing(false);
			}
		};
		
		callService(1, observer, async);
	}
	
	private void callService() {
		ObserverAsyncTask<PostResult> observer = new ObserverAsyncTask<PostResult>() {
			@Override
			public void onPreExecute() {
				if (mPage == 1) {
					showWaitDialog(false);
				}				
			}

			@Override
			public void onPostExecute(PostResult result) {
				if (result != null && result.getPosts() != null) {
					populateListView(result);
				} else {
					showMessage(R.string.msg_generic_error);
				}
				dismissWaitDialog();
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
				mPage--;

			}

			@Override
			public void onError(Exception e) {
				e.printStackTrace();
				showError(e);
				mPage--;
			}
		};
		
		if (mPage <= 0) {
			mPage = 1;
		}
		
		callService(mPage, observer, async);
	}
	
	private void callService(int page, ObserverAsyncTask<PostResult> observer, AsyncTaskService asyncTaskService) {

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_PAGE, String.valueOf(page));
		
		HippAppServiceBuilder sb = new HippAppServiceBuilder(getContext());
		sb.setUrl(Paths.FEED)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.GET)
			.setParams(params);
	
		asyncTaskService = sb.mappingInto(getContext(), PostResult.class, new FeedService(sb));
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			asyncTaskService.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			asyncTaskService.execute();
		}
	}
	
	private void populateNewItems(PostResult result) {
		if (adapter == null) {
			return;			
		}
		boolean added = false;
		int qtdeAdd = 0;
		if (result != null && result.getPosts() != null) {
			if (adapter.getCount() > 0) {
				Post firstItem = (Post)adapter.getItem(0);
				for (Post post : result.getPosts()) {
					if (!post.equals(firstItem)) {
						added = true;
						adapter.addItem(qtdeAdd, post);
						qtdeAdd++;
					} else {
						break;
					}
				}
			} else {
				//adiciona todos
				added = true;
				adapter.addMoreItems(result.getPosts());
			}
		}
		
		if (added) {
			// save index and top position
			int index = mListView.getFirstVisiblePosition();
			View v = mListView.getChildAt(0);
			int top = (v == null) ? 0 : v.getTop();

			// notify dataset changed or re-assign adapter here
			adapter.notifyDataSetChanged();
			// restore the position of listview
			mListView.setSelectionFromTop(index+qtdeAdd, top);
			
			//mListView.requestLayout();
		}
		
	}
	
	private void populateListView(PostResult result) {
		if (adapter == null) {
			mListView.setPagingableListener(new PagingListView.Pagingable() {
				@Override
				public void onLoadMoreItems() {
					mPage++;
					callService();
				}
			});

			if (getContext() != null) {
				adapter = new PostAdapter(getContext(), new ArrayList<Post>(), this, new PostViewInterface() {
					
					@Override
					public void removedPosts(List<Long> ids) {
						if (ids != null && ids.size() > 0) {
							LocalPost.deletePosts(ids, adapter);
							adapter.notifyDataSetChanged();
						}
					}
				});
				mListView.setAdapter(adapter);
			} else {
				return;
			}
			
		}
		
		boolean loadMore = true;
		
		if (result.getMeta() != null && result.getMeta().getPagination() != null && result.getMeta().getPagination().getTotalPages() != null) {
			if (mPage < result.getMeta().getPagination().getTotalPages()) {
				loadMore = true;
			} else {
				loadMore = false;
			}
		} else {
			if (Integer.valueOf(Constants.TOTAL_ITENS_PAGE).intValue() > result.getPosts().size()) {
				loadMore = false;
	    	} else {
	    		loadMore = true;
	    	} 
		}
		   	
		mListView.onFinishLoading(loadMore, result.getPosts());
		
		mListView.setVisibility(View.VISIBLE);
		mSwipeRefreshLayout.setVisibility(View.VISIBLE);
    	adapter.notifyDataSetChanged();
    	mListView.requestLayout();
		
	}
	
	@Override
	public void onDestroyView() {
		instance = null;
		super.onDestroyView();
	}
}
