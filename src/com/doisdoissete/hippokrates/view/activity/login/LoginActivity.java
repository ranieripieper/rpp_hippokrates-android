package com.doisdoissete.hippokrates.view.activity.login;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ObserverAsyncTask;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.android.util.ddsutil.view.custom.EditTextPlus;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.model.result.LoginResult;
import com.doisdoissete.hippokrates.service.background.RegisterDeviceBackgroundService;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.HomeActivity;
import com.doisdoissete.hippokrates.view.activity.base.HippBaseActivity;

public class LoginActivity extends HippBaseActivity {

	private EditTextPlus edtEmail;
	private EditTextPlus edtSenha;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.login_activity);
		initComponents();
		Preferences.logout(getContext());
	}

	private void initComponents() {
		edtEmail = (EditTextPlus) findViewById(R.id.edt_email);
		edtSenha = (EditTextPlus) findViewById(R.id.edt_senha);
		if (Constants.DEGUB) {
			edtEmail.setText(Constants.EMAIL_LOGIN_DEBUG);
			edtSenha.setText(Constants.SENHA_LOGIN_DEBUG);
		}
	}

	//Clicks
	public void esqueceuSenhaClick(View v) {
		callServiceEsqueceSenha();
	}

	public void btEntrarClick(View v) {
		callServiceLogin();
	}
	
	//métodos privados
	
	private void callServiceLogin() {
		//valida form
		if (edtEmail.isNotValid() || edtSenha.isNotValid()) {
			showMessage(R.string.msg_nome_senha_invalidos);
			return;
		}
		
		ObserverAsyncTask<LoginResult> observer = new ObserverAsyncTask<LoginResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(true);
			}

			@Override
			public void onPostExecute(LoginResult result) {
				if (result != null && result.isSuccess() && result.getUser() != null && result.getAuthData() != null) {
					
					LocalUser user = new LocalUser();
					user.setId(result.getUser().getId());
					user.setEmail(edtEmail.getTextStr());
					user.setFullName(result.getUser().getFullName());
					user.setUsername(user.getUsername());
					user.setAuthToken(result.getAuthData().getAuthToken());
					user.setProfileType(result.getUser().getProfileType());
					user.setProfileImg(result.getUser().getProfileImageUrl());
					
					Preferences.setLocalUser(getContext(), user);
					
					RegisterDeviceBackgroundService.callService(getContext());
					
					finishAffinity();
					
					HomeActivity.showActivity(getContext());
					
				} else {
					dismissWaitDialog();
					showMessage(R.string.msg_nome_senha_invalidos);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_EMAIL, edtEmail.getTextStr());
		params.add(UrlParameters.PARAM_PASSWORD, edtSenha.getTextStr());
		params.add(UrlParameters.PARAM_PROVIDER, Constants.PROVIDER);
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Paths.LOGIN)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), LoginResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	private void callServiceEsqueceSenha() {
		//valida form
		if (edtEmail.isNotValid()) {
			showMessage(R.string.msg_email_invalido);
			return;
		}
		
		ObserverAsyncTask<BaseResult> observer = new ObserverAsyncTask<BaseResult>() {
			@Override
			public void onPreExecute() {
				showWaitDialog(false);
			}

			@Override
			public void onPostExecute(BaseResult result) {
				if (result != null && result.isSuccess()) {
					showMessage(R.string.msg_reset_password);
					dismissWaitDialog();
				} else if (result.isError()) {
					showError(null);
				}
			}

			@Override
			public void onCancelled() {
				dismissWaitDialog();
			}

			@Override
			public void onError(Exception e) {
				showError(e);	
			}
		};

		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add(UrlParameters.PARAM_USER_EMAIL, edtEmail.getTextStr());
		
		ServiceBuilder sb = new ServiceBuilder();
		sb.setUrl(Paths.RECOVER_PASSWORD)
			.setObserverAsyncTask(observer)
			.setNeedConnection(true)
			.setHttpMethod(HttpMethod.POST)
			.setCharset(Constants.CHARSET)
			.setParams(params);
	
		async = sb.mappingInto(getContext(), LoginResult.class);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			async.execute();
		}
	}
	
	@Override
	protected boolean showHomeAsUpEnabled() {
		return true;
	}
	@Override
	protected boolean showHomeEnabled() {
		return true;
	}
	
	protected void processDdsUtilIOException(DdsUtilIOException e) {
    	showMessage(R.string.msg_usuario_senha_invalidos);
    }
	
	public static void showActivity(Context ctx) {
		ctx.startActivity(getIntentActivity(ctx));
	}
	
	public static Intent getIntentActivity(Context ctx) {
		return new Intent(ctx, LoginActivity.class);
	}
}
