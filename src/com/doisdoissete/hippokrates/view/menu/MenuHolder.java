package com.doisdoissete.hippokrates.view.menu;

import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;

import android.widget.ImageView;
import android.widget.TextView;

public class MenuHolder {

	public TextView txt;
	public ImageView img;
	public CircleImageView circleImg;
}
