package com.doisdoissete.hippokrates.view.menu;

import android.content.Context;

public interface MenuItemClickListener {

	public void onClick(Context ctx);
}
