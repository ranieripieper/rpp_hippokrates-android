package com.doisdoissete.hippokrates.view.menu;

import android.content.Context;


public class MenuHomeChild {

	private String txt;
	private Integer icon;
	private String imgUrl;
	private MenuItemClickListener onClick;


	public MenuHomeChild() { }
	
	public MenuHomeChild(String txt, int icon) {
		super();
		this.txt = txt;
		this.icon = icon;
	}
	
	public MenuHomeChild(String txt, String imgUrl) {
		super();
		this.txt = txt;
		this.imgUrl = imgUrl;
	}
	
	public MenuHomeChild(String txt, int icon, MenuItemClickListener onClick) {
		super();
		this.txt = txt;
		this.icon = icon;
		this.onClick = onClick;
	}
	
	public MenuHomeChild(String txt, String imgUrl, MenuItemClickListener onClick) {
		super();
		this.txt = txt;
		this.imgUrl = imgUrl;
		this.onClick = onClick;
	}
	
	public boolean executeOnClick(Context context) {
		if (onClick != null) {
			onClick.onClick(context);
			return true;
		}
		return false;
	}
	
	/**
	 * @return the txt
	 */
	public String getTxt() {
		return txt;
	}
	/**
	 * @param txt the txt to set
	 */
	public void setTxt(String txt) {
		this.txt = txt;
	}
	/**
	 * @return the icon
	 */
	public Integer getIcon() {
		return icon;
	}
	/**
	 * @param icon the icon to set
	 */
	public void setIcon(Integer icon) {
		this.icon = icon;
	}
	/**
	 * @return the imgUrl
	 */
	public String getImgUrl() {
		return imgUrl;
	}
	/**
	 * @param imgUrl the imgUrl to set
	 */
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	/**
	 * @return the onClick
	 */
	public MenuItemClickListener getOnClick() {
		return onClick;
	}
	/**
	 * @param onClick the onClick to set
	 */
	public void setOnClick(MenuItemClickListener onClick) {
		this.onClick = onClick;
	}
}
