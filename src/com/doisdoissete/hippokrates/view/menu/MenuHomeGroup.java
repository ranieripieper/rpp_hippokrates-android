package com.doisdoissete.hippokrates.view.menu;

import java.util.List;

import android.content.Context;

public class MenuHomeGroup {

	private String txt;
	private Integer icon;
	
	private MenuItemClickListener onClickListener;
	private List<MenuHomeChild> child;

	public MenuHomeGroup() {}
	
	public MenuHomeGroup(String txt, Integer icon) {
		super();
		this.txt = txt;
		this.icon = icon;
	}
	
	public MenuHomeGroup(String txt, int icon, MenuItemClickListener onClickListener) {
		super();
		this.txt = txt;
		this.icon = icon;
		this.onClickListener = onClickListener;
	}
	
	public MenuHomeGroup(String txt, int icon, MenuItemClickListener onClickListener,
			List<MenuHomeChild> child) {
		super();
		this.txt = txt;
		this.icon = icon;
		this.onClickListener = onClickListener;
		this.child = child;
	}

	public boolean executeOnClick(Context context) {
		if (onClickListener != null) {
			onClickListener.onClick(context);
			return true;
		}
		return false;
	}
	/**
	 * @return the txt
	 */
	public String getTxt() {
		return txt;
	}

	/**
	 * @param txt the txt to set
	 */
	public void setTxt(String txt) {
		this.txt = txt;
	}

	/**
	 * @return the icon
	 */
	public Integer getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(Integer icon) {
		this.icon = icon;
	}

	/**
	 * @return the child
	 */
	public List<MenuHomeChild> getChild() {
		return child;
	}

	/**
	 * @param child the child to set
	 */
	public void setChild(List<MenuHomeChild> child) {
		this.child = child;
	}

	/**
	 * @param onClick the onClick to set
	 */
	public void setOnClick(MenuItemClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}
	
}
