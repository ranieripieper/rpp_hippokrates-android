package com.doisdoissete.hippokrates.view.menu;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.android.util.ddsutil.view.custom.imageview.CircleImageView;
import com.doisdoissete.android.util.ddsutil.view.custom.listview.AnimatedExpandableListView.AnimatedExpandableListAdapter;
import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.R;

public class MenuHomeAdapter extends AnimatedExpandableListAdapter {
	
    private LayoutInflater inflater;
    private List<MenuHomeGroup> items = new ArrayList<MenuHomeGroup>();
    private Activity context;
    
    public MenuHomeAdapter(Activity pContext, List<MenuHomeGroup> lstMenu) {
    	context = pContext; 
    	inflater = LayoutInflater.from(context);         
    	this.items = lstMenu;
    }

    public void setData(List<MenuHomeGroup> items) {
        this.items = items;
    }

    @Override
    public MenuHomeChild getChild(int groupPosition, int childPosition) {
        return items.get(groupPosition).getChild().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        MenuHolder holder;
        MenuHomeChild item = getChild(groupPosition, childPosition);
        if (convertView == null) {
            holder = new MenuHolder();
            convertView = inflater.inflate(R.layout.menu_row_child, parent, false);
            holder.txt = (TextView) convertView.findViewById(R.id.txt);
            holder.img = (ImageView) convertView.findViewById(R.id.img);
            holder.circleImg = (CircleImageView) convertView.findViewById(R.id.circle_img);
            convertView.setTag(holder);
        } else {
            holder = (MenuHolder) convertView.getTag();
        }
        
        holder.circleImg.setVisibility(View.GONE);
    	holder.img.setVisibility(View.GONE);
    	
        if (!TextUtils.isEmpty(item.getImgUrl())) {
        	holder.circleImg.setVisibility(View.VISIBLE);
        	HippokratesApplication.imageLoaderCircle.displayImage(item.getImgUrl(), holder.circleImg);
        } else {
	        if (item.getIcon() != null && item.getIcon() != -1) {
	        	holder.img.setVisibility(View.VISIBLE);
	        	holder.img.setImageResource(item.getIcon());
	        }
        }
        holder.txt.setText(item.getTxt());       
        
        return convertView;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
    	if (items.get(groupPosition).getChild() != null) {
    		 return items.get(groupPosition).getChild().size();
    	} else {
    		return 0;
    	}
       
    }

    @Override
    public MenuHomeGroup getGroup(int groupPosition) {
        return items.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
    	if (items == null) {
    		return 0;
    	}
        return items.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
    	MenuHolder holder;
        MenuHomeGroup item = getGroup(groupPosition);
        if (convertView == null) {
            holder = new MenuHolder();
            convertView = inflater.inflate(R.layout.menu_row_group, parent, false);
            holder.txt = (TextView) convertView.findViewById(R.id.txt);
            holder.img = (ImageView) convertView.findViewById(R.id.img_icon);
            convertView.setTag(holder);
        } else {
            holder = (MenuHolder) convertView.getTag();
        }
        
        holder.txt.setTextColor(Color.WHITE);
        holder.txt.setText(item.getTxt());
        if (item.getIcon() != null) {
        	holder.img.setImageResource(item.getIcon());
        	 if (item.getIcon() == R.drawable.icn_menu_logout) {
             	holder.txt.setTextColor(context.getResources().getColor(R.color.txt_logout));
        	 }
        }
        
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }
    

    
}
