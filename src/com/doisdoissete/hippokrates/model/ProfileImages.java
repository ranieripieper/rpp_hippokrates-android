package com.doisdoissete.hippokrates.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.doisdoissete.hippokrates.util.Paths;
import com.google.gson.annotations.SerializedName;

public class ProfileImages implements Parcelable {

	@SerializedName("mini_thumb")
	private String miniThumb;
	@SerializedName("small_thumb")
	private String smallThumb;
	@SerializedName("thumb")
	private String thumb;
	@SerializedName("medium")
	private String medium;
	
	
	public ProfileImages() {
	}
	
	public ProfileImages(String miniThumb, String smallThumb, String thumb,
			String medium) {
		super();
		this.miniThumb = miniThumb;
		this.smallThumb = smallThumb;
		this.thumb = thumb;
		this.medium = medium;
	}

	/**
	 * @return the miniThumb
	 */
	public String getMiniThumb() {
		return includeHost(miniThumb);
	}
	/**
	 * @param miniThumb the miniThumb to set
	 */
	public void setMiniThumb(String miniThumb) {
		this.miniThumb = miniThumb;
	}

	/**
	 * @return the smallThumb
	 */
	public String getSmallThumb() {
		return includeHost(smallThumb);
	}
	/**
	 * @param smallThumb the smallThumb to set
	 */
	public void setSmallThumb(String smallThumb) {
		this.smallThumb = smallThumb;
	}
	/**
	 * @return the thumb
	 */
	public String getThumb() {
		return includeHost(thumb);
	}
	/**
	 * @param thumb the thumb to set
	 */
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	/**
	 * @return the medium
	 */
	public String getMedium() {
		return includeHost(medium);
	}
	/**
	 * @param medium the medium to set
	 */
	public void setMedium(String medium) {
		this.medium = medium;
	}
	
	private String includeHost(String value) {
		if (!TextUtils.isEmpty(value) && value.indexOf("http") < 0) {
			return String.format("%s%s", Paths.HOST, value);
		}
		return value;
	}
	
	/******************************************/
    /********** Parcelable interface **********/
    /******************************************/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.medium);     
        out.writeString(this.miniThumb);
        out.writeString(this.smallThumb);
        out.writeString(this.thumb);
    }

    private static ProfileImages readFromParcel(Parcel in) {
    	ProfileImages obj = new ProfileImages();
    	obj.setMedium(in.readString());
    	obj.setMiniThumb(in.readString());
    	obj.setSmallThumb(in.readString());
    	obj.setThumb(in.readString());        
        return obj;
    }

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public ProfileImages createFromParcel(Parcel in) {
			return readFromParcel(in);
		}

		public ProfileImages[] newArray(int size) {
			return new ProfileImages[size];
		}
	};
	
}
