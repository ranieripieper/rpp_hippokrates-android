package com.doisdoissete.hippokrates.model;

import android.graphics.Bitmap;

public class ContactUser implements Comparable<ContactUser>{

	private String name;
	private String email;
	private Bitmap image;
	private boolean selected;
	
	public ContactUser(String name, String email, Bitmap bitmap) {
		super();
		this.name = name;
		this.email = email;
		this.image = bitmap;
	}
	
	public ContactUser(String name, String email, boolean selected) {
		super();
		this.name = name;
		this.email = email;
		this.selected = selected;
	}
	
	public String getNameEmailString() {
		return String.format("%s,%s", this.name == null ? "" : this.name, this.email);
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return name + " - "+ email;
	}
	/**
	 * @return the image
	 */
	public Bitmap getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(Bitmap image) {
		this.image = image;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	@Override
	public int compareTo(ContactUser another) {
		return this.name.compareTo(another.name);
	}
}
