package com.doisdoissete.hippokrates.model;

import com.google.gson.annotations.SerializedName;

public class Invite {
	
	private String status;
	private String code;
	private String email;
	@SerializedName("receiver_user_profile_type")
	private String receiverUserProfileType;
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the receiverUserProfileType
	 */
	public String getReceiverUserProfileType() {
		return receiverUserProfileType;
	}
	/**
	 * @param receiverUserProfileType the receiverUserProfileType to set
	 */
	public void setReceiverUserProfileType(String receiverUserProfileType) {
		this.receiverUserProfileType = receiverUserProfileType;
	}
	
}
