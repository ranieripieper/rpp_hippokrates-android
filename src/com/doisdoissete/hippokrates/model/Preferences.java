package com.doisdoissete.hippokrates.model;

import java.util.ArrayList;
import java.util.List;

import android.content.res.Resources;

import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.view.adapter.settings.PreferencesToAdapter;
import com.google.gson.annotations.SerializedName;

public class Preferences {

	@SerializedName("notify_signup")
	private boolean notifySignup;
	
	@SerializedName("notify_new_like")
	private boolean notifyNewLike;
	
	@SerializedName("notify_new_comment")
	private boolean notifyNewComment;
	
	@SerializedName("notify_post_deleted")
	private boolean notifyPostDeleted;
	
	@SerializedName("notify_post_reported")
	private boolean notifyPostReported;
	
	@SerializedName("notify_comment_reported")
	private boolean notifyCommentReported;
	
	@SerializedName("notify_password_updated")
	private boolean notifyPasswordUpdated;
	
	@SerializedName("notify_removed_from_circle")
	private boolean notifyRemovedFromCircle;
	
	@SerializedName("notify_friend_invite_signup")
	private boolean notifyFriendInviteSignup;
	
	@SerializedName("notify_pending_request_approved")
	private boolean notifyPendingRequestApproved;
	
	@SerializedName("notify_subscription_will_expire")
	private boolean notifySubscriptionWillExpire;
	
	@SerializedName("notify_friendship_circle_admin_received")
	private boolean notifyFriendshipCircleAdminReceived;
	
	@SerializedName("notify_subscription_successfully_charged")
	private boolean notifySubscriptionSuccessfullyCharged;
	
	@SerializedName("notify_circle_new_member")
	private boolean notifyCircleNewMember;

	@SerializedName("notify_pending_request_declined")
	private boolean notifyPendingRequestDeclined;

	@SerializedName("notify_new_pending_circle_member")
	private boolean notifyNewPendingCircleMember;
	
	public static List<PreferencesToAdapter> createPreferencesToAdapter(Resources res) {
		List<PreferencesToAdapter> lst = new ArrayList<PreferencesToAdapter>();
		lst.add(new PreferencesToAdapter(2, "preferences[notify_new_like]", res.getString(R.string.alerta_notify_new_like), true));
		lst.add(new PreferencesToAdapter(3, "preferences[notify_new_comment]", res.getString(R.string.alerta_notify_new_comment), true));
		lst.add(new PreferencesToAdapter(4, "preferences[notify_post_deleted]", res.getString(R.string.alerta_notify_post_deleted), true));
		lst.add(new PreferencesToAdapter(5, "preferences[notify_post_reported]", res.getString(R.string.alerta_notify_post_reported), true));
		lst.add(new PreferencesToAdapter(6, "preferences[notify_comment_reported]", res.getString(R.string.alerta_notify_comment_reported), true));
		lst.add(new PreferencesToAdapter(7, "preferences[notify_password_updated]", res.getString(R.string.alerta_notify_password_updated), true));
		lst.add(new PreferencesToAdapter(8, "preferences[notify_removed_from_circle]", res.getString(R.string.alerta_notify_removed_from_circle), true));
		lst.add(new PreferencesToAdapter(9, "preferences[notify_friend_invite_signup]", res.getString(R.string.alerta_notify_friend_invite_signup), true));
		lst.add(new PreferencesToAdapter(10, "preferences[notify_pending_request_approved]", res.getString(R.string.alerta_notify_pending_request_approved), true));
		lst.add(new PreferencesToAdapter(16, "preferences[notify_pending_request_declined]", res.getString(R.string.alerta_notify_pending_request_declined), true));
		lst.add(new PreferencesToAdapter(13, "preferences[notify_friendship_circle_admin_received]", res.getString(R.string.alerta_notify_friendship_circle_admin_received), true));
		lst.add(new PreferencesToAdapter(14, "preferences[notify_circle_new_member]", res.getString(R.string.alerta_notify_circle_new_member), true));
		lst.add(new PreferencesToAdapter(17, "preferences[notify_new_pending_circle_member]", res.getString(R.string.alerta_notify_new_pending_circle_member), true));

		return lst;
	}
	
	public List<PreferencesToAdapter> getPreferencesToAdapter(Resources res) {
		List<PreferencesToAdapter> lst = new ArrayList<PreferencesToAdapter>();
		lst.add(new PreferencesToAdapter(2, "preferences[notify_new_like]", res.getString(R.string.alerta_notify_new_like), notifyNewLike));
		lst.add(new PreferencesToAdapter(3, "preferences[notify_new_comment]", res.getString(R.string.alerta_notify_new_comment), notifyNewComment));
		lst.add(new PreferencesToAdapter(4, "preferences[notify_post_deleted]", res.getString(R.string.alerta_notify_post_deleted), notifyPostDeleted));
		lst.add(new PreferencesToAdapter(5, "preferences[notify_post_reported]", res.getString(R.string.alerta_notify_post_reported), notifyPostReported));
		lst.add(new PreferencesToAdapter(6, "preferences[notify_comment_reported]", res.getString(R.string.alerta_notify_comment_reported), notifyCommentReported));
		lst.add(new PreferencesToAdapter(7, "preferences[notify_password_updated]", res.getString(R.string.alerta_notify_password_updated), notifyPasswordUpdated));
		lst.add(new PreferencesToAdapter(8, "preferences[notify_removed_from_circle]", res.getString(R.string.alerta_notify_removed_from_circle), notifyRemovedFromCircle));
		lst.add(new PreferencesToAdapter(9, "preferences[notify_friend_invite_signup]", res.getString(R.string.alerta_notify_friend_invite_signup), notifyFriendInviteSignup));
		lst.add(new PreferencesToAdapter(10, "preferences[notify_pending_request_approved]", res.getString(R.string.alerta_notify_pending_request_approved), notifyPendingRequestApproved));
		lst.add(new PreferencesToAdapter(16, "preferences[notify_pending_request_declined]", res.getString(R.string.alerta_notify_pending_request_declined), notifyPendingRequestDeclined));
		lst.add(new PreferencesToAdapter(13, "preferences[notify_friendship_circle_admin_received]", res.getString(R.string.alerta_notify_friendship_circle_admin_received), notifyFriendshipCircleAdminReceived));
		lst.add(new PreferencesToAdapter(14, "preferences[notify_circle_new_member]", res.getString(R.string.alerta_notify_circle_new_member), notifyCircleNewMember));
		lst.add(new PreferencesToAdapter(17, "preferences[notify_new_pending_circle_member]", res.getString(R.string.alerta_notify_new_pending_circle_member), notifyNewPendingCircleMember));

		return lst;
	}

	/**
	 * @return the notifySignup
	 */
	public boolean isNotifySignup() {
		return notifySignup;
	}

	/**
	 * @param notifySignup the notifySignup to set
	 */
	public void setNotifySignup(boolean notifySignup) {
		this.notifySignup = notifySignup;
	}

	/**
	 * @return the notifyNewLike
	 */
	public boolean isNotifyNewLike() {
		return notifyNewLike;
	}

	/**
	 * @param notifyNewLike the notifyNewLike to set
	 */
	public void setNotifyNewLike(boolean notifyNewLike) {
		this.notifyNewLike = notifyNewLike;
	}

	/**
	 * @return the notifyNewComment
	 */
	public boolean isNotifyNewComment() {
		return notifyNewComment;
	}

	/**
	 * @param notifyNewComment the notifyNewComment to set
	 */
	public void setNotifyNewComment(boolean notifyNewComment) {
		this.notifyNewComment = notifyNewComment;
	}

	/**
	 * @return the notifyPostDeleted
	 */
	public boolean isNotifyPostDeleted() {
		return notifyPostDeleted;
	}

	/**
	 * @param notifyPostDeleted the notifyPostDeleted to set
	 */
	public void setNotifyPostDeleted(boolean notifyPostDeleted) {
		this.notifyPostDeleted = notifyPostDeleted;
	}

	/**
	 * @return the notifyPostReported
	 */
	public boolean isNotifyPostReported() {
		return notifyPostReported;
	}

	/**
	 * @param notifyPostReported the notifyPostReported to set
	 */
	public void setNotifyPostReported(boolean notifyPostReported) {
		this.notifyPostReported = notifyPostReported;
	}

	/**
	 * @return the notifyCommentReported
	 */
	public boolean isNotifyCommentReported() {
		return notifyCommentReported;
	}

	/**
	 * @param notifyCommentReported the notifyCommentReported to set
	 */
	public void setNotifyCommentReported(boolean notifyCommentReported) {
		this.notifyCommentReported = notifyCommentReported;
	}

	/**
	 * @return the notifyPasswordUpdated
	 */
	public boolean isNotifyPasswordUpdated() {
		return notifyPasswordUpdated;
	}

	/**
	 * @param notifyPasswordUpdated the notifyPasswordUpdated to set
	 */
	public void setNotifyPasswordUpdated(boolean notifyPasswordUpdated) {
		this.notifyPasswordUpdated = notifyPasswordUpdated;
	}

	/**
	 * @return the notifyRemovedFromCircle
	 */
	public boolean isNotifyRemovedFromCircle() {
		return notifyRemovedFromCircle;
	}

	/**
	 * @param notifyRemovedFromCircle the notifyRemovedFromCircle to set
	 */
	public void setNotifyRemovedFromCircle(boolean notifyRemovedFromCircle) {
		this.notifyRemovedFromCircle = notifyRemovedFromCircle;
	}

	/**
	 * @return the notifyFriendInviteSignup
	 */
	public boolean isNotifyFriendInviteSignup() {
		return notifyFriendInviteSignup;
	}

	/**
	 * @param notifyFriendInviteSignup the notifyFriendInviteSignup to set
	 */
	public void setNotifyFriendInviteSignup(boolean notifyFriendInviteSignup) {
		this.notifyFriendInviteSignup = notifyFriendInviteSignup;
	}

	/**
	 * @return the notifyPendingRequestApproved
	 */
	public boolean isNotifyPendingRequestApproved() {
		return notifyPendingRequestApproved;
	}

	/**
	 * @param notifyPendingRequestApproved the notifyPendingRequestApproved to set
	 */
	public void setNotifyPendingRequestApproved(boolean notifyPendingRequestApproved) {
		this.notifyPendingRequestApproved = notifyPendingRequestApproved;
	}

	/**
	 * @return the notifySubscriptionWillExpire
	 */
	public boolean isNotifySubscriptionWillExpire() {
		return notifySubscriptionWillExpire;
	}

	/**
	 * @param notifySubscriptionWillExpire the notifySubscriptionWillExpire to set
	 */
	public void setNotifySubscriptionWillExpire(boolean notifySubscriptionWillExpire) {
		this.notifySubscriptionWillExpire = notifySubscriptionWillExpire;
	}
	
	/**
	 * @return the notifyFriendshipCircleAdminReceived
	 */
	public boolean isNotifyFriendshipCircleAdminReceived() {
		return notifyFriendshipCircleAdminReceived;
	}

	/**
	 * @param notifyFriendshipCircleAdminReceived the notifyFriendshipCircleAdminReceived to set
	 */
	public void setNotifyFriendshipCircleAdminReceived(
			boolean notifyFriendshipCircleAdminReceived) {
		this.notifyFriendshipCircleAdminReceived = notifyFriendshipCircleAdminReceived;
	}

	/**
	 * @return the notifySubscriptionSuccessfullyCharged
	 */
	public boolean isNotifySubscriptionSuccessfullyCharged() {
		return notifySubscriptionSuccessfullyCharged;
	}

	/**
	 * @param notifySubscriptionSuccessfullyCharged the notifySubscriptionSuccessfullyCharged to set
	 */
	public void setNotifySubscriptionSuccessfullyCharged(
			boolean notifySubscriptionSuccessfullyCharged) {
		this.notifySubscriptionSuccessfullyCharged = notifySubscriptionSuccessfullyCharged;
	}

	/**
	 * @return the notifyCircleNewMember
	 */
	public boolean isNotifyCircleNewMember() {
		return notifyCircleNewMember;
	}

	/**
	 * @param notifyCircleNewMember the notifyCircleNewMember to set
	 */
	public void setNotifyCircleNewMember(boolean notifyCircleNewMember) {
		this.notifyCircleNewMember = notifyCircleNewMember;
	}

	/**
	 * @return the notifyPendingRequestDeclined
	 */
	public boolean isNotifyPendingRequestDeclined() {
		return notifyPendingRequestDeclined;
	}

	/**
	 * @param notifyPendingRequestDeclined the notifyPendingRequestDeclined to set
	 */
	public void setNotifyPendingRequestDeclined(boolean notifyPendingRequestDeclined) {
		this.notifyPendingRequestDeclined = notifyPendingRequestDeclined;
	}

	/**
	 * @return the notifyNewPendingCircleMember
	 */
	public boolean isNotifyNewPendingCircleMember() {
		return notifyNewPendingCircleMember;
	}

	/**
	 * @param notifyNewPendingCircleMember the notifyNewPendingCircleMember to set
	 */
	public void setNotifyNewPendingCircleMember(boolean notifyNewPendingCircleMember) {
		this.notifyNewPendingCircleMember = notifyNewPendingCircleMember;
	}

}
