package com.doisdoissete.hippokrates.model;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.doisdoissete.hippokrates.util.HippUtil;
import com.google.gson.annotations.SerializedName;

public class Post implements Parcelable {

	private Long id;
	@SerializedName("base_post_id")
	private Long basePostId;
	@SerializedName("image_url")
	private String imageUrl;
	@SerializedName("likes_count")
	private Long qtLikes;
	@SerializedName("comments_count")
	private Long qtComentarios;
	@SerializedName("content")
	private String descricao;
	
	@SerializedName("friendship_circle_id")
	private Long circleId;
	
	@SerializedName("user_id")
	private Long userId;
	@SerializedName("created_at")
	private Date createdAt;
	
	@SerializedName("updated_at")
	private Date updatedAt;

	@SerializedName("medical_specialty_id")
	private Long medicalSpecialtyId;
	
	private IdNameDescModel medicalSpecialty;
	
	@SerializedName("user_liked")
	private boolean userLiked;
	
	//usado para deletar
	private boolean mark;
	
	@SerializedName("base_post")
	private BasePost basePost;

	
	private User user;
	
	@SerializedName("friendship_circle")
	private Circle circle;
	
	public static Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>(){
		public Post createFromParcel(Parcel source) {
			return new Post(source);
		}
		public Post[] newArray(int size) {
			return new Post[size];
		}
	};

	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the basePostId
	 */
	public Long getBasePostId() {
		return basePostId;
	}
	/**
	 * @param basePostId the basePostId to set
	 */
	public void setBasePostId(Long basePostId) {
		this.basePostId = basePostId;
	}

	/**
	 * @return the qtLikes
	 */
	public String getQtLikesStr() {
		return HippUtil.formatQtde(qtLikes);
	}
	/**
	 * @return the qtLikes
	 */
	public Long getQtLikes() {
		return qtLikes;
	}
	/**
	 * @param qtLikes the qtLikes to set
	 */
	public void setQtLikes(Long qtLikes) {
		this.qtLikes = qtLikes;
	}
	/**
	 * @return the qtComentarios
	 */
	public String getQtComentariosStr() {
		return HippUtil.formatQtde(qtComentarios);
	}
	/**
	 * @return the qtComentarios
	 */
	public Long getQtComentarios() {
		return qtComentarios;
	}
	/**
	 * @param qtComentarios the qtComentarios to set
	 */
	public void setQtComentarios(Long qtComentarios) {
		this.qtComentarios = qtComentarios;
	}
	public void addQtComentarios() {
		if (this.qtComentarios == null) {
			this.qtComentarios = 1L;
		} else {
			this.qtComentarios += 1;
		}
	}
	public void reduceQtComentarios() {
		if (this.qtComentarios == null) {
			this.qtComentarios = 0L;
		} else {
			this.qtComentarios -= 1;
		}
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the circleId
	 */
	public Long getCircleId() {
		return circleId;
	}
	/**
	 * @param circleId the circleId to set
	 */
	public void setCircleId(Long circleId) {
		this.circleId = circleId;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the createdAt
	 */
	public String getCreatedAtStr() {
		if (createdAt == null) {
			return "";
		}
		return DateUtil.dfDiaMesAnoHoraMinuto.get().format(createdAt);
	}
	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}
	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the circle
	 */
	public Circle getCircle() {
		return circle;
	}
	/**
	 * @param circle the circle to set
	 */
	public void setCircle(Circle circle) {
		this.circle = circle;
	}
	
	
	/**
	 * @return the medicalSpecialtyId
	 */
	public Long getMedicalSpecialtyId() {
		return medicalSpecialtyId;
	}
	/**
	 * @param medicalSpecialtyId the medicalSpecialtyId to set
	 */
	public void setMedicalSpecialtyId(Long medicalSpecialtyId) {
		this.medicalSpecialtyId = medicalSpecialtyId;
	}
	
	
	/**
	 * @return the medicalSpecialty
	 */
	public IdNameDescModel getMedicalSpecialty() {
		return medicalSpecialty;
	}
	
	/**
	 * @return the mark
	 */
	public boolean isMark() {
		return mark;
	}
	/**
	 * @param mark the mark to set
	 */
	public void setMark(boolean mark) {
		this.mark = mark;
	}
	/**
	 * @param medicalSpecialty the medicalSpecialty to set
	 */
	public void setMedicalSpecialty(IdNameDescModel medicalSpecialty) {
		this.medicalSpecialty = medicalSpecialty;
	}
	
	/**
	 * @return the userLiked
	 */
	public boolean isUserLiked() {
		return userLiked;
	}
	
	/**
	 * @return the imagesUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * @param imagesUrl the imagesUrl to set
	 */
	public void setImageUrl(String imagesUrl) {
		this.imageUrl = imagesUrl;
	}
	/**
	 * @param userLiked the userLiked to set
	 */
	public void setUserLiked(boolean userLiked) {
		this.userLiked = userLiked;
	}
	
	
	
	/**
	 * @return the basePost
	 */
	public BasePost getBasePost() {
		return basePost;
	}
	/**
	 * @param basePost the basePost to set
	 */
	public void setBasePost(BasePost basePost) {
		this.basePost = basePost;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((circleId == null) ? 0 : circleId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (circleId == null) {
			if (other.circleId != null)
				return false;
		} else if (!circleId.equals(other.circleId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
	
	private Post(Parcel in) {
		this.id = (Long)in.readValue(Long.class.getClassLoader());
		this.basePostId = (Long)in.readValue(Long.class.getClassLoader());
		this.imageUrl = in.readString();
		this.qtLikes = (Long)in.readValue(Long.class.getClassLoader());
		this.qtComentarios = (Long)in.readValue(Long.class.getClassLoader());
		this.descricao = in.readString();
		this.circleId = (Long)in.readValue(Long.class.getClassLoader());
		this.userId = (Long)in.readValue(Long.class.getClassLoader());
		long tmpCreatedAt = in.readLong(); 
		this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
		long tmpUpdatedAt = in.readLong(); 
		this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
		this.medicalSpecialtyId = (Long)in.readValue(Long.class.getClassLoader());
		this.medicalSpecialty = in.readParcelable(IdNameDescModel.class.getClassLoader());
		this.userLiked = in.readByte() != 0;
		this.mark = in.readByte() != 0;
		this.user = in.readParcelable(User.class.getClassLoader());
		this.circle = in.readParcelable(Circle.class.getClassLoader());
	}
	public int describeContents() { 
		return 0; 
	}
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.basePostId);
		dest.writeString(this.imageUrl);
		dest.writeValue(this.qtLikes);
		dest.writeValue(this.qtComentarios);
		dest.writeString(this.descricao);
		dest.writeValue(this.circleId);
		dest.writeValue(this.userId);
		dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
		dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1);
		dest.writeValue(this.medicalSpecialtyId);
		dest.writeParcelable(this.medicalSpecialty, flags);
		dest.writeByte(userLiked ? (byte) 1 : (byte) 0);
		dest.writeByte(mark ? (byte) 1 : (byte) 0);
		dest.writeParcelable(this.user, flags);
		dest.writeParcelable(this.circle, flags);
	}
	
	
	
}
