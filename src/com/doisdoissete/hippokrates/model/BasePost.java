package com.doisdoissete.hippokrates.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class BasePost implements Parcelable {
	
	private Long id;
	
	@SerializedName("privacy_status")
	private String privacyStatus;

	@SerializedName("image_url")
	private String imagesUrl;

	@SerializedName("created_at")	
	private Date createdAt;
	
	@SerializedName("updatedAt")	
	private Date updated_at;
	
	@SerializedName("user_id")
	private Long userId;
	
	@SerializedName("friendship_circle_ids")
	private List<Long> friendshipCircleIds;
	
	@SerializedName("medical_specialty_id")
	private Long medicalSpecialtyId;
	
	@SerializedName("content")
	private String content;
	
	private List<Post> posts;

	private User user;
	
	private IdNameDescModel medicalSpecialty;

	public static Parcelable.Creator<BasePost> CREATOR = new Parcelable.Creator<BasePost>(){
		public BasePost createFromParcel(Parcel source) {
			return new BasePost(source);
		}
		public BasePost[] newArray(int size) {
			return new BasePost[size];
		}
	};
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the privacyStatus
	 */
	public String getPrivacyStatus() {
		return privacyStatus;
	}

	/**
	 * @param privacyStatus the privacyStatus to set
	 */
	public void setPrivacyStatus(String privacyStatus) {
		this.privacyStatus = privacyStatus;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updated_at
	 */
	public Date getUpdated_at() {
		return updated_at;
	}

	/**
	 * @param updated_at the updated_at to set
	 */
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the friendshipCircleIds
	 */
	public List<Long> getFriendshipCircleIds() {
		return friendshipCircleIds;
	}

	/**
	 * @param friendshipCircleIds the friendshipCircleIds to set
	 */
	public void setFriendshipCircleIds(List<Long> friendshipCircleIds) {
		this.friendshipCircleIds = friendshipCircleIds;
	}

	/**
	 * @return the medicalSpecialtyId
	 */
	public Long getMedicalSpecialtyId() {
		return medicalSpecialtyId;
	}

	/**
	 * @param medicalSpecialtyId the medicalSpecialtyId to set
	 */
	public void setMedicalSpecialtyId(Long medicalSpecialtyId) {
		this.medicalSpecialtyId = medicalSpecialtyId;
	}

	/**
	 * @return the post
	 */
	public List<Post> getPosts() {
		return posts;
	}

	/**
	 * @param post the post to set
	 */
	public void setPosts(List<Post> post) {
		this.posts = post;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the medicalSpecialty
	 */
	public IdNameDescModel getMedicalSpecialty() {
		return medicalSpecialty;
	}

	/**
	 * @param medicalSpecialty the medicalSpecialty to set
	 */
	public void setMedicalSpecialty(IdNameDescModel medicalSpecialty) {
		this.medicalSpecialty = medicalSpecialty;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the imagesUrl
	 */
	public String getImagesUrl() {
		return imagesUrl;
	}

	/**
	 * @param imagesUrl the imagesUrl to set
	 */
	public void setImagesUrl(String imagesUrl) {
		this.imagesUrl = imagesUrl;
	}

	private BasePost(Parcel in) {
		this.id = (Long)in.readValue(Long.class.getClassLoader());
		this.privacyStatus = in.readString();
		this.imagesUrl = in.readString();
		long tmpCreatedAt = in.readLong(); 
		this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
		long tmpUpdated_at = in.readLong(); 
		this.updated_at = tmpUpdated_at == -1 ? null : new Date(tmpUpdated_at);
		this.userId = (Long)in.readValue(Long.class.getClassLoader());
		this.friendshipCircleIds = new ArrayList<Long>();
		in.readList(this.friendshipCircleIds ,Long.class.getClassLoader());
		this.medicalSpecialtyId = (Long)in.readValue(Long.class.getClassLoader());
		this.content = in.readString();
		this.posts = new ArrayList<Post>();
		in.readTypedList(posts, Post.CREATOR);
		this.user = in.readParcelable(User.class.getClassLoader());
		this.medicalSpecialty = in.readParcelable(IdNameDescModel.class.getClassLoader());//Unknown Type. Consider to implement Parcelable
	}

	public int describeContents() { 
		return 0; 
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.privacyStatus);
		dest.writeString(this.imagesUrl);
		dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
		dest.writeLong(updated_at != null ? updated_at.getTime() : -1);
		dest.writeValue(this.userId);
		dest.writeList(this.friendshipCircleIds);
		dest.writeValue(this.medicalSpecialtyId);
		dest.writeString(this.content);
		dest.writeTypedList(posts);
		dest.writeParcelable(this.user, flags);
		dest.writeParcelable(this.medicalSpecialty, flags);//Unknown Type. Consider to implement Parcelable
	}
	
/*
 "id": 10242,
      "privacy_status": "public",
      "content": "#cirurgia-2 Teste de postagem com foto e multiplas linha de conteudo.\\\\nLinha 2\\\\nLinha 3\\\\n\\\\n\\\\n\\\\n\\\\nLinha  9 #cirurgia #ops",
      "images": {
        "small_thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/small_thumb_daft.jpg",
        "thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/thumb_daft.jpg",
        "medium": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/medium_daft.jpg",
        "big": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/big_daft.jpg"
      },
      "created_at": "2015-06-08T13:49:19.139-03:00",
      "updated_at": "2015-06-08T13:49:19.139-03:00",
      "user_id": 1,
      "friendship_circle_ids": [
        1
      ],
      "medical_specialty_id": 50,
 */
}
