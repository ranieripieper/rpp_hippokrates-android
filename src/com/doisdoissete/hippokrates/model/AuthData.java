package com.doisdoissete.hippokrates.model;

import com.google.gson.annotations.SerializedName;

public class AuthData {

	@SerializedName("auth_token")
	private String authToken;
	private String provider;
	@SerializedName("expires_at") // 2015-06-18T15:36:12.922-03:00
	private String expiresAt;
	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}
	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}
	/**
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}
	/**
	 * @return the expiresAt
	 */
	public String getExpiresAt() {
		return expiresAt;
	}
	/**
	 * @param expiresAt the expiresAt to set
	 */
	public void setExpiresAt(String expiresAt) {
		this.expiresAt = expiresAt;
	}

	
}
