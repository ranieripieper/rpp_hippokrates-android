package com.doisdoissete.hippokrates.model.local;

import java.util.ArrayList;
import java.util.List;

import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.util.ProfileUtil;

public class LocalUser {

	private Long id;
	private String email;
	private String username;
	private String fullName;
	private String profileType;
	private String authToken;
	private String profileImg;
	private List<Circle> circles; 
	private int remainingInvites;
	
	public User getUser() {
		User user = new User();
		user.setId(this.id);
		user.setFullName(this.fullName);
		user.setProfileImageUrl(this.profileImg);
		user.setProfileType(this.profileType);
		return user;
	}
	
	public void update(User user) {
		setId(user.getId());
		setEmail(user.getEmail());
		setFullName(user.getFullName());
		setUsername(user.getUsername());
		setProfileType(user.getProfileType());
		setProfileImg(user.getProfileImageUrl());
	}
	
	public boolean isCircleAdmin(Long circleId) {
		if (circleId != null && this.circles != null) {
			for (Circle circle : this.circles) {
				if (circleId.equals(circle.getId())) {
					if (this.id.equals(circle.getAdminUserId())) {
						return true;
					}
					return false;
				}
			}
		}
		return false;
		
	}
	
	public Circle getCircle(Long circleId) {
		if (circleId != null && this.circles != null) {
			for (Circle circle : this.circles) {
				if (circleId.equals(circle.getId())) {
					return circle;
				}
			}
		}
		return null;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}
	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @return the profileImg
	 */
	public String getProfileImg() {
		return profileImg;
	}

	/**
	 * @param profileImg the profileImg to set
	 */
	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	/**
	 * @return the profileType
	 */
	public String getProfileType() {
		return profileType;
	}
	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	/**
	 * @return the circles
	 */
	public List<Circle> getCircles() {
		return circles;
	}
	/**
	 * @param circles the circles to set
	 */
	public void setCircles(List<Circle> circles) {
		this.circles = circles;
	}
	
	
	
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the remainingInvites
	 */
	public int getRemainingInvites() {
		return remainingInvites;
	}

	/**
	 * @param remainingInvites the remainingInvites to set
	 */
	public void setRemainingInvites(int remainingInvites) {
		this.remainingInvites = remainingInvites;
	}

	public void addCircles(List<Circle> lstCircles) {
		if (this.circles == null) {
			this.circles = new ArrayList<Circle>();
		}
		if (lstCircles != null) {
			this.circles.addAll(lstCircles);
		}
		
	}
	
	public void addCircle(Circle circle) {
		if (this.circles == null) {
			this.circles = new ArrayList<Circle>();
		}
		this.circles.remove(circle);
		if (circles != null && circle != null) {
			this.circles.add(circle);
		}		
	}
	
	public void updateCircle(Circle circle) {
		if (circle != null && this.circles != null) {
			int indexRemove = -1;
			for (int i = 0; i < this.circles.size(); i++) {
				if (circle.getId().equals(this.circles.get(i).getId())) {
					indexRemove = i;
					break;
				}
			}
			if (indexRemove >= 0) {
				this.circles.remove(indexRemove);
				this.circles.add(indexRemove, circle);
			}
		}
	}
	
	public void removeCircle(Circle circle) {
		if (circle != null && this.circles != null) {
			int indexRemove = -1;
			for (int i = 0; i < this.circles.size(); i++) {
				if (circle.getId().equals(this.circles.get(i).getId())) {
					indexRemove = i;
					break;
				}
			}
			if (indexRemove >= 0) {
				this.circles.remove(indexRemove);
			}
		}
	}
	
	
	public boolean isDoctor() {
		return ProfileUtil.isDoctor(this.profileType);
	}
	
	public boolean isStudent() {
		return ProfileUtil.isStudent(this.profileType);
	}
	
	public boolean isTeacher() {
		return ProfileUtil.isTeacher(this.profileType);
	}
	
	public boolean isAdmin() {
		return ProfileUtil.isAdmin(this.profileType);
	}
	
	
}
