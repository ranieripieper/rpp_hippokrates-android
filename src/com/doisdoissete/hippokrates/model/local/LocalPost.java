package com.doisdoissete.hippokrates.model.local;

import java.util.Date;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.util.HippUtil;
import com.doisdoissete.hippokrates.view.adapter.post.PostAdapter;

@Table(name="LocalPost")
public class LocalPost extends Model {

	@Column(name = "postId")
	private Long postId;
	@Column(name = "basePostId")
	private Long basePostId;
	@Column(name = "likesCount")
	private Long likesCount;
	@Column(name = "commentsCount")
	private Long commentsCount;
	@Column(name = "circleId")
	private Long circleId;
	@Column(name = "dtUpdated")
	private Date dtUpdated;
	@Column(name = "userLiked")
	private boolean userLiked;
	@Column(name = "imageUrl")
	private String imageUrl;
	@Column(name = "description")
	private String description;
	@Column(name = "deleted")
	private boolean deleted;
	
	public LocalPost() {
	}
	
	public static LocalPost getLocalPost(Post post) {
		return new Select().from(LocalPost.class).where("postId = ? and circleId = ? ", post.getId(), post.getCircleId()).executeSingle();
	}
	
	public static List<LocalPost> getDeletedLocalPost() {
		return new Select().from(LocalPost.class).where("deleted = ? ", true).execute();
	}
	
/*	public static void remove(List<Long> ids) {
		if (ids != null) {
			ActiveAndroid.beginTransaction();
			try {
				for (Long id : ids) {
					new Delete().from(LocalPost.class).where("postId = ? ", id).execute();
				}
			}
			finally {
				ActiveAndroid.endTransaction();
			}
		}
	}*/
	
	public static void updatePost(Post post) {
		LocalPost localPost = new Select().from(LocalPost.class).where("postId = ? and circleId = ? ", post.getId(), post.getCircleId()).executeSingle();
		if (localPost == null) {
			localPost = new LocalPost();
		}
		localPost.setBasePostId(post.getBasePostId());
		localPost.setCircleId(post.getCircleId());
		localPost.setCommentsCount(post.getQtComentarios());
		localPost.setLikesCount(post.getQtLikes());
		localPost.setDtUpdated(new Date());
		localPost.setPostId(post.getId());
		localPost.setUserLiked(post.isUserLiked());
		localPost.setImageUrl(post.getImageUrl());
		localPost.setDescription(post.getDescricao());
		localPost.save();
	}
	
	public static void updateImageUrl(Long basePostId, String imageUrl, String desc) {
		List<LocalPost> localPost = new Select().from(LocalPost.class).where("basePostId = ?", basePostId).execute();
		if (localPost == null) {
			return;
		}
		for (LocalPost obj : localPost) {
			obj.setImageUrl(imageUrl);
			obj.setDtUpdated(new Date());
			obj.setDescription(desc);
			obj.save();
		}
	}
	
	public static void updateLikesCount(Long postId, Long circleId, Long likesCount, boolean userLike) {
		LocalPost localPost = new Select().from(LocalPost.class).where("postId = ? and circleId = ? ", postId, circleId).executeSingle();
		if (localPost == null) {
			return;
		}
		localPost.setLikesCount(likesCount);
		localPost.setUserLiked(userLike);
		localPost.setDtUpdated(new Date());
		localPost.save();
	}
	
	public static void updateCommentsCount(Long postId, Long circleId, Long commentsCount) {
		LocalPost localPost = new Select().from(LocalPost.class).where("postId = ? and circleId = ? ", postId, circleId).executeSingle();
		if (localPost == null) {
			return;
		}
		localPost.setCommentsCount(commentsCount);
		localPost.setDtUpdated(new Date());
		localPost.save();
	}
	
	public static void deleteBasePost(Long basePostId) {
		List<LocalPost> localPost = new Select().from(LocalPost.class).where("basePostId = ?", basePostId).execute();
		if (localPost == null) {
			return;
		}
		for (LocalPost obj : localPost) {
			obj.setDeleted(true);
			obj.save();
		}
	}
	
	public static void deletePost(Long postId) {
		List<LocalPost> localPost = new Select().from(LocalPost.class).where("postId = ?", postId).execute();
		if (localPost == null) {
			return;
		}
		for (LocalPost obj : localPost) {
			obj.setDeleted(true);
			obj.save();
		}
	}
	
	public static void deletePosts(List<Long> postIds, PostAdapter postAdapter) {
		if (postIds != null) {
			for (Long id : postIds) {
				if (postAdapter != null) {
					postAdapter.removePost(id);
				}
				deletePost(id);
			}
		}
	}


	/**
	 * @return the postId
	 */
	public Long getPostId() {
		return postId;
	}

	/**
	 * @param postId the postId to set
	 */
	public void setPostId(Long postId) {
		this.postId = postId;
	}

	/**
	 * @return the likesCount
	 */
	public Long getLikesCount() {
		return likesCount;
	}
	/**
	 * @param likesCount the likesCount to set
	 */
	public void setLikesCount(Long likesCount) {
		this.likesCount = likesCount;
	}
	/**
	 * @return the commentsCount
	 */
	public Long getCommentsCount() {
		return commentsCount;
	}
	/**
	 * @param commentsCount the commentsCount to set
	 */
	public void setCommentsCount(Long commentsCount) {
		this.commentsCount = commentsCount;
	}
	/**
	 * @return the circleId
	 */
	public Long getCircleId() {
		return circleId;
	}
	/**
	 * @param circleId the circleId to set
	 */
	public void setCircleId(Long circleId) {
		this.circleId = circleId;
	}

	/**
	 * @return the dtUpdated
	 */
	public Date getDtUpdated() {
		return dtUpdated;
	}

	/**
	 * @param dtUpdated the dtUpdated to set
	 */
	public void setDtUpdated(Date dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	/**
	 * @return the userLike
	 */
	public boolean isUserLiked() {
		return userLiked;
	}

	/**
	 * @param userLike the userLike to set
	 */
	public void setUserLiked(boolean userLiked) {
		this.userLiked = userLiked;
	}
	
	/**
	 * @return the likesCount
	 */
	public String getLikesCountStr() {
		return HippUtil.formatQtde(likesCount);
	}
	
	/**
	 * @return the commentsCount
	 */
	public String getCommentsCountStr() {
		return HippUtil.formatQtde(commentsCount);
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the basePostId
	 */
	public Long getBasePostId() {
		return basePostId;
	}

	/**
	 * @param basePostId the basePostId to set
	 */
	public void setBasePostId(Long basePostId) {
		this.basePostId = basePostId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}
