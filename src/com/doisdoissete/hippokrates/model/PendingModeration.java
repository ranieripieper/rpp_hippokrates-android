package com.doisdoissete.hippokrates.model;

public class PendingModeration {

	private Long id;
	private String status;
	private User user;
	private Long moderableId;
	
	private boolean approve;
	private boolean decline;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the moderableId
	 */
	public Long getModerableId() {
		return moderableId;
	}
	/**
	 * @param moderableId the moderableId to set
	 */
	public void setModerableId(Long moderableId) {
		this.moderableId = moderableId;
	}
	/**
	 * @return the approve
	 */
	public boolean isApprove() {
		return approve;
	}
	/**
	 * @param approve the approve to set
	 */
	public void setApprove(boolean approve) {
		this.approve = approve;
	}
	/**
	 * @return the decline
	 */
	public boolean isDecline() {
		return decline;
	}
	/**
	 * @param decline the decline to set
	 */
	public void setDecline(boolean decline) {
		this.decline = decline;
	}
	
}
