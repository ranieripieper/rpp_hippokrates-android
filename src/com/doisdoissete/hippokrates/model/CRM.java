package com.doisdoissete.hippokrates.model;

import com.google.gson.annotations.SerializedName;

public class CRM {

	public static String FIELD_GET_FIRST_NAME = "first_name";
	public static String FIELD_GET_LAST_NAME = "last_name";
	public static String FIELD_GET_CRM_NUMBER = "number";
	public static String FIELD_GET_CRM_STATE = "state";
	
	@SerializedName("doctor_name")
	private String doctorName;
	private String status;
	private Integer number;
	private String state;
	private String specialities;
	
	/**
	 * @return the doctorName
	 */
	public String getDoctorName() {
		return doctorName;
	}
	/**
	 * @param doctorName the doctorName to set
	 */
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the number
	 */
	public Integer getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the specialities
	 */
	public String getSpecialities() {
		return specialities;
	}
	/**
	 * @param specialities the specialities to set
	 */
	public void setSpecialities(String specialities) {
		this.specialities = specialities;
	}
	
	
}
