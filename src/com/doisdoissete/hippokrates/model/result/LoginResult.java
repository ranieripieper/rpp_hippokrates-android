package com.doisdoissete.hippokrates.model.result;

import com.doisdoissete.hippokrates.model.AuthData;
import com.doisdoissete.hippokrates.model.User;
import com.google.gson.annotations.SerializedName;

public class LoginResult extends BaseResult {

	@SerializedName("user_data")
	private User user;
	@SerializedName("auth_data")
	private AuthData authData;
	
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the authData
	 */
	public AuthData getAuthData() {
		return authData;
	}
	/**
	 * @param authData the authData to set
	 */
	public void setAuthData(AuthData authData) {
		this.authData = authData;
	}
	
	
}
