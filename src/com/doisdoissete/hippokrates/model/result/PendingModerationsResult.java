package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.PendingModeration;
import com.google.gson.annotations.SerializedName;

public class PendingModerationsResult extends BaseResult {

	@SerializedName("pending_moderations")
	private List<PendingModeration> lstPendingModeration;

	/**
	 * @return the lstPendingModeration
	 */
	public List<PendingModeration> getLstPendingModeration() {
		return lstPendingModeration;
	}

	/**
	 * @param lstPendingModeration the lstPendingModeration to set
	 */
	public void setLstPendingModeration(List<PendingModeration> lstPendingModeration) {
		this.lstPendingModeration = lstPendingModeration;
	}
	
	
	
	
}
