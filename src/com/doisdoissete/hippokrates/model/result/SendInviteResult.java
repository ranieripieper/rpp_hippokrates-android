package com.doisdoissete.hippokrates.model.result;

import com.google.gson.annotations.SerializedName;

public class SendInviteResult extends BaseResult {

	@SerializedName("remaining_invites")
	private int remainingInvites;

	public int getRemainingInvites() {
		return remainingInvites;
	}

	public void setRemainingInvites(int remainingInvites) {
		this.remainingInvites = remainingInvites;
	}
	
	
}
