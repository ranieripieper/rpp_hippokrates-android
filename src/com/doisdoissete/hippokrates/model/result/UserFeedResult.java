package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.BasePost;
import com.doisdoissete.hippokrates.model.Post;
import com.google.gson.annotations.SerializedName;

public class UserFeedResult extends BaseResult {

	@SerializedName("base_posts")
	private List<BasePost> basePost;
	
	@SerializedName("linked_data")
	private LinkedData linkedData;

	private List<Post> post;
	
	/**
	 * @return the basePost
	 */
	public List<BasePost> getBasePost() {
		return basePost;
	}

	/**
	 * @param basePost the basePost to set
	 */
	public void setBasePost(List<BasePost> basePost) {
		this.basePost = basePost;
	}

	/**
	 * @return the linkedData
	 */
	public LinkedData getLinkedData() {
		return linkedData;
	}

	/**
	 * @param linkedData the linkedData to set
	 */
	public void setLinkedData(LinkedData linkedData) {
		this.linkedData = linkedData;
	}

	/**
	 * @return the post
	 */
	public List<Post> getPost() {
		return post;
	}

	/**
	 * @param post the post to set
	 */
	public void setPost(List<Post> post) {
		this.post = post;
	}
	
	/*
{
  "base_posts": [
    {
      "id": 10242,
      "privacy_status": "public",
      "content": "#cirurgia-2 Teste de postagem com foto e multiplas linha de conteudo.\\\\nLinha 2\\\\nLinha 3\\\\n\\\\n\\\\n\\\\n\\\\nLinha  9 #cirurgia #ops",
      "images": {
        "small_thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/small_thumb_daft.jpg",
        "thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/thumb_daft.jpg",
        "medium": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/medium_daft.jpg",
        "big": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10242/big_daft.jpg"
      },
      "created_at": "2015-06-08T13:49:19.139-03:00",
      "updated_at": "2015-06-08T13:49:19.139-03:00",
      "user_id": 1,
      "friendship_circle_ids": [
        1
      ],
      "medical_specialty_id": 50,
      "posts": [
        {
          "id": 242,
          "base_post_id": 10242,
          "likes_count": 1,
          "comments_count": 4,
          "friendship_circle_id": 1,
          "medical_specialty_id": 50,
          "created_at": "2015-06-08T13:49:24.332-03:00",
          "updated_at": "2015-06-08T13:49:24.332-03:00"
        }
      ]
    },
    {
      "id": 10241,
      "privacy_status": "public",
      "content": "Teste de postagem com foto e multiplas linha de conteudo.\\\\nLinha 2\\\\nLinha 3\\\\n\\\\n\\\\n\\\\n\\\\nLinha  9 #cirurgia #ops",
      "images": {
        "small_thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10241/small_thumb_daft.jpg",
        "thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10241/thumb_daft.jpg",
        "medium": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10241/medium_daft.jpg",
        "big": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10241/big_daft.jpg"
      },
      "created_at": "2015-06-08T13:48:01.258-03:00",
      "updated_at": "2015-06-08T13:48:01.258-03:00",
      "user_id": 1,
      "friendship_circle_ids": [
        1
      ],
      "medical_specialty_id": 50,
      "posts": [
        {
          "id": 241,
          "base_post_id": 10241,
          "likes_count": 1,
          "comments_count": 1,
          "friendship_circle_id": 1,
          "medical_specialty_id": 50,
          "created_at": "2015-06-08T13:48:06.552-03:00",
          "updated_at": "2015-06-08T13:48:06.552-03:00"
        }
      ]
    },
    {
      "id": 240,
      "privacy_status": "public",
      "content": "Teste de post2agem2 com foto e multiplas linha de conteudo.\\\\nLinha 2\\\\nLinha 3\\\\n\\\\n\\\\n\\\\n\\\\nLinha  9 #0-240-hashtag,#voluptatibusitaquequivoluptates,#1-240-hashtag,#iurevoluptaseosmolestias,#2-240-hashtag,#doloribusdelenitiimpeditlaborum,#3-240-hashtag,#natusenimmolestiasrerum,#4-240-hashtag,#minusvoluptatemetaut,#5-240-hashtag,#quasidoloresimiliqueoptio,#6-240-hashtag,#atquirationedicta,#7-240-hashtag,#sedmolestiaedoloribusvoluptatem,#8-240-hashtag,#etinvelquia,#9-240-hashtag,#ducimusprovidentasperioresplaceat,#10-240-hashtag,#nemoeumvelitnesciunt,#11-240-hashtag,#laboriosamtemporibusutlibero,#12-240-hashtag,#eosofficiisquorecusandae,#13-240-hashtag,#ipsamsuntexercitationemex,#14-240-hashtag,#sequimaioresillotempora",
      "images": {
        "small_thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/240/small_thumb_planet-earth-night-lights-space.jpg",
        "thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/240/thumb_planet-earth-night-lights-space.jpg",
        "medium": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/240/medium_planet-earth-night-lights-space.jpg",
        "big": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/240/big_planet-earth-night-lights-space.jpg"
      },
      "created_at": "2015-06-05T02:52:26.933-03:00",
      "updated_at": "2015-06-08T06:19:24.666-03:00",
      "user_id": 1,
      "friendship_circle_ids": [
        1
      ],
      "medical_specialty_id": 1,
      "posts": [
        {
          "id": 240,
          "base_post_id": 240,
          "likes_count": 1,
          "comments_count": 0,
          "friendship_circle_id": 1,
          "medical_specialty_id": 1,
          "created_at": "2015-06-05T02:52:33.038-03:00",
          "updated_at": "2015-06-08T06:19:24.668-03:00"
        }
      ]
    },
    {
      "id": 239,
      "privacy_status": "public",
      "content": "Teste de postagem2 com foto e multiplas linha de conteudo.\\\\nLinha 2\\\\nLinha 3\\\\n\\\\n\\\\n\\\\n\\\\nLinha  9 #0-239-hashtag,#iddelectusundequis,#1-239-hashtag,#sitnesciuntautemrerum,#2-239-hashtag,#reprehenderitsolutaconsequuntureos,#3-239-hashtag,#rationesapienteexcepturiet,#4-239-hashtag,#magnihicetomnis,#5-239-hashtag,#velitnisisuscipitcommodi,#6-239-hashtag,#illovelitveritatiseligendi,#7-239-hashtag,#idomnisveritatisvero,#8-239-hashtag,#enimaspernaturiureet,#9-239-hashtag,#voluptatumetoccaecativeritatis,#10-239-hashtag,#facerealiasquoaccusantium,#11-239-hashtag,#idevenieteligendiquia,#12-239-hashtag,#occaecatiasperioresetminus,#13-239-hashtag,#dictaassumendanostrumet,#14-239-hashtag,#estconsequaturpariaturarchitecto",
      "images": {
        "small_thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/239/small_thumb_fb-share-image.png",
        "thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/239/thumb_fb-share-image.png",
        "medium": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/239/medium_fb-share-image.png",
        "big": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/239/big_fb-share-image.png"
      },
      "created_at": "2015-06-05T02:49:05.441-03:00",
      "updated_at": "2015-06-08T06:19:24.651-03:00",
      "user_id": 1,
      "friendship_circle_ids": [
        1
      ],
      "medical_specialty_id": 1,
      "posts": [
        {
          "id": 239,
          "base_post_id": 239,
          "likes_count": 1,
          "comments_count": 0,
          "friendship_circle_id": 1,
          "medical_specialty_id": 1,
          "created_at": "2015-06-05T02:49:08.981-03:00",
          "updated_at": "2015-06-08T06:19:24.654-03:00"
        }
      ]
    },
    {
      "id": 238,
      "privacy_status": "public",
      "content": "Teste de postagem com foto e multiplas linha de conteudo.\\\\nLinha 2\\\\nLinha 3\\\\n\\\\n\\\\n\\\\n\\\\nLinha  9 #0-238-hashtag,#iustoprovidentmodiautem,#1-238-hashtag,#undemolestiaeetrecusandae,#2-238-hashtag,#seddolorsitsed,#3-238-hashtag,#aliquidquitotamaperiam,#4-238-hashtag,#magnamaliquaminciduntnumquam,#5-238-hashtag,#quoautesseaperiam,#6-238-hashtag,#avelnostrumomnis,#7-238-hashtag,#doloreharumearumnam,#8-238-hashtag,#placeatdoloreslaboriosamsapiente,#9-238-hashtag,#natusipsumisteeum,#10-238-hashtag,#perspiciatismaximeevenietoccaecati,#11-238-hashtag,#quiatemporacupiditatevitae,#12-238-hashtag,#ducimuslaborumquiaqui,#13-238-hashtag,#exercitationemipsumvoluptatemporro,#14-238-hashtag,#dictaeumrationeconsequatur",
      "images": {
        "small_thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/238/small_thumb_fb-share-image.png",
        "thumb": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/238/thumb_fb-share-image.png",
        "medium": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/238/medium_fb-share-image.png",
        "big": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/238/big_fb-share-image.png"
      },
      "created_at": "2015-06-05T02:48:28.713-03:00",
      "updated_at": "2015-06-08T06:19:24.639-03:00",
      "user_id": 1,
      "friendship_circle_ids": [
        1
      ],
      "medical_specialty_id": 1,
      "posts": [
        {
          "id": 238,
          "base_post_id": 238,
          "likes_count": 1,
          "comments_count": 0,
          "friendship_circle_id": 1,
          "medical_specialty_id": 1,
          "created_at": "2015-06-05T02:48:35.401-03:00",
          "updated_at": "2015-06-08T06:19:24.641-03:00"
        }
      ]
    }
  ],
  "linked_data": {
    "users": [
      {
        "id": 1,
        "profile_type": "admin",
        "name": "HipApp",
        "last_name": "Admin",
        "username": "hipappadmin",
        "fullname": "HipApp Admin",
        "profile_images": {
          "mini_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/mini_thumb_default.png",
          "small_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/small_thumb_default.png",
          "thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/thumb_default.png",
          "medium": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/medium_default.png"
        }
      }
    ],
    "friendship_circles": [
      {
        "id": 1,
        "name": "Public Friendship Circle",
        "remaining_members_count": 0,
        "circle_type": "common",
        "admin_user_id": 1,
        "profile_images": {
          "mini_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/mini_thumb_default.png",
          "small_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/small_thumb_default.png",
          "thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/thumb_default.png",
          "medium": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/medium_default.png"
        },
        "created_at": "2015-05-25T12:33:28.771-03:00"
      }
    ],
    "medical_specialties": [
      {
        "id": 50,
        "name": "Radiologia e Diagnóstico por Imagem",
        "description": null
      },
      {
        "id": 1,
        "name": "Acupuntura",
        "description": null
      }
    ]
  },
  "meta": {
    "pagination": {
      "total_count": 5,
      "total_pages": 1,
      "current_page": 1,
      "next_page": null,
      "prev_page": null,
      "per_page": 20
    }
  }
}
	 */
}
