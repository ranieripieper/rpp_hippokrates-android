package com.doisdoissete.hippokrates.model.result;

import java.util.List;

public class SearchHashtagResult extends BaseResult {

	private List<String> hashtags;

	/**
	 * @return the hashtags
	 */
	public List<String> getHashtags() {
		return hashtags;
	}

	/**
	 * @param hashtags the hashtags to set
	 */
	public void setHashtags(List<String> hashtags) {
		this.hashtags = hashtags;
	}
	
	
}
