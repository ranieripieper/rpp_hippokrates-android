package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.doisdoissete.hippokrates.model.User;
import com.google.gson.annotations.SerializedName;

public class LinkedData {

	private List<User> users;
	
	@SerializedName("friendship_circles")
	private List<Circle> friendshipCircles;
	
	@SerializedName("medical_specialties") 
	private List<IdNameDescModel> medicalSpecialties;
	
	/**
	 * @return the users
	 */
	public List<User> getUsers() {
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(List<User> users) {
		this.users = users;
	}
	/**
	 * @return the friendshipCircles
	 */
	public List<Circle> getFriendshipCircles() {
		return friendshipCircles;
	}
	/**
	 * @param friendshipCircles the friendshipCircles to set
	 */
	public void setFriendshipCircles(List<Circle> friendshipCircles) {
		this.friendshipCircles = friendshipCircles;
	}
	/**
	 * @return the medicalSpecialties
	 */
	public List<IdNameDescModel> getMedicalSpecialties() {
		return medicalSpecialties;
	}
	/**
	 * @param medicalSpecialties the medicalSpecialties to set
	 */
	public void setMedicalSpecialties(List<IdNameDescModel> medicalSpecialties) {
		this.medicalSpecialties = medicalSpecialties;
	}
		
}
