package com.doisdoissete.hippokrates.model.result;

import com.doisdoissete.hippokrates.model.Invite;
import com.google.gson.annotations.SerializedName;

public class ValidateInviteResult extends BaseResult {

	@SerializedName("invite_data")
	private Invite invite;

	/**
	 * @return the invite
	 */
	public Invite getInvite() {
		return invite;
	}

	/**
	 * @param invite the invite to set
	 */
	public void setInvite(Invite invite) {
		this.invite = invite;
	}
	
}
