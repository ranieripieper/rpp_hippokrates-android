package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.Notification;

public class NotificationResult extends BaseResult {

	private List<Notification> notifications;

	/**
	 * @return the notifications
	 */
	public List<Notification> getNotifications() {
		return notifications;
	}

	/**
	 * @param notifications the notifications to set
	 */
	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
	
}
