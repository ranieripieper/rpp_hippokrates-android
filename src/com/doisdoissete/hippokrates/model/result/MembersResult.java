package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.User;

public class MembersResult extends BaseResult {

	private List<User> members;

	/**
	 * @return the members
	 */
	public List<User> getMembers() {
		return members;
	}

	/**
	 * @param members the members to set
	 */
	public void setMembers(List<User> members) {
		this.members = members;
	}
	
	
}
