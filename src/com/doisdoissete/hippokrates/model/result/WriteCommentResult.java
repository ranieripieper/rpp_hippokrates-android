package com.doisdoissete.hippokrates.model.result;

import com.doisdoissete.hippokrates.model.Comment;

public class WriteCommentResult extends BaseResult {

	private Comment comment;

	/**
	 * @return the comment
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	
}
