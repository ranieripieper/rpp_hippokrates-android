package com.doisdoissete.hippokrates.model.result;

import com.doisdoissete.hippokrates.model.Circle;
import com.google.gson.annotations.SerializedName;

public class FriendshipCircleResult extends BaseResult {

	@SerializedName("friendship_circle")
	private Circle circle;

	/**
	 * @return the circle
	 */
	public Circle getCircle() {
		return circle;
	}

	/**
	 * @param circle the circle to set
	 */
	public void setCircle(Circle circle) {
		this.circle = circle;
	}


}
