package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.Circle;
import com.google.gson.annotations.SerializedName;

public class MeCirclesResult extends BaseResult {

	@SerializedName("friendship_circles")
	private List<Circle> circles;

	/**
	 * @return the circles
	 */
	public List<Circle> getCircles() {
		return circles;
	}

	/**
	 * @param circles the circles to set
	 */
	public void setCircles(List<Circle> circles) {
		this.circles = circles;
	}
	
	
}
