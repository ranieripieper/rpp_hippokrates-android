package com.doisdoissete.hippokrates.model.result;

import com.doisdoissete.hippokrates.model.User;

public class UserResult extends BaseResult {

	private User user;

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
}
