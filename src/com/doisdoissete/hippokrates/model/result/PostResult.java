package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.Post;
import com.google.gson.annotations.SerializedName;

public class PostResult extends BaseResult {

	@SerializedName("posts")
	private List<Post> posts;
	@SerializedName("linked_data")
	private LinkedData linkedData;

	@SerializedName("friendship_circle")
	private Circle circle;
	
	/**
	 * @return the posts
	 */
	public List<Post> getPosts() {
		return posts;
	}

	/**
	 * @param posts the posts to set
	 */
	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	/**
	 * @return the linkedData
	 */
	public LinkedData getLinkedData() {
		return linkedData;
	}

	/**
	 * @param linkedData the linkedData to set
	 */
	public void setLinkedData(LinkedData linkedData) {
		this.linkedData = linkedData;
	}

	/**
	 * @return the circle
	 */
	public Circle getCircle() {
		return circle;
	}

	/**
	 * @param circle the circle to set
	 */
	public void setCircle(Circle circle) {
		this.circle = circle;
	}
	
	
}
