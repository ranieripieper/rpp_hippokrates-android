package com.doisdoissete.hippokrates.model.result;

import com.doisdoissete.hippokrates.model.BasePost;
import com.doisdoissete.hippokrates.model.User;
import com.google.gson.annotations.SerializedName;

public class PostFotoResult extends BaseResult {

	private User user;
	@SerializedName("base_post")
	private BasePost basePost;
	
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the basePost
	 */
	public BasePost getBasePost() {
		return basePost;
	}

	/**
	 * @param basePost the basePost to set
	 */
	public void setBasePost(BasePost basePost) {
		this.basePost = basePost;
	}
	
	
	/*
{
  "status": 201,
  "success": true,
  "base_post": {
    "id": 10249,
    "privacy_status": "public",
    "content": "Teste de postagem com foto e multiplas linha de conteudo.\\\\nLinha 2\\\\nLinha 3\\\\n\\\\n\\\\n\\\\n\\\\nLinha  9",
    "images": [
        "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10418/IMG_img_to_post20150716_164653.jpg"
    ],
    "image_url": "https://hipapp-staging.s3.amazonaws.com/uploads/base_post/image/10418/IMG_img_to_post20150716_164653.jpg",
    "created_at": "2015-06-16T19:15:22.006-03:00",
    "updated_at": "2015-06-16T19:15:22.006-03:00",
    "user": {
      "id": 586,
      "profile_type": "doctor",
      "name": "jose",
      "last_name": "luiz ranieri",
      "username": "joseluiz_ranieri",
      "fullname": "jose luiz ranieri",
      "profile_images": {
        "mini_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/586/mini_thumb_cameraPic20141113_225639-611338311.jpg",
        "small_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/586/small_thumb_cameraPic20141113_225639-611338311.jpg",
        "thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/586/thumb_cameraPic20141113_225639-611338311.jpg",
        "medium": "https://hipapp-staging.s3.amazonaws.com/images/staging/user/profile_image/586/medium_cameraPic20141113_225639-611338311.jpg"
      }
    },
    "medical_specialty": {
      "id": 1,
      "name": "Acupuntura",
      "description": null
    },
    "friendship_circles": [
      {
        "id": 1,
        "name": "Public Friendship Circle",
        "remaining_members_count": 0,
        "circle_type": "common",
        "admin_user_id": 1,
        "profile_images": {
          "mini_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/mini_thumb_default.png",
          "small_thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/small_thumb_default.png",
          "thumb": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/thumb_default.png",
          "medium": "https://hipapp-staging.s3.amazonaws.com/images/staging/users/fallback/medium_default.png"
        },
        "created_at": "2015-05-25T12:33:28.771-03:00"
      }
    ],
    "posts": [
      {
        "id": 249,
        "base_post_id": 10249,
        "likes_count": 0,
        "comments_count": 0,
        "friendship_circle_id": 1,
        "medical_specialty_id": 1,
        "created_at": "2015-06-16T19:15:27.653-03:00",
        "updated_at": "2015-06-16T19:15:27.653-03:00"
      }
    ]
  }
}
	 */
}
