package com.doisdoissete.hippokrates.model.result;

import com.google.gson.annotations.SerializedName;

public class BaseResult {

	public static Integer STATUS_403 = 403;
	
	private boolean success;
	@SerializedName("status_code")
	private Integer statusCode;
	private boolean updated;
	private boolean error;
	private Meta meta;
	private String[] errors;
	
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}
	/**
	 * @return the error
	 */
	public boolean isError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(boolean error) {
		this.error = error;
	}
	/**
	 * @return the mega
	 */
	public Meta getMeta() {
		return meta;
	}
	/**
	 * @param mega the mega to set
	 */
	public void setMega(Meta meta) {
		this.meta = meta;
	}
	/**
	 * @return the updated
	 */
	public boolean isUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(boolean updated) {
		this.updated = updated;
	}
	/**
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the errors
	 */
	public String[] getErrors() {
		return errors;
	}
	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	
	
}
