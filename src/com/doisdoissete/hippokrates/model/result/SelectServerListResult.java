package com.doisdoissete.hippokrates.model.result;

import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.google.gson.annotations.SerializedName;

public class SelectServerListResult {

	@SerializedName("medical_specialities")
	private IdNameDescModel[] medicalSpecialities;
	
	@SerializedName("colleges")
	private IdNameDescModel[] colleges;

	/**
	 * @return the medicalSpecialities
	 */
	public IdNameDescModel[] getMedicalSpecialities() {
		return medicalSpecialities;
	}

	/**
	 * @param medicalSpecialities the medicalSpecialities to set
	 */
	public void setMedicalSpecialities(IdNameDescModel[] medicalSpecialities) {
		this.medicalSpecialities = medicalSpecialities;
	}

	/**
	 * @return the colleges
	 */
	public IdNameDescModel[] getColleges() {
		return colleges;
	}

	/**
	 * @param colleges the colleges to set
	 */
	public void setColleges(IdNameDescModel[] colleges) {
		this.colleges = colleges;
	}

	
}
