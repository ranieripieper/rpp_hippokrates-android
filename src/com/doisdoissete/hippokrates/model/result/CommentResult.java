package com.doisdoissete.hippokrates.model.result;

import java.util.List;

import com.doisdoissete.hippokrates.model.Comment;
import com.google.gson.annotations.SerializedName;

public class CommentResult extends BaseResult {

	@SerializedName("comments")
	private List<Comment> comments;
	@SerializedName("linked_data")
	private LinkedData linkedData;
	/**
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	/**
	 * @return the linkedData
	 */
	public LinkedData getLinkedData() {
		return linkedData;
	}
	/**
	 * @param linkedData the linkedData to set
	 */
	public void setLinkedData(LinkedData linkedData) {
		this.linkedData = linkedData;
	}
    
}
