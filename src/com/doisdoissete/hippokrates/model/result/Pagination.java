package com.doisdoissete.hippokrates.model.result;

import com.google.gson.annotations.SerializedName;

public class Pagination {

	@SerializedName("total_count")
	private Integer totalCount;
	@SerializedName("total_pages")
	private Integer totalPages;
	@SerializedName("current_page")
	private Integer currentPage;
	@SerializedName("next_page")
	private Integer nextPage;
	@SerializedName("pev_page")
	private Integer pevPage;
	@SerializedName("per_page")
	private Integer perPage;
	
	/**
	 * @return the totalCount
	 */
	public Integer getTotalCount() {
		return totalCount;
	}
	
	/**
	 * @return the totalCount
	 */
	public String getTotalCountToView() {
		return String.valueOf(totalCount);
	}
	
	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	/**
	 * @return the totalPages
	 */
	public Integer getTotalPages() {
		return totalPages;
	}
	/**
	 * @param totalPages the totalPages to set
	 */
	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}
	/**
	 * @return the currentPage
	 */
	public Integer getCurrentPage() {
		return currentPage;
	}
	/**
	 * @param currentPage the currentPage to set
	 */
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	/**
	 * @return the nextPage
	 */
	public Integer getNextPage() {
		return nextPage;
	}
	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}
	/**
	 * @return the pevPage
	 */
	public Integer getPevPage() {
		return pevPage;
	}
	/**
	 * @param pevPage the pevPage to set
	 */
	public void setPevPage(Integer pevPage) {
		this.pevPage = pevPage;
	}
	/**
	 * @return the perPage
	 */
	public Integer getPerPage() {
		return perPage;
	}
	/**
	 * @param perPage the perPage to set
	 */
	public void setPerPage(Integer perPage) {
		this.perPage = perPage;
	}
}
