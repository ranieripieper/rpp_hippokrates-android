package com.doisdoissete.hippokrates.model.result;

public class StringErrorResult {

	private String error;

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	
	
}
