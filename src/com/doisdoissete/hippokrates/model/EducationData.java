package com.doisdoissete.hippokrates.model;

import com.google.gson.annotations.SerializedName;

public class EducationData {

	private Long id;
	
	@SerializedName("user_id")
	private Long userId;
	
	@SerializedName("residency_program_instituition_name")
	private String residencyProgramInstituitionName;
	
	@SerializedName("university_institution_name")
	private String universityInstitutionName;
	
	@SerializedName("college_degree_state")
	private String collegeDegreeState;
	
	@SerializedName("classroom_name")
	private String classroomName;
	
	@SerializedName("classroom_number")
	private String classroomNumber;
	
	@SerializedName("college_degree_year")
	private String collegeDegreeYear;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the residencyProgramInstituitionName
	 */
	public String getResidencyProgramInstituitionName() {
		return residencyProgramInstituitionName;
	}

	/**
	 * @param residencyProgramInstituitionName the residencyProgramInstituitionName to set
	 */
	public void setResidencyProgramInstituitionName(
			String residencyProgramInstituitionName) {
		this.residencyProgramInstituitionName = residencyProgramInstituitionName;
	}

	/**
	 * @return the universityInstitutionName
	 */
	public String getUniversityInstitutionName() {
		return universityInstitutionName;
	}

	/**
	 * @param universityInstitutionName the universityInstitutionName to set
	 */
	public void setUniversityInstitutionName(String universityInstitutionName) {
		this.universityInstitutionName = universityInstitutionName;
	}

	/**
	 * @return the collegeDegreeState
	 */
	public String getCollegeDegreeState() {
		return collegeDegreeState;
	}

	/**
	 * @param collegeDegreeState the collegeDegreeState to set
	 */
	public void setCollegeDegreeState(String collegeDegreeState) {
		this.collegeDegreeState = collegeDegreeState;
	}

	/**
	 * @return the classroomName
	 */
	public String getClassroomName() {
		return classroomName;
	}

	/**
	 * @param classroomName the classroomName to set
	 */
	public void setClassroomName(String classroomName) {
		this.classroomName = classroomName;
	}

	/**
	 * @return the classroomNumber
	 */
	public String getClassroomNumber() {
		return classroomNumber;
	}

	/**
	 * @param classroomNumber the classroomNumber to set
	 */
	public void setClassroomNumber(String classroomNumber) {
		this.classroomNumber = classroomNumber;
	}

	/**
	 * @return the collegeDegreeYear
	 */
	public String getCollegeDegreeYear() {
		return collegeDegreeYear;
	}

	/**
	 * @param collegeDegreeYear the collegeDegreeYear to set
	 */
	public void setCollegeDegreeYear(String collegeDegreeYear) {
		this.collegeDegreeYear = collegeDegreeYear;
	}
	
	
	/*
"education_data": {
      "user_id": 6,
      "id": 4,
      "residency_program_instituition_name": "Faculdade de Medicina de São José do Rio Preto (FAMERP)",
      "university_institution_name": "Faculdade de Medicina de São José do Rio Preto (FAMERP)",
      "college_degree_state": null,
      "classroom_name": "Langworth-O'Conner",
      "classroom_number": 5771,
      "college_degree_year": null,
      "created_at": "2015-05-25T13:01:20.830-03:00",
      "updated_at": "2015-05-25T13:01:20.830-03:00"
    }
	 */
}
