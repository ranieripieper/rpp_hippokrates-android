package com.doisdoissete.hippokrates.model;

import com.google.gson.annotations.SerializedName;

public class PrivacyRoleData {

	
	public static String FIELD_OCUPACAO = "occupation";
	public static String FIELD_ESPECIALIDADE = "medical_specialty";
	public static String FIELD_INSTITUICAO_ENSINO = "university_institution_name";
	public static String FIELD_INSTITUICAO_RESIDENCIA = "residency_program_instituition_name";
	public static String FIELD_NOME_TURMA = "classroom_name";
	public static String FIELD_NR_TURMA = "classroom_number";
	public static String FIELD_UF_FORMACAO = "college_degree_state";
	public static String FIELD_ANO_FORMACAO = "college_degree_year";
	
	private static String EDUCATION_DATA = "education_data.";

	public static String getFieldToSendUpdate(String field) {
		if (FIELD_OCUPACAO.equals(field) || FIELD_ESPECIALIDADE.equals(field)) {
			return field;
		} else {
			return EDUCATION_DATA + field;
		}
	}
	
	@SerializedName("public")
	private RoleData privacyPublic;
	
	@SerializedName("private")
	private RoleData privacyPrivate;

	public void updatePrivacy(String field, boolean infoPrivada) {
		RoleData roldeAdd;
		RoleData roldeRemove;
		
		if (infoPrivada) {
			roldeAdd = privacyPrivate;
			roldeRemove = privacyPublic;
		} else {
			roldeAdd = privacyPublic;
			roldeRemove = privacyPrivate;
		}
		
		if (FIELD_OCUPACAO.equals(field)) {
			roldeAdd.setOccupation(true);
			roldeRemove.setOccupation(false);
		} else if (FIELD_ESPECIALIDADE.equals(field)) {
			roldeAdd.setMedicalSpecialty(true);
			roldeRemove.setMedicalSpecialty(false);
		} else {
			roldeAdd.addEducationData(field);
			roldeRemove.removeEducationData(field);
		}
	}
	
	public boolean isPrivate(String field) {
		if (privacyPrivate == null) {
			return false;
		}
		if (FIELD_OCUPACAO.equals(field)) {
			return privacyPrivate.isOccupation();
		} else if (FIELD_ESPECIALIDADE.equals(field)) {
			return privacyPrivate.isMedicalSpecialty();
		} else if (privacyPrivate.getEducationData() != null && privacyPrivate.getEducationData().contains(field)) {
			return true;
		}
		return false;
	}
	
	public boolean isPublic(String field) {
		return !isPrivate(field);
	}
	
	/**
	 * @return the privacyPublic
	 */
	public RoleData getPrivacyPublic() {
		return privacyPublic;
	}

	/**
	 * @param privacyPublic the privacyPublic to set
	 */
	public void setPrivacyPublic(RoleData privacyPublic) {
		this.privacyPublic = privacyPublic;
	}

	/**
	 * @return the privacyPrivate
	 */
	public RoleData getPrivacyPrivate() {
		return privacyPrivate;
	}

	/**
	 * @param privacyPrivate the privacyPrivate to set
	 */
	public void setPrivacyPrivate(RoleData privacyPrivate) {
		this.privacyPrivate = privacyPrivate;
	}
	
}
