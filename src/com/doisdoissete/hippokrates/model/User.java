package com.doisdoissete.hippokrates.model;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.doisdoissete.hippokrates.util.ProfileUtil;
import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {
	
	private Long id;
	@SerializedName("first_name")
	private String firstName;
	@SerializedName("last_name")
	private String username;
	@SerializedName("username")
	private String lastName;
	@SerializedName("crm_number")
	private String crmNumber;
	@SerializedName("crm_state")
	private String crmState;
	private String email;
	private String gender;
	@SerializedName("profile_type")
	private String profileType;
	@SerializedName("university_institution_name")
	private String universityInstitutionName;
	private String inviteCode;
	@SerializedName("profile_image_url")
	private String profileImageUrl;
	
	@SerializedName("remaining_invites")
	private int remainingInvites;
	private String name;
	@SerializedName("fullname")
	private String fullName;
	@SerializedName("mine_friendship_circles")
	private List<Circle> mineFriendshipCircles;
	@SerializedName("friendship_circles")
	private List<Circle> friendshipCircles;
	private List<Notification> notifications;
	
	@SerializedName("education_data")
	private EducationData eudcationData;
	
	@SerializedName("occupation")
	private String occupation;
	
	@SerializedName("privacy_role_data")
	private PrivacyRoleData privacyRoleData;
	
	@SerializedName("medical_specialty")
	private IdNameDescModel medicalSpecialty;
	
	private Preferences preferences;
	
	public static Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>(){
		public User createFromParcel(Parcel source) {
			return new User(source);
		}
		public User[] newArray(int size) {
			return new User[size];
		}
	};
	
	public User() {		
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the crmNumber
	 */
	public String getCrmNumber() {
		return crmNumber;
	}
	/**
	 * @param crmNumber the crmNumber to set
	 */
	public void setCrmNumber(String crmNumber) {
		this.crmNumber = crmNumber;
	}
	/**
	 * @return the crmState
	 */
	public String getCrmState() {
		return crmState;
	}
	/**
	 * @param crmState the crmState to set
	 */
	public void setCrmState(String crmState) {
		this.crmState = crmState;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the profileType
	 */
	public String getProfileType() {
		return profileType;
	}
	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	/**
	 * @return the universityInstitutionName
	 */
	public String getUniversityInstitutionName() {
		return universityInstitutionName;
	}
	/**
	 * @param universityInstitutionName the universityInstitutionName to set
	 */
	public void setUniversityInstitutionName(String universityInstitutionName) {
		this.universityInstitutionName = universityInstitutionName;
	}
	

	/**
	 * @return the inviteCode
	 */
	public String getInviteCode() {
		return inviteCode;
	}
	/**
	 * @param inviteCode the inviteCode to set
	 */
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	public boolean isDoctor() {
		return ProfileUtil.isDoctor(this.profileType);
	}
	
	public boolean isStudent() {
		return ProfileUtil.isStudent(this.profileType);
	}
	
	public boolean isTeacher() {
		return ProfileUtil.isTeacher(this.profileType);
	}
	
	public boolean isAdmin() {
		return ProfileUtil.isAdmin(this.profileType);
	}
	
	/**
	 * @return the remainingInvites
	 */
	public int getRemainingInvites() {
		return remainingInvites;
	}
	/**
	 * @param remainingInvites the remainingInvites to set
	 */
	public void setRemainingInvites(int remainingInvites) {
		this.remainingInvites = remainingInvites;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @return the mineFriendshipCircles
	 */
	public List<Circle> getMineFriendshipCircles() {
		return mineFriendshipCircles;
	}
	/**
	 * @param mineFriendshipCircles the mineFriendshipCircles to set
	 */
	public void setMineFriendshipCircles(List<Circle> mineFriendshipCircles) {
		this.mineFriendshipCircles = mineFriendshipCircles;
	}
	/**
	 * @return the friendshipCircles
	 */
	public List<Circle> getFriendshipCircles() {
		return friendshipCircles;
	}
	/**
	 * @param friendshipCircles the friendshipCircles to set
	 */
	public void setFriendshipCircles(List<Circle> friendshipCircles) {
		this.friendshipCircles = friendshipCircles;
	}
	/**
	 * @return the notifications
	 */
	public List<Notification> getNotifications() {
		return notifications;
	}
	/**
	 * @param notifications the notifications to set
	 */
	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	/**
	 * @return the privacyRoleData
	 */
	public PrivacyRoleData getPrivacyRoleData() {
		return privacyRoleData;
	}

	/**
	 * @param privacyRoleData the privacyRoleData to set
	 */
	public void setPrivacyRoleData(PrivacyRoleData privacyRoleData) {
		this.privacyRoleData = privacyRoleData;
	}

	/**
	 * @return the eudcationData
	 */
	public EducationData getEudcationData() {
		return eudcationData;
	}

	/**
	 * @param eudcationData the eudcationData to set
	 */
	public void setEudcationData(EducationData eudcationData) {
		this.eudcationData = eudcationData;
	}

	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	/**
	 * @return the medical_specialty
	 */
	public IdNameDescModel getMedicalSpecialty() {
		return medicalSpecialty;
	}

	/**
	 * @param medical_specialty the medical_specialty to set
	 */
	public void setMedicalSpecialty(IdNameDescModel medicalSpecialty) {
		this.medicalSpecialty = medicalSpecialty;
	}

	/**
	 * @return the profileImagesUrl
	 */
	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	/**
	 * @param profileImagesUrl the profileImagesUrl to set
	 */
	public void setProfileImageUrl(String profileImagesUrl) {
		this.profileImageUrl = profileImagesUrl;
	}

	private User(Parcel in) {
		this.id = (Long)in.readValue(Long.class.getClassLoader());
		this.firstName = in.readString();
		this.username = in.readString();
		this.lastName = in.readString();
		this.crmNumber = in.readString();
		this.crmState = in.readString();
		this.email = in.readString();
		this.gender = in.readString();
		this.profileType = in.readString();
		this.universityInstitutionName = in.readString();
		this.inviteCode = in.readString();
		this.profileImageUrl = in.readString();
		this.remainingInvites = in.readInt();
		this.name = in.readString();
		this.fullName = in.readString();
	}

	public int describeContents() { 
		return 0; 
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.firstName);
		dest.writeString(this.username);
		dest.writeString(this.lastName);
		dest.writeString(this.crmNumber);
		dest.writeString(this.crmState);
		dest.writeString(this.email);
		dest.writeString(this.gender);
		dest.writeString(this.profileType);
		dest.writeString(this.universityInstitutionName);
		dest.writeString(this.inviteCode);
		dest.writeString(this.profileImageUrl);
		dest.writeInt(this.remainingInvites);
		dest.writeString(this.name);
		dest.writeString(this.fullName);
	}

	/**
	 * @return the preferences
	 */
	public Preferences getPreferences() {
		return preferences;
	}

	/**
	 * @param preferences the preferences to set
	 */
	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}
	
	
}
