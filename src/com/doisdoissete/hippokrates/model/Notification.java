package com.doisdoissete.hippokrates.model;

import com.google.gson.annotations.SerializedName;

public class Notification {

	private int id;
	@SerializedName("formatted_body")
	private String formattedBody;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the formattedBody
	 */
	public String getFormattedBody() {
		return formattedBody;
	}
	/**
	 * @param formattedBody the formattedBody to set
	 */
	public void setFormattedBody(String formattedBody) {
		this.formattedBody = formattedBody;
	}
	
	
	
}
