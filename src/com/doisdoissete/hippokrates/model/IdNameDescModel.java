package com.doisdoissete.hippokrates.model;

import android.os.Parcelable;
import android.os.Parcel;

public class IdNameDescModel implements Parcelable {

	private Long id;
	private String name;
	private String description;
	public static Parcelable.Creator<IdNameDescModel> CREATOR = new Parcelable.Creator<IdNameDescModel>(){
		public IdNameDescModel createFromParcel(Parcel source) {
			return new IdNameDescModel(source);
		}
		public IdNameDescModel[] newArray(int size) {
			return new IdNameDescModel[size];
		}
	};
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	private IdNameDescModel(Parcel in) {
		this.id = (Long)in.readValue(Long.class.getClassLoader());
		this.name = in.readString();
		this.description = in.readString();
	}
	public int describeContents() { 
		return 0; 
	}
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeString(this.description);
	}
	
	
}
