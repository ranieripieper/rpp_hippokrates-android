package com.doisdoissete.hippokrates.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.SerializedName;

public class RoleData {
	
	private boolean occupation;

	@SerializedName("medical_specialty")
	private boolean medicalSpecialty;
	
	@SerializedName("education_data")
	private Set<String> educationData;

	public void addEducationData(String field) {
		if (educationData == null) {
			educationData = new HashSet<String>();
		}
		educationData.add(field);
	}
	
	public void removeEducationData(String field) {
		if (educationData != null) {
			educationData.remove(field);
		}		
	}
	
	/**
	 * @return the occupation
	 */
	public boolean isOccupation() {
		return occupation;
	}

	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(boolean occupation) {
		this.occupation = occupation;
	}

	/**
	 * @return the medicalSpecialty
	 */
	public boolean isMedicalSpecialty() {
		return medicalSpecialty;
	}

	/**
	 * @param medicalSpecialty the medicalSpecialty to set
	 */
	public void setMedicalSpecialty(boolean medicalSpecialty) {
		this.medicalSpecialty = medicalSpecialty;
	}

	/**
	 * @return the educationData
	 */
	public Set<String> getEducationData() {
		return educationData;
	}

	/**
	 * @param educationData the educationData to set
	 */
	public void setEducationData(Set<String> educationData) {
		this.educationData = educationData;
	}

	
}
