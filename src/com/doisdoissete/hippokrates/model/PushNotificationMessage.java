package com.doisdoissete.hippokrates.model;

import com.google.gson.annotations.SerializedName;

/*
 * formato
{:alert=>"Hello Hip welcome to HipApp!",
 :data=>
  {:receiver_user_id=>382,
   :sender_user_id=>nil,
   :receiver_user_image_url=>"/images/development/user/profile_image/382/thumb_eden-young-daft-punk.jpg",
   :sender_user_image_url=>nil,
   :notificable_image_url=>nil,
   :notificable_id=>nil,
   :notificable_type=>nil,
   :message=>"Hello Hip welcome to HipApp!",
   :notification_type=>:welcome}

 */
public class PushNotificationMessage {

	@SerializedName("alert")
	private String message;
	
	private PushNotificationMessageData data;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public PushNotificationMessageData getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(PushNotificationMessageData data) {
		this.data = data;
	}
}
