package com.doisdoissete.hippokrates.model;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.doisdoissete.hippokrates.util.Constants;
import com.google.gson.annotations.SerializedName;

public class Circle implements Parcelable {

	private Long id;
	
	private String name;	
	
	@SerializedName("admin_user_id")
	private Long adminUserId;

	@SerializedName("profile_image_url")
	private String profileImageUrl;
	
	@SerializedName("remaining_members_count")
	private Integer remainingMembersCount;
	
	@SerializedName("circle_type")
	private String circleType;
	
	@SerializedName("created_at")
	private Date createdAt;
	
	@SerializedName("users_count")
	private Integer totalUsers;
	
	@SerializedName("posts_count")
	private Integer totalPosts;
	
	@SerializedName("pending_users_count")
	private Integer pendingUsersCount;
	
	private String code;
	
	private boolean especialidade;
	public static Parcelable.Creator<Circle> CREATOR = new Parcelable.Creator<Circle>(){
		public Circle createFromParcel(Parcel source) {
			return new Circle(source);
		}
		public Circle[] newArray(int size) {
			return new Circle[size];
		}
	};

	public Circle() {
		
	}
	
	public Circle(IdNameDescModel especialidade) {
		setId(especialidade.getId());
		setAdminUserId(-1L);
		setName(especialidade.getName());
		setEspecialidade(true);
		//TODO
		//getProfileImages().setMedium(medium);
	}
	
	public void addTotalUsers() {
		if (this.totalUsers == null) {
			this.totalUsers = 1;
		} else {
			this.totalUsers = this.totalUsers + 1;
		}
	}
	
	public void reduceTotalUsers() {
		if (this.totalUsers == null) {
			this.totalUsers = 0;
		} else {
			this.totalUsers = this.totalUsers - 1;
		}
	}
	
	public boolean isStudent() {
		if (!isEspecialidade() && Constants.STUDENT_CIRCLE_TYPE.equals(circleType)) {
			return true;
		}
		
		return false;
	}
	
	public boolean isCommon() {
		if (!isEspecialidade() && Constants.COMMON_CIRCLE_TYPE.equals(circleType)) {
			return true;
		}
		
		return false;
	}
	
	public boolean isPublic() {
		if (!isEspecialidade() && Constants.PUBLIC_CIRCLE_TYPE.equals(circleType)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * @return the especialidade
	 */
	public boolean isEspecialidade() {
		return especialidade;
	}

	/**
	 * @param especialidade the especialidade to set
	 */
	public void setEspecialidade(boolean especialidade) {
		this.especialidade = especialidade;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the adminUserId
	 */
	public Long getAdminUserId() {
		return adminUserId;
	}

	/**
	 * @param adminUserId the adminUserId to set
	 */
	public void setAdminUserId(Long adminUserId) {
		this.adminUserId = adminUserId;
	}

	/**
	 * @return the remainingMembersCount
	 */
	public Integer getRemainingMembersCount() {
		return remainingMembersCount;
	}

	/**
	 * @param remainingMembersCount the remainingMembersCount to set
	 */
	public void setRemainingMembersCount(Integer remainingMembersCount) {
		this.remainingMembersCount = remainingMembersCount;
	}

	/**
	 * @return the circleType
	 */
	public String getCircleType() {
		return circleType;
	}

	/**
	 * @param circleType the circleType to set
	 */
	public void setCircleType(String circleType) {
		this.circleType = circleType;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return the totalUsers
	 */
	public Integer getTotalUsers() {
		return totalUsers;
	}

	/**
	 * @param totalUsers the totalUsers to set
	 */
	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}

	/**
	 * @return the totalPosts
	 */
	public Integer getTotalPosts() {
		return totalPosts;
	}

	/**
	 * @param totalPosts the totalPosts to set
	 */
	public void setTotalPosts(Integer totalPosts) {
		this.totalPosts = totalPosts;
	}

	/**
	 * @return the pendingUsersCount
	 */
	public Integer getPendingUsersCount() {
		return pendingUsersCount;
	}

	/**
	 * @param pendingUsersCount the pendingUsersCount to set
	 */
	public void setPendingUsersCount(Integer pendingUsersCount) {
		this.pendingUsersCount = pendingUsersCount;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the profileImagesUrl
	 */
	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	/**
	 * @param profileImagesUrl the profileImagesUrl to set
	 */
	public void setProfileImageUrl(String profileImagesUrl) {
		this.profileImageUrl = profileImagesUrl;
	}

	private Circle(Parcel in) {
		this.id = (Long)in.readValue(Long.class.getClassLoader());
		this.name = in.readString();
		this.adminUserId = (Long)in.readValue(Long.class.getClassLoader());
		this.remainingMembersCount = (Integer)in.readValue(Integer.class.getClassLoader());
		this.circleType = in.readString();
		this.profileImageUrl = in.readString();
		long tmpCreatedAt = in.readLong(); 
		this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
		this.code = in.readString();
		this.especialidade = in.readByte() != 0;
		this.totalPosts = (Integer)in.readValue(Integer.class.getClassLoader());
		this.totalUsers = (Integer)in.readValue(Integer.class.getClassLoader());
		this.pendingUsersCount = (Integer)in.readValue(Integer.class.getClassLoader());
	}

	public int describeContents() { 
		return 0; 
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeString(this.name);
		dest.writeValue(this.adminUserId);
		dest.writeValue(this.remainingMembersCount);
		dest.writeString(this.circleType);
		dest.writeString(this.profileImageUrl);
		dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
		dest.writeString(this.code);
		dest.writeByte(especialidade ? (byte) 1 : (byte) 0);
		dest.writeValue(this.totalPosts);
		dest.writeValue(this.totalUsers);
		dest.writeValue(this.pendingUsersCount);
	}


}
