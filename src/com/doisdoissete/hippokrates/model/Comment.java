package com.doisdoissete.hippokrates.model;

import java.util.Date;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.google.gson.annotations.SerializedName;
import android.os.Parcelable;
import android.os.Parcel;

public class Comment implements Parcelable {

	private Long id;

	@SerializedName("post_id")
	private Long postId;
	
	@SerializedName("user_id")
	private Long userId;
	
	private String content;
	
	@SerializedName("created_at")
	private Date createdAt;
	
	@SerializedName("updated_at")
	private Date updatedAt;

	private User user;
	
	private Post post;
	
	public static Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>(){
		public Comment createFromParcel(Parcel source) {
			return new Comment(source);
		}
		public Comment[] newArray(int size) {
			return new Comment[size];
		}
	};

	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the postId
	 */
	public Long getPostId() {
		return postId;
	}

	/**
	 * @param postId the postId to set
	 */
	public void setPostId(Long postId) {
		this.postId = postId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the createdAt
	 */
	public String getCreatedAtStr() {
		if (createdAt == null) {
			return "";
		}
		return DateUtil.dfDiaMesAnoHoraMinuto.get().format(createdAt);
	}

	/**
	 * @return the post
	 */
	public Post getPost() {
		return post;
	}

	/**
	 * @param post the post to set
	 */
	public void setPost(Post post) {
		this.post = post;
	}

	private Comment(Parcel in) {
		this.id = (Long)in.readValue(Long.class.getClassLoader());
		this.postId = (Long)in.readValue(Long.class.getClassLoader());
		this.userId = (Long)in.readValue(Long.class.getClassLoader());
		this.content = in.readString();
		long tmpCreatedAt = in.readLong(); 
		this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
		long tmpUpdatedAt = in.readLong(); 
		this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
	}

	public int describeContents() { 
		return 0; 
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.id);
		dest.writeValue(this.postId);
		dest.writeValue(this.userId);
		dest.writeString(this.content);
		dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
		dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1);
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
