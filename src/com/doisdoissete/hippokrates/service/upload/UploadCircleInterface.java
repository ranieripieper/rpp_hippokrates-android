package com.doisdoissete.hippokrates.service.upload;

import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

import com.doisdoissete.hippokrates.model.result.FriendshipCircleResult;

public interface UploadCircleInterface {
	

	@PUT("/v1/friendship_circles/{circleId}")
	@Multipart
	FriendshipCircleResult changePhoto(@Path("circleId") String cicleId, @Part("friendship_circle[profile_image]") TypedFile photo);
	
	@POST("/v1/friendship_circles/")
	@Multipart
	FriendshipCircleResult createCircle(@Part("friendship_circle[name]") TypedString name, @Part("friendship_circle[profile_image]") TypedFile photo);
	
	@POST("/v1/friendship_circles/")
	@Multipart
	FriendshipCircleResult createCircle(@Part("friendship_circle[name]") TypedString name, @Part("friendship_circle[profile_image]") TypedFile photo,
			@Part("friendship_circle[members_limit]") TypedString nrAlunos,
			@Part("friendship_circle[college_subject]") TypedString materina,
			@Part("friendship_circle[college_instituition_name]") TypedString universidade,
			@Part("friendship_circle[start_date]") TypedString startDate,
			@Part("friendship_circle[end_date]") TypedString endDate);

}
