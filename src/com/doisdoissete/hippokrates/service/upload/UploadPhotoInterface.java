package com.doisdoissete.hippokrates.service.upload;

import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

import com.doisdoissete.hippokrates.model.result.PostFotoResult;

public interface UploadPhotoInterface {
	

	@PUT("/v1/posts/{postId}")
	@Multipart
	PostFotoResult updatePostPhoto(@Path("postId") String postId, @Part("post[image]") TypedFile photo, @Part("post[content]") TypedString d);
	
	@POST("/v1/posts")
	@Multipart
	PostFotoResult postPhoto(@Part("post[image]") TypedFile photo, @Part("post[content]") TypedString d, 
			 @Part("post[privacy_status]") TypedString privacy, 
			 @Part("post[medical_specialty_id]") TypedString idMed);
	
	@POST("/v1/posts")
	@Multipart
	PostFotoResult postPhoto(@Part("post[image]") TypedFile photo, @Part("post[content]") TypedString d, 
			 @Part("post[privacy_status]") TypedString privacy, 
			 @Part("post[medical_specialty_id]") TypedString idMed,
			 @Part("post[friendship_circles_ids][]") TypedString circle1,
			 @Part("post[friendship_circles_ids][]") TypedString circle2,
			 @Part("post[friendship_circles_ids][]") TypedString circle3,
			 @Part("post[friendship_circles_ids][]") TypedString circle4,
			 @Part("post[friendship_circles_ids][]") TypedString circle5,
			 @Part("post[friendship_circles_ids][]") TypedString circle6,
			 @Part("post[friendship_circles_ids][]") TypedString circle7,
			 @Part("post[friendship_circles_ids][]") TypedString circle8,
			 @Part("post[friendship_circles_ids][]") TypedString circle9,
			 @Part("post[friendship_circles_ids][]") TypedString circle10,
			 @Part("post[friendship_circles_ids][]") TypedString circle11,
			 @Part("post[friendship_circles_ids][]") TypedString circle12,
			 @Part("post[friendship_circles_ids][]") TypedString circle13,
			 @Part("post[friendship_circles_ids][]") TypedString circle14,
			 @Part("post[friendship_circles_ids][]") TypedString circle15,
			 @Part("post[friendship_circles_ids][]") TypedString circle16,
			 @Part("post[friendship_circles_ids][]") TypedString circle17,
			 @Part("post[friendship_circles_ids][]") TypedString circle18,
			 @Part("post[friendship_circles_ids][]") TypedString circle19,
			 @Part("post[friendship_circles_ids][]") TypedString circle20);

}
