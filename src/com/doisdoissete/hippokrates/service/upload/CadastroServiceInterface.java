package com.doisdoissete.hippokrates.service.upload;

import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

import com.doisdoissete.hippokrates.model.result.LoginResult;
import com.doisdoissete.hippokrates.util.UrlParameters;

public interface CadastroServiceInterface {

	@POST("/v1/users")
	@Multipart
	LoginResult cadastrar(@Part(UrlParameters.PARAM_USER_PROFILE_IMAGE) TypedFile photo, 
			 @Part(UrlParameters.PARAM_USER_NAME) TypedString name, 
			 @Part(UrlParameters.PARAM_USER_LAST_NAME) TypedString lastName, 
			 @Part(UrlParameters.PARAM_USER_EMAIL) TypedString emal, 
			 @Part(UrlParameters.PARAM_USER_GENDER) TypedString gender,
			 @Part(UrlParameters.PARAM_USER_INVITE_CODE) TypedString inviteCode,
			 @Part(UrlParameters.PARAM_USER_EDUCATION_DATA_UNIVERSITY_INST_NAME) TypedString univInstName,
			 @Part(UrlParameters.PARAM_USER_PASSWORD) TypedString password,
			 @Part(UrlParameters.PARAM_USER_PASSWORD_CONF) TypedString confPassword,
			 @Part(UrlParameters.PARAM_USER_DEVICE_PLATFORM) TypedString plataform,
			 @Part(UrlParameters.PARAM_USER_DEVICE_TOKEN) TypedString deviceToken,
			 @Part(UrlParameters.PARAM_USER_PROFILE_TYPE) TypedString profileType,
			 @Part(UrlParameters.PARAM_USER_CRM_DATA_NUMBER) TypedString crmNumber,
			 @Part(UrlParameters.PARAM_USER_CRM_DATA_STATE) TypedString crmState,
			 @Part(UrlParameters.PARAM_USER_MEDICAL_SPECIALTY) TypedString medicalSpecialtyId,
			 @Part(UrlParameters.PARAM_USER_OCUPATION) TypedString ocupation,
			 @Part(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NUMBER) TypedString classroomNumber,
			 @Part(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NAME) TypedString classroomName,
			 @Part(UrlParameters.PARAM_USER_EDUCATION_DATA_RESIDENCY_INST_NAME) TypedString resInstName,
			 @Part(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_STATE) TypedString collegeDegree,
			 @Part(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_YEAR) TypedString collegeYear);

}
