package com.doisdoissete.hippokrates.service;

import java.net.ConnectException;
import java.util.Date;

import android.content.Context;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.doisdoissete.hippokrates.model.result.SelectServerListResult;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class MedicalSpecialitiesService extends BaseService {

	public MedicalSpecialitiesService(ServiceBuilder builder) {
		super(builder);
	}
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		IdNameDescModel[] cacheResult = Preferences.getMedicalSpecialities(ctx);
		
		SelectServerListResult selectServerListResult;
		
		if (cacheResult == null || cacheResult.length <= 0) {
			selectServerListResult = (SelectServerListResult)super.callService(ctx);
			if (selectServerListResult != null && selectServerListResult.getMedicalSpecialities().length > 0) {
				Preferences.setMedicalSpecialities(ctx, selectServerListResult.getMedicalSpecialities());
				Date dt = Preferences.getLastClearCacheServer(ctx);
				if (dt == null) {
					Preferences.setLastClearCacheServer(ctx, new Date());
				}
			}
		} else {
			selectServerListResult = new SelectServerListResult();
			selectServerListResult.setMedicalSpecialities(cacheResult);
		}
		
		return selectServerListResult;
	}
}
