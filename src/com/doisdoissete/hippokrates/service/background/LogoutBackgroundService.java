package com.doisdoissete.hippokrates.service.background;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;

public class LogoutBackgroundService extends IntentService {
	
	public static String TAG = "LogoutBackgroundService";
	public static String PARAM_AUTH_TOKEN = "PARAM_AUTH_TOKEN";
	
	public LogoutBackgroundService() {
		super("LogoutBackgroundService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		synchronized (TAG) {
			String authToken = intent.getExtras().getString(PARAM_AUTH_TOKEN);
			BaseService baseService = null;
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			HippAppServiceBuilder sb = new HippAppServiceBuilder(LogoutBackgroundService.this);
			
			sb.addHeaders(UrlParameters.PARAM_HEADER_X_TOKEN, authToken);
			sb.addHeaders(UrlParameters.PARAM_HEADER_X_PROVIDER, Constants.PROVIDER );
			sb.setUrl(Paths.LOGOUT)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.DELETE)
				.setParams(params);
			
			baseService = sb.mappingIntoBaseService(LogoutBackgroundService.this, String.class);
			try {
				if (baseService != null) {
					//Object resultObj = 
					baseService.execute(LogoutBackgroundService.this);
					//String result = (String)resultObj;
				}			
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (ConnectionException e) {
			} catch (Exception e) {
			}
		}
	}
	
	public static void callService(Context context, String authToken) {
		Intent intentService = new Intent(context, LogoutBackgroundService.class);
		intentService.putExtra(PARAM_AUTH_TOKEN, authToken);
		context.startService(intentService);
	}

}
