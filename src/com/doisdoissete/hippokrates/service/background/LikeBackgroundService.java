package com.doisdoissete.hippokrates.service.background;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;

public class LikeBackgroundService extends IntentService {
	
	public static String PARAM_POST_ID = "PARAM_POST_ID";
	public static String PARAM_LIKE = "PARAM_LIKE";
	public static String TAG = "LikeBackgroundService";
	
	public LikeBackgroundService() {
		super("LikeBackgroundService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		synchronized (TAG) {
			final Long postId = intent.getExtras().getLong(PARAM_POST_ID);
			final boolean like = intent.getExtras().getBoolean(PARAM_LIKE);

		
			BaseService baseService = null;
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			HippAppServiceBuilder sb = new HippAppServiceBuilder(LikeBackgroundService.this);
			String url = Paths.LIKE_POST.replace(UrlParameters.PARAM_POST_ID_URL, String.valueOf(postId));
			
			HttpMethod method;
			if (like) {
				method = HttpMethod.POST;			
			} else {
				method = HttpMethod.DELETE;
			}	
			
			sb.setUrl(url)
				.setNeedConnection(true)
				.setHttpMethod(method)
				.setParams(params);
			
			baseService = sb.mappingIntoBaseService(LikeBackgroundService.this, BaseResult.class);
			try {
				if (baseService != null) {
					//Object resultObj = 
					baseService.execute(LikeBackgroundService.this);
					//BaseResult result = (BaseResult)resultObj;
				}			
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
			} catch (ConnectionException e) {
			} catch (Exception e) {
			}
		}
	}
	
	public static void callService(Context context, Long postId, boolean like) {
		Intent intentService = new Intent(context, LikeBackgroundService.class);
		intentService.putExtra(PARAM_POST_ID, postId);
		intentService.putExtra(PARAM_LIKE, like);
		context.startService(intentService);
	}

}
