package com.doisdoissete.hippokrates.service.background;

import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;

public class DeclinePendingUsersBackgroundService extends IntentService {

	private static final String TAG = "DeclinePendingUsersBackgroundService";
	private static final String PARAM_CIRCLE_ID = "PARAM_CIRCLE_ID";
	private static final String PARAM_USER_IDS = "PARAM_USER_IDS";
	
	public DeclinePendingUsersBackgroundService() {
		super("DeclinePendingUsersBackgroundService");
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
		synchronized (TAG) {	
			long[] userIds = intent.getExtras().getLongArray(PARAM_USER_IDS);
			long circleId = intent.getExtras().getLong(PARAM_CIRCLE_ID);

			BaseService baseService = null;
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			
			for (long userId : userIds) {
				params.add(UrlParameters.PARAM_USERS_IDS, String.valueOf(userId));
			}
			
			HippAppServiceBuilder sb = new HippAppServiceBuilder(DeclinePendingUsersBackgroundService.this);
			String url = Paths.DECLINE_PENDING_USERS.replace(UrlParameters.PARAM_CIRCLE_ID_URL, String.valueOf(circleId));
			sb.setUrl(url)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.POST)
				.setParams(params);
			
			baseService = sb.mappingIntoBaseService(getBaseContext(), BaseResult.class);
			
			try {
				Object resultObj = baseService.execute(getBaseContext());
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
			} catch (ConnectionException e) {
			} catch (Exception e) {
			}
		}
		
	}
	
	public static void callService(Context context, Long circleId, List<Long> userIdsList) {
		Intent intentService = new Intent(context, DeclinePendingUsersBackgroundService.class);
		intentService.putExtra(PARAM_CIRCLE_ID, circleId);
		long[] userIds = new long[userIdsList.size()];
		for (int i=0; i < userIdsList.size(); i++) {
			userIds[i] = userIdsList.get(i);
		}
		intentService.putExtra(PARAM_USER_IDS, userIds);
		context.startService(intentService);
	}

}
