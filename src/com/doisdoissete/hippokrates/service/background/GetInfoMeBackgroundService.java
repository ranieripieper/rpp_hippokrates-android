package com.doisdoissete.hippokrates.service.background;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.MeResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class GetInfoMeBackgroundService extends IntentService {

	private static final String TAG = "GetInfoMeBackgroundService";
	
	public GetInfoMeBackgroundService() {
		super("GetInfoMeBackgroundService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		synchronized (TAG) {			
			BaseService baseService = null;
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			HippAppServiceBuilder sb = new HippAppServiceBuilder(getBaseContext());
			sb.setUrl(Paths.ME_INFO)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.GET)
				.setParams(params);
			
			baseService = sb.mappingIntoBaseService(getBaseContext(), MeResult.class);
			
			try {
				Object resultObj = baseService.execute(getBaseContext());
				MeResult result = (MeResult)resultObj;
				if (result != null && result.getUser() != null) {
					LocalUser localUser = Preferences.getLocalUser(getBaseContext());
					if (localUser != null && localUser.getId().equals(result.getUser().getId())) {
						localUser.setRemainingInvites(result.getUser().getRemainingInvites());
						localUser.setFullName(result.getUser().getFullName());
						Preferences.setLocalUser(getBaseContext(), localUser);
					}				
				}
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
			} catch (ConnectionException e) {
			} catch (Exception e) {
			}
		}
	}
	
	public static void callService(Context context) {
		Intent intentService = new Intent(context, GetInfoMeBackgroundService.class);
		context.startService(intentService);
	}

}
