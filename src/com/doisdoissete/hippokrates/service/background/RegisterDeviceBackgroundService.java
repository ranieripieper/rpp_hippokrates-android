package com.doisdoissete.hippokrates.service.background;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class RegisterDeviceBackgroundService extends IntentService {
	
	public static String PARAM_POST_ID = "PARAM_POST_ID";
	public static String PARAM_LIKE = "PARAM_LIKE";
	public static String TAG = "LikeBackgroundService";
	
	public RegisterDeviceBackgroundService() {
		super("LikeBackgroundService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		synchronized (TAG) {		
			String deviceToken = Preferences.getParseToken(RegisterDeviceBackgroundService.this);
			if (TextUtils.isEmpty(deviceToken) || Preferences.getLocalUser(this) == null) {
				return;
			}
			BaseService baseService = null;
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			HippAppServiceBuilder sb = new HippAppServiceBuilder(RegisterDeviceBackgroundService.this);
			params.add(UrlParameters.PARAM_DEVICE_PLATFORM, Constants.PROVIDER);
			params.add(UrlParameters.PARAM_DEVICE_TOKEN, deviceToken);
			
			sb.setUrl(Paths.REGISTER_DEVICE)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.POST)
				.setParams(params);
			
			baseService = sb.mappingIntoBaseService(RegisterDeviceBackgroundService.this, BaseResult.class);
			try {
				if (baseService != null) {
					Object resultObj = 
					baseService.execute(RegisterDeviceBackgroundService.this);
					BaseResult result = (BaseResult)resultObj;
				}			
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
			} catch (ConnectionException e) {
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void callService(Context context) {
		Intent intentService = new Intent(context, RegisterDeviceBackgroundService.class);
		context.startService(intentService);
	}

}
