package com.doisdoissete.hippokrates.service.background;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v8.renderscript.RenderScript;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;

public class DenunciarComentarioBackgroundService extends IntentService {
	
	private static final String TAG = "DenunciarComentarioBackgroundService";
	public static String PARAM_POST_ID = "PARAM_POST_ID";
	public static String PARAM_COMMENT_ID = "PARAM_COMMENT_ID";
	
	public DenunciarComentarioBackgroundService() {
		super("LikeBackgroundService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		synchronized (TAG) {	
			final Long postId = intent.getExtras().getLong(PARAM_POST_ID);
			final Long commentId = intent.getExtras().getLong(PARAM_COMMENT_ID);
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			HippAppServiceBuilder sb = new HippAppServiceBuilder(DenunciarComentarioBackgroundService.this);
			
			String url = Paths.REPORT_COMMENT.replace(UrlParameters.PARAM_POST_ID_URL, String.valueOf(postId)).replace(UrlParameters.PARAM_COMMENT_ID_URL, String.valueOf(commentId));
			sb.setUrl(url)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.POST)
				.setParams(params);
			
			BaseService baseService = sb.mappingIntoBaseService(DenunciarComentarioBackgroundService.this, BaseResult.class);
			
			try {
				if (baseService != null) {
					//Object resultObj = 
					baseService.execute(DenunciarComentarioBackgroundService.this);
					//BaseResult result = (BaseResult)resultObj;
				}			
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
			} catch (ConnectionException e) {
			} catch (Exception e) {
			}
		}
	}
	
	public static void callService(Context context, Long postId, Long commentId) {
		Intent intentService = new Intent(context, DenunciarComentarioBackgroundService.class);
		intentService.putExtra(PARAM_POST_ID, postId);
		intentService.putExtra(PARAM_COMMENT_ID, commentId);
		context.startService(intentService);
	}

}
