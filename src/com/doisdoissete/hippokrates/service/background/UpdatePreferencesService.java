package com.doisdoissete.hippokrates.service.background;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.result.BaseResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.view.adapter.settings.PreferencesToAdapter;

public class UpdatePreferencesService extends IntentService {
	
	public static String TAG = "UpdatePreferencesService";
	
	public static String PARAM_HASH_MAP_PARAMS = "PARAM_HASH_MAP_PARAMS";
	
	public UpdatePreferencesService() {
		super("UpdatePreferencesService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		synchronized (TAG) {
			ArrayList<PreferencesToAdapter> preferences = intent.getExtras().getParcelableArrayList(PARAM_HASH_MAP_PARAMS);
			BaseService baseService = null;
			MultiValueMap<String, String> params = createParamsToUpdate(preferences);		
			
			HippAppServiceBuilder sb = new HippAppServiceBuilder(UpdatePreferencesService.this);
			sb.setUrl(Paths.REFRESH_PREFERENCES)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.PUT)
				.setParams(params);
		
			
			baseService = sb.mappingIntoBaseService(UpdatePreferencesService.this, BaseResult.class);
			try {
				if (baseService != null) {
					//Object resultObj = 
					baseService.execute(UpdatePreferencesService.this);
					//BaseResult result = (BaseResult)resultObj;
				}			
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
			} catch (ConnectionException e) {
			} catch (Exception e) {
			}
		}
		
	}
	
	private MultiValueMap<String, String> createParamsToUpdate(List<PreferencesToAdapter> preferences) {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		for (PreferencesToAdapter p : preferences) {
			params.add(p.getParamServer(), String.valueOf(p.isChecked()));
		}
		
		return params;
	}
	
	public static void callService(Context context, ArrayList<PreferencesToAdapter> preferences) {
		Intent intentService = new Intent(context, UpdatePreferencesService.class);
		intentService.putExtra(PARAM_HASH_MAP_PARAMS, preferences);
		context.startService(intentService);
	}

}
