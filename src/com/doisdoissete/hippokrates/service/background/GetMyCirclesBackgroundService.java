package com.doisdoissete.hippokrates.service.background;

import java.util.Date;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionNotFoundException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.model.result.MeCirclesResult;
import com.doisdoissete.hippokrates.service.HippAppServiceBuilder;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class GetMyCirclesBackgroundService extends IntentService {

	private static final String TAG = "RegIntentService";
	
	private static String PARAM_UPDATE_INFO = "PARAM_UPDATE_INFO";
	
	public GetMyCirclesBackgroundService() {
		super("GetMyCirclesBackgroundService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		synchronized (TAG) {
			final boolean updateInfo = intent.getBooleanExtra(PARAM_UPDATE_INFO, false);
			Preferences.setLastCallMyCircles(getBaseContext(), new Date());
			BaseService baseService = null;
			MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
			HippAppServiceBuilder sb = new HippAppServiceBuilder(getBaseContext());
			sb.setUrl(Paths.ME_CIRCLES)
				.setNeedConnection(true)
				.setHttpMethod(HttpMethod.GET)
				.setParams(params);
			
			baseService = sb.mappingIntoBaseService(getBaseContext(), MeCirclesResult.class);
			boolean success = false;
			try {
				Object resultObj = baseService.execute(getBaseContext());
				MeCirclesResult result = (MeCirclesResult)resultObj;
				if (result != null && result.getCircles() != null) {
					LocalUser localUser = Preferences.getLocalUser(getBaseContext());
					if (localUser != null) {
						localUser.setCircles(result.getCircles());
						Preferences.setLocalUser(getBaseContext(), localUser);
						Preferences.setLastCallMyCircles(getBaseContext(), new Date());
					}	
					success = true;
				}
			} catch (ConnectionNotFoundException e) {
			} catch (ParseException e) {
			} catch (ConnectionException e) {
			} catch (Exception e) {
			} finally {
				if (!success) {
					Preferences.setLastCallMyCircles(getBaseContext(), null);
				}
			}
			
			if (updateInfo) {
				GetInfoMeBackgroundService.callService(GetMyCirclesBackgroundService.this);
			}
		}
	}
	
	public static void callService(Context context, boolean updateInfo) {
		Intent intentService = new Intent(context, GetMyCirclesBackgroundService.class);
		intentService.putExtra(PARAM_UPDATE_INFO, updateInfo);
		context.startService(intentService);
	}
	
	public static void callService(Context context) {
		callService(context, false);
	}

}
