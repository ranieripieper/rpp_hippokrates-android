package com.doisdoissete.hippokrates.service;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.ContactUser;

public class ContactService extends BaseService {
	
	public ContactService(ServiceBuilder builder) {
		super(builder);
	}
	
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException {
		List<ContactUser> emlRecs = new ArrayList<ContactUser>();
        HashSet<String> emlRecsHS = new HashSet<String>();
        Context context = mServiceBuilder.getmContext();
        ContentResolver cr = context.getContentResolver();
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID, 
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA, 
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID };
        String order = "CASE WHEN " 
                + ContactsContract.Contacts.DISPLAY_NAME 
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, " 
                + ContactsContract.Contacts.DISPLAY_NAME 
                + ", " 
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
        
        
        if (cur.moveToFirst()) {
            do {
                // names comes in hand sometimes
            	int photoId = cur.getInt(2);
                Bitmap bitmap = queryContactImage(photoId);
                
                String name = cur.getString(1);
                String emlAddr = cur.getString(3);

                // keep unique only
                if (emlRecsHS.add(emlAddr.toLowerCase())) {
                    emlRecs.add(new ContactUser(name, emlAddr, bitmap));
                }
            } while (cur.moveToNext());
        }

        cur.close();
        
        ContactUser[] result = new ContactUser[emlRecs.size()];
        Collections.sort(emlRecs);
        
        result = emlRecs.toArray(result);
        
        return result;
	}

    
    private Bitmap queryContactImage(int imageDataRow) {
        Cursor c = mServiceBuilder.getmContext().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[] {
            ContactsContract.CommonDataKinds.Photo.PHOTO
        }, ContactsContract.Data._ID + "=?", new String[] {
            Integer.toString(imageDataRow)
        }, null);
        byte[] imageBytes = null;
        if (c != null) {
            if (c.moveToFirst()) {
                imageBytes = c.getBlob(0);
            }
            c.close();
        }

        if (imageBytes != null) {
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length); 
        } else {
            return null;
        }
    }
	
}
