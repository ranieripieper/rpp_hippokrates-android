package com.doisdoissete.hippokrates.service;

import java.net.ConnectException;
import java.util.HashMap;

import android.content.Context;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.result.LinkedData;
import com.doisdoissete.hippokrates.model.result.PostResult;

public class FeedService extends BaseService {
	
	private HashMap<Long, Circle> circles = new HashMap<Long, Circle>();
	private HashMap<Long, User> users = new HashMap<Long, User>();
	private HashMap<Long, IdNameDescModel> medicalSpecialties = new HashMap<Long, IdNameDescModel>();
	
	public FeedService(ServiceBuilder builder) {
		super(builder);
	}	
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		PostResult postResult = (PostResult)super.callService(ctx);

		if (postResult != null && postResult.getPosts() != null && postResult.getLinkedData() != null) {
			for (Post post : postResult.getPosts()) {
				post.setCircle(getCircle(post.getCircleId(), postResult.getLinkedData()));
				post.setUser(getUser(post.getUserId(), postResult.getLinkedData()));
				post.setMedicalSpecialty(getMedicalSpecialty(post.getMedicalSpecialtyId(), postResult.getLinkedData()));
				LocalPost.updatePost(post);
			}
		}
		
		return postResult;
	}
	
	private User getUser(Long userId, LinkedData linkedData) {
		User user = users.get(userId);
		if (user == null) {
			for (User userTmp : linkedData.getUsers() ) {
				if (userId.equals(userTmp.getId())) {
					user = userTmp;
					users.put(userId, user);
					break;
				}
			}
		}
		return user;
	}
	
	private Circle getCircle(Long circleId, LinkedData linkedData) {
		Circle circle = circles.get(circleId);
		if (circle == null) {
			for (Circle objTmp : linkedData.getFriendshipCircles() ) {
				if (circleId.equals(objTmp.getId())) {
					circle = objTmp;
					circles.put(circleId, circle);
					break;
				}
			}
		}
		return circle;
	}
	
	private IdNameDescModel getMedicalSpecialty(Long medicalSpecialtyId, LinkedData linkedData) {
		IdNameDescModel obj = medicalSpecialties.get(medicalSpecialtyId);
		if (obj == null) {
			for (IdNameDescModel objTmp : linkedData.getMedicalSpecialties()) {
				if (medicalSpecialtyId.equals(objTmp.getId())) {
					obj = objTmp;
					medicalSpecialties.put(medicalSpecialtyId, obj);
					break;
				}
			}
		}
		return obj;
	}

}
