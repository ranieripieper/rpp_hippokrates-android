package com.doisdoissete.hippokrates.service;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.media.BitmapUtil;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;

public class ResizeBitmapService extends BaseService {

	public static String PARAM_FILE_PATH = "PARAM_FILE_PATH";
	public static String PARAM_BITMAP = "PARAM_BITMAP";
	public static String PARAM_HEIGHT = "PARAM_HEIGHT";
	public static String PARAM_WIDTH = "WIDTH";
	
	private int MAX_HEIGHT = 700;
	private int MAX_WIDTH = 700;
	
	public ResizeBitmapService(ServiceBuilder builder) {
		super(builder);
	}	
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException {
		
		Bitmap bitmap = (Bitmap)params.get(PARAM_BITMAP).get(0);
		String filePath = (String)params.get(PARAM_FILE_PATH).get(0);
		Integer h = MAX_HEIGHT;
		Integer w = MAX_WIDTH;
		if (params.get(PARAM_HEIGHT) != null) {
			 h = (Integer)params.get(PARAM_HEIGHT).get(0);
		}
		if (params.get(PARAM_WIDTH) != null) {
			 w = (Integer)params.get(PARAM_WIDTH).get(0);
		}
		ExifInterface exif;
		int orientation = ExifInterface.ORIENTATION_NORMAL;
		try {
			exif = new ExifInterface(filePath);
			orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
		} catch (IOException e) {
		}

		return saveBitmap(bitmap, filePath, h, w, orientation);
		
	}
	
	private File saveBitmap(Bitmap bitmap, String filePath, int h, int w, int orientation) {
		File file = new File(filePath);
		BitmapUtil.copy(getResizedBitmap(bitmap, h, w, orientation), file);
		return file;
	}
	
	
	private Bitmap getResizedBitmap(Bitmap bm, int h, int w, int orientation) {
		
	    int width = bm.getWidth();
	    int height = bm.getHeight();
	    float scaleWidth = ((float) h) / width;
	    float scaleHeight = ((float) w) / height;
	    // CREATE A MATRIX FOR THE MANIPULATION
	    Matrix matrix = new Matrix();
	    Matrix matrixRotate = BitmapUtil.getMatrixRotate(matrix, orientation);
	    if (matrixRotate != null) {
	    	matrix = matrixRotate;
	    }
	    // RESIZE THE BIT MAP
	    if (width <= w) {
	    	return bm;
	    }
	    
	    float scale = Math.max(scaleWidth, scaleHeight);
	    
	    matrix.postScale(scale, scale);

	    // "RECREATE" THE NEW BITMAP
	    Bitmap resizedBitmap = Bitmap.createBitmap(
	        bm, 0, 0, width, height, matrix, false);
	    
	    return resizedBitmap;
	}
}
