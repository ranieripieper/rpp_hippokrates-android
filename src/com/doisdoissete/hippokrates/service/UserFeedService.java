package com.doisdoissete.hippokrates.service;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.BasePost;
import com.doisdoissete.hippokrates.model.Circle;
import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.doisdoissete.hippokrates.model.Post;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.result.LinkedData;
import com.doisdoissete.hippokrates.model.result.UserFeedResult;

public class UserFeedService extends BaseService {
	
	private HashMap<Long, Circle> circles = new HashMap<Long, Circle>();
	private HashMap<Long, User> users = new HashMap<Long, User>();
	private HashMap<Long, IdNameDescModel> medicalSpecialties = new HashMap<Long, IdNameDescModel>();
	
	public UserFeedService(ServiceBuilder builder) {
		super(builder);
	}	
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		UserFeedResult userFeedResult = (UserFeedResult)super.callService(ctx);
		userFeedResult.setPost(new ArrayList<Post>());
		if (userFeedResult != null && userFeedResult.getBasePost() != null && userFeedResult.getLinkedData() != null) {
			
			for (BasePost basePost : userFeedResult.getBasePost()) {
				User user = getUser(basePost.getUserId(), userFeedResult.getLinkedData());
				IdNameDescModel medicalSpecialty = getMedicalSpecialty(basePost.getMedicalSpecialtyId(), userFeedResult.getLinkedData());
				basePost.setUser(user);
				basePost.setMedicalSpecialty(medicalSpecialty);
				if (basePost.getPosts() != null) {
					for (Post post : basePost.getPosts()) {
						post.setCircle(getCircle(post.getCircleId(), userFeedResult.getLinkedData()));
						post.setMedicalSpecialty(medicalSpecialty);
						post.setUser(user);
						post.setDescricao(basePost.getContent());
						post.setImageUrl(basePost.getImagesUrl());
						userFeedResult.getPost().add(post);
						LocalPost.updatePost(post);
					}
				}				
			}			
		}
		
		return userFeedResult;
	}
	
	private IdNameDescModel getMedicalSpecialty(Long medicalSpecialtyId, LinkedData linkedData) {
		IdNameDescModel medicalSpecialty = medicalSpecialties.get(medicalSpecialtyId);
		if (medicalSpecialty == null) {
			for (IdNameDescModel objTmp : linkedData.getMedicalSpecialties()) {
				if (medicalSpecialtyId.equals(objTmp.getId())) {
					medicalSpecialty = objTmp;
					medicalSpecialties.put(medicalSpecialtyId, medicalSpecialty);
					break;
				}
			}
		}
		return medicalSpecialty;
	}
	
	private User getUser(Long userId, LinkedData linkedData) {
		User user = users.get(userId);
		if (user == null) {
			for (User userTmp : linkedData.getUsers() ) {
				if (userId.equals(userTmp.getId())) {
					user = userTmp;
					users.put(userId, user);
					break;
				}
			}
		}
		return user;
	}
	
	private Circle getCircle(Long circleId, LinkedData linkedData) {
		Circle circle = circles.get(circleId);
		if (circle == null) {
			for (Circle objTmp : linkedData.getFriendshipCircles() ) {
				if (circleId.equals(objTmp.getId())) {
					circle = objTmp;
					circles.put(circleId, circle);
					break;
				}
			}
		}
		return circle;
	}

}