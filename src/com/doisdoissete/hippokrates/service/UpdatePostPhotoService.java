package com.doisdoissete.hippokrates.service;

import java.io.File;
import java.net.ConnectException;
import java.util.List;

import org.springframework.http.HttpStatus;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import android.content.Context;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.model.result.PostFotoResult;
import com.doisdoissete.hippokrates.service.upload.UploadPhotoInterface;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class UpdatePostPhotoService extends BaseService {

	public UpdatePostPhotoService(ServiceBuilder builder) {
		super(builder);
	}
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		String privacy = params.get(UrlParameters.PARAM_POST_PRIVACY_STATUS).get(0).toString();
		String medicalSpecialty = params.get(UrlParameters.PARAM_POST_MEDICAL_SPECIALTY).get(0).toString();
		String filePath = params.get(UrlParameters.PARAM_POST_FILE_PATH).get(0).toString();
		String desc = params.get(UrlParameters.PARAM_POST_DESCRIPTION).get(0).toString();
		String postId = params.get(UrlParameters.PARAM_POST_ID).get(0).toString();
		List<Object> lstCirclesId = params.get(UrlParameters.PARAM_POST_CIRCLES_ID);
		
		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader(UrlParameters.PARAM_HEADER_X_TOKEN, Preferences.getLocalUser(mServiceBuilder.getmContext()).getAuthToken());
				request.addHeader(UrlParameters.PARAM_HEADER_X_PROVIDER, Constants.PROVIDER );
			}
		};

		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(Paths.HOST)
				.setErrorHandler(new MyErrorHandler())
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(Util.getGson()))
				.setRequestInterceptor(requestInterceptor).build();

		PostFotoResult result = null;
		
		UploadPhotoInterface service = restAdapter.create(UploadPhotoInterface.class);
		
		result = service.updatePostPhoto(postId, new TypedFile("image/jpeg", new File(filePath)), new TypedString(desc));
		
		if (result != null && result.getBasePost() != null) {
			LocalPost.updateImageUrl(result.getBasePost().getId(), result.getBasePost().getImagesUrl(), result.getBasePost().getContent());
		}
		return result;
	}

	class MyErrorHandler implements ErrorHandler {
		@Override
		public Throwable handleError(RetrofitError cause) {
			Response r = cause.getResponse();
			if (r != null && r.getStatus() == 401) {
				return new DdsUtilIOException(cause.getMessage(),
						HttpStatus.valueOf(r.getStatus()));
			}
			return cause;
		}
	}
}
