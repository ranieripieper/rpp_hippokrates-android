package com.doisdoissete.hippokrates.service;

import android.content.Context;

import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class HippAppServiceBuilder extends ServiceBuilder {

	public HippAppServiceBuilder() {
		super();
		this.setCharset(Constants.CHARSET);
	}
	
	public HippAppServiceBuilder(Context ctx) {
		this();
		if (Preferences.getLocalUser(ctx) != null) {
			this.addHeaders(UrlParameters.PARAM_HEADER_X_TOKEN, Preferences.getLocalUser(ctx).getAuthToken());
			this.addHeaders(UrlParameters.PARAM_HEADER_X_PROVIDER, Constants.PROVIDER );
		}		
	}
	
}
