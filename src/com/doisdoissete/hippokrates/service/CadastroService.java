package com.doisdoissete.hippokrates.service;

import java.io.File;
import java.net.ConnectException;
import java.util.List;

import org.springframework.http.HttpStatus;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import android.content.Context;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.result.LoginResult;
import com.doisdoissete.hippokrates.service.upload.CadastroServiceInterface;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;

public class CadastroService extends BaseService {

	public CadastroService(ServiceBuilder builder) {
		super(builder);
	}
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader(UrlParameters.PARAM_HEADER_X_PROVIDER, Constants.PROVIDER );
			}
		};

		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(Paths.HOST)
				.setErrorHandler(new MyErrorHandler())
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(Util.getGson()))
				.setRequestInterceptor(requestInterceptor).build();

		LoginResult result = null;
		
		CadastroServiceInterface service = restAdapter.create(CadastroServiceInterface.class);
		
		result = service.cadastrar(getTypedFile(UrlParameters.PARAM_USER_PROFILE_IMAGE), 
				getTypedString(UrlParameters.PARAM_USER_NAME), 
				getTypedString(UrlParameters.PARAM_USER_LAST_NAME),
				getTypedString(UrlParameters.PARAM_USER_EMAIL),
				getTypedString(UrlParameters.PARAM_USER_GENDER),
				getTypedString(UrlParameters.PARAM_USER_INVITE_CODE),
				getTypedString(UrlParameters.PARAM_USER_EDUCATION_DATA_UNIVERSITY_INST_NAME),
				getTypedString(UrlParameters.PARAM_USER_PASSWORD),
				getTypedString(UrlParameters.PARAM_USER_PASSWORD_CONF),
				getTypedString(UrlParameters.PARAM_USER_DEVICE_PLATFORM),
				getTypedString(UrlParameters.PARAM_USER_DEVICE_TOKEN),
				getTypedString(UrlParameters.PARAM_USER_PROFILE_TYPE),
				getTypedString(UrlParameters.PARAM_USER_CRM_DATA_NUMBER),
				getTypedString(UrlParameters.PARAM_USER_CRM_DATA_STATE),
				getTypedString(UrlParameters.PARAM_USER_MEDICAL_SPECIALTY),
				getTypedString(UrlParameters.PARAM_USER_OCUPATION),
				getTypedString(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NUMBER),
				getTypedString(UrlParameters.PARAM_USER_EDUCATION_DATA_CLASSROOM_NAME),
				getTypedString(UrlParameters.PARAM_USER_EDUCATION_DATA_RESIDENCY_INST_NAME),
				getTypedString(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_STATE),
				getTypedString(UrlParameters.PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_YEAR));
		
		return result;
	}
	
	private TypedFile getTypedFile(String key) {
		String v = getValue(key);
		if (v != null) {
			return new TypedFile("image/jpeg", new File(v));
		}
		return null;
	}
	
	private TypedString getTypedString(String key) {
		String v = getValue(key);
		if (v != null) {
			return new TypedString(v);
		}
		return null;
	}
	
	private String getValue(String key) {
		List<Object> values = params.get(key);
		if (values != null && values.size() > 0) {
			return values.get(0).toString();
		}
		return null;
	}
	
	class MyErrorHandler implements ErrorHandler {
		  @Override public Throwable handleError(RetrofitError cause) {
		    Response r = cause.getResponse();
		    if (r != null && r.getStatus() == 401) {
		      return new DdsUtilIOException(cause.getMessage(), HttpStatus.valueOf(r.getStatus()));
		    }
		    return cause;
		  }
		}
}
