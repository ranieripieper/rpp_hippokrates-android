package com.doisdoissete.hippokrates.service;

import java.net.ConnectException;
import java.util.HashMap;

import android.content.Context;

import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.Comment;
import com.doisdoissete.hippokrates.model.User;
import com.doisdoissete.hippokrates.model.result.CommentResult;
import com.doisdoissete.hippokrates.model.result.LinkedData;

public class CommentService extends BaseService {
	
	private HashMap<Long, User> users = new HashMap<Long, User>();
	
	public CommentService(ServiceBuilder builder) {
		super(builder);
	}
	
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		CommentResult result = (CommentResult)super.callService(ctx);

		if (result != null && result.getComments() != null && result.getLinkedData() != null) {
			for (Comment obj : result.getComments()) {
				obj.setUser(getUser(obj.getUserId(), result.getLinkedData()));
			}
		}
		
		return result;
	}
	
	private User getUser(Long userId, LinkedData linkedData) {
		User user = users.get(userId);
		if (user == null) {
			for (User userTmp : linkedData.getUsers() ) {
				if (userId.equals(userTmp.getId())) {
					user = userTmp;
					users.put(userId, user);
					break;
				}
			}
		}
		return user;
	}
}
