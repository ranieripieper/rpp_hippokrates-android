package com.doisdoissete.hippokrates.service;

import java.io.File;
import java.net.ConnectException;
import java.util.List;

import org.springframework.http.HttpStatus;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import android.content.Context;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.android.util.ddsutil.exception.ConnectionException;
import com.doisdoissete.android.util.ddsutil.exception.DdsUtilIOException;
import com.doisdoissete.android.util.ddsutil.exception.ParseException;
import com.doisdoissete.android.util.ddsutil.service.asynctask.BaseService;
import com.doisdoissete.android.util.ddsutil.service.asynctask.ServiceBuilder;
import com.doisdoissete.hippokrates.model.result.FriendshipCircleResult;
import com.doisdoissete.hippokrates.service.upload.UploadCircleInterface;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.Paths;
import com.doisdoissete.hippokrates.util.UrlParameters;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class CreateCircleService extends BaseService {

	public CreateCircleService(ServiceBuilder builder) {
		super(builder);
	}
	
	public Object callService(Context ctx) throws ParseException, ConnectionException, ConnectException, DdsUtilIOException {
		
		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader(UrlParameters.PARAM_HEADER_X_TOKEN, Preferences.getLocalUser(mServiceBuilder.getmContext()).getAuthToken());
				request.addHeader(UrlParameters.PARAM_HEADER_X_PROVIDER, Constants.PROVIDER );
			}
		};

		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(Paths.HOST)
				.setErrorHandler(new MyErrorHandler())
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(Util.getGson()))
				.setRequestInterceptor(requestInterceptor).build();

		FriendshipCircleResult result = null;
		
		UploadCircleInterface service = restAdapter.create(UploadCircleInterface.class);
		
		if (getValue(UrlParameters.PARAM_CIRCLE_START_DATE) != null) {
			result = service.createCircle(getTypedString(UrlParameters.PARAM_CIRCLE_NAME), getTypedFile(UrlParameters.PARAM_CIRCLE_PROFILE_IMAGE),
					getTypedString(UrlParameters.PARAM_CIRCLE_MEMBERS_LIMIT),
					getTypedString(UrlParameters.PARAM_CIRCLE_COLLEGE_SUBJECT),
					getTypedString(UrlParameters.PARAM_CIRCLE_UNIVERSIDADE),
					getTypedString(UrlParameters.PARAM_CIRCLE_START_DATE),
					getTypedString(UrlParameters.PARAM_CIRCLE_END_DATE));
		} else {
			result = service.createCircle(getTypedString(UrlParameters.PARAM_CIRCLE_NAME), getTypedFile(UrlParameters.PARAM_CIRCLE_PROFILE_IMAGE));
		}

		return result;
	}
	
	private TypedFile getTypedFile(String key) {
		String v = getValue(key);
		if (v != null) {
			return new TypedFile("image/jpeg", new File(v));
		}
		return null;
	}
	
	private TypedString getTypedString(String key) {
		String v = getValue(key);
		if (v != null) {
			return new TypedString(v);
		}
		return null;
	}
	
	private String getValue(String key) {
		List<Object> values = params.get(key);
		if (values != null && values.size() > 0) {
			return values.get(0).toString();
		}
		return null;
	}
	
	class MyErrorHandler implements ErrorHandler {
		  @Override public Throwable handleError(RetrofitError cause) {
		    Response r = cause.getResponse();
		    if (r != null && r.getStatus() == 401) {
		      return new DdsUtilIOException(cause.getMessage(), HttpStatus.valueOf(r.getStatus()));
		    }
		    return cause;
		  }
		}
}
