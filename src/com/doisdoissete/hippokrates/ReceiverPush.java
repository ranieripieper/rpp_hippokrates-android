package com.doisdoissete.hippokrates;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.View;

import com.doisdoissete.android.util.ddsutil.Util;
import com.doisdoissete.hippokrates.model.PushNotificationMessage;
import com.doisdoissete.hippokrates.model.PushNotificationMessageData;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.NotificationUtil;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.activity.HomeActivity;
import com.doisdoissete.hippokrates.view.activity.SplashScreenActivity;
import com.doisdoissete.hippokrates.view.activity.circle.ViewCircleActivity;
import com.doisdoissete.hippokrates.view.activity.post.DetailPostActivity;
import com.doisdoissete.hippokrates.view.activity.user.ViewUserProfileActivity;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

public class ReceiverPush extends ParsePushBroadcastReceiver {
	
	@Override
	protected void onPushReceive(Context context, Intent intent) {
		//super.onPushReceive(context, intent);
		
		try {
            String data = intent.getStringExtra("com.parse.Data");
            PushNotificationMessage pushNotificationMsg = Util.getGson().fromJson(data, PushNotificationMessage.class);
            
            if (pushNotificationMsg.getData() != null) {
            	createNotification(context, pushNotificationMsg.getData());
            } else {
            	createBaseNotification(context, pushNotificationMsg.getMessage());
            }
            
        } catch (Exception e) {
            Log.e("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ", e);
        }
	}
	
    @Override
    protected void onPushOpen(Context context, Intent intent) {
    	
        ParseAnalytics.trackAppOpened(intent);

        String uriString = null;
        try {
            JSONObject pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
            uriString = pushData.optString("uri");
        } catch (JSONException e) {
            Log.v("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ", e);
        }
        Class<? extends Activity> cls = getActivity(context, intent);
        Intent activityIntent;
        if (uriString != null && !uriString.isEmpty()) {
            activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
        } else {
            activityIntent = new Intent(context, SplashScreenActivity.class);
        }
        activityIntent.putExtras(intent.getExtras());
        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(activityIntent);
        }
    }
    
    public static void createNotification(final Context context, final PushNotificationMessageData message) {
    	LocalUser localUser = Preferences.getLocalUser(context);
    	if (Constants.DEGUB || (localUser != null && message.getReceiverUserId() != null &&
    			message.getReceiverUserId().equals(localUser.getId()))) {
    		
    		HippokratesApplication.imageLoaderUser.loadImage(message.getNotificableImageUrl(), new ImageLoadingListener() {
    			@Override
    			public void onLoadingStarted(String imageUri, View view) {
    			}
    			
    			@Override
    			public void onLoadingFailed(String imageUri, View view,
    					FailReason failReason) {
    				showNotification(context, message, null);
    			}
    			
    			@Override
    			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
    				showNotification(context, message, loadedImage);
    			}
    			
    			@Override
    			public void onLoadingCancelled(String imageUri, View view) {
    				showNotification(context, message, null);
    			}
    		});
    		
    	}
    	
    }
    
    private static void configUserTaskStackBuilder(Context ctx, TaskStackBuilder stackBuilder, Long userId) {
    	stackBuilder.addNextIntent(ViewUserProfileActivity.getIntentActivity(ctx, userId));
    }
    
    private static void configCircleTaskStackBuilder(Context ctx, TaskStackBuilder stackBuilder, Long circleId) {
    	stackBuilder.addNextIntent(ViewCircleActivity.getIntentActivity(ctx, circleId));
    }
    
    private static void configDetailFotoTaskStackBuilder(Context ctx, TaskStackBuilder stackBuilder, Long postId) {
    	stackBuilder.addNextIntent(DetailPostActivity.getIntentActivity(ctx, postId));
    }
    
    private static void showNotification(Context context, PushNotificationMessageData message, Bitmap bitmap) {

    	Bitmap circularBitmap = getCircularBitmap(bitmap);
    	
    	if (circularBitmap == null) {
    		circularBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
    	}
    	
    	NotificationCompat.Builder mBuilder =
    	        new NotificationCompat.Builder(context)
    	        .setSmallIcon(R.drawable.ic_notification)
    	        .setContentTitle(context.getString(R.string.app_name))
    	        .setContentText(message.getMessage())
    	        .setStyle(new NotificationCompat.BigTextStyle().bigText(message.getMessage()))
    	        .setAutoCancel(true)
    	        .setCategory(NotificationCompat.CATEGORY_SOCIAL)
    	        .setGroup(message.getNotificationType())
    	        .setGroupSummary(true)
    	        .setColor(context.getResources().getColor(R.color.color_primary))
    	        .setLargeIcon(circularBitmap);

    	Intent resultIntent = new Intent(context, HomeActivity.class);
    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    	stackBuilder.addParentStack(HomeActivity.class);
    	stackBuilder.addNextIntent(resultIntent);
    	
    	if (NotificationUtil.NOTIFICABLE_TYPE_CIRCLE.equalsIgnoreCase(message.getNotificableType()) ||
    			NotificationUtil.NOTIFICABLE_TYPE_FRIENDSHIP_CIRCLE.equalsIgnoreCase(message.getNotificableType())) {
    		configCircleTaskStackBuilder(context, stackBuilder, message.getNotificableId());
    	} else if (NotificationUtil.NOTIFICABLE_TYPE_POST.equalsIgnoreCase(message.getNotificableType())) {
    		configDetailFotoTaskStackBuilder(context, stackBuilder, message.getNotificableId());
    	} else if (NotificationUtil.NOTIFICABLE_TYPE_USER.equalsIgnoreCase(message.getNotificableType())) {
    		configUserTaskStackBuilder(context, stackBuilder, message.getNotificableId());
    	}
    	
    	PendingIntent resultPendingIntent =
    	        stackBuilder.getPendingIntent(
    	            0,
    	            PendingIntent.FLAG_CANCEL_CURRENT
    	        );
    	mBuilder.setContentIntent(resultPendingIntent);
    	
    	NotificationManager mNotificationManager =
    	    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    	
    	mNotificationManager.notify(new Double(Math.random()*1000000).intValue(), mBuilder.build());
    }
    
	private static void createBaseNotification(Context context, String msg) {
	    	NotificationCompat.Builder mBuilder =
	    	        new NotificationCompat.Builder(context)
	    	        .setSmallIcon(R.drawable.ic_notification)
	    	        .setContentTitle(context.getString(R.string.app_name))
	    	        .setContentText(msg)
	    	        .setAutoCancel(true)
	    	        .setColor(context.getResources().getColor(R.color.color_primary));
	
	    	Intent resultIntent = new Intent(context, SplashScreenActivity.class);
	    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
	    	stackBuilder.addParentStack(SplashScreenActivity.class);
	    	stackBuilder.addNextIntent(resultIntent);
	    	PendingIntent resultPendingIntent =
	    	        stackBuilder.getPendingIntent(
	    	            0,
	    	            PendingIntent.FLAG_CANCEL_CURRENT
	    	        );
	    	mBuilder.setContentIntent(resultPendingIntent);
	    	
	    	NotificationManager mNotificationManager =
	    	    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	    	mNotificationManager.notify(NotificationUtil.ID_BASE_NOTIFICATION, mBuilder.build());
	}
	
    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap != null) {
        	if (bitmap.getWidth() > bitmap.getHeight()) {
                output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Config.ARGB_8888);
            } else {
                output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            float r = 0;

            if (bitmap.getWidth() > bitmap.getHeight()) {
                r = bitmap.getHeight() / 2;
            } else {
                r = bitmap.getWidth() / 2;
            }

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(r, r, r, paint);
            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return output;
        }
        
        return null;
        
    }
    
    
} 