package com.doisdoissete.hippokrates;

import java.util.Date;

import android.app.Application;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.doisdoissete.android.util.ddsutil.view.SplashScreenActivity;
import com.doisdoissete.hippokrates.model.local.LocalPost;
import com.doisdoissete.hippokrates.service.background.RegisterDeviceBackgroundService;
import com.doisdoissete.hippokrates.util.AuthImageDownloader;
import com.doisdoissete.hippokrates.util.Constants;
import com.doisdoissete.hippokrates.util.preferences.Preferences;
import com.doisdoissete.hippokrates.view.custom.CircleImageLoader;
import com.doisdoissete.hippokrates.view.custom.UserImageLoader;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.parse.SaveCallback;

public class HippokratesApplication extends Application {  

	public static ImageLoader imageLoader;
	public static ImageLoader imageLoaderCircle;
	public static ImageLoader imageLoaderUser;
	
    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        
        new Delete().from(LocalPost.class).where("Id > ?", 0).execute();
        
        Parse.initialize(this, Constants.PARSE_APP_ID, Constants.PARSE_CLIENT_KEY);
        PushService.setDefaultPushCallback(getBaseContext(), SplashScreenActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				String deviceToken = (String) ParseInstallation.getCurrentInstallation().get("deviceToken");
				Preferences.setParseToken(HippokratesApplication.this, deviceToken);
				if (!TextUtils.isEmpty(deviceToken)) {
					RegisterDeviceBackgroundService.callService(HippokratesApplication.this);
				}				
			}
		});
        
        imageLoader = configImageLoader(R.drawable.img_placeholder_foto, false, false);
        imageLoaderCircle = configImageLoader(R.drawable.img_place_holder_circulo, false, true);
        imageLoaderUser = configImageLoader(R.drawable.img_placeholder_friend, true, false);        

		Date dt = Preferences.getLastClearCacheImages(this);

		if (dt == null || (int)((new Date().getTime() - dt.getTime())/ (24*60*60*1000)) > Constants.DIAS_CACHE_IMAGES) {
        	HippokratesApplication.imageLoader.clearDiscCache();
        	HippokratesApplication.imageLoader.clearMemoryCache();
        	HippokratesApplication.imageLoaderCircle.clearDiscCache();
        	HippokratesApplication.imageLoaderCircle.clearMemoryCache();
        	HippokratesApplication.imageLoaderUser.clearDiscCache();
        	HippokratesApplication.imageLoaderUser.clearMemoryCache();
            Preferences.setLastClearCacheImages(this, new Date());
        }
		
		dt = Preferences.getLastClearCacheServer(this);
		
		if (dt == null || (int)((new Date().getTime() - dt.getTime())/ (24*60*60*1000)) > Constants.DIAS_CACHE_SERVER) {
			Preferences.setColleges(this, null);
			Preferences.setMedicalSpecialities(this, null);
			Preferences.setLastClearCacheServer(this, null);
		}
    }
    
	private ImageLoader configImageLoader(int defaultRes, boolean user, boolean circle) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(defaultRes)
				.showImageForEmptyUri(defaultRes)
				.showImageOnFail(defaultRes)
				.resetViewBeforeLoading(false)
				.delayBeforeLoading(100).considerExifParams(true)
				.cacheInMemory(true) // default
				.cacheOnDisc(true).build(); // default

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this).defaultDisplayImageOptions(options) // default
				// .writeDebugLogs()
				.imageDownloader(new AuthImageDownloader(this, 1000, 1000))
				.build();
		ImageLoader imgLoader = null;
		if (user) {
			imgLoader = UserImageLoader.getInstance();
		} else if (circle) {
			imgLoader = CircleImageLoader.getInstance();
		} else {
			imgLoader = ImageLoader.getInstance();
		}

		imgLoader.init(config);

		return imgLoader;
	}
}   
