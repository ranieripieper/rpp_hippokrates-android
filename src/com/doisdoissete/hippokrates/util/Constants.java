package com.doisdoissete.hippokrates.util;

public class Constants {

	public static final boolean DEGUB = false;
	public static final String VERSION = "1.1";

	public static final int MAX_HEIGHT_CIRCLE = 300;
	public static final int MAX_WIDTH_CIRCLE = 300;
	
	public static final int MAX_HEIGHT_USER = 300;
	public static final int MAX_WIDTH_USER = 300;
	
	public static final String PUBLIC_CIRCLE_TYPE = "public";
	public static final String COMMON_CIRCLE_TYPE = "common";
	public static final String STUDENT_CIRCLE_TYPE = "students";
	public static final String INVITE_SENT = "sent";
	
	public static final String PARSE_APP_ID = "4afhV4nQyg8qJx4FcIRUnfKh0LfQ1MjRsO3zotjB";
	public static final String PARSE_CLIENT_KEY = "be4BsZt69el7cQ90LugGhBFxJylNCIPG8J2wzP1Y";
	
	//public static final String PARSE_APP_ID = "C25DzkHjjDtJv1RTwkWQ4p9MoT9fC0sLbFn4DzZX";
	//public static final String PARSE_CLIENT_KEY = "xbWMxtByQy2ShtY0zyP3GeLMX52yFQBhtB5DQxyb";
	
	public static final String EMAIL_FALE_CONOSCO = "rani.pieper@doisdoissete.com";
	public static final String EMAIL_DEBUG = "ranieripieper@gmail.com";
	
	public static final String MALE = "male";
	public static final String FEMALE = "female";
	
	public static final String PROVIDER = "android";
	public static final String CHARSET = "UTF-8";
	public static final int TOTAL_ITENS_PAGE = 60;
	public static final int HORAS_CACHE_MENU = 1;
	public static final int DIAS_CACHE_IMAGES = 1;
	public static final int DIAS_CACHE_SERVER = 1;
	
	public static final String PRIVACY_PRIVATE = "private";
	public static final String PRIVACY_PUBLIC = "public";
	
	//constants de debug
	//public static final String EMAIL_LOGIN_DEBUG = "hipapp_student@doisdoissete.com.br";
	//public static final String SENHA_LOGIN_DEBUG = "student_password";
	//public static final String EMAIL_LOGIN_DEBUG = "hipapp_teacher@doisdoissete.com.br";
	//public static final String SENHA_LOGIN_DEBUG = "teacher_password";
	//public static final String EMAIL_LOGIN_DEBUG = "ranieripieper@yahoo.com.br";
	//public static final String SENHA_LOGIN_DEBUG = "1234567";
	public static final String EMAIL_LOGIN_DEBUG = "hipapp_admin@doisdoissete.com.br";
	public static final String SENHA_LOGIN_DEBUG = "m4573r_r007P455w0rd";
	
	
	public static final String FILE_PATH_TESTE = "/storage/sdcard0/DCIM/Camera/20140830_165205.jpg";
	
	//tipos de denuncias
	public static final int DENUNCIA_INAPROPRIADO = 1;
	public static final int DENUNCIA_IDENTIFIQUEI_PACIENTE = 2;
}
