package com.doisdoissete.hippokrates.util;

public class ProfileUtil {

	public static final String RECEIVER_PROFILE_MEDIC = "medic";
	public static final String PROFILE_TYPE_DOCTOR = "doctor";
	public static final String PROFILE_TYPE_STUDENT = "student";
	public static final String PROFILE_TYPE_TEACHER = "teacher";
	public static final String PROFILE_TYPE_ADMIN = "admin";
	
	public static boolean isDoctor(String profileType) {
		if (PROFILE_TYPE_DOCTOR.equals(profileType)) {
			return true;
		}
		return false;
	}
	
	public static boolean isStudent(String profileType) {
		if (PROFILE_TYPE_STUDENT.equals(profileType)) {
			return true;
		}
		return false;
	}
	
	public static boolean isTeacher(String profileType) {
		if (PROFILE_TYPE_TEACHER.equals(profileType)) {
			return true;
		}
		return false;
	}
	
	public static boolean isAdmin(String profileType) {
		if (PROFILE_TYPE_ADMIN.equals(profileType)) {
			return true;
		}
		return false;
	}
	
	public static String getProfileTypeByReceiver(String receiverUserProfileType) {
		if (RECEIVER_PROFILE_MEDIC.equals(receiverUserProfileType)) {
			return ProfileUtil.PROFILE_TYPE_DOCTOR;
		}
		return ProfileUtil.PROFILE_TYPE_STUDENT;
	}
}
