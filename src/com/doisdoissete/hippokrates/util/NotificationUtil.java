package com.doisdoissete.hippokrates.util;

public class NotificationUtil {

	public static String NOTIFICATION_TYPE_WELCOME = "welcome";
	public static String NOTIFICATION_TYPE_CIRCLE_JOIN_REQ_APPROVED = "circle_join_request_approved";
	public static String NOTIFICATION_TYPE_USER_ACCEPTED_INVITE = "user_accepted_invite";
	public static String NOTIFICATION_TYPE_USER_REMOVED_CIRCLE = "user_removed_from_friendship_circle";
	public static String NOTIFICATION_TYPE_RECEIVED = "received_friendship_circle_administration";
	public static String NOTIFICATION_TYPE_POST_REMOVED = "post_removed_from_friendship_circle";
	public static String NOTIFICATION_TYPE_NEW_SUBSCRIPTION_APPROVED = "new_subscription_approved";
	public static String NOTIFICATION_TYPE_PASSWORD_UPDATED = "password_updated";
	public static String NOTIFICATION_TYPE_NEW_COMMENT_ON_POST = "new_comment_on_post";
	public static String NOTIFICATION_TYPE_POST_REPORTED = "post_reported";
	public static String NOTIFICATION_TYPE_POST_COMMENT_REPORTED = "post_comment_reported";
	
	public static String NOTIFICABLE_TYPE_CIRCLE = "circle";
	public static String NOTIFICABLE_TYPE_FRIENDSHIP_CIRCLE = "friendship_circle";
	
	public static String NOTIFICABLE_TYPE_USER = "user";
	public static String NOTIFICABLE_TYPE_POST = "post";
	public static String NOTIFICABLE_TYPE_SUBSCRIPTION = "subscription";
	
	public static int ID_BASE_NOTIFICATION = 1;

}
