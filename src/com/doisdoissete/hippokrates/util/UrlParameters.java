package com.doisdoissete.hippokrates.util;

public class UrlParameters {

	//ACTIONS MODERATE CIRCLE
	public static final String ACTION_REMOVE_USER = "remove_user";
	public static final String ACTION_REMOVE_POST = "remove_post";
	public static final String ACTION_TRANSFER_USER = "transfer_admin";
	
	//parâmetros
	public static final String PARAM_PAGE = "page";
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_PROVIDER = "provider";
	public static final String PARAM_CODE = "code";
	public static final String PARAM_HEADER_X_TOKEN = "X-Token";
	public static final String PARAM_HEADER_X_PROVIDER = "X-Provider";
	public static final String PARAM_EMAIL_ADDRESSES = "email_addresses";
	public static final String PARAM_POSTS_IDS = "posts_ids";
	public static final String PARAM_USERS_IDS = "user_ids[]";
	public static final String PARAM_REPORT_TYPE = "type";
	public static final String PARAM_USER_ID = "user_id";
	public static final String PARAM_ACTION = "action";
	public static final String PARAM_POST_ID = "post_id";
	public static final String PARAM_KEY = "key";
	public static final String PARAM_VALUE = "value";
	public static final String PARAM_CIRCLE_ID = "circle_id";
	
	
	public static final String PARAM_CIRCLE_ID_URL = "{circle_id}";
	public static final String PARAM_CIRCLE_CODE_URL = "{circle_code}";
	public static final String PARAM_POST_ID_URL = "{post_id}";
	public static final String PARAM_USER_ID_URL = "{user_id}";
	public static final String PARAM_USER_NAME_URL = "{user_name}";
	public static final String PARAM_HASHTAG_URL = "{q}";
	public static final String PARAM_ESPECIALIDADE_ID_URL = "{especialidade_id}";
	public static final String PARAM_COMMENT_ID_URL = "{comment_id}";
	
	public static final String PARAM_COMMENT_POST = "comment[content]";
			
	//parâmetros post
	public static final String PARAM_POST_IMAGE = "post[image]";
	public static final String PARAM_POST_PRIVACY_STATUS = "post[privacy_status]";
	public static final String PARAM_POST_MEDICAL_SPECIALTY = "post[medical_specialty_id]";
	public static final String PARAM_POST_FILE_PATH = "post[file_path]";
	public static final String PARAM_POST_CIRCLES_ID = "post[friendship_circles_ids][]";
	public static final String PARAM_POST_DESCRIPTION = "post[content]";
	
	//parâmetros circulos
	public static final String PARAM_CIRCLE_START_DATE = "friendship_circle[start_date]";
	public static final String PARAM_CIRCLE_END_DATE = "friendship_circle[end_date]";
	public static final String PARAM_CIRCLE_PROFILE_IMAGE = "friendship_circle[profile_image]";
	public static final String PARAM_CIRCLE_MEMBERS_LIMIT = "friendship_circle[members_limit]";
	public static final String PARAM_CIRCLE_COLLEGE_SUBJECT = "friendship_circle[college_subject]";
	public static final String PARAM_CIRCLE_UNIVERSIDADE = "friendship_circle[college_instituition_name]";
	public static final String PARAM_CIRCLE_NAME = "friendship_circle[name]";

	//parâmetros usuário
	public static final String PARAM_USER_EMAIL = "user[email]";
	public static final String PARAM_USER_FIRST_ID = "user[id]";
	public static final String PARAM_USER_FIRST_NAME = "user[first_name]";
	public static final String PARAM_USER_NAME = "user[name]";
	public static final String PARAM_USER_LAST_NAME = "user[last_name]";
	public static final String PARAM_USER_CRM_NUMBER = "user[crm_number]";
	public static final String PARAM_USER_CRM_STATE = "user[crm_state]";
	public static final String PARAM_USER_GENDER = "user[gender]";
	public static final String PARAM_USER_PROFILE_TYPE = "user[profile_type]";
	public static final String PARAM_USER_UNIVERSITY_INSTITUTION_NAME = "user[university_institution_name]";
	
	public static final String PARAM_USER_PASSWORD_CONF	 = "user[password_confirmation]";
	public static final String PARAM_USER_PASSWORD = "user[password]";
	public static final String PARAM_USER_MEDICAL_SPECIALTY = "user[medical_specialty_id]";
	public static final String PARAM_USER_PROFILE_IMAGE = "user[profile_image]";
	public static final String PARAM_USER_OCUPATION = "user[occupation]";
	public static final String PARAM_USER_INVITE_CODE = "user[invite_code]";
	public static final String PARAM_USER_CRM_DATA_NUMBER = "user[crm][number]";
	public static final String PARAM_USER_CRM_DATA_STATE = "user[crm][state]";
	public static final String PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_STATE = "user[education_data][college_degree_state]";
	public static final String PARAM_USER_EDUCATION_DATA_COLLEGE_DEGREE_YEAR = "user[education_data][college_degree_year]";
	public static final String PARAM_USER_EDUCATION_DATA_RESIDENCY_INST_NAME = "user[education_data][residency_program_instituition_name]";
	public static final String PARAM_USER_EDUCATION_DATA_CLASSROOM_NAME = "user[education_data][classroom_name]";
	public static final String PARAM_USER_EDUCATION_DATA_CLASSROOM_NUMBER = "user[education_data][classroom_number]";
	public static final String PARAM_USER_EDUCATION_DATA_UNIVERSITY_INST_NAME = "user[education_data][university_institution_name]";
	public static final String PARAM_USER_DEVICE_TOKEN = "user[device][token]";
	public static final String PARAM_USER_DEVICE_PLATFORM = "user[device][platform]";
	
	public static final String PARAM_DEVICE_TOKEN = "device[token]";
	public static final String PARAM_DEVICE_PLATFORM = "device[platform]";
	
	//SOMENTE PARA DEBUG
	public static String PARAM_TO_EMAIL = "to_email";
}
