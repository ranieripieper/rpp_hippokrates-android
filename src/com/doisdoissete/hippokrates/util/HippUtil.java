package com.doisdoissete.hippokrates.util;

import java.util.Date;

import android.content.Context;
import android.text.TextUtils;

import com.doisdoissete.android.util.ddsutil.DateUtil;
import com.doisdoissete.hippokrates.R;
import com.doisdoissete.hippokrates.model.local.LocalUser;
import com.doisdoissete.hippokrates.util.preferences.Preferences;

public class HippUtil {

	public static boolean isLogged(Context ctx) {
		LocalUser user = Preferences.getLocalUser(ctx);
		if (user != null && !TextUtils.isEmpty(user.getAuthToken())) {
			return true;
		}
		return false;
	}
	
	public static String getSiglaUF(Context ctx, String estado) {
		if (TextUtils.isEmpty(estado)) {
			return "";
		}
		if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_acre))) {
			return ctx.getString(R.string.txt_sigla_acre);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_alagoas))) {
			return ctx.getString(R.string.txt_sigla_alagoas);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_amapa))) {
			return ctx.getString(R.string.txt_sigla_amapa);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_amazonas))) {
			return ctx.getString(R.string.txt_sigla_amazonas);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_ceara))) {
			return ctx.getString(R.string.txt_sigla_ceara);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_distrito_federal))) {
			return ctx.getString(R.string.txt_sigla_distrito_federal);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_espirito_santo))) {
			return ctx.getString(R.string.txt_sigla_espirito_santo);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_goias))) {
			return ctx.getString(R.string.txt_sigla_goias);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_maranhao))) {
			return ctx.getString(R.string.txt_sigla_maranhao);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_mato_grosso))) {
			return ctx.getString(R.string.txt_sigla_mato_grosso);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_mato_grosso_do_sul))) {
			return ctx.getString(R.string.txt_sigla_mato_grosso_do_sul);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_minas_gerais))) {
			return ctx.getString(R.string.txt_sigla_minas_gerais);				
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_para))) {
			return ctx.getString(R.string.txt_sigla_para);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_paraiba))) {
			return ctx.getString(R.string.txt_sigla_paraiba);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_pernambuco))) {
			return ctx.getString(R.string.txt_sigla_pernambuco);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_piaui))) {
			return ctx.getString(R.string.txt_sigla_piaui);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_rio_de_janeiro))) {
			return ctx.getString(R.string.txt_sigla_rio_de_janeiro);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_rio_grande_do_norte))) {
			return ctx.getString(R.string.txt_sigla_rio_grande_do_norte);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_rio_grande_do_sul))) {
			return ctx.getString(R.string.txt_sigla_rio_grande_do_sul);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_rondonia))) {
			return ctx.getString(R.string.txt_sigla_rondonia);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_roraima))) {
			return ctx.getString(R.string.txt_sigla_roraima);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_santa_catarina))) {
			return ctx.getString(R.string.txt_sigla_santa_catarina);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_sao_paulo))) {
			return ctx.getString(R.string.txt_sigla_sao_paulo);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_sergipe))) {
			return ctx.getString(R.string.txt_sigla_sergipe);
		} else if (estado.equalsIgnoreCase(ctx.getString(R.string.txt_tocantis))) {
			return ctx.getString(R.string.txt_sigla_tocantis);
		}	

		return estado;
	}
	
	public static boolean callMeInfo(Context ctx) {
		Date dtLastCall = Preferences.getLastCallMeInfo(ctx);
		if (dtLastCall == null || DateUtil.getDifHoras(dtLastCall, new Date()) > Constants.HORAS_CACHE_MENU) {
			return true;
		}
		return false;
	}
	
	public static String formatQtde(Long nr) {
		if (nr == null) {
			return "0";
		}
		return String.valueOf(nr);
	}
}
