package com.doisdoissete.hippokrates.util;

import com.doisdoissete.hippokrates.model.CRM;

public class Paths {	
	
	public static final String HOST = "http://api.hippokrates.us";
	public static final String API_V1 = String.format("%s/v1/", HOST);
	public static final String API_V1_CACHED = String.format("%s/", API_V1);
	//public static final String HOST = "http://192.168.0.11:3000/api/v1/";
	
	//INVITE
	public static final String ASK_FOR_INVITE = String.format("%sinvites/ask", API_V1);
	public static final String VALIDATE_INVITE = String.format("%sinvites/validate?%s={%s}", API_V1, UrlParameters.PARAM_CODE, UrlParameters.PARAM_CODE);
	public static final String SEND_INVITE = String.format("%susers/me/invites", API_V1);
	
	//CRM
	public static final String VALIDATE_CRM = String.format("%scrm/validate?%s={%s}&%s={%s}&%s={%s}&%s={%s}&return_data=true", API_V1, 
			CRM.FIELD_GET_CRM_NUMBER, CRM.FIELD_GET_CRM_NUMBER, CRM.FIELD_GET_CRM_STATE, CRM.FIELD_GET_CRM_STATE, 
			CRM.FIELD_GET_FIRST_NAME, CRM.FIELD_GET_FIRST_NAME, CRM.FIELD_GET_LAST_NAME, CRM.FIELD_GET_LAST_NAME);

	//SIGNUP
	public static final String SIGNUP = String.format("%susers", API_V1);
	
	//AUTH
	public static final String LOGIN = String.format("%susers/auth", API_V1);
	public static final String LOGOUT = String.format("%susers/logout", API_V1);
	//DEVICE
	public static final String REGISTER_DEVICE = String.format("%susers/me/devices", API_V1);
		
	//PASSWORD
	public static final String RECOVER_PASSWORD = String.format("%susers/password_reset", API_V1);
	
	//USER 
	public static final String USER_FEED = String.format("%susers/%s/feed", API_V1, UrlParameters.PARAM_USER_ID_URL);
	public static final String USER_INFO = String.format("%susers/%s", API_V1, UrlParameters.PARAM_USER_ID_URL);
	
	//Current USER
	public static final String ME_INFO = String.format("%susers/me", API_V1);
	public static final String UPDATE_ME_INFO = String.format("%susers/me", API_V1);
	public static final String ME_CIRCLES = String.format("%susers/me/friendship_circles", API_V1);
	public static final String FEED = String.format("%susers/me/feed?%s={%s}", API_V1, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);
	public static final String USER_DELETE_POSTS = String.format("%susers/me/posts", API_V1);
	public static final String UPDATE_PRIVACY_INFO = String.format("%susers/me/update_privacy", API_V1);
	public static final String DELETE_ACCOUNT = String.format("%susers/me/account", API_V1);
	public static final String USER_PREFERENCES = String.format("%susers/me/preferences", API_V1);
	public static final String REFRESH_PREFERENCES = String.format("%susers/me/preferences", API_V1);

	//NOTIFICATION
	public static final String NOTIFICATION = String.format("%susers/me/notifications?%s={%s}", API_V1, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);
	
		
	//Especialidades
	public static final String MEDICAL_SPECIALITIES = String.format("%smedical_specialities", API_V1_CACHED);
	
	//Instituições de ensino
	public static final String COLLEGES = String.format("%scolleges", API_V1_CACHED);
	
	//POST
	public static final String POST_COMMENTS = String.format("%sposts/%s/comments", API_V1, UrlParameters.PARAM_POST_ID_URL);
	public static final String POST_FOTO = String.format("%sposts", API_V1);
	public static final String UPDATE_POST_FOTO = String.format("%sposts/%s", API_V1, UrlParameters.PARAM_POST_ID_URL);	
	public static final String POST_FETCH = String.format("%sposts/%s", API_V1, UrlParameters.PARAM_POST_ID_URL);	
	public static final String REPORT_POST = String.format("%sposts/%s/report", API_V1, UrlParameters.PARAM_POST_ID_URL);
	
	//Like
	public static final String LIKE_POST = String.format("%sposts/%s/like", API_V1, UrlParameters.PARAM_POST_ID_URL);
	
	//comentário
	public static final String COMMENT_POST = String.format("%sposts/%s/comment", API_V1, UrlParameters.PARAM_POST_ID_URL);
	public static final String UPDATE_COMMENT_POST = String.format("%sposts/%s/comment/%s", API_V1, UrlParameters.PARAM_POST_ID_URL, UrlParameters.PARAM_COMMENT_ID_URL);
	public static final String DELETE_COMMENT_POST = String.format("%sposts/%s/comment/%s", API_V1, UrlParameters.PARAM_POST_ID_URL, UrlParameters.PARAM_COMMENT_ID_URL);
	public static final String REPORT_COMMENT = String.format("%sposts/%s/comment/%s/report", API_V1, UrlParameters.PARAM_POST_ID_URL, UrlParameters.PARAM_COMMENT_ID_URL);
	
	//circulos
	public static final String NEW_CIRCLE_TEACHER = String.format("%sfriendship_circles", API_V1);
	public static final String FEED_CIRCLE = String.format("%sfriendship_circles/%s/feed?%s={%s}", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);
	public static final String MEMBERS_CIRCLE = String.format("%sfriendship_circles/%s/members?%s={%s}", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);	
	public static final String DEIXAR_CIRCLE = String.format("%sfriendship_circles/%s/quit", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String JOIN_CIRCLE = String.format("%sfriendship_circles/by_code/%s/join", API_V1, UrlParameters.PARAM_CIRCLE_CODE_URL);
	public static final String FETCH_CIRCLE = String.format("%sfriendship_circles/%s", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	
	//admin círculos
	public static final String MODERATE_CIRCLE = String.format("%sfriendship_circles/%s/moderate", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String MODERATE_CIRCLE_REMOVE_USER = String.format("%sfriendship_circles/%s/moderate/remove_users", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String MODERATE_CIRCLE_TRANSFER_ADMIN = String.format("%sfriendship_circles/%s/moderate/transfer_admin", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String MODERATE_CIRCLE_REMOVE_POST = String.format("%sfriendship_circles/%s/moderate/remove_posts", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String DELETE_CIRCLE = String.format("%sfriendship_circles/%s", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String UPDATE_CIRCLE = String.format("%sfriendship_circles/%s", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String CIRCLE_ADD_INVITE_MEMBER = String.format("%sfriendship_circles/%s/add_member", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String CIRCLE_PENDING_REQUESTS = String.format("%sfriendship_circles/%s/pending_requests", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String APPROVE_PENDING_USERS = String.format("%sfriendship_circles/%s/moderate/approve_pending_users", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);
	public static final String DECLINE_PENDING_USERS = String.format("%sfriendship_circles/%s/moderate/decline_pending_users", API_V1, UrlParameters.PARAM_CIRCLE_ID_URL);

	//busca
	public static final String SEARCH_USER_NO_STUDENT = String.format("%ssearch/users?name=%s&%s={%s}&profile_types[]=teacher&profile_types[]=doctor", API_V1, UrlParameters.PARAM_USER_NAME_URL, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);
	public static final String SEARCH_USER = String.format("%ssearch/users?name=%s&%s={%s}", API_V1, UrlParameters.PARAM_USER_NAME_URL, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);
	public static final String SEARCH_HASHTAG = String.format("%ssearch/hashtags?q=%s&%s={%s}", API_V1, UrlParameters.PARAM_HASHTAG_URL, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);

	public static final String FEED_HASHTAG = String.format("%ssearch/posts?hashtag=%s&%s={%s}", API_V1, UrlParameters.PARAM_HASHTAG_URL, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);
	public static final String FEED_ESPECIALIDADE = String.format("%sposts/by_medical_specialty/%s?%s={%s}", API_V1, UrlParameters.PARAM_ESPECIALIDADE_ID_URL, UrlParameters.PARAM_PAGE, UrlParameters.PARAM_PAGE);
}
