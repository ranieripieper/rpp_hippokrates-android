package com.doisdoissete.hippokrates.util.preferences;

import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;

import com.doisdoissete.hippokrates.HippokratesApplication;
import com.doisdoissete.hippokrates.model.IdNameDescModel;
import com.doisdoissete.hippokrates.model.local.LocalUser;

public class Preferences {

	private static final String PREFERENCES = Preferences.class + "";
	private static final String PREF_USER = "PREF_USER";
	private static final String PREF_MEDICAL_SPECIALITIES = "PREF_MEDICAL_SPECIALITIES";
	private static final String PREF_COLLEGES = "PREF_COLLEGES";
	private static final String NOTIFICATION_USER = "NOTIFICATION_USER";
	private static final String LAST_CALL_MY_CIRCLES = "LAST_CALL_MY_CIRCLES";
	private static final String PREF_LAST_CLEAR_CACHE_IMAGES = "PREF_LAST_CLEAR_CACHE_IMAGES";
	private static final String PREF_LAST_CLEAR_CACHE_SERVER = "PREF_LAST_CLEAR_CACHE_SERVER";
	private static final String PREF_REFRESH_MENU = "PREF_REFRESH_MENU";
	
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    
    private static final String PREF_PARSE_TOKEN = "PREF_PARSE_TOKEN";
    
	public static String getParseToken(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		String value = settings.getString(PREF_PARSE_TOKEN, "");
		return value;
	}
	
	public static void setParseToken(Context ctx, String value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREF_PARSE_TOKEN, value);
		editor.commit();
	}
	
	public static Date getLastClearCacheImages(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		long time = settings.getLong(PREF_LAST_CLEAR_CACHE_IMAGES, -1);
		if (time > 0) {
			return new Date(time);
		}
		return null;
	}
	
	public static void setLastClearCacheImages(Context ctx, Date value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(PREF_LAST_CLEAR_CACHE_IMAGES, value != null ? value.getTime() : -1);
		editor.commit();
	}
	
	public static Date getLastClearCacheServer(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		long time = settings.getLong(PREF_LAST_CLEAR_CACHE_SERVER, -1);
		if (time > 0) {
			return new Date(time);
		}
		return null;
	}
	
	public static void setLastClearCacheServer(Context ctx, Date value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(PREF_LAST_CLEAR_CACHE_SERVER, value != null ? value.getTime() : -1);
		editor.commit();
	}
	
	public static boolean isRefreshMenu(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		boolean value = settings.getBoolean(PREF_REFRESH_MENU, true);
		return value;
	}
	
	public static void setRefreshMenu(Context ctx, boolean value) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PREF_REFRESH_MENU, value);
		editor.commit();
	}
	
	public static LocalUser getLocalUser(Context ctx) {
		ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, PREFERENCES, Context.MODE_PRIVATE);
		LocalUser user = complexPreferences.getObject(PREF_USER, LocalUser.class);
	    return user;
	}
	
	public static void setLocalUser(Context ctx, LocalUser user) {
		ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, PREFERENCES, Context.MODE_PRIVATE);
	    complexPreferences.putObject(PREF_USER, user);
	    complexPreferences.commit();
	}
	
	public static IdNameDescModel[] getMedicalSpecialities(Context ctx) {
		ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, PREFERENCES, Context.MODE_PRIVATE);
		IdNameDescModel[] res = complexPreferences.getObject(PREF_MEDICAL_SPECIALITIES, IdNameDescModel[].class);
	    return res;
	}
	
	public static void setMedicalSpecialities(Context ctx, IdNameDescModel[] value) {
		ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, PREFERENCES, Context.MODE_PRIVATE);
	    complexPreferences.putObject(PREF_MEDICAL_SPECIALITIES, value);
	    complexPreferences.commit();
	}
	
	public static IdNameDescModel[] getColleges(Context ctx) {
		ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, PREFERENCES, Context.MODE_PRIVATE);
		IdNameDescModel[] res = complexPreferences.getObject(PREF_COLLEGES, IdNameDescModel[].class);
	    return res;
	}
	
	public static void setColleges(Context ctx, IdNameDescModel[] value) {
		ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, PREFERENCES, Context.MODE_PRIVATE);
	    complexPreferences.putObject(PREF_COLLEGES, value);
	    complexPreferences.commit();
	}
	
	
	public static void logout(Context ctx) {
		setLocalUser(ctx, null);
		setLastSeenNotification(ctx, null);
		setLastCallMyCircles(ctx, null);
		HippokratesApplication.imageLoader.clearDiscCache();
    	HippokratesApplication.imageLoader.clearMemoryCache();
        Preferences.setLastClearCacheImages(ctx, null);
	}
	
	public static void setLastSeenNotification(Context ctx, Date date) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(NOTIFICATION_USER, date != null ? date.getTime() : -1);
		editor.commit();
	}
	
	public static Date getLastSeenNotification(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		long dt = settings.getLong(NOTIFICATION_USER, -1);
		
		if (dt > 0) {
			return new Date(dt);
		}
		return null;
	}
	
	public static void setLastCallMyCircles(Context ctx, Date date) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(LAST_CALL_MY_CIRCLES, date != null ? date.getTime() : -1);
		editor.commit();
	}
	
	public static Date getLastCallMeInfo(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(PREFERENCES, 0);
		long dt = settings.getLong(LAST_CALL_MY_CIRCLES, -1);
		
		if (dt > 0) {
			return new Date(dt);
		}
		return null;
	}
}
